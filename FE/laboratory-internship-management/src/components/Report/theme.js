// src/theme.js
import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      main: '#1976d2', // Màu chính (ví dụ: xanh dương)
    },
    secondary: {
      main: '#dc004e', // Màu phụ (ví dụ: đỏ)
    },
    background: {
      default: '#f5f5f5', // Màu nền mặc định
    },
    text: {
      primary: '#333', // Màu chữ chính
      secondary: '#555', // Màu chữ phụ
    },
  },
  typography: {
    h4: {
      fontSize: '2rem',
      fontWeight: 600,
    },
  },
});

export default theme;
