import React, { useState, useEffect } from 'react';
import { TextField, Button, Box, Typography } from '@mui/material';
import { useNavigate, useParams } from 'react-router-dom';
import { useCandidates } from '../../context/CandidatesContext';

const CandidateForm = () => {
  const { candidates, updateCandidate, addCandidate } = useCandidates();
  const [candidate, setCandidate] = useState({
    rollNumber: '',
    firstName: '',
    lastName: '',
    dob: '',
    email: '',
    labName: '',
    cvName: '',
  });
  const [isEditMode, setIsEditMode] = useState(false);
  const navigate = useNavigate();
  const { id } = useParams();

  useEffect(() => {
    if (id) {
      setIsEditMode(true);
      const candidateData = candidates.find(c => c.id === parseInt(id));
      if (candidateData) {
        setCandidate(candidateData);
      } else {
        // Handle case where candidate is not found
        console.error('Candidate not found');
        navigate('/candidates');
      }
    }
  }, [id, candidates, navigate]);

  const handleChange = (event) => {
    const { name, value } = event.target;
    setCandidate((prev) => ({ ...prev, [name]: value }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (isEditMode) {
      updateCandidate(candidate);
    } else {
      addCandidate({ ...candidate, id: Date.now() }); // Generate a unique ID for new candidates
    }
    navigate('/candidates');
  };

  return (
    <Box sx={{ padding: 3 }}>
      <Typography variant="h4">{isEditMode ? 'Edit Candidate' : 'Add Candidate'}</Typography>
      <form onSubmit={handleSubmit}>
        <TextField
          label="Roll Number"
          name="rollNumber"
          variant="outlined"
          fullWidth
          margin="normal"
          value={candidate.rollNumber}
          onChange={handleChange}
        />
        <TextField
          label="First Name"
          name="firstName"
          variant="outlined"
          fullWidth
          margin="normal"
          value={candidate.firstName}
          onChange={handleChange}
          required
        />
        <TextField
          label="Last Name"
          name="lastName"
          variant="outlined"
          fullWidth
          margin="normal"
          value={candidate.lastName}
          onChange={handleChange}
          required
        />
        <TextField
          label="Date of Birth"
          name="dob"
          variant="outlined"
          fullWidth
          margin="normal"
          type="date"
          InputLabelProps={{ shrink: true }}
          value={candidate.dob}
          onChange={handleChange}
        />
        <TextField
          label="Email"
          name="email"
          variant="outlined"
          fullWidth
          margin="normal"
          type="email"
          value={candidate.email}
          onChange={handleChange}
          required
        />
        <TextField
          label="Lab Name"
          name="labName"
          variant="outlined"
          fullWidth
          margin="normal"
          value={candidate.labName}
          onChange={handleChange}
        />
        <TextField
          label="CV Name"
          name="cvName"
          variant="outlined"
          fullWidth
          margin="normal"
          value={candidate.cvName}
          onChange={handleChange}
        />
        <Box sx={{ mt: 2 }}>
          <Button variant="contained" color="primary" type="submit">
            {isEditMode ? 'Update' : 'Add'} Candidate
          </Button>
          <Button
            variant="outlined"
            color="secondary"
            onClick={() => navigate('/candidates')}
            sx={{ ml: 2 }}
          >
            Cancel
          </Button>
        </Box>
      </form>
    </Box>
  );
};

export default CandidateForm;
