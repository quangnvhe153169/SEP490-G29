import { useState } from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Link from "@mui/material/Link";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Divider from "@mui/material/Divider";
import GoogleLoginComponent from "./GoogleLoginComponent";
import { useAuth } from "../../customs/authService";
import Logo from "../../assets/images/LIM.svg";
import { CSSTransition } from "react-transition-group";
import "../../assets/css/Form.css";
import { useNavigate } from "react-router-dom";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { handleLogin, handleGoogleLogin } = useAuth();
  const navigate = useNavigate();

  const handleSubmit = (event) => {
    event.preventDefault();
    const account = { email, password };
    setPassword("");
    handleLogin(account);
  };

  return (
    <Container component="main" maxWidth="xs">
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          height: "90vh",
        }}
      >
        <Box display="flex" justifyContent="center" alignItems="center">
          <img src={Logo} alt="Logo" style={{ width: "50%", height: "auto" }} />
        </Box>

        <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
          <TextField
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            autoFocus
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <TextField
            margin="normal"
            required
            fullWidth
            label="Password"
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item xs>
              <Link
                onClick={() => navigate("/admin/login")}
                style={{
                  textDecoration: "none",
                  cursor: "pointer", // Ensures the pointer cursor is shown
                }}
              >
                System Admin Login
              </Link>
            </Grid>
          </Grid>
          <Divider sx={{ my: 2 }}>OR</Divider>
          <Box sx={{ display: "flex", justifyContent: "center" }}>
            <GoogleLoginComponent loginGoogle={handleGoogleLogin} />
          </Box>
        </Box>
      </Box>
    </Container>
  );
}

export function LoginAdmin({ show, handleShow }) {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { handleAdminLogin } = useAuth();
  const navigate = useNavigate();

  const handleSubmit = (event) => {
    const account = { username, password };
    setUsername("");
    event.preventDefault();
    handleAdminLogin(account);
  };

  return (
    <CSSTransition in={show} timeout={300} classNames="form" unmountOnExit>
      <Container component="main" maxWidth="xs">
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            height: "90vh",
          }}
        >
          <Box display="flex" justifyContent="center" alignItems="center">
            <img
              src={Logo}
              alt="Logo"
              style={{ width: "50%", height: "auto" }}
            />
          </Box>

          <Box
            component="form"
            onSubmit={handleSubmit}
            noValidate
            sx={{ mt: 1 }}
          >
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Username"
              autoFocus
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              autoComplete="current-password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Sign In
            </Button>
            {/* <Grid container>
              <Grid item xs>
                <Link
                  onClick={() => handleShow(!show)}
                  style={{ textDecoration: "none" }}
                >
                  Forgot password?
                </Link>
              </Grid>
            </Grid> */}
            <Grid container>
              <Grid item xs>
                <Link
                  onClick={() => navigate("/login")}
                  style={{
                    textDecoration: "none",
                    cursor: "pointer", // Ensures the pointer cursor is shown
                  }}
                >
                  Normal Login
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Container>
    </CSSTransition>
  );
}

export default Login;
