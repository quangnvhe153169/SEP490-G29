import React, { useState } from "react";
import { useAuth } from "../../customs/authService";
import { useNavigate } from "react-router-dom";
import { LoginAdmin } from "./Login";
// import ForgotPassword from "./ForgotPassword";

const AdminAuthenticate = () => {
  const navigator = useNavigate();
  const [showLogin, setShowLogin] = useState(true);
  const { isLogged } = useAuth();

  return isLogged() ? navigator("/") : (
    <>
      <LoginAdmin
        show={showLogin}
        handleShow={() => setShowLogin(!showLogin)}
      />
      {/* <ForgotPassword
        show={!showLogin}
        handleShow={() => setShowLogin(!showLogin)}
      /> */}
    </>
  );
};

export default AdminAuthenticate;
