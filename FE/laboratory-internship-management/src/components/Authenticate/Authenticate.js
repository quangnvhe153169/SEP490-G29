import React from "react";
import { useAuth } from "../../customs/authService";
import { useNavigate } from "react-router-dom";
import Login from "./Login";

const Authenticate = () => {
  const navigator = useNavigate();
  const { isLogged } = useAuth();

  return isLogged() ? navigator("/") : <Login />;
};

export default Authenticate;
