import { useAuth } from "../../customs/authService";

const PrivateRoute = ({ element, allowedRoles = [] }) => {
  const { privateAuth } = useAuth();
  return privateAuth({children: element, allowedRoles})
};

export default PrivateRoute;
