// ForgotPassword.js
import React from "react";
import {
  Box,
  TextField,
  Button,
  Typography,
  Container,
  Divider,
} from "@mui/material";
import Logo from "../../assets/images/LIM.svg";
import { CSSTransition } from "react-transition-group";
import "../../assets/css/Form.css";

const ForgotPassword = ({ show, handleShow }) => {
  const handleForgotPassword = (event) => {
    event.preventDefault();
    // Xử lý logic quên mật khẩu ở đây
  };

  return (
    <CSSTransition in={show} timeout={300} classNames="form" unmountOnExit>
      <Container component="main" maxWidth="xs">
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            height: "90vh",
          }}
        >
          <Box display="flex" justifyContent="center" alignItems="center">
            <img
              src={Logo}
              alt="Logo"
              style={{ width: "50%", height: "auto" }}
            />
          </Box>
          <Typography component="h1" variant="h5">
            Forgot Password
          </Typography>
          <Box
            component="form"
            onSubmit={handleForgotPassword}
            noValidate
            sx={{ mt: 1 }}
          >
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Reset Password
            </Button>
            <Divider sx={{ my: 2 }}>OR</Divider>
            <Button
              fullWidth
              onClick={handleShow}
              variant="outlined"
              sx={{ mt: 3, mb: 2 }}
            >
              Reset Password
            </Button>
          </Box>
        </Box>
      </Container>
    </CSSTransition>
  );
};

export default ForgotPassword;
