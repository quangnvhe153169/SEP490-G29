import React from "react";
import { GoogleLogin } from "@react-oauth/google";
import { toast } from 'react-toastify';

const GoogleLoginComponent = ({loginGoogle}) => {
  // const navigator = useNavigate();

  const handleLoginFailure = (error) => {
    console.log("Login failed: ", error);
    toast.error(error.response.data)
  };

  return (
    <div>
      <GoogleLogin
        onSuccess={loginGoogle}
        onError={handleLoginFailure}
      />
    </div>
  );
};

export default GoogleLoginComponent;
