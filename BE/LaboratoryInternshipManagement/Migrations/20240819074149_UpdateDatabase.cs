﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace LaboratoryInternshipManagement.Migrations
{
    public partial class UpdateDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase().Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "AspNetRoles",
                    columns: table => new
                    {
                        Id = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Name = table
                            .Column<string>(type: "varchar(256)", maxLength: 256, nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        NormalizedName = table
                            .Column<string>(type: "varchar(256)", maxLength: 256, nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        ConcurrencyStamp = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4")
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "ImportHistories",
                    columns: table => new
                    {
                        Id = table
                            .Column<int>(type: "int", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        FileName = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        ImportDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                        Success = table.Column<bool>(type: "tinyint(1)", nullable: false),
                        ErrorMessage = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4")
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_ImportHistories", x => x.Id);
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "ImportInternErrors",
                    columns: table => new
                    {
                        Id = table
                            .Column<int>(type: "int", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        RollNumber = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        FirstName = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        LastName = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        DOB = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                        Email = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        LabName = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        ErrorMessage = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4")
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_ImportInternErrors", x => x.Id);
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "Semesters",
                    columns: table => new
                    {
                        SemesterId = table
                            .Column<int>(type: "int", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        SemesterName = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        StartDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                        EndDate = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_Semesters", x => x.SemesterId);
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "Tasks",
                    columns: table => new
                    {
                        Id = table
                            .Column<long>(type: "bigint", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        InternId = table
                            .Column<string>(type: "varchar(450)", maxLength: 450, nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        MissionId = table.Column<long>(type: "bigint", nullable: false),
                        Title = table
                            .Column<string>(type: "varchar(256)", maxLength: 256, nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Description = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Status = table.Column<byte>(type: "tinyint unsigned", nullable: false)
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_Tasks", x => x.Id);
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "Teams",
                    columns: table => new
                    {
                        TeamId = table
                            .Column<int>(type: "int", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        Name = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4")
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_Teams", x => x.TeamId);
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "AspNetRoleClaims",
                    columns: table => new
                    {
                        Id = table
                            .Column<int>(type: "int", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        RoleId = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        ClaimType = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        ClaimValue = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4")
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                        table.ForeignKey(
                            name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                            column: x => x.RoleId,
                            principalTable: "AspNetRoles",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Cascade
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "AspNetUserClaims",
                    columns: table => new
                    {
                        Id = table
                            .Column<int>(type: "int", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        UserId = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        ClaimType = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        ClaimValue = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4")
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "AspNetUserLogins",
                    columns: table => new
                    {
                        LoginProvider = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        ProviderKey = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        ProviderDisplayName = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        UserId = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4")
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey(
                            "PK_AspNetUserLogins",
                            x => new { x.LoginProvider, x.ProviderKey }
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "AspNetUserRoles",
                    columns: table => new
                    {
                        UserId = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        RoleId = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4")
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                        table.ForeignKey(
                            name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                            column: x => x.RoleId,
                            principalTable: "AspNetRoles",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Cascade
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "AspNetUsers",
                    columns: table => new
                    {
                        Id = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Discriminator = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        LabId = table.Column<int>(type: "int", nullable: true),
                        MentorId = table
                            .Column<string>(type: "varchar(255)", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        BirthDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                        LastName = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        FirstName = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        RollName = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Status = table.Column<bool>(type: "tinyint(1)", nullable: true),
                        IsTestAccount = table.Column<bool>(type: "tinyint(1)", nullable: true),
                        Avatar = table.Column<byte[]>(type: "longblob", nullable: true),
                        Cv = table.Column<byte[]>(type: "longblob", nullable: true),
                        Language = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        NotifyToMail = table.Column<bool>(type: "tinyint(1)", nullable: true),
                        ReportId = table.Column<int>(type: "int", nullable: true),
                        SemesterId = table.Column<int>(type: "int", nullable: true),
                        UserName = table
                            .Column<string>(type: "varchar(256)", maxLength: 256, nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        NormalizedUserName = table
                            .Column<string>(type: "varchar(256)", maxLength: 256, nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Email = table
                            .Column<string>(type: "varchar(256)", maxLength: 256, nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        NormalizedEmail = table
                            .Column<string>(type: "varchar(256)", maxLength: 256, nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        EmailConfirmed = table.Column<bool>(type: "tinyint(1)", nullable: false),
                        PasswordHash = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        SecurityStamp = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        ConcurrencyStamp = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        PhoneNumber = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        PhoneNumberConfirmed = table.Column<bool>(
                            type: "tinyint(1)",
                            nullable: false
                        ),
                        TwoFactorEnabled = table.Column<bool>(type: "tinyint(1)", nullable: false),
                        LockoutEnd = table.Column<DateTimeOffset>(
                            type: "datetime(6)",
                            nullable: true
                        ),
                        LockoutEnabled = table.Column<bool>(type: "tinyint(1)", nullable: false),
                        AccessFailedCount = table.Column<int>(type: "int", nullable: false)
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                        table.ForeignKey(
                            name: "FK_AspNetUsers_AspNetUsers_MentorId",
                            column: x => x.MentorId,
                            principalTable: "AspNetUsers",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Restrict
                        );
                        table.ForeignKey(
                            name: "ForeignKey_User_Semester_Interns",
                            column: x => x.SemesterId,
                            principalTable: "Semesters",
                            principalColumn: "SemesterId",
                            onDelete: ReferentialAction.Restrict
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "AspNetUserTokens",
                    columns: table => new
                    {
                        UserId = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        LoginProvider = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Name = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Value = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4")
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey(
                            "PK_AspNetUserTokens",
                            x => new
                            {
                                x.UserId,
                                x.LoginProvider,
                                x.Name
                            }
                        );
                        table.ForeignKey(
                            name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                            column: x => x.UserId,
                            principalTable: "AspNetUsers",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Cascade
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "Attendances",
                    columns: table => new
                    {
                        Id = table
                            .Column<int>(type: "int", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        Status = table.Column<int>(type: "int", nullable: false),
                        CheckInTime = table.Column<TimeSpan>(type: "time(6)", nullable: true),
                        CheckOutTime = table.Column<TimeSpan>(type: "time(6)", nullable: true),
                        InternId = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Reason = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        IsInAllowedLimit = table.Column<bool>(type: "tinyint(1)", nullable: true),
                        IsSuspense = table.Column<bool>(type: "tinyint(1)", nullable: true),
                        Date = table.Column<DateTime>(type: "datetime(6)", nullable: false)
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_Attendances", x => x.Id);
                        table.ForeignKey(
                            name: "FK_Attendance_Intern",
                            column: x => x.InternId,
                            principalTable: "AspNetUsers",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Restrict
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "Comments",
                    columns: table => new
                    {
                        CommentId = table
                            .Column<int>(type: "int", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        ParentCommentId = table.Column<int>(type: "int", nullable: true),
                        CreateById = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Content = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        CreatedAt = table.Column<DateTime>(
                            type: "timestamp",
                            nullable: false,
                            defaultValueSql: "CURRENT_TIMESTAMP"
                        ),
                        UpdatedAt = table.Column<DateTime>(
                            type: "timestamp",
                            nullable: false,
                            defaultValueSql: "CURRENT_TIMESTAMP"
                        ),
                        ReletedObjectId = table.Column<int>(type: "int", nullable: false),
                        ReletedObjectType = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        MentionedUserIds = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        AttachedFiles = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4")
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_Comments", x => x.CommentId);
                        table.ForeignKey(
                            name: "FK_Comments_AspNetUsers_CreateById",
                            column: x => x.CreateById,
                            principalTable: "AspNetUsers",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Cascade
                        );
                        table.ForeignKey(
                            name: "FK_Comments_Comments_ParentCommentId",
                            column: x => x.ParentCommentId,
                            principalTable: "Comments",
                            principalColumn: "CommentId",
                            onDelete: ReferentialAction.Restrict
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "Events",
                    columns: table => new
                    {
                        EventId = table
                            .Column<int>(type: "int", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        UserId = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Title = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Description = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        StartDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                        EndDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                        Location = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        CreatedAt = table.Column<DateTime>(
                            type: "timestamp",
                            nullable: false,
                            defaultValueSql: "CURRENT_TIMESTAMP"
                        ),
                        UpdatedAt = table.Column<DateTime>(
                            type: "timestamp",
                            nullable: false,
                            defaultValueSql: "CURRENT_TIMESTAMP"
                        ),
                        RowVersion = table
                            .Column<DateTime>(
                                type: "timestamp(6)",
                                rowVersion: true,
                                nullable: false
                            )
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.ComputedColumn
                            )
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_Events", x => x.EventId);
                        table.ForeignKey(
                            name: "FK_Events_AspNetUsers_UserId",
                            column: x => x.UserId,
                            principalTable: "AspNetUsers",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Restrict
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "ImportInterns",
                    columns: table => new
                    {
                        Id = table
                            .Column<int>(type: "int", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        MentorID = table
                            .Column<string>(type: "varchar(255)", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        RollNumber = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        FirstName = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        LastName = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        DOB = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                        Email = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        LabName = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        CVName = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        CVDescript = table.Column<byte[]>(type: "longblob", nullable: false)
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_ImportInterns", x => x.Id);
                        table.ForeignKey(
                            name: "FK_ImprotIntern_Mentor",
                            column: x => x.MentorID,
                            principalTable: "AspNetUsers",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Restrict
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "LabInformationRegist",
                    columns: table => new
                    {
                        Id = table
                            .Column<int>(type: "int", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        LabID = table.Column<int>(type: "int", nullable: false),
                        MacAdress = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        IpAdress = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        FaceVector = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        InternId = table
                            .Column<string>(type: "varchar(255)", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4")
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_LabInformationRegist", x => x.Id);
                        table.ForeignKey(
                            name: "FK_LabInformationRegist_AspNetUsers_InternId",
                            column: x => x.InternId,
                            principalTable: "AspNetUsers",
                            principalColumn: "Id"
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "Laboratories",
                    columns: table => new
                    {
                        Id = table
                            .Column<int>(type: "int", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        LabName = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        CheckInTime = table.Column<TimeSpan>(type: "time(6)", nullable: false),
                        CheckOutTime = table.Column<TimeSpan>(type: "time(6)", nullable: false),
                        LeaveRequestNum = table.Column<int>(type: "int", nullable: true),
                        LabManagerId = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4")
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_Laboratories", x => x.Id);
                        table.ForeignKey(
                            name: "FK_Laboratories_AspNetUsers_LabManagerId",
                            column: x => x.LabManagerId,
                            principalTable: "AspNetUsers",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Cascade
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "Notifications",
                    columns: table => new
                    {
                        NotificationId = table
                            .Column<int>(type: "int", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        CreatedByUserId = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Type = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Message = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        IsRead = table.Column<bool>(type: "tinyint(1)", nullable: false),
                        CreatedAt = table.Column<DateTime>(
                            type: "timestamp",
                            nullable: false,
                            defaultValueSql: "CURRENT_TIMESTAMP"
                        ),
                        TargetUrl = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4")
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_Notifications", x => x.NotificationId);
                        table.ForeignKey(
                            name: "FK_Notifications_AspNetUsers_CreatedByUserId",
                            column: x => x.CreatedByUserId,
                            principalTable: "AspNetUsers",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Restrict
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "Objectives",
                    columns: table => new
                    {
                        ObjectiveId = table
                            .Column<int>(type: "int", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        CreateBy = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        TeamId = table.Column<int>(type: "int", nullable: false),
                        Title = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Description = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        IsApprove = table.Column<bool>(type: "tinyint(1)", nullable: false),
                        Status = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        StartDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                        EndDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                        CreatedAt = table.Column<DateTime>(
                            type: "timestamp",
                            nullable: false,
                            defaultValueSql: "CURRENT_TIMESTAMP"
                        ),
                        UpdatedAt = table.Column<DateTime>(
                            type: "timestamp",
                            nullable: false,
                            defaultValueSql: "CURRENT_TIMESTAMP"
                        ),
                        RowVersion = table
                            .Column<DateTime>(
                                type: "timestamp(6)",
                                rowVersion: true,
                                nullable: false
                            )
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.ComputedColumn
                            )
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_Objectives", x => x.ObjectiveId);
                        table.ForeignKey(
                            name: "FK_Objectives_AspNetUsers_CreateBy",
                            column: x => x.CreateBy,
                            principalTable: "AspNetUsers",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Cascade
                        );
                        table.ForeignKey(
                            name: "FK_Objectives_Teams_TeamId",
                            column: x => x.TeamId,
                            principalTable: "Teams",
                            principalColumn: "TeamId",
                            onDelete: ReferentialAction.Cascade
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "OKRLogs",
                    columns: table => new
                    {
                        OKRLogId = table
                            .Column<int>(type: "int", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        ObjectiveId = table.Column<int>(type: "int", nullable: true),
                        KeyResultId = table.Column<int>(type: "int", nullable: true),
                        UserId = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        ChangeDate = table.Column<DateTime>(
                            type: "timestamp",
                            nullable: false,
                            defaultValueSql: "CURRENT_TIMESTAMP"
                        ),
                        ChangeType = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        ChangeDetails = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4")
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_OKRLogs", x => x.OKRLogId);
                        table.ForeignKey(
                            name: "FK_OKRLogs_AspNetUsers_UserId",
                            column: x => x.UserId,
                            principalTable: "AspNetUsers",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Restrict
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "Reports",
                    columns: table => new
                    {
                        ReportId = table
                            .Column<int>(type: "int", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        StaffId = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        RollName = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        MentorId = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        InternId = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        NameIntern = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        NameLab = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        NameMentor = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Allowance = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        ComOfCompany = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        MajorSkill = table.Column<double>(type: "double", nullable: false),
                        SoftSkill = table.Column<double>(type: "double", nullable: false),
                        Attitude = table.Column<double>(type: "double", nullable: false),
                        CreatedDate = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                        SemesterId = table.Column<int>(type: "int", nullable: true)
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_Reports", x => x.ReportId);
                        table.ForeignKey(
                            name: "FK_Reports_Mentor",
                            column: x => x.MentorId,
                            principalTable: "AspNetUsers",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Restrict
                        );
                        table.ForeignKey(
                            name: "ForeignKey_Reports_Semester",
                            column: x => x.SemesterId,
                            principalTable: "Semesters",
                            principalColumn: "SemesterId",
                            onDelete: ReferentialAction.SetNull
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "TeamMemberships",
                    columns: table => new
                    {
                        TeamId = table.Column<int>(type: "int", nullable: false),
                        UserId = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4")
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_TeamMemberships", x => new { x.TeamId, x.UserId });
                        table.ForeignKey(
                            name: "FK_TeamMemberships_AspNetUsers_UserId",
                            column: x => x.UserId,
                            principalTable: "AspNetUsers",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Cascade
                        );
                        table.ForeignKey(
                            name: "FK_TeamMemberships_Teams_TeamId",
                            column: x => x.TeamId,
                            principalTable: "Teams",
                            principalColumn: "TeamId",
                            onDelete: ReferentialAction.Cascade
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "Tickets",
                    columns: table => new
                    {
                        Id = table
                            .Column<long>(type: "bigint", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        CreateId = table
                            .Column<string>(type: "varchar(450)", maxLength: 450, nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        AssignId = table
                            .Column<string>(type: "varchar(450)", maxLength: 450, nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Title = table
                            .Column<string>(type: "varchar(256)", maxLength: 256, nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Description = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        DateRequire = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                        Type = table.Column<byte>(type: "tinyint unsigned", nullable: false),
                        Status = table.Column<byte>(type: "tinyint unsigned", nullable: false)
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_Tickets", x => x.Id);
                        table.ForeignKey(
                            name: "FK_Tickets_AspNetUsers_AssignId",
                            column: x => x.AssignId,
                            principalTable: "AspNetUsers",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Restrict
                        );
                        table.ForeignKey(
                            name: "FK_Tickets_AspNetUsers_CreateId",
                            column: x => x.CreateId,
                            principalTable: "AspNetUsers",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Restrict
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "EventAttendees",
                    columns: table => new
                    {
                        EventAttendeeId = table
                            .Column<int>(type: "int", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        EventId = table.Column<int>(type: "int", nullable: false),
                        UserId = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Status = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        CreatedAt = table.Column<DateTime>(
                            type: "timestamp",
                            nullable: false,
                            defaultValueSql: "CURRENT_TIMESTAMP"
                        ),
                        UpdatedAt = table.Column<DateTime>(
                            type: "timestamp",
                            nullable: false,
                            defaultValueSql: "CURRENT_TIMESTAMP"
                        ),
                        RowVersion = table
                            .Column<DateTime>(
                                type: "timestamp(6)",
                                rowVersion: true,
                                nullable: false
                            )
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.ComputedColumn
                            )
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_EventAttendees", x => x.EventAttendeeId);
                        table.ForeignKey(
                            name: "FK_EventAttendees_AspNetUsers_UserId",
                            column: x => x.UserId,
                            principalTable: "AspNetUsers",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Cascade
                        );
                        table.ForeignKey(
                            name: "FK_EventAttendees_Events_EventId",
                            column: x => x.EventId,
                            principalTable: "Events",
                            principalColumn: "EventId",
                            onDelete: ReferentialAction.Cascade
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "DayOff",
                    columns: table => new
                    {
                        Id = table
                            .Column<int>(type: "int", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        LabId = table.Column<int>(type: "int", nullable: false),
                        Date = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                        Year = table.Column<int>(type: "int", nullable: false)
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_DayOff", x => x.Id);
                        table.ForeignKey(
                            name: "FK_DayOff_Laboratories_LabId",
                            column: x => x.LabId,
                            principalTable: "Laboratories",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Restrict
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "NotificationUser",
                    columns: table => new
                    {
                        NotificationId = table.Column<int>(type: "int", nullable: false),
                        UserId = table
                            .Column<string>(type: "varchar(255)", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4")
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey(
                            "PK_NotificationUser",
                            x => new { x.NotificationId, x.UserId }
                        );
                        table.ForeignKey(
                            name: "FK_NotificationUser_AspNetUsers_UserId",
                            column: x => x.UserId,
                            principalTable: "AspNetUsers",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Cascade
                        );
                        table.ForeignKey(
                            name: "FK_NotificationUser_Notifications_NotificationId",
                            column: x => x.NotificationId,
                            principalTable: "Notifications",
                            principalColumn: "NotificationId",
                            onDelete: ReferentialAction.Cascade
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "KeyResults",
                    columns: table => new
                    {
                        KeyResultId = table
                            .Column<int>(type: "int", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        ObjectiveId = table.Column<int>(type: "int", nullable: false),
                        Assign = table
                            .Column<string>(type: "varchar(255)", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Title = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Priority = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Status = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Notes = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        AttachedFiles = table
                            .Column<string>(type: "longtext", nullable: true)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        TargetValue = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                        CurrentValue = table.Column<decimal>(
                            type: "decimal(18,2)",
                            nullable: false
                        ),
                        StartDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                        EndDate = table.Column<DateTime>(type: "datetime(6)", nullable: false),
                        EstimateManDay = table.Column<decimal>(
                            type: "decimal(18,2)",
                            nullable: false
                        ),
                        ActualManDay = table.Column<decimal>(
                            type: "decimal(18,2)",
                            nullable: false
                        ),
                        CreatedAt = table.Column<DateTime>(
                            type: "timestamp",
                            nullable: false,
                            defaultValueSql: "CURRENT_TIMESTAMP"
                        ),
                        UpdatedAt = table.Column<DateTime>(
                            type: "timestamp",
                            nullable: false,
                            defaultValueSql: "CURRENT_TIMESTAMP"
                        ),
                        RowVersion = table
                            .Column<DateTime>(
                                type: "timestamp(6)",
                                rowVersion: true,
                                nullable: false
                            )
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.ComputedColumn
                            )
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_KeyResults", x => x.KeyResultId);
                        table.ForeignKey(
                            name: "FK_KeyResults_AspNetUsers_Assign",
                            column: x => x.Assign,
                            principalTable: "AspNetUsers",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Restrict
                        );
                        table.ForeignKey(
                            name: "FK_KeyResults_Objectives_ObjectiveId",
                            column: x => x.ObjectiveId,
                            principalTable: "Objectives",
                            principalColumn: "ObjectiveId",
                            onDelete: ReferentialAction.Cascade
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "TicketComments",
                    columns: table => new
                    {
                        Id = table
                            .Column<long>(type: "bigint", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        TicketId = table.Column<long>(type: "bigint", nullable: false),
                        UserId = table
                            .Column<string>(type: "varchar(450)", maxLength: 450, nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Description = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4")
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_TicketComments", x => x.Id);
                        table.ForeignKey(
                            name: "FK_TicketComments_Tickets_TicketId",
                            column: x => x.TicketId,
                            principalTable: "Tickets",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Cascade
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "TicketTask",
                    columns: table => new
                    {
                        TaskId = table.Column<long>(type: "bigint", nullable: false),
                        TicketId = table.Column<long>(type: "bigint", nullable: false)
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_TicketTask", x => new { x.TaskId, x.TicketId });
                        table.ForeignKey(
                            name: "FK_TicketTask_Tasks_TaskId",
                            column: x => x.TaskId,
                            principalTable: "Tasks",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Cascade
                        );
                        table.ForeignKey(
                            name: "FK_TicketTask_Tickets_TicketId",
                            column: x => x.TicketId,
                            principalTable: "Tickets",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Cascade
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder
                .CreateTable(
                    name: "ProgressPerformances",
                    columns: table => new
                    {
                        ProgressPerformanceId = table
                            .Column<int>(type: "int", nullable: false)
                            .Annotation(
                                "MySql:ValueGenerationStrategy",
                                MySqlValueGenerationStrategy.IdentityColumn
                            ),
                        KeyResultId = table.Column<int>(type: "int", nullable: false),
                        ReportDate = table.Column<DateTime>(
                            type: "timestamp",
                            nullable: false,
                            defaultValueSql: "CURRENT_TIMESTAMP"
                        ),
                        CurrentValue = table.Column<decimal>(
                            type: "decimal(18,2)",
                            nullable: false
                        ),
                        TargetValue = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                        Variance = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                        Comments = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Insights = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4"),
                        Recommendations = table
                            .Column<string>(type: "longtext", nullable: false)
                            .Annotation("MySql:CharSet", "utf8mb4")
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_ProgressPerformances", x => x.ProgressPerformanceId);
                        table.ForeignKey(
                            name: "FK_ProgressPerformances_KeyResults_KeyResultId",
                            column: x => x.KeyResultId,
                            principalTable: "KeyResults",
                            principalColumn: "KeyResultId",
                            onDelete: ReferentialAction.Cascade
                        );
                    }
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "1", "454d3d60-e23c-468a-bd30-65e1b48a74e4", "System Admin", "System Admin" },
                    { "2", "ca1ab8bc-a0f0-4ad7-91e4-d59707f1725f", "Lab Manager", "Lab Manager" },
                    { "3", "a5e43938-e439-4cd6-a5f6-1403d7784941", "Mentor", "Mentor" },
                    { "4", "497564d4-0324-4dd2-8686-390cd16e9fd6", "Intern", "Intern" }
                }
            );

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[]
                {
                    "Id",
                    "AccessFailedCount",
                    "Avatar",
                    "BirthDate",
                    "ConcurrencyStamp",
                    "Cv",
                    "Discriminator",
                    "Email",
                    "EmailConfirmed",
                    "FirstName",
                    "IsTestAccount",
                    "LabId",
                    "Language",
                    "LastName",
                    "LockoutEnabled",
                    "LockoutEnd",
                    "MentorId",
                    "NormalizedEmail",
                    "NormalizedUserName",
                    "NotifyToMail",
                    "PasswordHash",
                    "PhoneNumber",
                    "PhoneNumberConfirmed",
                    "ReportId",
                    "RollName",
                    "SecurityStamp",
                    "SemesterId",
                    "Status",
                    "TwoFactorEnabled",
                    "UserName"
                },
                values: new object[]
                {
                    "1",
                    0,
                    null,
                    new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                    "3675df88-ae3c-4dcb-b984-b7b383a5c737",
                    null,
                    "User",
                    null,
                    true,
                    null,
                    false,
                    null,
                    "en",
                    null,
                    false,
                    null,
                    null,
                    null,
                    "admin",
                    true,
                    "AQAAAAEAACcQAAAAEOwAvRadLztWJl6SkcFVExJCNmZTe1VbwrB3V9AuZ2HTtq1hgJnNEkTWMMOBCPpjew==",
                    null,
                    false,
                    null,
                    null,
                    "",
                    null,
                    true,
                    false,
                    "admin"
                }
            );

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "1", "1" }
            );

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId"
            );

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true
            );

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId"
            );

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail"
            );

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_LabId",
                table: "AspNetUsers",
                column: "LabId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_MentorId",
                table: "AspNetUsers",
                column: "MentorId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_SemesterId",
                table: "AspNetUsers",
                column: "SemesterId"
            );

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true
            );

            migrationBuilder.CreateIndex(
                name: "IX_Attendances_InternId",
                table: "Attendances",
                column: "InternId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_Comments_CreateById",
                table: "Comments",
                column: "CreateById"
            );

            migrationBuilder.CreateIndex(
                name: "IX_Comments_ParentCommentId",
                table: "Comments",
                column: "ParentCommentId"
            );

            migrationBuilder.CreateIndex(name: "IX_DayOff_LabId", table: "DayOff", column: "LabId");

            migrationBuilder.CreateIndex(
                name: "IX_EventAttendees_EventId",
                table: "EventAttendees",
                column: "EventId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_EventAttendees_UserId",
                table: "EventAttendees",
                column: "UserId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_Events_UserId",
                table: "Events",
                column: "UserId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_ImportInterns_MentorID",
                table: "ImportInterns",
                column: "MentorID"
            );

            migrationBuilder.CreateIndex(
                name: "IX_KeyResults_Assign",
                table: "KeyResults",
                column: "Assign"
            );

            migrationBuilder.CreateIndex(
                name: "IX_KeyResults_ObjectiveId",
                table: "KeyResults",
                column: "ObjectiveId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_LabInformationRegist_InternId",
                table: "LabInformationRegist",
                column: "InternId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_Laboratories_LabManagerId",
                table: "Laboratories",
                column: "LabManagerId",
                unique: true
            );

            migrationBuilder.CreateIndex(
                name: "IX_Notifications_CreatedByUserId",
                table: "Notifications",
                column: "CreatedByUserId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_NotificationUser_UserId",
                table: "NotificationUser",
                column: "UserId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_Objectives_CreateBy",
                table: "Objectives",
                column: "CreateBy"
            );

            migrationBuilder.CreateIndex(
                name: "IX_Objectives_TeamId",
                table: "Objectives",
                column: "TeamId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_OKRLogs_UserId",
                table: "OKRLogs",
                column: "UserId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_ProgressPerformances_KeyResultId",
                table: "ProgressPerformances",
                column: "KeyResultId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_Reports_MentorId",
                table: "Reports",
                column: "MentorId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_Reports_SemesterId",
                table: "Reports",
                column: "SemesterId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_TeamMemberships_UserId",
                table: "TeamMemberships",
                column: "UserId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_TicketComments_TicketId",
                table: "TicketComments",
                column: "TicketId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_Tickets_AssignId",
                table: "Tickets",
                column: "AssignId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_Tickets_CreateId",
                table: "Tickets",
                column: "CreateId"
            );

            migrationBuilder.CreateIndex(
                name: "IX_TicketTask_TicketId",
                table: "TicketTask",
                column: "TicketId"
            );

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade
            );

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade
            );

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade
            );

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Laboratories_LabId",
                table: "AspNetUsers",
                column: "LabId",
                principalTable: "Laboratories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Laboratories_AspNetUsers_LabManagerId",
                table: "Laboratories"
            );

            migrationBuilder.DropTable(name: "AspNetRoleClaims");

            migrationBuilder.DropTable(name: "AspNetUserClaims");

            migrationBuilder.DropTable(name: "AspNetUserLogins");

            migrationBuilder.DropTable(name: "AspNetUserRoles");

            migrationBuilder.DropTable(name: "AspNetUserTokens");

            migrationBuilder.DropTable(name: "Attendances");

            migrationBuilder.DropTable(name: "Comments");

            migrationBuilder.DropTable(name: "DayOff");

            migrationBuilder.DropTable(name: "EventAttendees");

            migrationBuilder.DropTable(name: "ImportHistories");

            migrationBuilder.DropTable(name: "ImportInternErrors");

            migrationBuilder.DropTable(name: "ImportInterns");

            migrationBuilder.DropTable(name: "LabInformationRegist");

            migrationBuilder.DropTable(name: "NotificationUser");

            migrationBuilder.DropTable(name: "OKRLogs");

            migrationBuilder.DropTable(name: "ProgressPerformances");

            migrationBuilder.DropTable(name: "Reports");

            migrationBuilder.DropTable(name: "TeamMemberships");

            migrationBuilder.DropTable(name: "TicketComments");

            migrationBuilder.DropTable(name: "TicketTask");

            migrationBuilder.DropTable(name: "AspNetRoles");

            migrationBuilder.DropTable(name: "Events");

            migrationBuilder.DropTable(name: "Notifications");

            migrationBuilder.DropTable(name: "KeyResults");

            migrationBuilder.DropTable(name: "Tasks");

            migrationBuilder.DropTable(name: "Tickets");

            migrationBuilder.DropTable(name: "Objectives");

            migrationBuilder.DropTable(name: "Teams");

            migrationBuilder.DropTable(name: "AspNetUsers");

            migrationBuilder.DropTable(name: "Laboratories");

            migrationBuilder.DropTable(name: "Semesters");
        }
    }
}
