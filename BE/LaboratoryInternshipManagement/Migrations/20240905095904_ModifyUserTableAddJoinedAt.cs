﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace LaboratoryInternshipManagement.Migrations
{
    public partial class ModifyUserTableAddJoinedAt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(name: "ReportId", table: "AspNetUsers");

            migrationBuilder.AddColumn<DateTime>(
                name: "JoinedAt",
                table: "AspNetUsers",
                type: "timestamp",
                nullable: true,
                defaultValueSql: "CURRENT_TIMESTAMP"
            );

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1",
                column: "ConcurrencyStamp",
                value: "21685199-be71-4b9a-9474-e36299675a3f"
            );

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2",
                column: "ConcurrencyStamp",
                value: "2401ea24-0e96-45a0-886a-889af51f4b43"
            );

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "3",
                column: "ConcurrencyStamp",
                value: "c3404a93-4f89-40fb-b14a-c2f6ea26d9ef"
            );

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "4",
                column: "ConcurrencyStamp",
                value: "7af41760-a619-4ed7-a78f-9eaf1be04df7"
            );

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[]
                {
                    "8a589912-1603-4672-96dd-700544f0a19c",
                    "AQAAAAEAACcQAAAAELqgoIsV95bszyoY1S/5iT9f/cuBMZfWPBm7bRSqX8m9VjvtKnqcolqqoYdbtKVv7g=="
                }
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(name: "JoinedAt", table: "AspNetUsers");

            migrationBuilder.AddColumn<int>(
                name: "ReportId",
                table: "AspNetUsers",
                type: "int",
                nullable: true
            );

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1",
                column: "ConcurrencyStamp",
                value: "6dac3b44-e786-4f91-853b-9f478eca0c2b"
            );

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2",
                column: "ConcurrencyStamp",
                value: "f2c0a885-e058-46e1-8063-09d4ca5a29f7"
            );

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "3",
                column: "ConcurrencyStamp",
                value: "ffcdf4fb-b7b5-476b-924e-ddcc5f73497e"
            );

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "4",
                column: "ConcurrencyStamp",
                value: "8cb836d8-8494-40ee-941e-d92842e04004"
            );

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[]
                {
                    "a8b1e35f-829d-4f69-a5e3-983f45f5ccb5",
                    "AQAAAAEAACcQAAAAEKiO++uV/nwN/38bGJ3PnZ9fQk1DlXw2neqPjlQKMSSc8t3ecLjMyfTtSHzY83pg/w=="
                }
            );
        }
    }
}
