﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace LaboratoryInternshipManagement.Migrations
{
    public partial class ModifyImportIntern : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ExpiredAt",
                table: "ImportInterns",
                type: "datetime(6)",
                nullable: true
            );

            migrationBuilder
                .AddColumn<string>(
                    name: "Signature",
                    table: "ImportInterns",
                    type: "longtext",
                    nullable: true
                )
                .Annotation("MySql:CharSet", "utf8mb4");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1",
                column: "ConcurrencyStamp",
                value: "929ad092-168a-439f-b321-a5e3cd80302e"
            );

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2",
                column: "ConcurrencyStamp",
                value: "7048736d-c815-4fbe-9b81-717ac3958ba4"
            );

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "3",
                column: "ConcurrencyStamp",
                value: "04f1193c-bb5e-4e0b-88ef-6c27e730589d"
            );

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "4",
                column: "ConcurrencyStamp",
                value: "78e1dfaa-9dca-4fa0-bebe-7ea8582231d5"
            );

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[]
                {
                    "bb5f5b13-0cbe-4478-a786-4d6e01bc2c87",
                    "AQAAAAEAACcQAAAAEDxTvYmcsm45mtX0VXR693mZMw6NZd14w6QOYcPXZ+RmkEVpFuH3OdLg/qGCqCapRg=="
                }
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(name: "ExpiredAt", table: "ImportInterns");

            migrationBuilder.DropColumn(name: "Signature", table: "ImportInterns");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1",
                column: "ConcurrencyStamp",
                value: "21685199-be71-4b9a-9474-e36299675a3f"
            );

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2",
                column: "ConcurrencyStamp",
                value: "2401ea24-0e96-45a0-886a-889af51f4b43"
            );

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "3",
                column: "ConcurrencyStamp",
                value: "c3404a93-4f89-40fb-b14a-c2f6ea26d9ef"
            );

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "4",
                column: "ConcurrencyStamp",
                value: "7af41760-a619-4ed7-a78f-9eaf1be04df7"
            );

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[]
                {
                    "8a589912-1603-4672-96dd-700544f0a19c",
                    "AQAAAAEAACcQAAAAELqgoIsV95bszyoY1S/5iT9f/cuBMZfWPBm7bRSqX8m9VjvtKnqcolqqoYdbtKVv7g=="
                }
            );
        }
    }
}
