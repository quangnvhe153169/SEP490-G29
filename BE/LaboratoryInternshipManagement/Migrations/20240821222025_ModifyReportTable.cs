﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace LaboratoryInternshipManagement.Migrations
{
    public partial class ModifyReportTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ReportType",
                table: "Reports",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1",
                column: "ConcurrencyStamp",
                value: "6dac3b44-e786-4f91-853b-9f478eca0c2b");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2",
                column: "ConcurrencyStamp",
                value: "f2c0a885-e058-46e1-8063-09d4ca5a29f7");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "3",
                column: "ConcurrencyStamp",
                value: "ffcdf4fb-b7b5-476b-924e-ddcc5f73497e");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "4",
                column: "ConcurrencyStamp",
                value: "8cb836d8-8494-40ee-941e-d92842e04004");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "a8b1e35f-829d-4f69-a5e3-983f45f5ccb5", "AQAAAAEAACcQAAAAEKiO++uV/nwN/38bGJ3PnZ9fQk1DlXw2neqPjlQKMSSc8t3ecLjMyfTtSHzY83pg/w==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReportType",
                table: "Reports");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "1",
                column: "ConcurrencyStamp",
                value: "c7a2ef26-f2f2-4aac-9878-6abb430d974b");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "2",
                column: "ConcurrencyStamp",
                value: "32c5d4d9-9dfe-4a7d-a0be-4967f2ec693b");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "3",
                column: "ConcurrencyStamp",
                value: "d9e61956-5f18-41d0-8917-42df0af8b7cf");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "4",
                column: "ConcurrencyStamp",
                value: "fd7c4e83-38e6-45b9-b98b-64187f47f677");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1",
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "e1fb69df-212c-4a54-8fd4-51405ec755ed", "AQAAAAEAACcQAAAAEAbS4B/es8ASD3WpBk987k3JoXhdK4VJAauVBarRcOrVGr9n+KW3RWQd/aqWb6LHYg==" });
        }
    }
}
