using System.Text;
using LaboratoryInternshipManagement.Controllers;
using LaboratoryInternshipManagement.DataAccess.Filters;
using LaboratoryInternshipManagement.DataAccess.HubHelpers;
using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.DataAccess.Quartz;
using LaboratoryInternshipManagement.DataAccess.Repositories;
using LaboratoryInternshipManagement.Hubs;
using LaboratoryInternshipManagement.Models;
using LaboratoryInternshipManagement.Models.Enums;
using LaboratoryInternshipManagement.Services;
using LaboratoryInternshipManagement.Services.IServices;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.IdentityModel.Tokens;
using OfficeOpenXml;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;

var builder = WebApplication.CreateBuilder(args);
var env = builder.Environment;

// Set up configuration sources.
builder
    .Configuration.SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
    .AddJsonFile($"appsettings.{builder.Environment.EnvironmentName}.json", optional: true)
    .AddEnvironmentVariables();

// Add services to the container.
builder
    .Services.AddControllers()
    .ConfigureApiBehaviorOptions(options => options.SuppressModelStateInvalidFilter = true);
builder.Services.Configure<ForwardedHeadersOptions>(options =>
{
    // ForwardedHeaders.All should be enabled when behind a proxy
    options.ForwardedHeaders = ForwardedHeaders.All;
});

builder.Services.AddDistributedMemoryCache();
builder.Services.AddHttpContextAccessor();
builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromDays(1);
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddSingleton<IUserIdProvider, CustomUserIdProvider>();
builder.Services.AddSignalR();
builder.Services.AddAutoMapper(
    cfg =>
    {
        cfg.CreateMap<Objective, ObjectiveDTO>()
            .ForMember(dest => dest.Status, opt => opt.MapFrom(src => (int)src.Status)); // Enum to int

        cfg.CreateMap<ObjectiveDTO, Objective>()
            .ForMember(
                dest => dest.Status,
                opt => opt.MapFrom(src => (ConstantEnum.ObjectiveStatus)src.Status)
            ); // Int to enum

        cfg.CreateMap<KeyResult, KeyResultDTO>()
            .ForMember(dest => dest.Status, opt => opt.MapFrom(src => (int)src.Status)); // Enum to int

        cfg.CreateMap<KeyResultDTO, KeyResult>()
            .ForMember(
                dest => dest.Status,
                opt => opt.MapFrom(src => (ConstantEnum.KeyResultStatus)src.Status)
            ); // Int to enum
        cfg.CreateMap<OKRLog, OKRLogDTO>();
        cfg.CreateMap<OKRLogDTO, OKRLog>();
        cfg.CreateMap<Comment, CommentDTO>();
        cfg.CreateMap<CommentDTO, Comment>();
        cfg.CreateMap<Notification, NotificationDTO>();
        cfg.CreateMap<NotificationDTO, Notification>();
        cfg.CreateMap<OKRLog, OKRLogDTO>();
        cfg.CreateMap<OKRLogDTO, OKRLog>();
        cfg.CreateMap<EventDTO, Event>()
            .ForMember(dest => dest.EventAttendees, opt => opt.Ignore());
        cfg.CreateMap<Event, EventDTO>()
            .AfterMap(
                (src, dest) =>
                    dest.AttendeeIds = src.EventAttendees.Select(ea => ea.UserId).ToList()
            );
        cfg.CreateMap<EventAttendee, EventAttendeeDTO>();
        cfg.CreateMap<EventAttendeeDTO, EventAttendee>();
        cfg.CreateMap<ImportIntern, ImportInternDTO>()
            .ForMember(dest => dest.CVDescript, opt => opt.Ignore());

        cfg.CreateMap<ImportInternDTO, ImportIntern>();
        // Add other mappings here if needed
    },
    AppDomain.CurrentDomain.GetAssemblies()
);
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<LabInternManagementDBcontext>(options =>
    options.UseMySql(
        builder.Configuration.GetConnectionString("MySqlDB"),
        new MySqlServerVersion(new Version(9, 0, 0))
    )
);

//.EnableSensitiveDataLogging().LogTo(Console.WriteLine, LogLevel.Information));
builder.Services.AddCors(option =>
    option.AddPolicy(
        "AllowedReactApp",
        build =>
        {
            build
                .WithOrigins(builder.Configuration["AllowedView"])
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials();
        }
    )
);

builder
    .Services.AddIdentity<User, IdentityRole>()
    .AddEntityFrameworkStores<LabInternManagementDBcontext>()
    .AddDefaultTokenProviders();
builder.Services.AddScoped<ICommentService, CommentService>();
builder.Services.AddScoped<INotificationService, NotificationService>();
builder.Services.AddScoped<ICommentRepository, CommentRepository>();
builder.Services.AddScoped<INotificationRepository, NotificationRepository>();
builder.Services.AddScoped<IEventAttendeeRepository, EventAttendeeRepository>();
builder.Services.AddScoped<IEventRepository, EventRepository>();
builder.Services.AddScoped<IImportInternRepository, ImportInternRepository>();
builder.Services.AddScoped<IKeyResultRepository, KeyResultRepository>();
builder.Services.AddScoped<IObjectiveRepository, ObjectiveRepository>();
builder.Services.AddScoped<IOKRLogRepository, OKRLogRepository>();
builder.Services.AddScoped<ITeamMembershipRepository, TeamMembershipRepository>();
builder.Services.AddScoped<ITeamRepository, TeamRepository>();
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddSingleton<FaceRecognitionService>();
builder.Logging.ClearProviders();
builder.Logging.AddConsole();

//builder.Logging.AddFilter("Microsoft.EntityFrameworkCore.Database.Command", LogLevel.Information);
builder.Services.AddSingleton<IJobFactory, SingletonJobFactory>();
builder.Services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
builder.Services.AddSingleton<IImportCSVRepository, ImportCSVRepository>();
builder.Services.AddScoped<IImportHistoryRepository<ImportHistory>, ImportHistoryRepository>();
builder.Services.AddSingleton<IMemoryCache, MemoryCache>();

//builder.Services.AddSingleton<IGoogleEmailRepository, GoogleEmailRepository>();
builder.Services.AddMemoryCache();
builder.Services.AddScoped<IAttendenceRepository, AttendenceRepository>();
builder.Services.AddSingleton<DailyCheckAttendence>();

builder.Services.AddSingleton(
    new JobSchedule(jobType: typeof(DailyCheckAttendence), cronExpression: "0 00 00 * * ?")
); // Chạy lúc 0:00 mỗi ngày

//builder.Services.AddHttpClient<IGoogleEmailRepository, GoogleEmailRepository>();
builder.Services.AddScoped<ImportLabService>();
builder.Services.AddScoped<ImportMentorService>();
builder.Services.AddScoped<GoogleEmailService>();
builder.Services.AddHttpClient();

// Scoped service
builder.Services.AddScoped<LabInternManagementDBcontext>();
builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AddHostedService<QuartzHostedService>();
builder.Services.AddScoped<IReportRepository, ReportRepository>();
builder.Services.AddScoped<ExcelService>();
builder.Services.AddScoped<ISemesterRepository, SemesterRepository>();
builder.Services.AddScoped<IDashBoardRepository, DashBoardRepository>();

builder.Services.AddTransient<IEmailService>(provider => new EmailService(
    builder.Configuration["Email:SmtpServer"],
    int.Parse(builder.Configuration["Email:SmtpPort"]),
    builder.Configuration["Email:SmtpUser"],
    builder.Configuration["Email:SmtpPass"]
));

builder
    .Services.AddAuthentication(auth =>
    {
        auth.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        auth.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    })
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters()
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidAudience = builder.Configuration["JwtSecret:Audience"],
            ValidIssuer = builder.Configuration["JwtSecret:Issuer"],
            IssuerSigningKey = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(builder.Configuration["JwtSecret:Key"])
            ),
            ValidateIssuerSigningKey = true,
        };
        options.Events = new JwtBearerEvents
        {
            OnMessageReceived = context =>
            {
                var accessToken = context.Request.Query["access_token"];
                var path = context.HttpContext.Request.Path;
                if (
                    !string.IsNullOrEmpty(accessToken)
                    && path.StartsWithSegments("/notificationHub")
                )
                {
                    context.Token = accessToken;
                }
                return Task.CompletedTask;
            }
        };
    })
    .AddGoogle(options =>
    {
        options.ClientId = builder.Configuration["Authentication:Google:ClientId"];
        options.ClientSecret = builder.Configuration["Authentication:Google:ClientSecret"];
    });
builder.Services.AddScoped<IpRestrictionFilter>(provider =>
{
    var allowedSubnets = builder.Configuration.GetSection("AllowedSubnet").Get<List<string>>();
    if (allowedSubnets == null)
    {
        throw new InvalidOperationException("AllowedSubnet is not configured properly.");
    }
    return new IpRestrictionFilter(allowedSubnets);
});

ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
builder.Services.AddMemoryCache();
builder.Services.AddAuthorization();
var app = builder.Build();

//Apply migrations at startup
using (var scope = app.Services.CreateScope())
{
    var dbContext = scope.ServiceProvider.GetRequiredService<LabInternManagementDBcontext>();
    dbContext.Database.Migrate();
}

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseDeveloperExceptionPage();
}
app.UseForwardedHeaders(
    new ForwardedHeadersOptions
    {
        ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
    }
);
app.UseCors("AllowedReactApp");
app.UseHttpsRedirection();
app.UseAuthentication();
app.MapControllers();
app.UseSession();
app.UseRouting();
app.UseAuthorization();
app.UseEndpoints(endpoints =>
{
    endpoints.MapHub<NotificationHub>("/notificationHub");
});
app.Run();
