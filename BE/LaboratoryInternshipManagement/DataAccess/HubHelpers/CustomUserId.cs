﻿using Microsoft.AspNetCore.SignalR;

namespace LaboratoryInternshipManagement.DataAccess.HubHelpers
{
    public class CustomUserIdProvider : IUserIdProvider
    {
        public string GetUserId(HubConnectionContext connection)
        {
            var userId = connection.User?.FindFirst("jti")?.Value;
            return userId;
        }
    }
}
