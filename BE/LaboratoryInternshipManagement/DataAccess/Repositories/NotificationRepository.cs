﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using Microsoft.EntityFrameworkCore;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class NotificationRepository : BaseRepository<Notification>, INotificationRepository
    {
        private readonly ILogger<NotificationRepository> _logger;

        public NotificationRepository(
            LabInternManagementDBcontext context,
            ILogger<NotificationRepository> logger
        )
            : base(context)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task AddNotificationToUsersAsync(
            Notification notification,
            List<string> userIds
        )
        {
            if (notification == null)
            {
                _logger.LogError("Notification is null.");
                throw new ArgumentNullException(nameof(notification));
            }

            if (userIds == null || !userIds.Any())
            {
                _logger.LogError("UserIds is null or empty.");
                throw new ArgumentNullException(nameof(userIds));
            }
            // Ensure CreatedByUserId is not null or empty
            if (string.IsNullOrEmpty(notification.CreatedByUserId))
            {
                _logger.LogError("CreatedByUserId is null or empty.");
                throw new ArgumentNullException(nameof(notification.CreatedByUserId));
            }
            // Add the notification to the database
            await Context.Notifications.AddAsync(notification);
            await Context.SaveChangesAsync();

            var users = await Context.Users.Where(u => userIds.Contains(u.Id)).ToListAsync();
            if (users == null || !users.Any())
            {
                _logger.LogError("No users found with the provided userIds.");
                throw new InvalidOperationException("No users found with the provided userIds.");
            }

            // Associate the notification with the users
            foreach (var user in users)
            {
                user.Notifications.Add(notification);
            }

            // Create join table entries
            /* foreach (var user in users)
             {
                 if (user.Notifications == null)
                 {
                     user.Notifications = new List<Notification>();
                 }
                 user.Notifications.Add(notification);
                 // Ensure NotificationUser entity is added to track the relationship
                 var notificationUser = new Dictionary<string, object>
                 {
                     { "UserId", user.Id },
                     { "NotificationId", notification.NotificationId }
                 };
                 Context.Set<Dictionary<string, object>>("NotificationUser").Add(notificationUser);
             }*/

            await Context.SaveChangesAsync();
        }

        public async Task DeleteNotificationAsync(int notificationId)
        {
            var notification = await Context
                .Set<Notification>()
                .Include(n => n.Users)
                .FirstOrDefaultAsync(n => n.NotificationId == notificationId);
            if (notification != null)
            {
                Context.Set<Notification>().Remove(notification);
                await Context.SaveChangesAsync(); // Save changes to persist the deletion
            }
            // Optionally, throw an exception or handle if notification is not found
        }

        public async Task<List<Notification>> GetAllNotificationsAsync()
        {
            return await Context.Set<Notification>().Include(n => n.Users).ToListAsync();
        }

        public async Task<Notification> GetNotificationByIdAsync(int notificationId)
        {
            return await Context
                .Set<Notification>()
                .Include(n => n.Users)
                .FirstOrDefaultAsync(n => n.NotificationId == notificationId);
        }

        public async Task<List<Notification>> GetNotificationsByTypeAsync(string type)
        {
            return await Context
                .Set<Notification>()
                .Include(n => n.Users)
                .Where(n => n.Type.Equals(type, StringComparison.OrdinalIgnoreCase))
                .ToListAsync();
        }

        public async Task<List<Notification>> GetNotificationsByUserIdAsync(string userId)
        {
            return await Context
                .Set<Notification>()
                .Include(n => n.Users)
                .Where(n => n.Users.Any(u => u.Id.Equals(userId)))
                .ToListAsync();
        }

        public async Task<List<Notification>> GetUnreadNotificationsAsync()
        {
            return await Context.Set<Notification>().Where(n => !n.IsRead).ToListAsync();
        }

        public async Task<List<Notification>> GetNotificationsByUserInvolvementAsync(string userId)
        {
            return await Context
                .Set<Notification>()
                .Where(n =>
                    n.Users.Any(u => u.Id.Equals(userId)) || n.CreatedByUserId.Equals(userId)
                )
                .ToListAsync();
        }

        public async Task<List<Notification>> SearchNotificationsAsync(
            string searchTerm,
            DateTime? startDate,
            DateTime? endDate
        )
        {
            var query = Context.Set<Notification>().AsQueryable();

            if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                query = query.Where(n => n.Message.Contains(searchTerm));
            }

            if (startDate.HasValue)
            {
                query = query.Where(n => n.CreatedAt >= startDate.Value);
            }

            if (endDate.HasValue)
            {
                query = query.Where(n => n.CreatedAt <= endDate.Value);
            }

            return await query.ToListAsync();
        }

        public async Task UpdateNotificationAsync(Notification notification)
        {
            // Attach the updated entity to the context
            Context.Attach(notification);

            // Get the entry for the existing entity
            var entry = Context.Entry(notification);

            // Specify the properties you want to skip updating
            var propertiesToSkip = new HashSet<string>
            {
                nameof(Notification.NotificationId),
                nameof(Notification.CreatedAt),
                nameof(Notification.CreatedByUserId),
                nameof(Notification.Type),
                nameof(Notification.Message),
                nameof(Notification.TargetUrl),
            };

            // Iterate through all properties and skip the ones you don't want to update
            foreach (var property in entry.Properties)
            {
                if (propertiesToSkip.Contains(property.Metadata.Name))
                {
                    property.IsModified = false;
                }
                else
                {
                    property.IsModified = true;
                }
            }
            await Context.SaveChangesAsync();
        }
    }
}
