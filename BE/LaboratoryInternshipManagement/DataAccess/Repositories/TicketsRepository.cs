﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using static LaboratoryInternshipManagement.Controllers.TicketsController;
using static LaboratoryInternshipManagement.Models.Enums.ConstantEnum;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class TicketsRepository : BaseRepository<Tickets>, ITicketsRepository
    {
        public TicketsRepository(LabInternManagementDBcontext context)
            : base(context) { }

        public int getNumIsInAllowedLimit(string InternId)
        {
            var numAllowedIntern = Context
                .Attendances.Where(x =>
                    string.Equals(x.InternId, InternId) && x.IsInAllowedLimit == true
                )
                .Count();
            return numAllowedIntern;
        }

        public bool AddTicketAttendance(
            string CreateId,
            string AssignId,
            string DateRequire,
            string Title,
            string Description
        )
        {
            Context.Tickets.Add(
                new Tickets
                {
                    CreateId = CreateId,
                    AssignId = AssignId,
                    Description = Description,
                    Status = (byte)TicketStatus.PENDING,
                    Title = Title,
                    DateRequire = DateTime.Parse(DateRequire),
                    Type = 1,
                }
            );
            var result = Context.SaveChanges();
            return result > 0 ? true : false;
        }

        public List<GetAllTicketByCreatorOutput> GetAllTicketByAssignee(
            GetTicketsInput getTicketsInput
        )
        {
            var lstTickets = new List<GetAllTicketByCreatorOutput>();
            var query = Context
                .Tickets.Where(x => string.Equals(x.AssignId, getTicketsInput.Assignee))
                .Include(x => x.Assignee);
            var result =
                from tickets in query
                select new GetAllTicketByCreatorOutput
                {
                    NameCreator = tickets.Creator.FirstName + " " + tickets.Creator.LastName,
                    NameAssignee = tickets.Assignee.FirstName + " " + tickets.Assignee.LastName,
                    Id = tickets.Id,
                    DateRequire = tickets.DateRequire,
                    Title = tickets.Title,
                    Description = tickets.Description,
                    AssignId = tickets.AssignId,
                    CreateId = tickets.CreateId,
                    Status = tickets.Status,
                    TicketComments = tickets.TicketComments,
                    Type = tickets.Type,
                };
            result = result.Where(x =>
                (
                    getTicketsInput.Keyword.IsNullOrEmpty()
                        ? true
                        : x.Title.ToLower().Contains(getTicketsInput.Keyword.ToLower())
                ) && (getTicketsInput.Status == 0 ? true : x.Status == getTicketsInput.Status)
            );
            lstTickets.AddRange(result);
            return lstTickets;
        }

        public List<GetAllTicketByCreatorOutput> GetAllTicketByCreator(string Creator)
        {
            var lstTickets = new List<GetAllTicketByCreatorOutput>();
            var query = Context
                .Tickets.Where(x => string.Equals(x.CreateId, Creator))
                .Include(x => x.Creator);
            var result =
                from tickets in query
                select new GetAllTicketByCreatorOutput
                {
                    NameCreator = tickets.Creator.FirstName + " " + tickets.Creator.LastName,
                    NameAssignee = tickets.Assignee.FirstName + " " + tickets.Assignee.LastName,
                    Id = tickets.Id,
                    DateRequire = tickets.DateRequire,
                    Title = tickets.Title,
                    Description = tickets.Description,
                    AssignId = tickets.AssignId,
                    CreateId = tickets.CreateId,
                    Status = tickets.Status,
                    TicketComments = tickets.TicketComments,
                    Type = tickets.Type,
                };
            lstTickets.AddRange(result);
            return lstTickets;
        }

        public GetAllTicketByCreatorOutput GetTicketById(long Id)
        {
            var query = Context
                .Tickets.Where(x => x.Id == Id)
                .Include(x => x.Creator)
                .Include(x => x.TicketComments);
            if (query != null)
            {
                var result =
                    from tickets in query
                    select new GetAllTicketByCreatorOutput
                    {
                        NameCreator = tickets.Creator.FirstName + " " + tickets.Creator.LastName,
                        NameAssignee = tickets.Assignee.FirstName + " " + tickets.Assignee.LastName,
                        Id = tickets.Id,
                        DateRequire = tickets.DateRequire,
                        Title = tickets.Title,
                        Description = tickets.Description,
                        AssignId = tickets.AssignId,
                        CreateId = tickets.CreateId,
                        Status = tickets.Status,
                        TicketComments = tickets.TicketComments,
                        Type = tickets.Type,
                    };
                return result.FirstOrDefault();
            }
            return new GetAllTicketByCreatorOutput();
        }

        public int UpdateTicketById(Tickets tickets)
        {
            if (tickets != null)
            {
                if (tickets.Id != null)
                {
                    var recordUpdate = GetTicketById(tickets.Id);
                    if (recordUpdate != null)
                    {
                        recordUpdate.Title = tickets.Title;
                        recordUpdate.Description = tickets.Description;
                        recordUpdate.DateRequire = tickets.DateRequire;
                        recordUpdate.Status = tickets.Status;
                        Context.Tickets.Update(recordUpdate);
                    }
                }
            }
            return Context.SaveChanges();
        }

        public class GetAllTicketByCreatorOutput : Tickets
        {
            public string NameCreator { get; set; }
            public string NameAssignee { get; set; }
        }
    }
}
