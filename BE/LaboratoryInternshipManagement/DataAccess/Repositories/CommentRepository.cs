﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using Microsoft.EntityFrameworkCore;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class CommentRepository : BaseRepository<Comment>, ICommentRepository
    {
        private readonly ILogger<CommentRepository> _logger;

        public CommentRepository(
            LabInternManagementDBcontext context,
            ILogger<CommentRepository> logger
        )
            : base(context)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task AddCommentAsync(Comment comment)
        {
            if (comment == null)
            {
                throw new ArgumentNullException(nameof(comment));
            }

            await Context.Set<Comment>().AddAsync(comment);
            await Context.SaveChangesAsync();
        }

        public async Task DeleteCommentAsync(int commentId)
        {
            var comment = await Context
                .Set<Comment>()
                .Include(c => c.Replies) // Include replies to handle them
                .Include(c => c.ParentComment) // Include parent comment to handle removal from parent
                .FirstOrDefaultAsync(c => c.CommentId == commentId);

            if (comment != null)
            {
                // If the comment is a reply, remove it from its parent's replies collection
                if (comment.ParentComment != null)
                {
                    comment.ParentComment.Replies.Remove(comment);
                }

                // Recursively delete all replies
                foreach (var reply in comment.Replies.ToList())
                {
                    // Recursive deletion for each reply
                    await DeleteCommentAsync(reply.CommentId);
                }

                // Remove the comment itself
                Context.Set<Comment>().Remove(comment);

                await Context.SaveChangesAsync();
            }
            else
            {
                // Optionally, handle the case where the comment is not found
                throw new InvalidOperationException("Comment not found.");
            }
        }

        public async Task<List<Comment>> GetAllCommentsAsync()
        {
            return await Context.Set<Comment>().Include(c => c.CreateBy).ToListAsync();
        }

        public async Task<Comment> GetCommentByCommentIdAsync(int commentId)
        {
            return await Context
                .Set<Comment>()
                .Include(c => c.CreateBy)
                .SingleOrDefaultAsync(c => c.CommentId == commentId);
        }

        public async Task<List<Comment>> GetCommentsByUserIdAsync(string userId)
        {
            return await Context.Set<Comment>().Where(c => c.CreateBy.Equals(userId)).ToListAsync();
        }

        public async Task<List<Comment>> SearchCommentsAsync(string searchTerm)
        {
            var query = Context.Set<Comment>().AsQueryable();

            DateTime? startDate = null;
            DateTime? endDate = null;

            // Attempt to parse searchTerm as DateTime to determine date range filter
            if (DateTime.TryParse(searchTerm, out DateTime parsedDate))
            {
                startDate = parsedDate.Date;
                endDate = parsedDate.Date.AddDays(1).AddTicks(-1); // End of the day
            }

            // Apply filters based on the determined type
            if (startDate.HasValue)
            {
                query = query.Where(c => c.CreatedAt >= startDate.Value);
            }
            else if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                query = query.Where(c =>
                    c.Content.Contains(searchTerm)
                    || c.CreateBy.FirstName.Contains(searchTerm)
                    || c.CreateBy.LastName.Contains(searchTerm)
                );
            }

            if (endDate.HasValue)
            {
                query = query.Where(c => c.CreatedAt <= endDate.Value);
            }

            return await query.ToListAsync();
        }

        public async Task UpdateCommentAsync(Comment comment)
        {
            Context.Attach(comment);

            // Get the entry for the existing entity
            var entry = Context.Entry(comment);
            // Specify the properties you want to skip updating
            var propertiesToSkip = new HashSet<string>
            {
                nameof(Comment.CreatedAt),
                nameof(Comment.CreateBy),
                nameof(Comment.CommentId),
                nameof(Comment.ParentCommentId),
                nameof(Comment.ReletedObjectId),
                nameof(Comment.ReletedObjectType),
            };

            // Iterate through all properties and skip the ones you don't want to update
            foreach (var property in entry.Properties)
            {
                if (propertiesToSkip.Contains(property.Metadata.Name))
                {
                    property.IsModified = false;
                }
                else
                {
                    property.IsModified = true;
                }
            }
            _logger.LogInformation("Saving changes to the database.");
            await Context.SaveChangesAsync();
            _logger.LogInformation("Changes saved successfully.");
        }
    }
}
