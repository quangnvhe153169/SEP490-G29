﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using Microsoft.EntityFrameworkCore;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class EventRepository : BaseRepository<Event>, IEventRepository
    {
        private readonly ILogger<EventRepository> _logger;

        public EventRepository(
            LabInternManagementDBcontext context,
            ILogger<EventRepository> logger
        )
            : base(context)
        {
            _logger = logger;
        }

        public async Task AddEvent(Event @event)
        {
            if (@event == null)
            {
                throw new ArgumentNullException(nameof(@event));
            }

            Context.Set<Event>().Add(@event);
            Context.SaveChanges();
        }

        public async Task DeleteEvent(int eventId)
        {
            var @event = Context.Set<Event>().Find(eventId);
            if (@event != null)
            {
                Context.Set<Event>().Remove(@event);
                Context.SaveChanges();
            }
            // Optionally, throw an exception or handle if event is not found
        }

        public async Task<List<Event>> GetAllEvent()
        {
            return await Context.Set<Event>().Include(e => e.EventAttendees).ToListAsync();
        }

        public async Task<Event> GetEventByEventId(int eventId)
        {
            return await Context
                .Set<Event>()
                .Include(e => e.EventAttendees)
                .FirstOrDefaultAsync(e => e.EventId == eventId);
        }

        public async Task<List<Event>> GetEventByLocation(string location)
        {
            return await Context.Set<Event>().Where(e => e.Location == location).ToListAsync();
        }

        public async Task<List<Event>> GetEventByUserId(string userId)
        {
            return await Context.Set<Event>().Where(e => e.UserId.Equals(userId)).ToListAsync();
        }

        public async Task<List<Event>> SearchEvent(string searchTerm)
        {
            try
            {
                // Log the incoming searchTerm for debugging
                var query = Context.Set<Event>().AsQueryable();
                if (string.IsNullOrEmpty(searchTerm?.Trim().Trim('\"')))
                {
                    return await query.ToListAsync();
                }

                DateTime? startDate = null;
                DateTime? endDate = null;

                // Attempt to parse searchTerm as DateTime to determine date range filter
                if (DateTime.TryParse(searchTerm, out DateTime parsedDate))
                {
                    startDate = parsedDate.Date;
                    endDate = parsedDate.Date.AddDays(1).AddTicks(-1); // End of the day
                }

                // Apply filters based on the determined type
                if (!string.IsNullOrWhiteSpace(searchTerm))
                {
                    // Filter by searchTerm if it's not a date
                    query = query.Where(o =>
                        o.Title.Contains(searchTerm) || o.Description.Contains(searchTerm)
                    );
                }
                else if (startDate.HasValue)
                {
                    query = query.Where(o => o.StartDate >= startDate.Value);
                }
                else if (endDate.HasValue)
                {
                    query = query.Where(o => o.EndDate <= endDate.Value);
                }

                var results = await query.ToListAsync();

                return results;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred while searching events: {ex}");
                throw;
            }
        }

        public async Task UpdateEvent(Event @event)
        {
            // Attach the updated entity to the context
            Context.Attach(@event);

            // Get the entry for the existing entity
            var entry = Context.Entry(@event);

            // Specify the properties you want to skip updating
            var propertiesToSkip = new HashSet<string>
            {
                nameof(Event.EventId),
                nameof(Event.CreatedAt),
                nameof(Event.UserId)
            };

            // Iterate through all properties and skip the ones you don't want to update
            foreach (var property in entry.Properties)
            {
                if (propertiesToSkip.Contains(property.Metadata.Name))
                {
                    property.IsModified = false;
                }
                else
                {
                    property.IsModified = true;
                }
            }
            await Context.SaveChangesAsync();
        }
    }
}
