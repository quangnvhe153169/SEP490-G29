﻿using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using CsvHelper;
using LaboratoryInternshipManagement.DataAccess.IRepository;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class ImportCSVRepository : IImportCSVRepository
    {
        //private readonly IMongoDatabase _context;
        public ImportCSVRepository()
        {
            //_context = context;
        }

        public IEnumerable<T> ReadCSV<T>(Stream file)
        {
            var reader = new StreamReader(file);
            var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
            var records = csv.GetRecords<T>();
            return records;
        }

        private static Random random = new Random();

        //public ImportIntern GetInternByEmail(string email)
        //{
        //    var collection = _context.GetCollection<ImportIntern>("Intern");
        //    var filter = Builders<ImportIntern>.Filter.Eq(i => i.Email, email);
        //    return collection.Find(filter).FirstOrDefault();
        //}
        public bool IsValidEmail(string Email)
        {
            const string emailRegex = @"^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*\.(\w{2,})$";
            return Regex.IsMatch(Email, emailRegex, RegexOptions.IgnoreCase);
        }

        public string GenerateRandomString(int length = 8)
        {
            const string lower = "abcdefghijklmnopqrstuvwxyz";
            const string upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string digits = "0123456789";
            const string special = "@$!%*?&";

            if (length < 8)
                throw new ArgumentException("Password length must be at least 8 characters.");

            StringBuilder password = new StringBuilder();

            // Add at least one character from each required set
            password.Append(lower[random.Next(lower.Length)]);
            password.Append(upper[random.Next(upper.Length)]);
            password.Append(digits[random.Next(digits.Length)]);
            password.Append(special[random.Next(special.Length)]);

            // Fill the rest of the password length with a mix of all allowed characters
            string allChars = lower + upper + digits + special;
            for (int i = 4; i < length; i++)
            {
                password.Append(allChars[random.Next(allChars.Length)]);
            }

            // Shuffle the characters in the password to avoid predictable patterns
            return Shuffle(password.ToString());
        }

        private static string Shuffle(string str)
        {
            char[] array = str.ToCharArray();
            int n = array.Length;
            for (int i = n - 1; i > 0; i--)
            {
                int j = random.Next(i + 1);
                char temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
            return new string(array);
        }

        public string ComputeSha256Hash(string rawData)
        {
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }
    }
}
