﻿using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using Microsoft.EntityFrameworkCore;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class ImportHistoryRepository : IImportHistoryRepository<ImportHistory>
    {
        private readonly LabInternManagementDBcontext _dbContext;
        public ImportHistoryRepository(LabInternManagementDBcontext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<ImportHistory> AddImportHistory(ImportHistory entity)
        {
            await _dbContext.ImportHistories.AddAsync(entity);
            await _dbContext.SaveChangesAsync();
            return entity;
        }
        public async Task<IEnumerable<ImportHistory>> GetAllImportHistory()
        {
            return await _dbContext.ImportHistories.ToListAsync();
        }
    }
}
