﻿using LaboratoryInternshipManagement.Controllers;
using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using LaboratoryInternshipManagement.Models.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class AttendenceRepository : BaseRepository<Attendance>, IAttendenceRepository
    {
        private readonly UserManager<User> _userManager;

        public AttendenceRepository(
            LabInternManagementDBcontext context,
            UserManager<User> userManager
        )
            : base(context)
        {
            _userManager = userManager;
        }

        public Attendance GetByInternId(string internId)
        {
            var attendenUser = Context
                .Set<Attendance>()
                .Where(x => x.InternId.Equals(internId))
                .FirstOrDefault();
            return attendenUser != null ? attendenUser : new Attendance();
        }

        public List<Attendance> ListAllAttendenceInLab(long lapID)
        {
            var userInLapAttendence =
                from user in Context.Set<LaboratoryInternshipManagement.Models.User>()
                join attdence in Context.Attendances on user.Id equals attdence.InternId
                where user.LabId == lapID
                select attdence;
            return userInLapAttendence != null
                ? userInLapAttendence.ToList()
                : new List<Attendance>();
        }

        public List<Attendance> ListAttendence()
        {
            return Context.Set<Attendance>().ToList();
        }

        public List<Attendance> ListAttendanceByDay(string date)
        {
            var dateData = DateTime.Parse(date);
            var listAttendance =
                from attendence in Context.Attendances
                where attendence.Date.CompareTo(dateData) == 0
                select attendence;
            return listAttendance.ToList().Count > 0
                ? listAttendance.ToList()
                : new List<Attendance>();
        }

        public List<ListAttendanceOutputDto> ListAttendanceByRoleItern(string InternId, int month)
        {
            var inforUser = Context
                .Users.Where(x => string.Equals(x.Id, InternId))
                .FirstOrDefault();

            var lstAttendance = Context
                .Attendances.Where(a =>
                    string.Equals(a.InternId, InternId) && a.Date.Date < DateTime.Now.Date
                )
                .Select(x => new OutputAttendance
                {
                    Id = x.Id,
                    CheckInTime = x.CheckInTime,
                    CheckOutTime = x.CheckOutTime,
                    Date = x.Date,
                    IsInAllowedLimit = x.IsInAllowedLimit,
                    IsSuspense = x.IsSuspense,
                    Reason = x.Reason,
                    Status = x.Status,
                })
                .ToList();

            // Tạo đối tượng ListAttendanceByLabManager với danh sách attendance
            var result = new List<ListAttendanceOutputDto>
            {
                new ListAttendanceOutputDto
                {
                    InternId = InternId,
                    InternName = string.Concat(inforUser.FirstName, " ", inforUser.LastName),
                    InternEmail = inforUser.Email,
                    RollName = inforUser.RollName,
                    attendances = lstAttendance
                        .Where(x => (0 < month && month <= 12) ? x.Date.Month == month : false)
                        .ToList(),
                }
            };

            return result.ToList().Count > 0
                ? result.ToList()
                : new List<ListAttendanceOutputDto>();
        }

        public List<ListAttendanceOutputDto> ListAttendanceByRoleLab(
            ListAttendanceByRoleLabInputDto inputDto
        )
        {
            var query = Context
                .Users.Include(a => a.Attendances)
                .Where(b =>
                    b.Attendances.Any(c => c.Date.Date < DateTime.Now.Date)
                    && b.LabId == inputDto.LabId
                    && (
                        !string.IsNullOrEmpty(inputDto.RollName)
                            ? b.RollName.ToLower().Contains(inputDto.RollName.ToLower())
                            : true
                    )
                    && (
                        !string.IsNullOrEmpty(inputDto.Email)
                            ? b.Email.ToLower().Contains(inputDto.Email.ToLower())
                            : true
                    )
                    && (
                        (
                            !string.IsNullOrEmpty(inputDto.Name)
                                ? (
                                    (string.Concat(b.FirstName, b.LastName))
                                        .ToLower()
                                        .Contains(inputDto.Name.ToLower())
                                )
                                : true
                        )
                    )
                );
            var lstAttendance = query.Select(a => new ListAttendanceOutputDto
            {
                InternId = a.Id,
                InternName = string.Concat(a.FirstName, " ", a.LastName).Trim(),
                InternEmail = a.Email,
                RollName = a.RollName,
                attendances = a
                    .Attendances.Where(x =>
                        (0 < inputDto.Month && inputDto.Month <= 12)
                            ? x.Date.Month == inputDto.Month
                            : false
                    )
                    .Select(x => new OutputAttendance
                    {
                        Id = x.Id,
                        CheckInTime = x.CheckInTime,
                        CheckOutTime = x.CheckOutTime,
                        Date = x.Date,
                        IsInAllowedLimit = x.IsInAllowedLimit,
                        IsSuspense = x.IsSuspense,
                        Reason = x.Reason,
                        Status = x.Status,
                    })
                    .ToList(),
            });

            return lstAttendance.ToList().Count > 0
                ? lstAttendance.ToList()
                : new List<ListAttendanceOutputDto>();
        }

        public Attendance UpdateStatusAttendance(UpdateStatusAttendanceInputDto inputDto)
        {
            var query = Context
                .Attendances.Where(x => x.Id == inputDto.AttendanceId)
                .FirstOrDefault();
            var laboratory = Context
                .Laboratories.Where(x => string.Equals(x.LabManagerId, inputDto.LabManagerId))
                .FirstOrDefault();
            var attendanceUpdate = new Attendance();
            if (query != null && laboratory != null)
            {
                if (inputDto.CheckInTime == null || inputDto.CheckOutTime == null)
                {
                    query.Status = 4;
                }
                else
                {
                    var checkInTime = TimeSpan.Parse(inputDto.CheckInTime);
                    var checkOutTime = TimeSpan.Parse(inputDto.CheckOutTime);
                    if (
                        checkInTime > checkOutTime
                        || checkInTime < laboratory.CheckInTime
                        || checkOutTime < laboratory.CheckOutTime
                    )
                    {
                        throw new Exception("invalidTime");
                    }
                    query.CheckInTime = checkInTime;
                    query.CheckOutTime = checkOutTime;
                    query.Reason = "Lab " + inputDto.LabManagerId + " Edit";
                    query.Status = 1;
                    if (query.CheckInTime > laboratory.CheckInTime.Add(new TimeSpan(0, 30, 0)))
                    {
                        query.Status = 2;
                    }
                    if (query.CheckOutTime < laboratory.CheckOutTime)
                    {
                        query.Status = 3;
                    }
                }
                attendanceUpdate = new Attendance
                {
                    Id = query.Id,
                    CheckInTime = query.CheckInTime,
                    CheckOutTime = query.CheckOutTime,
                    Date = query.Date,
                    Reason = query.Reason,
                    Status = query.Status,
                };
            }
            Context.SaveChanges();
            return attendanceUpdate != null ? attendanceUpdate : new Attendance();
        }

        public bool UpdateTimeLaboratoryAttendance(UpdateTimeLaboratoryAttendanceInput laboratory)
        {
            var labInfor = Context
                .Laboratories.Where(x => x.Id == laboratory.LabId)
                .FirstOrDefault();
            if (labInfor != null)
            {
                labInfor.LeaveRequestNum = laboratory.LeaveRequestNum;
                labInfor.CheckInTime = laboratory.CheckInTime;
                labInfor.CheckOutTime = laboratory.CheckOutTime;
            }
            var result = Context.SaveChanges();
            return result > 0 ? true : false;
        }

        public async Task<dynamic> GetListAttendance(
            string userId,
            InputGetAttendanceLabManager inputDto,
            int labId
        )
        {
            var dateEndDefault = DateTime.Now.Date;
            var dateStartDefault = dateEndDefault.AddDays(-7).Date;

            DateTime? startDate = dateStartDefault;
            if (DateTime.TryParse(inputDto.StartDate, out DateTime parsedStartDate))
            {
                startDate = parsedStartDate;
            }

            DateTime? endDate = dateEndDefault;

            if (DateTime.TryParse(inputDto.EndDate, out DateTime parsedEndDate))
            {
                endDate = parsedEndDate;
            }

            var user = await _userManager.FindByIdAsync(userId);
            var isLabManager = await _userManager.IsInRoleAsync(user, "Lab Manager");

            var query = Context
                .Users.Include(u => u.Attendances)
                .Where(u => u.LabId == labId && u.Attendances.Any());

            if (!isLabManager)
            {
                query = query.Where(u => u.MentorId.Equals(user.Id));
            }
            if (!string.IsNullOrEmpty(inputDto.Keyword))
            {
                var keyword = inputDto.Keyword.ToLower().Trim();
                query = query.Where(u =>
                    u.FirstName.ToLower().Contains(keyword)
                    || u.LastName.ToLower().Contains(keyword)
                    || u.Email.ToLower().Contains(keyword)
                    || u.RollName.ToLower().Contains(keyword)
                );
            }

            var result = await GetAttendanceFilters(query, inputDto.PageNumber, startDate, endDate);

            return result;
        }

        public async Task<dynamic> GetListAttendanceIntern(
            User user,
            InputGetAttendanceLabManager inputDto
        )
        {
            DateTime? startDate = null;
            if (DateTime.TryParse(inputDto.StartDate, out DateTime parsedStartDate))
            {
                startDate = parsedStartDate;
            }

            DateTime? endDate = null;

            if (DateTime.TryParse(inputDto.EndDate, out DateTime parsedEndDate))
            {
                endDate = parsedEndDate;
            }

            var dateEndDefault = DateTime.Now.Date;
            var dateStartDefault = dateEndDefault.AddDays(-7).Date;

            var roles = await _userManager.GetRolesAsync(user);
            var lstAttendance = await Context
                .Attendances.Where(a =>
                    a.InternId.Equals(user.Id)
                    && (startDate != null ? a.Date.Date >= startDate.Value.Date : true)
                    && (endDate != null ? a.Date.Date <= endDate.Value.Date : true)
                )
                .OrderByDescending(a => a.Date)
                .ToListAsync();
            var settingOutputDto = lstAttendance
                .Select(x => new OutputAttendance
                {
                    CheckInTime = x.CheckInTime,
                    CheckOutTime = x.CheckOutTime,
                    Date = x.Date,
                    Id = x.Id,
                    Reason = x.Reason,
                    Status = x.Status,
                })
                .ToList();

            var listAttendanceOutputDto = new List<ListAttendanceOutputDto>
            {
                new ListAttendanceOutputDto
                {
                    InternId = user.Id,
                    InternEmail = user.Email,
                    RollName = user.RollName,
                    InternName = user.FirstName + " " + user.LastName,
                    attendances = settingOutputDto
                }
            };

            return new { LstAttendance = listAttendanceOutputDto };
        }

        private async Task<dynamic> GetAttendanceFilters(
            IQueryable<User> query,
            int pageNum,
            DateTime? startDate,
            DateTime? endDate
        )
        {
            var lstUserAttendance = await query
                .Where(x =>
                    x.Attendances.Any(a =>
                        (startDate != null ? a.Date.Date >= startDate.Value.Date : true)
                        && (endDate != null ? a.Date.Date <= endDate.Value.Date : true)
                    )
                )
                .ToPagedListAsync(pageNum, (int)ConstantEnum.pagging.pagging5Size);

            var lstAttendance = lstUserAttendance.Select(x => new
            {
                InternId = x.Id,
                InternEmail = x.Email,
                RollName = x.RollName,
                InternName = x.FirstName + " " + x.LastName,
                Attendances = x
                    .Attendances.Where(a =>
                        (startDate != null ? a.Date.Date >= startDate.Value.Date : true)
                        && (endDate != null ? a.Date.Date <= endDate.Value.Date : true)
                    )
                    .OrderByDescending(a => a.Date)
                    .Select(a => new
                    {
                        Id = a.Id,
                        Status = a.Status,
                        CheckInTime = a.CheckInTime.HasValue
                            ? a.CheckInTime.Value.ToString(@"hh\:mm")
                            : "",
                        CheckOutTime = a.CheckOutTime.HasValue
                            ? a.CheckOutTime.Value.ToString(@"hh\:mm")
                            : "",
                        Reason = a.Reason,
                        IsInAllowedLimit = a.IsInAllowedLimit,
                        IsSuspense = a.IsSuspense,
                        Date = a.Date.ToString("dd/MM/yyyy")
                    }),
            });

            return (
                new
                {
                    TotalRecords = lstUserAttendance.TotalItemCount,
                    TotalPages = lstUserAttendance.PageCount,
                    PageNumber = lstUserAttendance.PageNumber,
                    PageSize = ConstantEnum.pagging.pagging5Size,
                    LstAttendance = lstAttendance
                }
            );
        }

        public class UpdateTimeLaboratoryAttendanceInput
        {
            public TimeSpan CheckInTime { get; set; }
            public TimeSpan CheckOutTime { get; set; }
            public int? LeaveRequestNum { get; set; }
            public int? LabId { get; set; }
        }

        public class UpdateTimeLaboratoryAttendanceInputDto
        {
            public string CheckInTime { get; set; }
            public string CheckOutTime { get; set; }
            public int? LeaveRequestNum { get; set; }
        }

        public class OutputAttendance
        {
            public int Id { get; set; }
            public int Status { get; set; }
            public TimeSpan? CheckInTime { get; set; }
            public TimeSpan? CheckOutTime { get; set; }
            public string? Reason { get; set; }
            public bool? IsInAllowedLimit { get; set; }
            public bool? IsSuspense { get; set; }
            public DateTime Date { get; set; }
        }

        public class ListAttendanceOutputDto
        {
            public string? InternId { get; set; }
            public string? InternName { get; set; }
            public string? InternEmail { get; set; }
            public string? RollName { get; set; }
            public List<OutputAttendance> attendances { get; set; }
        }

        public class ListAttendanceByRoleLabInputDto
        {
            public int LabId { get; set; }
            public string RollName { get; set; }
            public string Name { get; set; }
            public int Month { get; set; }
            public string Email { get; set; }
            public int PageNumber { get; set; }
        }

        public class UpdateStatusAttendanceInputDto
        {
            public int AttendanceId { get; set; }
            public string? LabManagerId { get; set; }
            public string? CheckInTime { get; set; }
            public string? CheckOutTime { get; set; }
        }
    }
}
