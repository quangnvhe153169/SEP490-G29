﻿using LaboratoryInternshipManagement.Controllers;
using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using LaboratoryInternshipManagement.Models.DTOs;
using LaboratoryInternshipManagement.Models.Enums;
using Microsoft.EntityFrameworkCore;
using NetTools.Extensions;
using X.PagedList;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class ReportRepository : BaseRepository<Report>, IReportRepository
    {
        public ReportRepository(LabInternManagementDBcontext context)
            : base(context) { }

        public void AddReport(ReportDTO r)
        {
            try
            {
                // Lấy thông tin học kỳ (semester)
                var semester = Context.Semesters.FirstOrDefault(s => s.SemesterId == r.SemesterId);
                if (semester == null)
                {
                    throw new Exception("Semester not found.");
                }

                // Xác định ngày hiện tại
                var currentDate = DateTime.Now;

                // Tính ngày cuối kỳ (endterm) và khoảng thời gian cho phép
                var threeDaysAfterEnd = semester.EndDate.AddDays(3);

                // Kiểm tra xem có nằm trong khoảng thời gian báo cáo giữa kỳ hay cuối kỳ không
                bool isEndOfSemesterReportPeriod = currentDate <= threeDaysAfterEnd;

                // Kiểm tra nếu ngày hiện tại không nằm trong bất kỳ khoảng thời gian hợp lệ nào
                if (!isEndOfSemesterReportPeriod)
                {
                    throw new InvalidOperationException(
                        "Reports can only be added before 3 days of the end of previous semester (End Date: "
                            + semester.EndDate.ToString("dd/MM/yyyy")
                            + ")."
                    );
                }

                // Kiểm tra số lượng báo cáo đã tồn tại cho kỳ giữa kỳ và cuối kỳ

                var reports = Context
                    .Reports.Where(report =>
                        report.MentorId == r.MentorId && report.InternId == r.InternId
                    )
                    .Select(x => x.ReportType)
                    .ToList();

                if (reports.Count >= 2)
                {
                    throw new InvalidOperationException(
                        "Can only add 2 reports per intern (Mid-term/End-term)."
                    );
                }

                // Kiểm tra xem StaffId đã tồn tại cho intern này chưa
                var firstReport = Context.Reports.FirstOrDefault(report =>
                    report.InternId == r.InternId
                );
                string staffId;
                if (firstReport != null)
                {
                    // Sử dụng StaffId đã tồn tại
                    staffId = firstReport.StaffId;
                }
                else
                {
                    // Tạo StaffId mới
                    staffId = GenerateUniqueStaffId(r.StaffId);
                }

                // Tạo báo cáo mới
                var newReport = new Report
                {
                    MentorId = r.MentorId,
                    InternId = r.InternId,
                    RollName = r.RollName,
                    NameIntern = r.NameIntern,
                    NameLab = r.NameLab,
                    NameMentor = r.NameMentor,
                    Allowance = r.Allowance,
                    ComOfCompany = r.ComOfCompany,
                    MajorSkill = r.MajorSkill,
                    SoftSkill = r.SoftSkill,
                    Attitude = r.Attitude,
                    StaffId = staffId,
                    SemesterId = r.SemesterId,
                    CreatedDate = DateTime.Now,
                };

                // Gắn loại báo cáo (midterm hoặc end-term) cho báo cáo mới (mặc định là mid-term)
                if (
                    reports.Count == 1
                    && reports.Any(r => r == (int)ConstantEnum.ReportType.MID_TERM)
                )
                {
                    newReport.ReportType = (int)ConstantEnum.ReportType.END_OF_TERM;
                }

                // Thêm báo cáo vào cơ sở dữ liệu
                Context.Reports.Add(newReport);
                Context.SaveChanges();
            }
            catch (DbUpdateException dbEx)
            {
                throw new Exception(dbEx.Message, dbEx);
            }
            catch (InvalidOperationException ex)
            {
                throw new Exception(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        private string GenerateUniqueStaffId(string email)
        {
            var baseStaffId = email.Split('@')[0];
            var staffId = baseStaffId;
            int counter = 1;

            while (Context.Reports.Any(r => r.StaffId == staffId))
            {
                staffId = $"{baseStaffId}{counter}";
                counter++;
            }

            return staffId;
        }

        public async Task<List<Report>> GetReportsBySemesterIdAndDateRange(
            int semesterId,
            DateTime startDate,
            DateTime endDate
        )
        {
            return await Context
                .Reports.Where(r =>
                    r.SemesterId == semesterId
                    && r.CreatedDate >= startDate
                    && r.CreatedDate <= endDate
                )
                .ToListAsync();
        }

        public async Task<List<Report>> GetFinalReportsBySemesterId(int semesterId)
        {
            return await Context
                .Reports.Where(r =>
                    r.SemesterId == semesterId
                    && r.ReportType == (int)ConstantEnum.ReportType.END_OF_TERM
                )
                .ToListAsync();
        }

        public async Task<List<Report>> GetMidTermReportBySemsterId(int semesterId)
        {
            return await Context
                .Reports.Where(r =>
                    r.SemesterId == semesterId
                    && r.ReportType == (int)ConstantEnum.ReportType.MID_TERM
                )
                .ToListAsync();
        }

        public async Task<List<Report>> GetLabFinalReportsBySemesterId(
            int semesterId,
            string labName
        )
        {
            return await Context
                .Reports.Where(r =>
                    r.SemesterId == semesterId
                    && r.NameLab.ToLower().Equals(labName.ToLower())
                    && r.ReportType == (int)ConstantEnum.ReportType.END_OF_TERM
                )
                .ToListAsync();
        }

        public async Task<List<Report>> GetLabMidTermReportBySemsterId(
            int semesterId,
            string labName
        )
        {
            return await Context
                .Reports.Where(r =>
                    r.SemesterId == semesterId
                    && r.NameLab.ToLower().Equals(labName.ToLower())
                    && r.ReportType == (int)ConstantEnum.ReportType.MID_TERM
                )
                .ToListAsync();
        }

        public Report GetReportByName(string name)
        {
            try
            {
                var report = Context.Reports.FirstOrDefault(r =>
                    r.NameIntern.Equals(name, StringComparison.OrdinalIgnoreCase)
                );
                if (report == null)
                {
                    throw new KeyNotFoundException("Report not found.");
                }
                return report;
            }
            catch (Exception ex)
            {
                throw new Exception(
                    "An unexpected error occurred. " + $"Details: {ex.Message}",
                    ex
                );
            }
        }

        public Report GetReportById(int id)
        {
            try
            {
                var report = Context.Reports.Find(id);
                if (report == null)
                {
                    throw new KeyNotFoundException("Report not found.");
                }
                return report;
            }
            catch (Exception ex)
            {
                throw new Exception(
                    "An unexpected error occurred. " + $"Details: {ex.Message}",
                    ex
                );
            }
        }

        public void UpdateReport(ReportDTO r)
        {
            try
            {
                // Retrieve the existing report based on StaffId
                var existingReport = Context.Reports.FirstOrDefault(report =>
                    report.StaffId == r.StaffId
                );
                if (existingReport == null)
                {
                    throw new KeyNotFoundException("Report not found.");
                }

                // Retrieve the semester details
                var semester = Context.Semesters.FirstOrDefault(s => s.SemesterId == r.SemesterId);
                if (semester == null)
                {
                    throw new Exception("Semester not found.");
                }

                var endDate = semester.EndDate;
                // Xác định ngày hiện tại
                var currentDate = DateTime.Now;

                // Tính ngày cuối kỳ (endterm) và khoảng thời gian cho phép
                var threeDaysAfterEnd = semester.EndDate.AddDays(3);

                // Kiểm tra xem có nằm trong khoảng thời gian báo cáo giữa kỳ hay cuối kỳ không
                bool isEndOfSemesterReportPeriod = currentDate <= threeDaysAfterEnd;

                // Kiểm tra nếu ngày hiện tại không nằm trong bất kỳ khoảng thời gian hợp lệ nào
                if (!isEndOfSemesterReportPeriod)
                {
                    throw new InvalidOperationException(
                        "Reports can only be added before 3 days of the end of previous semester (End Date: "
                            + semester.EndDate.ToString("dd/MM/yyyy")
                            + ")."
                    );
                }

                // Update only specific fields
                existingReport.Allowance = r.Allowance;
                existingReport.ComOfCompany = r.ComOfCompany;
                existingReport.MajorSkill = r.MajorSkill;
                existingReport.SoftSkill = r.SoftSkill;
                existingReport.Attitude = r.Attitude;
                existingReport.CreatedDate = DateTime.Now;

                // Mark the entity as modified and save changes
                Context.Reports.Update(existingReport);
                Context.SaveChanges();
            }
            catch (DbUpdateException dbEx)
            {
                throw new Exception(
                    "An error occurred while updating the report. "
                        + $"Details: {dbEx.InnerException?.Message ?? dbEx.Message}",
                    dbEx
                );
            }
            catch (InvalidOperationException ex)
            {
                throw new Exception("Failed to update report: " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception(
                    "An unexpected error occurred. " + $"Details: {ex.Message}",
                    ex
                );
            }
        }

        public async Task<string> GetMentorIdByUserIdAsync(string userId)
        {
            // Fetch the user by userId
            var user = await Context.Users.Where(u => u.Id == userId).FirstOrDefaultAsync();

            // Return the MentorId or handle case where user is not found
            return user?.MentorId;
        }

        public IEnumerable<Report> GetReportsByRollName(string rollname)
        {
            try
            {
                return Context.Reports.Where(r => r.RollName.Contains(rollname)).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(
                    "An unexpected error occurred. " + $"Details: {ex.Message}",
                    ex
                );
            }
        }

        public async Task<List<Report>> GetAllReports(string mentorId)
        {
            try
            {
                // Fetch reports where MentorId matches the given mentorId
                return await Context.Reports.Where(r => r.MentorId == mentorId).ToListAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Error fetching reports: " + ex.Message);
            }
        }

        public async Task<IPagedList<dynamic>> GetReportGroupByInterns(
            string mentorId,
            SearchReportDTO searchReport
        )
        {
            try
            {
                var mentor = await Context
                    .Users.Include(x => x.Interns)
                    .ThenInclude(u => u.Semester)
                    .Include(x => x.Reports)
                    .SingleOrDefaultAsync(x => x.Id.Equals(mentorId));

                var mentorName = (mentor.FirstName + " " + mentor.LastName).Trim();

                var keyword = !string.IsNullOrEmpty(searchReport.Keyword)
                    ? searchReport.Keyword.Trim()
                    : "";

                var internWithReports = await mentor
                    .Interns.Where(intern =>
                        mentor.Reports.Any(x => x.InternId == intern.Id)
                        // Use SearchReportDTO for filtering
                        && (
                            string.IsNullOrEmpty(keyword)
                            || intern.Email.Contains(keyword)
                            || intern.RollName.Contains(keyword)
                            || mentor.Reports.Any(x => x.StaffId.Contains(keyword))
                        )
                        && (
                            !searchReport.SemesterId.HasValue
                            || intern.SemesterId == searchReport.SemesterId
                        )
                    )
                    .Select(intern => new
                    {
                        InternId = intern.Id,
                        NameIntern = intern.FirstName + " " + intern.LastName,
                        RollName = intern.RollName,
                        SemesterId = intern.SemesterId,
                        Semester = new
                        {
                            Name = intern?.Semester.SemesterName,
                            StartDate = intern?.Semester.StartDate,
                            EndDate = intern?.Semester.EndDate
                        },
                        Email = intern.Email,
                        NameMentor = mentorName.IsNotNullOrEmpty()
                            ? mentorName
                            : mentor.Email.Split('@')[0],
                        Reports = mentor
                            .Reports.Where(x => x.InternId == intern.Id)
                            .Select(report => new
                            {
                                StaffId = report.StaffId,
                                SemesterId = report.SemesterId,
                                ReportId = report.ReportId,
                                ReportTitle = report.ReportType,
                                CreatedDate = report.CreatedDate,
                                ComOfCompany = report.ComOfCompany,
                                MajorSkill = report.MajorSkill,
                                Attitude = report.Attitude,
                                SoftSkill = report.SoftSkill,
                                FinalResult = report.FinalResult,
                                Allowance = report.Allowance
                            })
                            .ToList()
                    })
                    .Cast<dynamic>()
                    .ToPagedListAsync(searchReport.PageNumber, searchReport.PageSize);

                return internWithReports;
            }
            catch (Exception ex)
            {
                throw new Exception("Error fetching reports: " + ex.Message);
            }
        }

        public void DeleteReport(int reportId)
        {
            try
            {
                // Retrieve the existing report
                var existingReport = Context.Reports.Find(reportId);
                if (existingReport == null)
                {
                    throw new KeyNotFoundException("Report not found.");
                }

                // Retrieve the semester details
                var semester = Context.Semesters.FirstOrDefault(s =>
                    s.SemesterId == existingReport.SemesterId
                );
                if (semester == null)
                {
                    throw new Exception("Associated semester not found.");
                }
                // Xác định ngày hiện tại
                var currentDate = DateTime.Now;

                // Tính ngày cuối kỳ (endterm) và khoảng thời gian cho phép
                var threeDaysAfterEnd = semester.EndDate.AddDays(3);

                // Kiểm tra xem có nằm trong khoảng thời gian báo cáo giữa kỳ hay cuối kỳ không
                bool isEndOfSemesterReportPeriod = currentDate <= threeDaysAfterEnd;

                // Kiểm tra nếu ngày hiện tại không nằm trong bất kỳ khoảng thời gian hợp lệ nào
                if (!isEndOfSemesterReportPeriod)
                {
                    throw new InvalidOperationException(
                        "Reports can only be added before 3 days of the end of previous semester (End Date: "
                            + semester.EndDate.ToString("dd/MM/yyyy")
                            + ")."
                    );
                }

                // Remove the report
                Context.Reports.Remove(existingReport);
                Context.SaveChanges();
            }
            catch (DbUpdateException dbEx)
            {
                throw new Exception(
                    "An error occurred while deleting the report. "
                        + $"Details: {dbEx.InnerException?.Message ?? dbEx.Message}",
                    dbEx
                );
            }
            catch (InvalidOperationException ex)
            {
                throw new Exception("Failed to delete report: " + ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception($"Details: {ex.Message}", ex);
            }
        }

        public async Task<ReportDTO> GetReportByIdAsync(int reportId)
        {
            try
            {
                var report = await Context
                    .Reports.Where(r => r.ReportId == reportId)
                    .Select(r => new ReportDTO
                    {
                        ReportId = r.ReportId,
                        MentorId = r.MentorId,
                        RollName = r.RollName,
                        InternId = r.InternId,
                        NameIntern = r.NameIntern,
                        NameLab = r.NameLab,
                        NameMentor = r.NameMentor,
                        Allowance = r.Allowance,
                        ComOfCompany = r.ComOfCompany,
                        MajorSkill = r.MajorSkill,
                        SoftSkill = r.SoftSkill,
                        Attitude = r.Attitude,
                        StaffId = GenerateUniqueStaffId(r.StaffId),
                        SemesterId = r.SemesterId,
                        CreatedDate = DateTime.Now,
                        ReportType = r.ReportType
                    })
                    .FirstOrDefaultAsync();

                if (report == null)
                {
                    throw new KeyNotFoundException("Report not found.");
                }

                return report;
            }
            catch (Exception ex)
            {
                throw new Exception(
                    "An unexpected error occurred. " + $"Details: {ex.Message}",
                    ex
                );
            }
        }
    }
}
