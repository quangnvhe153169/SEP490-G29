﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using Microsoft.EntityFrameworkCore;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class OKRLogRepository : BaseRepository<OKRLog>, IOKRLogRepository
    {
        public OKRLogRepository(LabInternManagementDBcontext context) : base(context)
        {
        }

        public async Task AddOKRLog(OKRLog okrLog)
        {
            if (okrLog == null) { throw new ArgumentNullException(nameof(okrLog)); }
            Context.Set<OKRLog>().Add(okrLog);
            Context.SaveChanges();
        }

        public async Task<List<OKRLog>> GetAllOKRLog()
        {
            return Context.Set<OKRLog>().ToList();
        }

        public async Task<List<OKRLog>> GetOKRLogByTeamId(int teamId)
        {
            // Assuming you have a method to get objectives by teamId
            var objectiveIds = await Context.Objectives
                .Where(o => o.TeamId == teamId)
                .Select(o => o.ObjectiveId)
                .ToListAsync();

            // Fetch OKR logs related to those objectives
            return await Context.OKRLogs
                .Include(log => log.User) // Include User details
                .Where(log => log.ObjectiveId.HasValue && objectiveIds.Contains(log.ObjectiveId.Value))
                .ToListAsync();
        }

        public async Task<List<OKRLog>> GetOKRLogByUserId(string userId)
        {
            return Context.OKRLogs
                .Include(log => log.User)
                .Where(log => log.UserId == userId)
                .ToList();
        }

    }
}
