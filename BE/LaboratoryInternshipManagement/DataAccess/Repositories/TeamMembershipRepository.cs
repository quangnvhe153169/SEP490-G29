﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using Microsoft.EntityFrameworkCore;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class TeamMembershipRepository
        : BaseRepository<TeamMembership>,
            ITeamMembershipRepository
    {
        private readonly ILogger<TeamMembershipRepository> _logger;

        public TeamMembershipRepository(
            LabInternManagementDBcontext context,
            ILogger<TeamMembershipRepository> logger
        )
            : base(context)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task AddTeamMembership(TeamMembership teamMembership)
        {
            if (teamMembership == null)
            {
                throw new ArgumentNullException(nameof(teamMembership));
            }
            Context.Set<TeamMembership>().Add(teamMembership);
            await Context.SaveChangesAsync();
        }

        public async Task DeleteTeamMembership(int teamId, string userId)
        {
            var teamMembership = await Context
                .Set<TeamMembership>()
                .Include(tm => tm.Team)
                .SingleOrDefaultAsync(tm => tm.TeamId == teamId && tm.UserId.Equals(userId));
            if (teamMembership != null)
            {
                Context.Set<TeamMembership>().Remove(teamMembership);
                await Context.SaveChangesAsync(); // Save changes to persist the deletion
            }
            // Optionally, throw an exception or handle if teamMembership is not found
        }

        public async Task<List<TeamMembership>> GetAllTeamMembership()
        {
            return await Context
                .Set<TeamMembership>()
                .Include(tm => tm.Team)
                .Include(tm => tm.User)
                .ToListAsync();
        }

        public async Task<List<TeamMembership>> GetTeamMembershipsByUserId(string userId)
        {
            return await Context
                .Set<TeamMembership>()
                .Include(tm => tm.Team)
                .Where(tm => tm.UserId.Equals(userId))
                .ToListAsync();
        }

        public async Task<List<TeamMembership>> GetTeamMembershipByTeamId_UserId(
            int teamId,
            string userId
        )
        {
            try
            {
                var memberships = await Context
                    .Set<TeamMembership>()
                    .Where(tm => tm.TeamId == teamId && tm.UserId.Equals(userId))
                    .ToListAsync();
                return memberships;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error fetching team memberships: {ex}");
                throw;
            }
        }

        public async Task<List<TeamMembership>> SearchTeamMembership(string searchTerm)
        {
            var query = Context.Set<TeamMembership>().Include(tm => tm.Team).AsQueryable();

            if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                query = query.Where(tm =>
                    tm.User.FirstName.Contains(searchTerm)
                    || tm.User.LastName.Contains(searchTerm)
                    || tm.Team.Name.Contains(searchTerm)
                );
            }
            return await query.ToListAsync();
        }

        public async Task UpdateTeamMembership(TeamMembership teamMembership)
        {
            // Assuming teamMembership is attached to the context or tracked
            Context.Entry(teamMembership).State = EntityState.Modified;
            await Context.SaveChangesAsync(); // Save changes to persist the update
        }
    }
}
