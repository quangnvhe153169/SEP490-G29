﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using Microsoft.EntityFrameworkCore;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class EventAttendeeRepository : BaseRepository<EventAttendee>, IEventAttendeeRepository
    {
        public EventAttendeeRepository(LabInternManagementDBcontext context)
            : base(context) { }

        public async Task AddEventAttendee(EventAttendee eventAttendee)
        {
            if (eventAttendee == null)
            {
                throw new ArgumentNullException(nameof(eventAttendee));
            }

            Context.Set<EventAttendee>().Add(eventAttendee);
            Context.SaveChanges();
        }

        public async Task DeleteEventAttendee(int eventAttendeeId)
        {
            var eventAttendee = Context.Set<EventAttendee>().Find(eventAttendeeId);
            if (eventAttendee != null)
            {
                Context.Set<EventAttendee>().Remove(eventAttendee);
                Context.SaveChanges();
            }
            // Optionally, throw an exception or handle if eventAttendee is not found
        }

        public async Task<List<EventAttendee>> GetAllEventAttendee()
        {
            return Context.Set<EventAttendee>().ToList();
        }

        public async Task<EventAttendee> GetEventAttendeeByEventAttendeeId(int eventAttendeeId)
        {
            return Context.Set<EventAttendee>().Find(eventAttendeeId);
        }

        public async Task<List<EventAttendee>> GetEventAttendeeByEventId(int eventId)
        {
            return Context.Set<EventAttendee>().Where(ea => ea.EventId == eventId).ToList();
        }

        public async Task<List<EventAttendee>> GetEventAttendeeByUserId(string userId)
        {
            return Context.Set<EventAttendee>().Where(ea => ea.UserId.Equals(userId)).ToList();
        }

        public async Task<List<EventAttendee>> SearchEventAttendee(string searchTerm)
        {
            var query = Context.Set<EventAttendee>().AsQueryable();
            DateTime? startDate = null;
            DateTime? endDate = null;

            // Attempt to parse searchTerm as DateTime to determine date range filter
            if (DateTime.TryParse(searchTerm, out DateTime parsedDate))
            {
                startDate = parsedDate.Date;
                endDate = parsedDate.Date.AddDays(1).AddTicks(-1); // End of the day
            }

            // Apply filters based on the determined type
            if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                query = query.Where(ea =>
                    ea.User.FirstName.Contains(searchTerm) || ea.User.LastName.Contains(searchTerm)
                );
            }

            if (startDate.HasValue)
            {
                query = query.Where(ea => ea.CreatedAt >= startDate.Value);
            }

            if (endDate.HasValue)
            {
                query = query.Where(ea => ea.CreatedAt <= endDate.Value);
            }

            return await query.ToListAsync();
        }

        public async Task UpdateEventAttendee(EventAttendee eventAttendee)
        {
            // Assuming eventAttendee is attached to the context or tracked
            Context.Entry(eventAttendee).State = EntityState.Modified;
            Context.SaveChanges();
        }
    }
}
