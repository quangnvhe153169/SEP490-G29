﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using Microsoft.EntityFrameworkCore;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class ProgressPerformanceRepository : BaseRepository<ProgressPerformance>, IProgressPerformanceRepository
    {
        public ProgressPerformanceRepository(LabInternManagementDBcontext context) : base(context)
        {
        }

        public async Task AddProgressPerformance(ProgressPerformance progressPerformance)
        {
            if (progressPerformance == null)
            {
                throw new ArgumentNullException(nameof(progressPerformance));
            }
            Context.Set<ProgressPerformance>().Add(progressPerformance);
            Context.SaveChanges();
        }

        public async Task DeleteProgressPerformance(int progressPerformanceId)
        {
            var progressPerformance = Context.Set<ProgressPerformance>().Find(progressPerformanceId);
            if (progressPerformance != null)
            {
                Context.Set<ProgressPerformance>().Remove(progressPerformance);
                Context.SaveChanges();  // Save changes to persist the deletion
            }
            // Optionally, throw an exception or handle if progressPerformance is not found
        }

        public async Task<List<ProgressPerformance>> GetAllProgressPerformance()
        {
            return Context.Set<ProgressPerformance>().ToList();
        }

        public async Task<List<ProgressPerformance>> GetProgressPerformanceByKeyResultId(string keyResultId)
        {
            return Context.Set<ProgressPerformance>()
                          .Where(pp => pp.KeyResultId.Equals(keyResultId))
                          .ToList();
        }

        public async Task<ProgressPerformance> GetProgressPerformanceByProgressPerformanceId(int progressPerformanceId)
        {
            return Context.Set<ProgressPerformance>().Find(progressPerformanceId);
        }

        public async Task<List<ProgressPerformance>> SearchProgressPerformance(string searchTerm)
        {
            var query = Context.Set<ProgressPerformance>().AsQueryable();
            DateTime? startDate = null;
            DateTime? endDate = null;

            // Attempt to parse searchTerm as DateTime to determine date range filter
            if (DateTime.TryParse(searchTerm, out DateTime parsedDate))
            {
                startDate = parsedDate.Date;
                endDate = parsedDate.Date.AddDays(1).AddTicks(-1); // End of the day
            }

            // Apply filters based on the determined type
            if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                query = query.Where(pp => pp.Insights.Contains(searchTerm)
                                       || pp.Recommendations.Contains(searchTerm)
                                       || pp.Comments.Contains(searchTerm));
            }

            if (startDate.HasValue)
            {
                query = query.Where(pp => pp.ReportDate >= startDate.Value);
            }

            if (endDate.HasValue)
            {
                query = query.Where(pp => pp.ReportDate <= endDate.Value);
            }

            return await query.ToListAsync();
        }

        public async Task UpdateProgressPerformance(ProgressPerformance progressPerformance)
        {
            // Assuming progressPerformance is attached to the context or tracked
            Context.Entry(progressPerformance).State = EntityState.Modified;
            Context.SaveChanges();  // Save changes to persist the update
        }
    }
}
