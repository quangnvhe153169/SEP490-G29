﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using LaboratoryInternshipManagement.Services.IServices;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using NetTools.Extensions;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class ImportInternRepository : BaseRepository<ImportIntern>, IImportInternRepository
    {
        private readonly UserManager<User> _userManager;
        private readonly IImportCSVRepository _importCSVRepository;
        private readonly IConfiguration _configuration;
        private readonly IEmailService _emailService;
        private readonly IMemoryCache _memoryCache;

        public ImportInternRepository(
            LabInternManagementDBcontext context,
            UserManager<User> userManager,
            IImportCSVRepository importCSVRepository,
            IConfiguration configuration,
            IEmailService emailService,
            IMemoryCache memoryCache
        )
            : base(context)
        {
            _userManager = userManager;
            _importCSVRepository = importCSVRepository;
            _configuration = configuration;
            _emailService = emailService;
            _memoryCache = memoryCache;
        }

        public async Task AddImportIntern(ImportIntern candidate)
        {
            if (candidate == null)
            {
                throw new ArgumentNullException(nameof(candidate));
            }
            Context.Set<ImportIntern>().Add(candidate);
            Context.SaveChanges();
        }

        public async Task DeleteImportIntern(int importInternId)
        {
            var importIntern = await Context
                .Set<ImportIntern>()
                .SingleOrDefaultAsync(o => o.Id == importInternId);
            if (importIntern != null)
            {
                Context.Set<ImportIntern>().Remove(importIntern);
                Context.SaveChanges(); // Save changes to persist the deletion
            }
            // Optionally, throw an exception or handle if objective is not found
        }

        public async Task<List<ImportIntern>> GetAllImportInterns()
        {
            return Context.Set<ImportIntern>().ToList();
        }

        public async Task<List<ImportIntern>> GetUnassignedInternByLab(int labId)
        {
            var lab = Context.Set<Laboratory>().Find(labId);

            return Context
                .Set<ImportIntern>()
                .Where(i => string.IsNullOrEmpty(i.MentorID) && i.LabName.Equals(lab.LabName))
                .ToList();
            ;
        }

        public async Task<ImportIntern> GetImportInternByImportInternId(int importInternId)
        {
            return Context.Set<ImportIntern>().Find(importInternId);
        }

        public async Task<List<ImportIntern>> SearchImportIntern(string searchTerm)
        {
            var query = Context.Set<ImportIntern>().AsQueryable();

            DateTime? Dob = null;
            // Attempt to parse searchTerm as DateTime to determine date range filter
            if (DateTime.TryParse(searchTerm, out DateTime parsedDate))
            {
                Dob = parsedDate.Date;
            }

            // Apply filters based on the determined type
            if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                query = query.Where(o =>
                    o.RollNumber.Contains(searchTerm)
                    || o.FirstName.Contains(searchTerm)
                    || o.LastName.Contains(searchTerm)
                    || o.Email.Contains(searchTerm)
                    || o.LabName.Contains(searchTerm)
                );
            }
            return query.ToList();
        }

        public async Task UpdateImportIntern(ImportIntern candidate)
        {
            // Assuming objective is attached to the context or tracked
            Context.Entry(candidate).State = EntityState.Modified;
            Context.SaveChanges(); // Save changes to persist the update
        }

        public async Task InitializeAssignments()
        {
            var labs = Context.Set<Laboratory>().ToList();
            var interns = (await GetAllImportInterns())
                .Where(i => string.IsNullOrEmpty(i.MentorID))
                .ToList();
            var mentors = GetAvailableMentor();

            if (labs.Any() && interns.Any())
            {
                foreach (var lab in labs)
                {
                    var labImportInterns = interns
                        .Where(i => i.LabName.Trim().Equals(lab.LabName.Trim()))
                        .ToList();
                    Shuffle(labImportInterns);
                    var labMentors = mentors
                        .Where(m =>
                            m.LabId == lab.Id
                            || (
                                _userManager.IsInRoleAsync(m, "Mentor").Result
                                && _userManager.IsInRoleAsync(m, "Lab Manager").Result
                            )
                                && m.Id == lab.LabManagerId
                        )
                        .ToList();
                    int mentorCount = labMentors.Count;
                    int index = 0;

                    foreach (var intern in interns)
                    {
                        intern.MentorID = labMentors[index % mentorCount].Id;
                        await UpdateImportIntern(intern);
                        index++;
                    }
                }
            }
        }

        public async Task SendMailToIntern(Dictionary<string, string> Interns)
        {
            foreach (var intern in Interns)
            {
                var emailTestSendMail = intern.Key;
                await _emailService.SendEmailAsync(
                    emailTestSendMail,
                    "Tải Lên CV Của Bạn",
                    intern.Value
                );
            }
        }

        public async Task<(List<InternCSV>, List<ImportInternError>)> ValidateCSV(IFormFile file)
        {
            var interns = _importCSVRepository.ReadCSV<InternCSV>(file.OpenReadStream());
            var validRecords = new List<InternCSV>();
            var invalidRecords = new List<ImportInternError>();

            var importInternEmails = await Context
                .ImportInterns.Select(i => i.Email.ToLower())
                .ToListAsync();

            var userEmails = await Context
                .Users.Where(u => u.Email != null && u.Email != "")
                .Select(u => u.Email.ToLower())
                .ToListAsync();

            var existingEmails = importInternEmails.Concat(userEmails).ToList();

            foreach (var intern in interns)
            {
                var existingLabs = await Context.Laboratories.FirstOrDefaultAsync(l =>
                    l.LabName == intern.LabName
                );

                // Check if email exists in the ImportInterns table
                if (existingEmails.Contains(intern.Email.ToLower()))
                {
                    invalidRecords.Add(
                        new ImportInternError
                        {
                            RollNumber = intern.RollNumber,
                            FirstName = intern.FirstName,
                            LastName = intern.LastName,
                            DOB = intern.DOB,
                            Email = intern.Email,
                            LabName = intern.LabName,
                            ErrorMessage = "emailIsExistInDataBase"
                        }
                    );
                }
                else if (await IsInternValid(intern, existingLabs))
                {
                    validRecords.Add(intern);
                }
                else
                {
                    invalidRecords.Add(await GenerateImportErrorAsync(intern, existingLabs));
                }
            }

            return (validRecords, invalidRecords);
        }

        public async Task SaveValidRecords(
            List<InternCSV> validRecords,
            Dictionary<string, string> internSendMails
        )
        {
            foreach (var validRecord in validRecords)
            {
                string randomText = _importCSVRepository.GenerateRandomString(8);
                string signatureText = _importCSVRepository.ComputeSha256Hash(randomText);
                var importIntern = new ImportIntern
                {
                    RollNumber = validRecord.RollNumber,
                    FirstName = validRecord.FirstName,
                    LastName = validRecord.LastName,
                    DOB = validRecord.DOB,
                    Email = validRecord.Email,
                    LabName = validRecord.LabName,
                    CVName = validRecord.CVName,
                    CVDescript = validRecord.CVDescript,
                    Signature = signatureText,
                    ExpiredAt = DateTime.Now.AddDays(3)
                };
                Context.ImportInterns.Add(importIntern);
                Context.SaveChanges();

                internSendMails.Add(importIntern.Email, signatureText);
            }
        }

        private async Task<bool> IsInternValid(InternCSV intern, Laboratory existingLabs)
        {
            var rollNumbExist = await Context
                .Users.Where(u => u.RollName != null && u.RollName != "")
                .AnyAsync(x => x.RollName.Equals(intern.RollNumber));

            return _importCSVRepository.IsValidEmail(intern.Email)
                && !string.IsNullOrEmpty(intern.Email)
                && !string.IsNullOrEmpty(intern.LabName)
                && !string.IsNullOrEmpty(intern.RollNumber)
                && !rollNumbExist
                && intern.DOB != null
                && existingLabs != null;
        }

        private async Task<ImportInternError> GenerateImportErrorAsync(
            InternCSV intern,
            Laboratory existingLabs
        )
        {
            return new ImportInternError
            {
                RollNumber = intern.RollNumber,
                FirstName = intern.FirstName,
                LastName = intern.LastName,
                DOB = intern.DOB,
                Email = intern.Email,
                LabName = intern.LabName,
                ErrorMessage = await GetErrorMessageAsync(intern, existingLabs)
            };
        }

        private async Task<string> GetErrorMessageAsync(InternCSV intern, Laboratory existingLabs)
        {
            var errorMessage = string.Empty;
            var rollNumbExist = await Context
                .Users.Where(x => x.RollName != null || x.RollName != "")
                .AnyAsync(x => x.RollName.Equals(intern.RollNumber));

            if (string.IsNullOrEmpty(intern.RollNumber))
                errorMessage += "rollNumberIsNull.";
            else if (rollNumbExist)
            {
                errorMessage += "rollNumberIsExisted.";
            }
            if (intern.DOB == null)
                errorMessage += "dobIsNull.";
            if (string.IsNullOrEmpty(intern.Email))
                errorMessage += "emailIsNull.";
            if (!_importCSVRepository.IsValidEmail(intern.Email))
                errorMessage += "invalidEmailFormat.";
            if (string.IsNullOrEmpty(intern.LabName))
                errorMessage += "labNameIsNull.";
            else if (existingLabs == null)
                errorMessage += "labNameNotExist.";
            return errorMessage.TrimEnd('.');
        }

        private void Shuffle<T>(List<T> list)
        {
            var rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        private List<User> GetAvailableMentor()
        {
            var allUsers = _userManager.Users.ToList();
            return allUsers
                .Where(user => _userManager.IsInRoleAsync(user, "Mentor").Result)
                .ToList();
        }
    }
}
