﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using Microsoft.EntityFrameworkCore;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class KeyResultRepository : BaseRepository<KeyResult>, IKeyResultRepository
    {
        public KeyResultRepository(LabInternManagementDBcontext context)
            : base(context) { }

        public async Task AddKeyResult(KeyResult keyResult)
        {
            if (keyResult == null)
            {
                throw new ArgumentNullException(nameof(keyResult));
            }

            Context.Set<KeyResult>().Add(keyResult);
            Context.SaveChanges();
        }

        public async Task DeleteKeyResult(int keyResultId)
        {
            var keyResult = await Context
                .Set<KeyResult>()
                .FirstOrDefaultAsync(kr => kr.KeyResultId == keyResultId);

            if (keyResult != null)
            {
                // Delete associated discussion if exists

                // Remove the key result itself
                Context.Set<KeyResult>().Remove(keyResult);
                await Context.SaveChangesAsync();
            }
            // Optionally, throw an exception or handle if key result is not found
        }

        public async Task<List<KeyResult>> GetAllKeyResult()
        {
            return Context.Set<KeyResult>().Include(k => k.Objective).ToList();
        }

        public async Task<KeyResult> GetKeyResultByKeyResultId(int keyResultId)
        {
            return await Context
                .Set<KeyResult>()
                .Include(k => k.Objective)
                .SingleOrDefaultAsync(k => k.KeyResultId == keyResultId);
        }

        public async Task<List<KeyResult>> GetKeyResultByObjectiveId(int objectiveId)
        {
            return Context.Set<KeyResult>().Where(kr => kr.ObjectiveId == objectiveId).ToList();
        }

        public async Task<List<KeyResult>> GetKeyResultByAssignUserId(string userId)
        {
            return Context
                .Set<KeyResult>()
                .Include(k => k.Objective)
                .Where(kr => kr.Assign.Equals(userId))
                .ToList();
        }

        public async Task<List<KeyResult>> SearchKeyResult(string searchTerm)
        {
            var query = Context.Set<KeyResult>().Include(k => k.Objective).AsQueryable();

            DateTime? startDate = null;
            DateTime? endDate = null;

            // Attempt to parse searchTerm as DateTime to determine date range filter
            if (DateTime.TryParse(searchTerm, out DateTime parsedDate))
            {
                startDate = parsedDate.Date;
                endDate = parsedDate.Date.AddDays(1).AddTicks(-1); // End of the day
            }

            // Apply filters based on the determined type
            if (startDate.HasValue)
            {
                query = query.Where(kr => kr.CreatedAt >= startDate.Value);
            }
            else if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                query = query.Where(kr =>
                    kr.Title.Contains(searchTerm)
                    || kr.Notes.Contains(searchTerm)
                    || kr.Objective.Title.Contains(searchTerm)
                    || kr.Objective.Description.Contains(searchTerm)
                );
            }

            if (endDate.HasValue)
            {
                query = query.Where(kr => kr.CreatedAt <= endDate.Value);
            }

            return query.ToList();
        }

        public async Task UpdateKeyResult(KeyResult keyResult)
        {
            // Attach the updated entity to the context
            Context.Attach(keyResult);

            // Get the entry for the existing entity
            var entry = Context.Entry(keyResult);

            // Specify the properties you want to skip updating
            var propertiesToSkip = new HashSet<string>
            {
                nameof(KeyResult.ObjectiveId),
                nameof(KeyResult.CreatedAt),
                nameof(KeyResult.KeyResultId),
                nameof(KeyResult.TargetValue),
            };

            // Iterate through all properties and skip the ones you don't want to update
            foreach (var property in entry.Properties)
            {
                if (propertiesToSkip.Contains(property.Metadata.Name))
                {
                    property.IsModified = false;
                }
                else
                {
                    property.IsModified = true;
                }
            }
            await Context.SaveChangesAsync();
        }
    }
}
