﻿using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using LaboratoryInternshipManagement.Models.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class DashBoardRepository : IDashBoardRepository
    {
        private readonly LabInternManagementDBcontext _context;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly UserManager<User> _userManager;

        public DashBoardRepository(
            LabInternManagementDBcontext context,
            IHttpContextAccessor httpContextAccessor,
            UserManager<User> userManager
        )
        {
            _context = context;
            _httpContextAccessor = httpContextAccessor;
            _userManager = userManager;
        }

        public async Task<List<MonthlyAttendanceSummary>> GetMonthlyAttendanceSummaryAsync()
        {
            return await _context
                .Attendances.GroupBy(a => new { a.Date.Year, a.Date.Month })
                .Select(g => new MonthlyAttendanceSummary
                {
                    Year = g.Key.Year,
                    Month = g.Key.Month,
                    TotalAttendanced = g.Count(a => a.Status == 1),
                    TotalAbsent = g.Count(a => a.Status == 4),
                    TotalLateIn = g.Count(a => a.Status == 2),
                    TotalSoonOut = g.Count(a => a.Status == 3)
                })
                .ToListAsync();
        }

        private string GenerateUniqueStaffId(string email)
        {
            var baseStaffId = email.Split('@')[0];
            var staffId = baseStaffId;
            int counter = 1;

            while (_context.Reports.Any(r => r.StaffId == staffId))
            {
                staffId = $"{baseStaffId}{counter}";
                counter++;
            }

            return staffId;
        }

        public async Task<int> CountInternsBySemesterNameAsync(string semesterName)
        {
            if (string.IsNullOrWhiteSpace(semesterName))
            {
                throw new ArgumentException(
                    "The semester name cannot be null or empty.",
                    nameof(semesterName)
                );
            }

            try
            {
                var semester = await _context.Semesters.FirstOrDefaultAsync(s =>
                    s.SemesterName == semesterName
                );

                if (semester == null)
                {
                    // Semester not found
                    throw new KeyNotFoundException(
                        $"No semester found with the name '{semesterName}'."
                    );
                }

                var count = await _context
                    .Semesters.Where(s => s.SemesterName == semesterName)
                    .SelectMany(s => s.Interns)
                    .CountAsync();

                return count;
            }
            catch (KeyNotFoundException knfEx)
            {
                // Re-throw the KeyNotFoundException to be handled by the calling code or controller
                throw knfEx;
            }
            catch (Exception ex)
            {
                // Log the exception details here
                Console.Error.WriteLine($"Error in CountInternsBySemesterNameAsync: {ex.Message}");
                // Wrap the exception in a more generic exception to avoid exposing internal details
                throw new ApplicationException("An error occurred while counting interns.", ex);
            }
        }

        public async Task<SemesterNameSummary> GetSemesterSummaryAsync(
            string semesterName,
            string mentorId
        )
        {
            // Validate semesterName
            if (string.IsNullOrEmpty(semesterName))
            {
                throw new ArgumentException(
                    "Semester name must not be null or empty.",
                    nameof(semesterName)
                );
            }

            // Validate mentorId
            if (string.IsNullOrEmpty(mentorId))
            {
                throw new ArgumentException(
                    "Mentor ID must not be null or empty.",
                    nameof(mentorId)
                );
            }

            try
            {
                // Retrieve the semesterId based on the semesterName
                var semester = await _context
                    .Semesters.AsNoTracking()
                    .Where(s => s.SemesterName == semesterName)
                    .Select(s => new { s.SemesterId })
                    .FirstOrDefaultAsync();

                if (semester == null)
                {
                    throw new InvalidOperationException("No semester found with the given name.");
                }

                // Ensure filtering by MentorId in the Attendances query
                var attendancesSummarySemester = await _context
                    .Attendances.Where(a =>
                        a.Intern.SemesterId == semester.SemesterId && a.Intern.MentorId == mentorId
                    )
                    .GroupBy(a => new { a.Date.Year, a.Date.Month })
                    .Select(g => new MonthlyInternAttendance
                    {
                        Year = g.Key.Year,
                        Month = g.Key.Month,
                        TotalAttendanced = g.Count(a => a.Status == 1),
                        TotalAbsent = g.Count(a => a.Status == 4),
                        TotalLateIn = g.Count(a => a.Status == 2),
                        TotalSoonOut = g.Count(a => a.Status == 3)
                    })
                    .ToListAsync();

                // Ensure filtering by MentorId in the KeyResults query
                var semesterKeyResultsSummary = await _context
                    .KeyResults.Include(x => x.User)
                    .Where(kr =>
                        kr.User.MentorId == mentorId && kr.User.SemesterId == semester.SemesterId
                    )
                    .GroupBy(kr => new { kr.CreatedAt.Year, kr.CreatedAt.Month })
                    .Select(g => new MonthlyKeyResultsSummary
                    {
                        Year = g.Key.Year,
                        Month = g.Key.Month,
                        TotalFailed = g.Count(kr =>
                            kr.Status == ConstantEnum.KeyResultStatus.Failed
                        ),
                        TotalInProgress = g.Count(kr =>
                            kr.Status == ConstantEnum.KeyResultStatus.In_Progress
                        ),
                        TotalCompleted = g.Count(kr =>
                            kr.Status == ConstantEnum.KeyResultStatus.Completed
                        ),
                        TotalNotStarted = g.Count(kr =>
                            kr.Status == ConstantEnum.KeyResultStatus.Not_Started
                        )
                    })
                    .ToListAsync();

                // Map to MonthlyInternKeyResults
                var mappedKeyResultsSummary = semesterKeyResultsSummary
                    .Select(s => new MonthlyInternKeyResults
                    {
                        Year = s.Year,
                        Month = s.Month,
                        TotalFailed = s.TotalFailed,
                        TotalInProgress = s.TotalInProgress,
                        TotalCompleted = s.TotalCompleted,
                        TotalNotStarted = s.TotalNotStarted
                    })
                    .ToList();

                // Return the summary for the semester
                return new SemesterNameSummary
                {
                    KeyResultsSummary = mappedKeyResultsSummary,
                    AttendancesSumary = attendancesSummarySemester
                };
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(
                    "An error occurred while retrieving the semester summary.",
                    ex
                );
            }
        }

        public async Task<IEnumerable<InternDTO>> GetInternsBySemesterNameAndMentorAsync(
            string semesterName,
            string mentorId
        )
        {
            try
            {
                // Fetch interns for the specified semester and mentor
                var interns = await _context
                    .Users.Include(s => s.Mentor)
                    .Include(s => s.Laboratory)
                    .Include(s => s.Semester)
                    .Where(s =>
                        s.Semester.SemesterName.Equals(semesterName) && s.MentorId == mentorId
                    )
                    .ToListAsync();

                var internDTOs = interns
                    .Select(intern => new InternDTO
                    {
                        Id = intern.Id,
                        InternId = intern.Id,
                        NameIntern = $"{intern.FirstName} {intern.LastName}",
                        BirthDate = intern.BirthDate,
                        RollName = intern.RollName,
                        MentorId = intern.Mentor?.Id,
                        NameMentor =
                            intern.Mentor != null
                                ? $"{intern.Mentor.FirstName} {intern.Mentor.LastName}"
                                : "N/A",
                        NameLab = intern.Laboratory?.LabName ?? "N/A",
                        StaffId = GenerateUniqueStaffId(intern.Email),
                        SemesterId = intern.Semester?.SemesterId ?? 0
                    })
                    .ToList();

                if (!internDTOs.Any())
                {
                    throw new KeyNotFoundException(
                        $"No interns available for the semester '{semesterName}' and mentor '{mentorId}'."
                    );
                }

                return internDTOs;
            }
            catch (KeyNotFoundException knfEx)
            {
                Console.Error.WriteLine(
                    $"KeyNotFoundException in GetInternsBySemesterNameAndMentorAsync: {knfEx.Message}"
                );
                throw;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(
                    $"Error in GetInternsBySemesterNameAndMentorAsync: {ex.Message}"
                );
                throw new ApplicationException("An error occurred while fetching interns.", ex);
            }
        }

        public async Task<int> CountInternsBySemesterNameAndMentorAsync(
            string semesterName,
            string mentorId
        )
        {
            if (string.IsNullOrWhiteSpace(semesterName))
            {
                throw new ArgumentException(
                    "The semester name cannot be null or empty.",
                    nameof(semesterName)
                );
            }

            if (string.IsNullOrWhiteSpace(mentorId))
            {
                throw new ArgumentException(
                    "The mentor ID cannot be null or empty.",
                    nameof(mentorId)
                );
            }

            try
            {
                var semester = await _context
                    .Semesters.Include(s => s.Interns)
                    .ThenInclude(i => i.Mentor)
                    .FirstOrDefaultAsync(s => s.SemesterName == semesterName);

                if (semester == null)
                {
                    // Semester not found
                    throw new KeyNotFoundException(
                        $"No semester found with the name '{semesterName}'."
                    );
                }

                // Count the interns associated with the specific mentor
                var count = semester.Interns.Count(i => i.MentorId == mentorId);

                return count;
            }
            catch (KeyNotFoundException knfEx)
            {
                // Re-throw the KeyNotFoundException to be handled by the calling code or controller
                throw knfEx;
            }
            catch (Exception ex)
            {
                // Log the exception details here
                Console.Error.WriteLine(
                    $"Error in CountInternsBySemesterNameAndMentorAsync: {ex.Message}"
                );
                // Wrap the exception in a more generic exception to avoid exposing internal details
                throw new ApplicationException("An error occurred while counting interns.", ex);
            }
        }

        public async Task<List<MonthlyInternAttendance>> GetMonthlyInternAttendanceSummaryAsync(
            string internId
        )
        {
            return await _context
                .Attendances.Where(a => a.InternId == internId)
                .GroupBy(a => new { a.Date.Year, a.Date.Month })
                .Select(g => new MonthlyInternAttendance
                {
                    Year = g.Key.Year,
                    Month = g.Key.Month,
                    TotalAttendanced = g.Count(a => a.Status == 1),
                    TotalAbsent = g.Count(a => a.Status == 4),
                    TotalLateIn = g.Count(a => a.Status == 2),
                    TotalSoonOut = g.Count(a => a.Status == 3)
                })
                .ToListAsync();
        }

        public async Task<List<MonthlyInternKeyResults>> GetMonthlyInternKeyResultSummaryAsync(
            string internId
        )
        {
            // Validate internId
            if (string.IsNullOrWhiteSpace(internId))
            {
                throw new ArgumentException("Intern ID cannot be null or empty.", nameof(internId));
            }

            try
            {
                var result = await _context
                    .KeyResults.Where(a => a.Assign.Equals(internId))
                    .GroupBy(a => new { a.CreatedAt.Year, a.CreatedAt.Month })
                    .Select(g => new MonthlyInternKeyResults
                    {
                        Year = g.Key.Year,
                        Month = g.Key.Month,
                        TotalFailed = g.Count(a => a.Status == ConstantEnum.KeyResultStatus.Failed),
                        TotalInProgress = g.Count(a =>
                            a.Status == ConstantEnum.KeyResultStatus.In_Progress
                        ),
                        TotalCompleted = g.Count(a =>
                            a.Status == ConstantEnum.KeyResultStatus.Completed
                        ),
                        TotalNotStarted = g.Count(a =>
                            a.Status == ConstantEnum.KeyResultStatus.Not_Started
                        )
                    })
                    .ToListAsync();

                return result;
            }
            catch (Exception ex)
            {
                // Log the exception details for debugging
                // Example: logger.LogError(ex, "An error occurred while getting monthly key result summary.");
                // You can use any logging mechanism you have in place, like Serilog, NLog, etc.

                // Return a meaningful error message or rethrow the exception
                throw new InvalidOperationException(
                    "An error occurred while retrieving the monthly key results summary.",
                    ex
                );
            }
        }

        public async Task<int> CountObjectivesByInternIdAsync(string internId)
        {
            // Validate internId
            if (string.IsNullOrWhiteSpace(internId))
            {
                throw new ArgumentException("Intern ID cannot be null or empty.", nameof(internId));
            }

            return await _context
                .Objectives.Where(o =>
                    o.CreateBy.Equals(internId)
                    || o.KeyResults.Any(kr => kr.Assign.Equals(internId))
                )
                .CountAsync();
        }

        public async Task<int> CountInternsBySemesterNameLabManageAsync(
            string semesterName,
            string labManagerId
        )
        {
            if (string.IsNullOrWhiteSpace(semesterName))
            {
                throw new ArgumentException(
                    "The semester name cannot be null or empty.",
                    nameof(semesterName)
                );
            }

            if (string.IsNullOrWhiteSpace(labManagerId))
            {
                throw new ArgumentException(
                    "The lab manager ID cannot be null or empty.",
                    nameof(labManagerId)
                );
            }

            try
            {
                var semester = await _context
                    .Semesters.Include(s => s.Interns)
                    .FirstOrDefaultAsync(s => s.SemesterName == semesterName);

                if (semester == null)
                {
                    throw new KeyNotFoundException(
                        $"No semester found with the name '{semesterName}'."
                    );
                }

                var count = await _context
                    .Users.Where(u =>
                        u.SemesterId == semester.SemesterId
                        && u.Laboratory.LabManagerId == labManagerId
                    )
                    .CountAsync();

                return count;
            }
            catch (KeyNotFoundException knfEx)
            {
                throw knfEx;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Error in CountInternsBySemesterNameAsync: {ex.Message}");
                throw new ApplicationException("An error occurred while counting interns.", ex);
            }
        }

        public async Task<SemesterNameSummary> GetSemesterSummaryLabAsync(
            string semesterName,
            string labmanageId
        )
        {
            // Validate semesterName
            if (string.IsNullOrEmpty(semesterName))
            {
                throw new ArgumentException(
                    "Semester name must not be null or empty.",
                    nameof(semesterName)
                );
            }

            // Validate labmanageId
            if (string.IsNullOrEmpty(labmanageId))
            {
                throw new ArgumentException(
                    "Lab Manager ID must not be null or empty.",
                    nameof(labmanageId)
                );
            }

            try
            {
                // Retrieve the semesterId based on the semesterName
                var semester = await _context
                    .Semesters.AsNoTracking()
                    .Where(s => s.SemesterName == semesterName)
                    .Select(s => new { s.SemesterId })
                    .FirstOrDefaultAsync();

                if (semester == null)
                {
                    throw new InvalidOperationException("No semester found with the given name.");
                }

                // Filter Attendances by the SemesterId and LabManagerId through the related Intern's LabManagerId
                var attendancesSummarySemester = await _context
                    .Attendances.Include(x => x.Intern)
                    .ThenInclude(i => i.Laboratory)
                    .Where(a =>
                        a.Intern.SemesterId == semester.SemesterId
                        && a.Intern.Laboratory.LabManagerId == labmanageId
                    )
                    .GroupBy(a => new { a.Date.Year, a.Date.Month })
                    .Select(g => new MonthlyInternAttendance
                    {
                        Year = g.Key.Year,
                        Month = g.Key.Month,
                        TotalAttendanced = g.Count(a => a.Status == 1),
                        TotalAbsent = g.Count(a => a.Status == 4),
                        TotalLateIn = g.Count(a => a.Status == 2),
                        TotalSoonOut = g.Count(a => a.Status == 3)
                    })
                    .ToListAsync();

                // Filter KeyResults by the SemesterId and LabManagerId
                var semesterKeyResultsSummary = await _context
                    .KeyResults.Include(i => i.User)
                    .ThenInclude(u => u.Laboratory)
                    .Where(kr =>
                        kr.User.SemesterId == semester.SemesterId
                        && kr.User.Laboratory.LabManagerId.Equals(labmanageId)
                    )
                    .GroupBy(kr => new { kr.CreatedAt.Year, kr.CreatedAt.Month })
                    .Select(g => new MonthlyKeyResultsSummary
                    {
                        Year = g.Key.Year,
                        Month = g.Key.Month,
                        TotalFailed = g.Count(kr =>
                            kr.Status == ConstantEnum.KeyResultStatus.Failed
                        ),
                        TotalInProgress = g.Count(kr =>
                            kr.Status == ConstantEnum.KeyResultStatus.In_Progress
                        ),
                        TotalCompleted = g.Count(kr =>
                            kr.Status == ConstantEnum.KeyResultStatus.Completed
                        ),
                        TotalNotStarted = g.Count(kr =>
                            kr.Status == ConstantEnum.KeyResultStatus.Not_Started
                        )
                    })
                    .ToListAsync();

                // Map to MonthlyInternKeyResults
                var mappedKeyResultsSummary = semesterKeyResultsSummary
                    .Select(s => new MonthlyInternKeyResults
                    {
                        Year = s.Year,
                        Month = s.Month,
                        TotalFailed = s.TotalFailed,
                        TotalInProgress = s.TotalInProgress,
                        TotalCompleted = s.TotalCompleted,
                        TotalNotStarted = s.TotalNotStarted
                    })
                    .ToList();

                // Return the summary for the semester
                return new SemesterNameSummary
                {
                    KeyResultsSummary = mappedKeyResultsSummary,
                    AttendancesSumary = attendancesSummarySemester
                };
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(
                    "An error occurred while retrieving the semester summary.",
                    ex
                );
            }
        }

        public async Task<IEnumerable<InternDTO>> GetSummaryInternLabManageAsync(
            string semesterName,
            string labManagerId
        )
        {
            try
            {
                // Fetch the semester and filter interns by labId and labManagerId
                var interns = await _context
                    .Users.Include(x => x.Semester)
                    .Include(x => x.Laboratory)
                    .Include(x => x.Mentor)
                    .Where(x =>
                        x.Laboratory.LabManagerId.Equals(labManagerId)
                        && x.Semester.SemesterName.Equals(semesterName)
                    )
                    .ToListAsync();

                // Map the intern entities to InternDTO
                var internDTOs = interns
                    .Select(intern => new InternDTO
                    {
                        Id = intern.Id,
                        InternId = intern.Id,
                        NameIntern = $"{intern.FirstName} {intern.LastName}",
                        BirthDate = intern.BirthDate,
                        RollName = intern?.RollName,
                        MentorId = intern.MentorId,
                        NameMentor =
                            intern.Mentor != null
                                ? $"{intern.Mentor.FirstName} {intern.Mentor.LastName}"
                                : "N/A",
                        NameLab = intern.Laboratory?.LabName ?? "N/A",
                        StaffId = GenerateUniqueStaffId(intern.Email),
                        SemesterId = intern.Semester?.SemesterId,
                        LabId = intern.LabId ?? 0,
                    })
                    .ToList();

                // Throw exception if no interns found
                if (!internDTOs.Any())
                {
                    throw new KeyNotFoundException(
                        $"No interns available for the semester '{semesterName}' in the lab managed by you."
                    );
                }

                return internDTOs;
            }
            catch (KeyNotFoundException knfEx)
            {
                Console.Error.WriteLine(
                    $"KeyNotFoundException in GetSummaryInternLabManageAsync: {knfEx.Message}"
                );
                throw;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Error in GetSummaryInternLabManageAsync: {ex.Message}");
                throw new ApplicationException("An error occurred while fetching interns.", ex);
            }
        }

        public async Task<IEnumerable<dynamic>> GetMentors(int labId)
        {
            try
            {
                // Fetch the semester and filter interns by labId and labManagerId
                var mentors = await _userManager.GetUsersInRoleAsync("Mentor");
                var takeMentors = mentors.Where(x => x.LabId == labId).ToList();

                // Map the intern entities to InternDTO
                var mentorDTOs = takeMentors
                    .Select(mentor => new
                    {
                        Id = mentor.Id,
                        NameMentor = $"{mentor.FirstName} {mentor.LastName}",
                        Email = mentor.Email,
                    })
                    .ToList();

                // Throw exception if no interns found
                if (!mentorDTOs.Any())
                {
                    throw new KeyNotFoundException($"No mentors in the lab managed by you.");
                }

                return mentorDTOs;
            }
            catch (KeyNotFoundException knfEx)
            {
                Console.Error.WriteLine(
                    $"KeyNotFoundException in GetSummaryInternLabManageAsync: {knfEx.Message}"
                );
                throw;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Error in GetSummaryInternLabManageAsync: {ex.Message}");
                throw new ApplicationException("An error occurred while fetching interns.", ex);
            }
        }

        public async Task<List<MonthlyKeyResultsSummary>> GetMonthlyKeyResultSummaryAsync()
        {
            return await _context
                .KeyResults.GroupBy(a => new { a.CreatedAt.Year, a.CreatedAt.Month })
                .Select(g => new MonthlyKeyResultsSummary
                {
                    Year = g.Key.Year,
                    Month = g.Key.Month,
                    TotalFailed = g.Count(a => a.Status == ConstantEnum.KeyResultStatus.Failed),
                    TotalInProgress = g.Count(a =>
                        a.Status == ConstantEnum.KeyResultStatus.In_Progress
                    ),
                    TotalCompleted = g.Count(a =>
                        a.Status == ConstantEnum.KeyResultStatus.Completed
                    ),
                    TotalNotStarted = g.Count(a =>
                        a.Status == ConstantEnum.KeyResultStatus.Not_Started
                    )
                })
                .ToListAsync();
        }

        public async Task<SemesterInternKeyResultsSummary> GetSemesterInternKeyResultsSummaryAsync(
            int semesterId,
            string internId
        )
        {
            // Validate semesterId
            if (semesterId <= 0)
            {
                throw new ArgumentException(
                    "Semester ID must be a positive number.",
                    nameof(semesterId)
                );
            }

            try
            {
                // Get a single intern in the given semester
                var intern = await _context
                    .Users.AsNoTracking()
                    .Where(i => i.SemesterId == semesterId && i.Id == internId)
                    .Select(i => new { i.Id, i.UserName })
                    .FirstOrDefaultAsync();

                if (intern == null)
                {
                    throw new InvalidOperationException(
                        "No intern found for the given semester and intern ID."
                    );
                }

                // Get attendance summary for the intern
                var attendancesSummaryIntern = await _context
                    .Attendances.Where(a => a.InternId == intern.Id)
                    .GroupBy(a => new { a.Date.Year, a.Date.Month })
                    .Select(g => new MonthlyInternAttendance
                    {
                        Year = g.Key.Year,
                        Month = g.Key.Month,
                        TotalAttendanced = g.Count(a => a.Status == 1),
                        TotalAbsent = g.Count(a => a.Status == 4),
                        TotalLateIn = g.Count(a => a.Status == 2),
                        TotalSoonOut = g.Count(a => a.Status == 3)
                    })
                    .ToListAsync();

                // Get key results summary for the intern
                var internKeyResultsSummary = await _context
                    .KeyResults.Where(a => a.Assign == intern.Id)
                    .GroupBy(a => new { a.CreatedAt.Year, a.CreatedAt.Month })
                    .Select(g => new MonthlyInternKeyResults
                    {
                        Year = g.Key.Year,
                        Month = g.Key.Month,
                        TotalFailed = g.Count(a => a.Status == ConstantEnum.KeyResultStatus.Failed),
                        TotalInProgress = g.Count(a =>
                            a.Status == ConstantEnum.KeyResultStatus.In_Progress
                        ),
                        TotalCompleted = g.Count(a =>
                            a.Status == ConstantEnum.KeyResultStatus.Completed
                        ),
                        TotalNotStarted = g.Count(a =>
                            a.Status == ConstantEnum.KeyResultStatus.Not_Started
                        )
                    })
                    .ToListAsync();

                // Return the summary for the single intern
                return new SemesterInternKeyResultsSummary
                {
                    InternId = intern.Id,
                    InternName = intern.UserName,
                    KeyResultsSummary = internKeyResultsSummary,
                    AttendancesSumaryIntern = attendancesSummaryIntern
                };
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException(
                    "An error occurred while retrieving the semester intern key results summary.",
                    ex
                );
            }
        }
    }

    public class SemesterNameSummary
    {
        public List<MonthlyInternKeyResults> KeyResultsSummary { get; set; }
        public List<MonthlyInternAttendance> AttendancesSumary { get; set; }
    }

    public class SemesterInternKeyResultsSummary
    {
        public string InternId { get; set; }
        public string InternName { get; set; } // Assuming there is a name or similar property in Intern model
        public List<MonthlyInternKeyResults> KeyResultsSummary { get; set; }
        public List<MonthlyInternAttendance> AttendancesSumaryIntern { get; set; }
    }

    public class MonthlyInternKeyResults
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int TotalNotStarted { get; set; }
        public int TotalInProgress { get; set; }
        public int TotalCompleted { get; set; }
        public int TotalFailed { get; set; }
    }

    public class MonthlyKeyResultsSummary
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int TotalNotStarted { get; set; }
        public int TotalInProgress { get; set; }
        public int TotalCompleted { get; set; }
        public int TotalFailed { get; set; }
    }

    public class MonthlyAttendanceSummary
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int TotalAttendanced { get; set; }
        public int TotalAbsent { get; set; }
        public int TotalLateIn { get; set; }
        public int TotalSoonOut { get; set; }
    }

    public class MonthlyInternAttendance
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int TotalAttendanced { get; set; }
        public int TotalAbsent { get; set; }
        public int TotalLateIn { get; set; }
        public int TotalSoonOut { get; set; }
    }
}
