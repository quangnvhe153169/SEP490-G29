﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using Microsoft.EntityFrameworkCore;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class ObjectiveRepository : BaseRepository<Objective>, IObjectiveRepository
    {
        private readonly ILogger<ObjectiveRepository> _logger;

        public ObjectiveRepository(
            LabInternManagementDBcontext context,
            ILogger<ObjectiveRepository> logger
        )
            : base(context)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task AddObjective(Objective objective)
        {
            if (objective == null)
            {
                throw new ArgumentNullException(nameof(objective));
            }
            Context.Set<Objective>().Add(objective);
            Context.SaveChanges();
        }

        public async Task DeleteObjective(int objectiveId)
        {
            var objective = await Context
                .Set<Objective>()
                .Include(o => o.KeyResults)
                .SingleOrDefaultAsync(o => o.ObjectiveId == objectiveId);
            if (objective == null)
            {
                // Optionally, handle the case where the objective is not found
                throw new Exception("Objective not found");
            }
            // Remove dependent Discussions
            // Remove dependent KeyResults
            Context.Set<KeyResult>().RemoveRange(objective.KeyResults);

            // Remove the objective itself
            Context.Set<Objective>().Remove(objective);

            // Save changes to persist the deletion
            await Context.SaveChangesAsync();
        }

        public async Task<List<Objective>> GetAllObjectives()
        {
            return await Context.Set<Objective>().Include(o => o.KeyResults).ToListAsync();
        }

        public async Task<Objective> GetObjectiveByObjectiveId(int objectiveId)
        {
            return await Context.Set<Objective>().FindAsync(objectiveId);
        }

        public async Task<List<Objective>> GetObjectiveByUserId(string userId)
        {
            return await Context
                .Set<Objective>()
                .Where(o => o.CreateBy.Equals(userId))
                .ToListAsync();
        }

        public async Task<List<Objective>> SearchObjective(string searchTerm)
        {
            try
            {
                // Log the incoming searchTerm for debugging
                var query = Context.Set<Objective>().AsQueryable();
                if (string.IsNullOrEmpty(searchTerm?.Trim().Trim('\"')))
                {
                    return await query.ToListAsync();
                }

                DateTime? startDate = null;
                DateTime? endDate = null;

                // Attempt to parse searchTerm as DateTime to determine date range filter
                if (DateTime.TryParse(searchTerm, out DateTime parsedDate))
                {
                    startDate = parsedDate.Date;
                    endDate = parsedDate.Date.AddDays(1).AddTicks(-1); // End of the day
                }

                // Apply filters based on the determined type
                if (startDate.HasValue)
                {
                    query = query.Where(o => o.StartDate >= startDate.Value);
                }
                else
                {
                    // Filter by searchTerm if it's not a date
                    query = query.Where(o =>
                        o.Title.Contains(searchTerm)
                        || o.Description.Contains(searchTerm)
                        || o.User.FirstName.Contains(searchTerm)
                        || o.User.LastName.Contains(searchTerm)
                    );
                }

                if (endDate.HasValue)
                {
                    query = query.Where(o => o.EndDate <= endDate.Value);
                }

                var results = await query.ToListAsync();

                return results;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred while searching objectives: {ex}");
                throw;
            }
        }

        public async Task UpdateObjective(Objective updatedObjective)
        {
            try
            {
                // Attach the updated entity to the context
                Context.Attach(updatedObjective);

                // Get the entry for the existing entity
                var entry = Context.Entry(updatedObjective);

                // Specify the properties you want to skip updating
                var propertiesToSkip = new HashSet<string>
                {
                    nameof(Objective.TeamId),
                    nameof(Objective.CreatedAt),
                    nameof(Objective.CreateBy),
                    nameof(Objective.ObjectiveId)
                };

                // Iterate through all properties and set their modification state
                foreach (var property in entry.Properties)
                {
                    property.IsModified = !propertiesToSkip.Contains(property.Metadata.Name);
                }

                await Context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                // Handle the concurrency exception
                var entry = ex.Entries.Single();
                var clientValues = (Objective)entry.Entity;
                var databaseEntry = entry.GetDatabaseValues();
                if (databaseEntry == null)
                {
                    throw new Exception(
                        "Unable to save changes. The objective was deleted by another user."
                    );
                }
                else
                {
                    var databaseValues = (Objective)databaseEntry.ToObject();

                    // Update the RowVersion to the new value
                    clientValues.RowVersion = (byte[])databaseValues.RowVersion;

                    // Inform the user of the conflict and handle accordingly
                    throw new DbUpdateConcurrencyException(
                        "The objective you attempted to edit was modified by another user after you got the original values. Your changes have been refreshed with the current values."
                    );
                }
            }
        }
    }
}
