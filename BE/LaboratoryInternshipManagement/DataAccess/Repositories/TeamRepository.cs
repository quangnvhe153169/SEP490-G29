﻿using LaboratoryInternshipManagement.Controllers;
using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using Microsoft.EntityFrameworkCore;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class TeamRepository : BaseRepository<Team>, ITeamRepository
    {
        private readonly ILogger<UserController> _logger;

        public TeamRepository(LabInternManagementDBcontext context, ILogger<UserController> logger)
            : base(context)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task AddTeam(Team team)
        {
            if (team == null)
            {
                throw new ArgumentNullException(nameof(team), "The team cannot be null.");
            }

            try
            {
                await Context.Set<Team>().AddAsync(team);
                await Context.SaveChangesAsync();
            }
            catch (DbUpdateException dbEx)
            {
                _logger.LogError(
                    $"Database update error: {dbEx.Message}\n{dbEx.InnerException?.Message}",
                    dbEx
                );
                throw new Exception(
                    "An error occurred while saving the team to the database.",
                    dbEx
                );
            }
            catch (Exception ex)
            {
                _logger.LogError(
                    $"An unexpected error occurred: {ex.Message}\n{ex.InnerException?.Message}",
                    ex
                );
                throw new Exception("An unexpected error occurred while saving the team.", ex);
            }
        }

        public async Task DeleteTeam(int teamId)
        {
            var team = Context.Set<Team>().Find(teamId);
            if (team != null)
            {
                Context.Set<Team>().Remove(team);
                await Context.SaveChangesAsync(); // Save changes to persist the deletion
            }
            // Optionally, throw an exception or handle if team is not found
        }

        public async Task<List<Team>> GetAllTeam()
        {
            return await Context.Set<Team>().ToListAsync();
        }

        public async Task<Team> GetTeamByName(string name)
        {
            return await Context.Set<Team>().SingleOrDefaultAsync(t => t.Name == name);
        }

        public async Task<Team> GetTeamByTeamId(int teamId)
        {
            return await Context.Set<Team>().FindAsync(teamId);
        }

        public async Task<List<Team>> SearchTeam(string searchTerm)
        {
            var query = Context.Set<Team>().AsQueryable();

            if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                query = query.Where(t => t.Name.Contains(searchTerm));
            }

            return await query.ToListAsync();
        }

        public async Task UpdateTeam(Team team)
        {
            // Assuming team is attached to the context or tracked
            Context.Entry(team).State = EntityState.Modified;
            await Context.SaveChangesAsync(); // Save changes to persist the update
        }
    }
}
