﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using LaboratoryInternshipManagement.Models.DTOs;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(LabInternManagementDBcontext context)
            : base(context) { }

        public User GetUserByEmail(string email)
        {
            return Context.Set<User>().Where(x => x.Email.Equals(email)).FirstOrDefault();
        }

        public User GetUserById(int id)
        {
            return Context.Set<User>().Where(x => x.Id.Equals(id)).FirstOrDefault();
        }

        public List<User> GetUserByName(string name)
        {
            return Context
                .Set<User>()
                .Where(x => x.FirstName.Equals(name) || x.LastName.Equals(name))
                .ToList();
        }

        public List<User> ListUserInLab(int lapID)
        {
            return Context.Set<User>().Where(x => x.LabId.Equals(lapID)).ToList();
        }

        public List<User> SearchUserInLab(string keySearch, int labID)
        {
            var getIntern =
                from user in Context.Users
                where
                    (
                        user.Id == keySearch
                        || user.FirstName.Equals(keySearch)
                        || user.LastName.Equals(keySearch)
                        || user.RollName.Equals(keySearch)
                        || user.Email.Equals(keySearch)
                    )
                    && user.LabId == labID
                select user;
            return getIntern.ToList();
        }

        public async Task<IPagedList<dynamic>> SearchInternLab(SearchInternDTO searchDTO, int labId)
        {
            var internRoleId = await Context
                .Roles.Where(r => r.Name == "Intern")
                .Select(r => r.Id)
                .FirstOrDefaultAsync();

            var query = Context.Users.Where(u =>
                u.LabId == labId
                && Context.UserRoles.Any(ur => ur.UserId == u.Id && ur.RoleId == internRoleId)
            );

            if (!string.IsNullOrEmpty(searchDTO.Keyword))
            {
                var keyword = searchDTO.Keyword.ToLower();
                query = query.Where(u =>
                    u.FirstName.ToLower().Contains(keyword)
                    || u.LastName.ToLower().Contains(keyword)
                    || u.Email.ToLower().Contains(keyword)
                );
            }

            if (searchDTO.isActive.HasValue)
            {
                query = query.Where(u => u.Status == searchDTO.isActive.Value);
            }

            if (searchDTO.SemesterId.HasValue)
            {
                query = query.Where(u => u.SemesterId == searchDTO.SemesterId.Value);
            }

            var interns = await query
                .Select(u => new
                {
                    Id = u.Id,
                    FullName = $"{u.FirstName} {u.LastName}".Trim(),
                    RollName = u.RollName,
                    BirthDate = u.BirthDate.ToString("dd/MM/yyyy"),
                    Status = u.Status,
                    Avatar = u.Avatar != null
                        ? $"data:image/jpeg;base64,{Convert.ToBase64String(u.Avatar)}"
                        : null,
                    Email = u.Email,
                    Semester = u.Semester.SemesterName,
                    Mentor = new
                    {
                        Fullname = $"{u.Mentor.FirstName} {u.Mentor.LastName}",
                        Id = u.Mentor.Id
                    }
                })
                .AsNoTracking()
                .ToPagedListAsync(searchDTO.PageNumber, searchDTO.PageSize);

            return interns;
        }

        public async Task<IPagedList<dynamic>> SearchMentorLab(SearchDTO searchDTO, int labId)
        {
            var mentorRoleId = await Context
                .Roles.Where(r => r.Name == "Mentor")
                .Select(r => r.Id)
                .FirstOrDefaultAsync();

            var query = Context.Users.Where(u =>
                u.LabId == labId
                && Context.UserRoles.Any(ur =>
                    ur.UserId.Equals(u.Id) && ur.RoleId.Equals(mentorRoleId)
                )
            );

            if (!string.IsNullOrEmpty(searchDTO.Keyword))
            {
                var keyword = searchDTO.Keyword.ToLower().Trim();
                query = query.Where(u =>
                    u.FirstName.ToLower().Contains(keyword)
                    || u.LastName.ToLower().Contains(keyword)
                    || u.Email.ToLower().Contains(keyword)
                );
            }

            if (searchDTO.isActive.HasValue)
            {
                query = query.Where(u => u.Status == searchDTO.isActive.Value);
            }

            var mentors = await query
                .Select(m => new
                {
                    Id = m.Id,
                    FullName = $"{m.FirstName} {m.LastName}".Trim(),
                    BirthDate = m.BirthDate.ToString("dd/MM/yyyy"),
                    Status = m.Status,
                    Avatar = m.Avatar != null
                        ? $"data:image/jpeg;base64,{Convert.ToBase64String(m.Avatar)}"
                        : null,
                    Email = m.Email,
                    NumbOfInterns = m.Interns.Count
                })
                .ToPagedListAsync(searchDTO.PageNumber, searchDTO.PageSize);

            return mentors;
        }
    }
}
