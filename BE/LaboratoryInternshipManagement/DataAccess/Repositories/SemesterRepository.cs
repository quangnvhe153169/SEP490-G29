﻿using LaboratoryInternshipManagement.Controllers;
using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using Microsoft.EntityFrameworkCore;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class SemesterRepository : BaseRepository<Semester>, ISemesterRepository
    {
        public SemesterRepository(LabInternManagementDBcontext context)
            : base(context) { }

        // Create
        public async Task AddAsync(SemesterDTO dto)
        {
            try
            {
                // Check if there are any active semesters
                var activeSemesters = await Context
                    .Semesters.Where(s => dto.StartDate < s.EndDate && dto.EndDate > s.StartDate)
                    .ToListAsync();

                if (activeSemesters.Any())
                {
                    throw new InvalidOperationException(
                        "Cannot create a new semester that in period of other semester(s)"
                    );
                }

                // Check if a semester with the same name already exists
                var existingSemester = await Context.Semesters.AnyAsync(s =>
                    s.SemesterName.Trim().ToLower() == dto.SemesterName.Trim().ToLower()
                );

                if (existingSemester)
                {
                    throw new InvalidOperationException(
                        "A semester with the same name already exists."
                    );
                }

                var semester = new Semester
                {
                    SemesterName = dto.SemesterName,
                    StartDate = dto.StartDate,
                    EndDate = dto.EndDate
                };

                await Context.Semesters.AddAsync(semester);
                await Context.SaveChangesAsync();
            }
            catch (InvalidOperationException ex)
            {
                // Log the exception if necessary
                throw new InvalidOperationException(ex.Message);
            }
            catch (Exception ex)
            {
                // Log the exception if necessary
                throw new Exception("Error adding semester: " + ex.Message);
            }
        }

        // Delete
        public async Task DeleteAsync(SemesterDTO dto)
        {
            var existingSemester = await Context
                .Semesters.Include(s => s.Interns) // Include related interns
                .FirstOrDefaultAsync(s => s.SemesterId == dto.SemesterId);

            if (existingSemester == null)
            {
                throw new KeyNotFoundException("Semester not found.");
            }

            // Check if the semester has started
            if (existingSemester.StartDate <= DateTime.Now)
            {
                throw new InvalidOperationException(
                    "Cannot delete a semester that has already started."
                );
            }

            // Check if there are any interns related to this semester
            if (existingSemester.Interns.Any())
            {
                throw new InvalidOperationException(
                    "Cannot delete semester with associated interns."
                );
            }

            Context.Semesters.Remove(existingSemester);
            await Context.SaveChangesAsync();
        }

        // Read All
        public async Task<List<Semester>> GetAllAsync()
        {
            try
            {
                return await Context.Semesters.ToListAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Error fetching semesters: " + ex.Message);
            }
        }

        // Read by Id
        public async Task<Semester> GetSemesterByIdAsync(int id)
        {
            try
            {
                return await Context.Semesters.SingleOrDefaultAsync(s => s.SemesterId == id);
            }
            catch (Exception ex)
            {
                throw new Exception("Error fetching semester: " + ex.Message);
            }
        }

        // Update
        public async Task UpdateAsync(SemesterDTO dto)
        {
            try
            {
                var existingSemester = await Context.Semesters.FindAsync(dto.SemesterId);
                if (existingSemester != null)
                {
                    // Validate dates
                    if (dto.StartDate > dto.EndDate)
                    {
                        throw new ArgumentException("Start Date cannot be later than End Date.");
                    }

                    // Update existing semester with validated data
                    existingSemester.SemesterName = dto.SemesterName;
                    existingSemester.StartDate = dto.StartDate;
                    existingSemester.EndDate = dto.EndDate;

                    await Context.SaveChangesAsync(); // Use SaveChangesAsync instead of SaveChanges
                }
                else
                {
                    throw new KeyNotFoundException("Semester not found.");
                }
            }
            catch (Exception ex)
            {
                // Re-throw with more detailed message if needed
                throw new Exception("Error updating semester: " + ex.Message);
            }
        }

        public async Task<IEnumerable<InternDTO>> GetInternsBySemesterIdAsync(
            int semesterId,
            string mentorId,
            int labId
        )
        {
            try
            {
                // Fetch the interns and their related data first

                var interns = await Context
                    .Users.Include(s => s.Mentor)
                    .Include(s => s.Laboratory)
                    .Include(s => s.Reports)
                    .Where(u =>
                        u.SemesterId == semesterId && u.MentorId == mentorId && u.LabId == labId
                    )
                    .ToListAsync();

                var reports = await Context
                    .Reports.Where(u => u.SemesterId == semesterId && u.MentorId == mentorId)
                    .ToListAsync();

                // Check if interns are found before processing
                if (!interns.Any())
                {
                    throw new InvalidOperationException("No interns found for this semester.");
                }

                // Process the data in memory to generate unique StaffId
                var internDTOs = interns
                    .Select(intern => new InternDTO
                    {
                        Id = intern.Id,
                        InternId = intern.Id,
                        NameIntern = $"{intern.FirstName} {intern.LastName}",
                        BirthDate = intern.BirthDate,
                        RollName = intern.RollName,
                        MentorId = intern.Mentor?.Id,
                        Email = intern.Email,
                        NameMentor =
                            intern.Mentor != null
                                ? $"{intern.Mentor.FirstName} {intern.Mentor.LastName}"
                                : "N/A",
                        SemesterId = semesterId,
                        ReportCount = reports.Where(x => x.InternId.Equals(intern.Id)).Count(),
                        NameLab = intern.Laboratory?.LabName ?? "N/A",
                        StaffId = GenerateUniqueStaffId(intern.Email)
                    })
                    .ToList();

                return internDTOs;
            }
            catch (InvalidOperationException knfEx)
            {
                throw new InvalidOperationException(knfEx.Message, knfEx);
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message, ex);
            }
        }

        private string GenerateUniqueStaffId(string email)
        {
            var baseStaffId = email.Split('@')[0];
            var staffId = baseStaffId;
            int counter = 1;

            while (Context.Reports.Any(r => r.StaffId == staffId))
            {
                staffId = $"{baseStaffId}{counter}";
                counter++;
            }

            return staffId;
        }

        public async Task<Semester> GetCurrentSemesterAsync()
        {
            DateTime today = DateTime.Today;
            return await Context.Semesters.FirstOrDefaultAsync(x =>
                x.StartDate <= today && x.EndDate >= today
            );
        }
    }

    public class InternDTO
    {
        public string Id { get; set; }
        public string NameIntern { get; set; }
        public string InternId { get; set; }
        public DateTime BirthDate { get; set; }
        public string RollName { get; set; }
        public string? MentorId { get; set; }
        public string? NameMentor { get; set; }
        public int? SemesterId { get; set; }
        public string? NameLab { get; set; }
        public string StaffId { get; set; }
        public int? LabId { get; set; }
        public string? Email { get; set; }
        public int ReportCount { get; set; }
    }
}
