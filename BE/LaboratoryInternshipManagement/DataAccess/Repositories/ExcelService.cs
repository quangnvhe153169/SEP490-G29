﻿using LaboratoryInternshipManagement.Models;
using OfficeOpenXml;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class ExcelService
    {
        public void AddDataAndGenerateCopy(
            ExcelPackage package,
            List<Report> reports,
            Semester semester
        )
        {
            // Lấy worksheet đầu tiên
            var worksheet = package.Workbook.Worksheets[0];

            // Giả sử dữ liệu bắt đầu từ hàng 14
            int startRow = 14;
            int currentRow = startRow;

            // Thêm dữ liệu vào các hàng tiếp theo
            foreach (var report in reports)
            {
                worksheet.Cells[currentRow, 2].Value = report.RollName;
                worksheet.Cells[currentRow, 3].Value = report.StaffId;
                worksheet.Cells[currentRow, 4].Value = report.NameIntern;
                worksheet.Cells[currentRow, 5].Value = report.NameLab;
                worksheet.Cells[currentRow, 6].Value = report.NameMentor;
                worksheet.Cells[currentRow, 7].Value = report.Allowance;
                worksheet.Cells[currentRow, 8].Value = report.ComOfCompany;
                worksheet.Cells[currentRow, 9].Value = report.MajorSkill;
                worksheet.Cells[currentRow, 10].Value = report.SoftSkill;
                worksheet.Cells[currentRow, 11].Value = report.Attitude;
                worksheet.Cells[currentRow, 12].Value = report.FinalResult;
                currentRow++;
            }

            // Thêm thông tin kỳ học vào các ô tương ứng
            worksheet.Cells["D8"].Value = semester.StartDate.ToString("dd/MM/yyyy");
            worksheet.Cells["D7"].Value = reports[0].NameLab;
            worksheet.Cells["G8"].Value = semester.EndDate.ToString("dd/MM/yyyy");

            // Lưu các thay đổi vào ExcelPackage
            package.Save();
        }

        public void AddDataAndGenerateCopyV1(
            ExcelPackage package,
            List<Report> reports,
            Semester semester
        )
        {
            // Get the first worksheet
            var worksheet = package.Workbook.Worksheets[0];

            // Assume data starts from row 8 based on the screenshot
            int startRow = 8;
            int currentRow = startRow;

            // Add data to subsequent rows
            foreach (var report in reports)
            {
                worksheet.Cells[currentRow, 2].Value = report.RollName; // Student ID
                worksheet.Cells[currentRow, 3].Value = report.StaffId; // Staff ID
                worksheet.Cells[currentRow, 4].Value = report.NameIntern; // Full Name
                worksheet.Cells[currentRow, 5].Value = report.NameLab; // Division
                worksheet.Cells[currentRow, 6].Value = report.NameMentor; // Line Manager
                // Calculate the evaluation based on the scores
                string evaluation = CalculateEvaluation(
                    report.MajorSkill,
                    report.SoftSkill,
                    report.Attitude
                );

                // Assign the evaluation to the appropriate cell based on the logic
                if (evaluation == "Excellent")
                {
                    worksheet.Cells[currentRow, 7].Value = "X"; // Excellent
                }
                else if (evaluation == "Very Good")
                {
                    worksheet.Cells[currentRow, 8].Value = "X"; // Very Good
                }
                else if (evaluation == "Good")
                {
                    worksheet.Cells[currentRow, 9].Value = "X"; // Good
                }
                else if (evaluation == "Average")
                {
                    worksheet.Cells[currentRow, 10].Value = "X"; // Average
                }
                else if (evaluation == "Poor")
                {
                    worksheet.Cells[currentRow, 11].Value = "X"; // Poor
                }

                worksheet.Cells[currentRow, 12].Value = report.ComOfCompany; // Company Comments

                worksheet.Cells["D4"].Value = report.NameLab;
                // Move to the next row for the next student
                currentRow++;
            }

            // Insert semester dates
            worksheet.Cells["F4"].Value = semester.StartDate.ToString("dd/MM/yyyy");
            worksheet.Cells["H4"].Value = semester.EndDate.ToString("dd/MM/yyyy");

            // Save changes to the Excel file
            package.Save();
        }

        private string CalculateEvaluation(double majorSkill, double softSkill, double attitude)
        {
            // Ensure the total score does not exceed 30
            double totalScore = majorSkill + softSkill + attitude;
            if (totalScore > 30)
            {
                throw new InvalidOperationException(
                    "Total score exceeds the maximum allowed points of 30."
                );
            }

            // Define your logic for evaluation based on a total of 30 points
            if (totalScore >= 27)
                return "Excellent";
            else if (totalScore >= 24)
                return "Very Good";
            else if (totalScore >= 21)
                return "Good";
            else if (totalScore >= 18)
                return "Average";
            else
                return "Poor";
        }
    }
}
