﻿using System.ComponentModel.DataAnnotations;

namespace LaboratoryInternshipManagement.DataAccess.ValidationAttributes
{
    [AtLeastOneRequired("OTP", "FaceVector")]
    public class RequiredIfOneExistsAttendance : ValidationAttribute
    {
        private readonly string _otherProperty;

        public RequiredIfOneExistsAttendance(string otherProperty)
        {
            _otherProperty = otherProperty;
        }

        protected override ValidationResult IsValid(
            object value,
            ValidationContext validationContext
        )
        {
            var otherProperty = validationContext.ObjectType.GetProperty(_otherProperty);
            if (otherProperty == null)
            {
                return new ValidationResult($"Unknown property: {_otherProperty}");
            }
            var otherValue = otherProperty.GetValue(validationContext.ObjectInstance, null);

            if (value == null && otherValue == null)
            {
                return new ValidationResult(
                    $"Either {validationContext.MemberName} or {_otherProperty} must be provided."
                );
            }

            return ValidationResult.Success;
        }
    }
}
