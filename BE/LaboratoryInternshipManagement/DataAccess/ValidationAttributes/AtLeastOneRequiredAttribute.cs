﻿using System.ComponentModel.DataAnnotations;

namespace LaboratoryInternshipManagement.DataAccess.ValidationAttributes
{
    public class AtLeastOneRequiredAttribute : ValidationAttribute
    {
        private readonly string[] _propertyNames;

        public AtLeastOneRequiredAttribute(params string[] propertyNames)
        {
            _propertyNames = propertyNames;
        }

        protected override ValidationResult IsValid(
            object value,
            ValidationContext validationContext
        )
        {
            foreach (var propertyName in _propertyNames)
            {
                var propertyValue = validationContext
                    .ObjectType.GetProperty(propertyName)
                    ?.GetValue(validationContext.ObjectInstance, null);
                if (propertyValue != null && !string.IsNullOrEmpty(propertyValue.ToString()))
                {
                    return ValidationResult.Success;
                }
            }

            return new ValidationResult(
                "At least one of the fields must be provided: " + string.Join(", ", _propertyNames)
            );
        }
    }
}
