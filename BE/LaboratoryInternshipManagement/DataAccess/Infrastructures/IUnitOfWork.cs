﻿using LaboratoryInternshipManagement.DataAccess.IRepository;

namespace LaboratoryInternshipManagement.DataAccess.Infrastructures
{
    public interface IUnitOfWork : IDisposable
    {
        public IAttendenceRepository AttendenceRepository { get; }
        public ITicketsRepository TicketsRepository { get; }

        int SaveChanges();
    }
}
