﻿using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.DataAccess.Repositories;
using LaboratoryInternshipManagement.Models;
using Microsoft.AspNetCore.Identity;

namespace LaboratoryInternshipManagement.DataAccess.Infrastructures
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly LabInternManagementDBcontext _context;
        private readonly UserManager<User> _userManager;

        public UnitOfWork(LabInternManagementDBcontext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        private IAttendenceRepository _attendenceRepository;
        private ITicketsRepository _ticketsRepository;
        public IAttendenceRepository AttendenceRepository
        {
            get
            {
                if (_attendenceRepository == null)
                {
                    _attendenceRepository = new AttendenceRepository(_context, _userManager);
                }
                return _attendenceRepository;
            }
        }

        public ITicketsRepository TicketsRepository
        {
            get
            {
                if (_ticketsRepository == null)
                {
                    _ticketsRepository = new TicketsRepository(_context);
                }
                return _ticketsRepository;
            }
        }

        public LabInternManagementDBcontext MyDbContext => _context;

        public void Dispose()
        {
            _context.Dispose();
        }

        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
    }
}
