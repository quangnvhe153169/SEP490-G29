﻿namespace LaboratoryInternshipManagement.DataAccess.IRepository
{
    public interface IImportHistoryRepository<T> where T : class
    {
        Task<T> AddImportHistory(T entity);
        Task<IEnumerable<T>> GetAllImportHistory();
    }
}
