﻿namespace LaboratoryInternshipManagement.DataAccess.IRepository
{
    public interface IImportCSVRepository
    {
        public IEnumerable<T> ReadCSV<T>(Stream file);

        //public ImportIntern GetInternByEmail(string email);
        public bool IsValidEmail(string Email);
        public string GenerateRandomString(int length);
        public string ComputeSha256Hash(string rawData);
    }
}
