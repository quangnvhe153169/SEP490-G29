﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.Models;

namespace LaboratoryInternshipManagement.DataAccess.IRepository
{
    public interface IObjectiveRepository : IBaseRepository<Objective>
    {
        Task<List<Objective>> GetAllObjectives();
        Task<Objective> GetObjectiveByObjectiveId(int objectiveId);
        Task<List<Objective>> GetObjectiveByUserId(string userId);
        Task AddObjective(Objective objective);
        Task UpdateObjective(Objective objective);
        Task DeleteObjective(int objectiveId);
        Task<List<Objective>> SearchObjective(string searchTerm);
    }
}
