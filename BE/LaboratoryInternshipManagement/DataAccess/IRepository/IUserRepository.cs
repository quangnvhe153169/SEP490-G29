﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.Models;
using LaboratoryInternshipManagement.Models.DTOs;
using X.PagedList;

namespace LaboratoryInternshipManagement.DataAccess.IRepository
{
    public interface IUserRepository : IBaseRepository<User>
    {
        public User GetUserById(int id);
        public List<User> GetUserByName(string name);
        public User GetUserByEmail(string email);
        public List<User> ListUserInLab(int lapID);
        Task<IPagedList<dynamic>> SearchInternLab(SearchInternDTO searchDTO, int labId);
        Task<IPagedList<dynamic>> SearchMentorLab(SearchDTO searchDTO, int labId);
    }
}
