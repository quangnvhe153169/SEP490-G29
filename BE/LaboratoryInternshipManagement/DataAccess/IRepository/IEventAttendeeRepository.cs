﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.Models;

namespace LaboratoryInternshipManagement.DataAccess.IRepository
{
    public interface IEventAttendeeRepository : IBaseRepository<EventAttendee>
    {
        Task<List<EventAttendee>> GetAllEventAttendee();
        Task<List<EventAttendee>> GetEventAttendeeByEventId(int eventId);
        Task<EventAttendee> GetEventAttendeeByEventAttendeeId(int eventAttendeeId);
        Task<List<EventAttendee>> GetEventAttendeeByUserId(string userId);
        Task AddEventAttendee(EventAttendee eventAttendee);
        Task UpdateEventAttendee(EventAttendee eventAttendee);
        Task DeleteEventAttendee(int eventAttendeeId);
        Task<List<EventAttendee>> SearchEventAttendee(string searchTerm);
    }
}
