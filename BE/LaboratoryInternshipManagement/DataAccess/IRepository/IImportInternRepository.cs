﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.Models;

namespace LaboratoryInternshipManagement.DataAccess.IRepository
{
    public interface IImportInternRepository : IBaseRepository<ImportIntern>
    {
        Task<List<ImportIntern>> GetAllImportInterns();
        Task<ImportIntern> GetImportInternByImportInternId(int iportInternId);
        Task AddImportIntern(ImportIntern importIntern);
        Task UpdateImportIntern(ImportIntern importIntern);
        Task DeleteImportIntern(int importInternId);
        Task<List<ImportIntern>> SearchImportIntern(string searchTerm);
        Task InitializeAssignments();
        Task<List<ImportIntern>> GetUnassignedInternByLab(int labId);
        Task SaveValidRecords(
            List<InternCSV> validRecords,
            Dictionary<string, string> internSendMails
        );
        Task<(List<InternCSV>, List<ImportInternError>)> ValidateCSV(IFormFile file);
        Task SendMailToIntern(Dictionary<string, string> Interns);
    }
}
