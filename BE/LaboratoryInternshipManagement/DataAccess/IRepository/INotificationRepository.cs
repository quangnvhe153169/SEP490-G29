﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.Models;

namespace LaboratoryInternshipManagement.DataAccess.IRepository
{
    public interface INotificationRepository : IBaseRepository<Notification>
    {
        Task<List<Notification>> GetAllNotificationsAsync();
        Task<Notification> GetNotificationByIdAsync(int notificationId);
        Task<List<Notification>> GetNotificationsByUserIdAsync(string userId);
        Task<List<Notification>> GetUnreadNotificationsAsync();
        Task<List<Notification>> GetNotificationsByTypeAsync(string type);
        Task<List<Notification>> GetNotificationsByUserInvolvementAsync(string userId);

        /*Task AddNotificationAsync(Notification notification);*/
        Task AddNotificationToUsersAsync(Notification notification, List<string> userIds);
        Task UpdateNotificationAsync(Notification notification);
        Task DeleteNotificationAsync(int notificationId);
    }
}
