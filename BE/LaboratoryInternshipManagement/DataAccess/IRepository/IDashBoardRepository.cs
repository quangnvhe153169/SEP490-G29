﻿using LaboratoryInternshipManagement.DataAccess.Repositories;

namespace LaboratoryInternshipManagement.DataAccess.IRepository
{
    public interface IDashBoardRepository
    {
        Task<List<MonthlyAttendanceSummary>> GetMonthlyAttendanceSummaryAsync();
        Task<int> CountInternsBySemesterNameAsync(string semesterName);
        Task<List<MonthlyInternAttendance>> GetMonthlyInternAttendanceSummaryAsync(string internId);

        Task<int> CountInternsBySemesterNameAndMentorAsync(string semesterName, string mentorId);

        Task<List<MonthlyInternKeyResults>> GetMonthlyInternKeyResultSummaryAsync(string internId);
        Task<List<MonthlyKeyResultsSummary>> GetMonthlyKeyResultSummaryAsync();
        Task<int> CountInternsBySemesterNameLabManageAsync(
            string semesterName,
            string labManagerId
        );

        Task<int> CountObjectivesByInternIdAsync(string internId);
        Task<SemesterInternKeyResultsSummary> GetSemesterInternKeyResultsSummaryAsync(
            int semesterId,
            string internId
        );

        Task<IEnumerable<InternDTO>> GetInternsBySemesterNameAndMentorAsync(
            string semesterName,
            string mentorId
        );
        Task<IEnumerable<InternDTO>> GetSummaryInternLabManageAsync(
            string semesterName,
            string labManagerId
        );
        Task<SemesterNameSummary> GetSemesterSummaryLabAsync(
            string semesterName,
            string labmanageId
        );
        Task<SemesterNameSummary> GetSemesterSummaryAsync(string semesterName, string mentorId);
        Task<IEnumerable<dynamic>> GetMentors(int labId);
    }
}
