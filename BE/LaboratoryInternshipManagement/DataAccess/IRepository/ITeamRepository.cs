﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.Models;

namespace LaboratoryInternshipManagement.DataAccess.IRepository
{
    public interface ITeamRepository : IBaseRepository<Team>
    {
        Task<List<Team>> GetAllTeam();
        Task<Team> GetTeamByTeamId(int teamId);
        Task<Team> GetTeamByName(string name);
        Task AddTeam(Team team);
        Task UpdateTeam(Team team);
        Task DeleteTeam(int teamId);
        Task<List<Team>> SearchTeam(string searchTerm);
    }
}
