﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.Models;

namespace LaboratoryInternshipManagement.DataAccess.IRepository
{
    public interface IProgressPerformanceRepository : IBaseRepository<ProgressPerformance>
    {
        Task<List<ProgressPerformance>> GetAllProgressPerformance();
        Task<ProgressPerformance> GetProgressPerformanceByProgressPerformanceId(int progressPerformanceId);
        Task<List<ProgressPerformance>> GetProgressPerformanceByKeyResultId(string keyResultId);
        Task AddProgressPerformance(ProgressPerformance progressPerformanceId);
        Task UpdateProgressPerformance(ProgressPerformance progressPerformanceId);
        Task DeleteProgressPerformance(int progressPerformanceIdId);
        Task<List<ProgressPerformance>> SearchProgressPerformance(string searchTerm);
    }
}
