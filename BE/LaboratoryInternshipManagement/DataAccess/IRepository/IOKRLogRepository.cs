﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.Models;

namespace LaboratoryInternshipManagement.DataAccess.IRepository
{
    public interface IOKRLogRepository : IBaseRepository<OKRLog>
    {
        Task<List<OKRLog>> GetAllOKRLog();
        Task<List<OKRLog>> GetOKRLogByTeamId(int teamId);
        Task<List<OKRLog>> GetOKRLogByUserId(string userId);
        Task AddOKRLog(OKRLog okrLog);
    }
}
