﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.Models;

namespace LaboratoryInternshipManagement.DataAccess.IRepository
{
    public interface IEventRepository : IBaseRepository<Event>
    {
        Task<List<Event>> GetAllEvent();
        Task<Event> GetEventByEventId(int eventId);
        Task<List<Event>> GetEventByLocation(string location);
        Task<List<Event>> GetEventByUserId(string userId);
        Task AddEvent(Event @event);
        Task UpdateEvent(Event @event);
        Task DeleteEvent(int eventId);
        Task<List<Event>> SearchEvent(string searchTerm);
    }
}
