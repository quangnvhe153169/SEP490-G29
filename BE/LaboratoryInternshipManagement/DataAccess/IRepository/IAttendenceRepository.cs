﻿using LaboratoryInternshipManagement.Controllers;
using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.Models;
using static LaboratoryInternshipManagement.DataAccess.Repositories.AttendenceRepository;

namespace LaboratoryInternshipManagement.DataAccess.IRepository
{
    public interface IAttendenceRepository : IBaseRepository<Attendance>
    {
        public Attendance GetByInternId(string internId);
        public List<Attendance> ListAttendence();

        public List<Attendance> ListAllAttendenceInLab(long lapID);

        public List<Attendance> ListAttendanceByDay(string date);
        public List<ListAttendanceOutputDto> ListAttendanceByRoleItern(string InternId, int month);
        public List<ListAttendanceOutputDto> ListAttendanceByRoleLab(
            ListAttendanceByRoleLabInputDto inputDto
        );
        public Attendance UpdateStatusAttendance(UpdateStatusAttendanceInputDto inputDto);
        public bool UpdateTimeLaboratoryAttendance(UpdateTimeLaboratoryAttendanceInput inputDto);
        Task<dynamic> GetListAttendance(
            string userId,
            InputGetAttendanceLabManager inputDto,
            int labId
        );
        Task<dynamic> GetListAttendanceIntern(User user, InputGetAttendanceLabManager inputDto);
    }
}
