﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.Models;

namespace LaboratoryInternshipManagement.DataAccess.IRepository
{
    public interface ITeamMembershipRepository : IBaseRepository<TeamMembership>
    {
        Task<List<TeamMembership>> GetAllTeamMembership();
        Task<List<TeamMembership>> GetTeamMembershipByTeamId_UserId(int teamId, string userId);
        Task<List<TeamMembership>> GetTeamMembershipsByUserId(string userId);
        Task AddTeamMembership(TeamMembership teamMembership);
        Task UpdateTeamMembership(TeamMembership teamMembership);
        Task DeleteTeamMembership(int teamId, string userId);
        Task<List<TeamMembership>> SearchTeamMembership(string searchTerm);
    }
}
