﻿using LaboratoryInternshipManagement.Controllers;
using LaboratoryInternshipManagement.DataAccess.Repositories;
using LaboratoryInternshipManagement.Models;

namespace LaboratoryInternshipManagement.DataAccess.IRepository
{
    public interface ISemesterRepository
    {
        Task<List<Semester>> GetAllAsync();
        Task AddAsync(SemesterDTO dto);
        Task UpdateAsync(SemesterDTO dto);
        Task DeleteAsync(SemesterDTO dto);
        Task<Semester> GetSemesterByIdAsync(int id);
        Task<Semester> GetCurrentSemesterAsync();
        Task<IEnumerable<InternDTO>> GetInternsBySemesterIdAsync(
            int semesterId,
            string mentorId,
            int labId
        );
    }
}
