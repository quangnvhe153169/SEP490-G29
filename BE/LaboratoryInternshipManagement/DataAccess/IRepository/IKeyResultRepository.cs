﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.Models;

namespace LaboratoryInternshipManagement.DataAccess.IRepository
{
    public interface IKeyResultRepository : IBaseRepository<KeyResult>
    {
        Task<List<KeyResult>> GetAllKeyResult();
        Task<KeyResult> GetKeyResultByKeyResultId(int KeyResultId);
        Task<List<KeyResult>> GetKeyResultByObjectiveId(int objectiveId);
        Task<List<KeyResult>> GetKeyResultByAssignUserId(string userId);
        Task AddKeyResult(KeyResult keyResult);
        Task UpdateKeyResult(KeyResult keyResult);
        Task DeleteKeyResult(int keyResultId);
        Task<List<KeyResult>> SearchKeyResult(string searchTerm);
    }
}
