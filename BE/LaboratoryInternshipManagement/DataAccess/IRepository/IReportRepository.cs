﻿using LaboratoryInternshipManagement.Controllers;
using LaboratoryInternshipManagement.Models;
using LaboratoryInternshipManagement.Models.DTOs;
using X.PagedList;

namespace LaboratoryInternshipManagement.DataAccess.IRepository
{
    public interface IReportRepository
    {
        Task<List<Report>> GetAllReports(string mentorId);

        void AddReport(ReportDTO report);

        void UpdateReport(ReportDTO r);

        void DeleteReport(int reportId);

        Report GetReportByName(string name);

        Report GetReportById(int id);

        IEnumerable<Report> GetReportsByRollName(string rollname);

        Task<ReportDTO> GetReportByIdAsync(int reportId);
        Task<List<Report>> GetReportsBySemesterIdAndDateRange(
            int semesterId,
            DateTime startDate,
            DateTime endDate
        );
        Task<string> GetMentorIdByUserIdAsync(string userId);
        Task<List<Report>> GetFinalReportsBySemesterId(int semesterId);
        Task<List<Report>> GetMidTermReportBySemsterId(int semesterId);
        Task<List<Report>> GetLabFinalReportsBySemesterId(int semesterId, string labName);
        Task<List<Report>> GetLabMidTermReportBySemsterId(int semesterId, string labName);
        Task<IPagedList<dynamic>> GetReportGroupByInterns(
            string mentorId,
            SearchReportDTO searchReport
        );
    }
}
