﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.Models;

namespace LaboratoryInternshipManagement.DataAccess.IRepository
{
    public interface ICommentRepository : IBaseRepository<Comment>
    {
        Task<List<Comment>> GetAllCommentsAsync();
        Task<Comment> GetCommentByCommentIdAsync(int commentId);
        Task<List<Comment>> GetCommentsByUserIdAsync(string userId);
        Task AddCommentAsync(Comment comment);
        Task UpdateCommentAsync(Comment comment);
        Task DeleteCommentAsync(int commentId);
        Task<List<Comment>> SearchCommentsAsync(string searchTerm);
    }
}
