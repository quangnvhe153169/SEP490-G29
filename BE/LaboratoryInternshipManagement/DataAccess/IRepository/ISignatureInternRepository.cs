﻿using LaboratoryInternshipManagement.Models;

namespace LaboratoryInternshipManagement.DataAccess.IRepository
{
    public interface ISignatureInternRepository
    {
        SignatureIntern GetSignatureByEmail(string email);
        void AddSignature(SignatureIntern signatureIntern);
        void DeleteSignature(string email);
        SignatureIntern GetSignatureByHashedSignature(string hashedSignature);
    }
}
