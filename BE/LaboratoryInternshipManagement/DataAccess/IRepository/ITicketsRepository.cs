﻿using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.Models;
using static LaboratoryInternshipManagement.Controllers.TicketsController;
using static LaboratoryInternshipManagement.DataAccess.Repositories.TicketsRepository;

namespace LaboratoryInternshipManagement.DataAccess.IRepository
{
    public interface ITicketsRepository : IBaseRepository<Tickets>
    {
        public List<GetAllTicketByCreatorOutput> GetAllTicketByAssignee(GetTicketsInput getTicketsInput);
        public List<GetAllTicketByCreatorOutput> GetAllTicketByCreator(string Creator);
        public GetAllTicketByCreatorOutput GetTicketById(long Id);
        public int UpdateTicketById(Tickets ticket);
        public int getNumIsInAllowedLimit(string InternId);
        public bool AddTicketAttendance(string CreateId, string AssignId, string DateRequire, string Title, string Description);
    }
}
