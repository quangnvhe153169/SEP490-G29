﻿using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace LaboratoryInternshipManagement.DataAccess.Filters
{
    public class IpRestrictionFilter : IAsyncActionFilter
    {
        private readonly List<IPNetwork2> _allowedNetwork;

        public IpRestrictionFilter(List<string> allowedSubnet)
        {
            var allowedNetworks = new List<IPNetwork2>();
            foreach (var subnet in allowedSubnet)
            {
                allowedNetworks.Add(IPNetwork2.Parse(subnet));
            }
            _allowedNetwork = allowedNetworks;
        }

        public async Task OnActionExecutionAsync(
            ActionExecutingContext context,
            ActionExecutionDelegate next
        )
        {
            string clientIp = context
                .HttpContext.Request.Headers["X-Forwarded-For"]
                .FirstOrDefault();
            if (clientIp != null)
            {
                var ips = clientIp.Split(",");

                foreach (var network in _allowedNetwork)
                {
                    if (clientIp != null && network.Contains(IPAddress.Parse(ips[0])))
                    {
                        await next();
                    }
                }
            }

            context.Result = new ForbidResult(); // Trả về 403 Forbidden
            return;
        }
    }
}
