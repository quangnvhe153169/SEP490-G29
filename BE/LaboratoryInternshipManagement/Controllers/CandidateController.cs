﻿using System.IdentityModel.Tokens.Jwt;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using AutoMapper;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using LaboratoryInternshipManagement.Services.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LaboratoryInternshipManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "System Admin,Lab Manager,Mentor")]
    public class CandidateController : ControllerBase
    {
        private readonly UserManager<User> _userManager;
        private readonly IConfiguration _config;
        private readonly ILogger<CandidateController> _logger;
        private readonly INotificationService _notificationService;
        private readonly IImportInternRepository _importInternRepository;
        private readonly IMapper _mapper;
        private readonly IEmailService _emailService;
        private readonly LabInternManagementDBcontext _context;
        private readonly IImportCSVRepository _importCSVRepository;

        public CandidateController(
            IConfiguration configuration,
            IImportInternRepository importInternRepository,
            IEmailService emailService,
            IMapper mapper,
            UserManager<User> userManager,
            ILogger<CandidateController> logger,
            INotificationService notificationService,
            LabInternManagementDBcontext context,
            IImportCSVRepository importCSVRepository
        )
        {
            _importInternRepository =
                importInternRepository
                ?? throw new ArgumentNullException(nameof(importInternRepository));
            _userManager = userManager;
            _notificationService = notificationService;
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _emailService = emailService;
            _context = context;
            _importCSVRepository = importCSVRepository;
            _config = configuration;
        }

        // GET: api/Candidate/GetAssignCandidate
        /// <summary>
        /// Returns interns assigned to the current logged-in mentor.
        /// </summary>
        /// <returns>A list of interns assigned to the current logged-in mentor.</returns>
        [HttpGet("GetAssignCandidate")]
        public async Task<ActionResult<IEnumerable<ImportInternDTO>>> GetAssignCandidate()
        {
            try
            {
                var user = await _userManager.FindByIdAsync(
                    User.FindFirstValue(JwtRegisteredClaimNames.Jti)
                );
                if (user == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }
                var assignedInterns = await _importInternRepository.GetAllImportInterns();
                assignedInterns = assignedInterns
                    .Where(i => i.MentorID != null && i.MentorID.Equals(user.Id))
                    .ToList();
                var assignedInternDTOs = new List<ImportInternDTO>();
                if (assignedInterns.Count > 0)
                {
                    assignedInternDTOs = _mapper.Map<List<ImportInternDTO>>(assignedInterns);
                }
                return Ok(new { Message = "assignedInterns", ImportInterns = assignedInternDTOs });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new { Message = "systemError", ExceptionMessage = ex.Message }
                );
            }
        }

        // GET: api/Candidate/GetCV
        /// <summary>
        /// Returns the intern's CV as a base64 encoded string.
        /// </summary>
        /// <param name="internId">The ID of the intern.</param>
        /// <returns>An ActionResult containing the base64 encoded CV if successful, or an error message if not.</returns>
        [HttpGet("GetCV/{internId}")]
        public async Task<ActionResult> GetCV(int internId)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(
                    User.FindFirstValue(JwtRegisteredClaimNames.Jti)
                );
                if (user == null)
                {
                    _logger.LogWarning($"User not found.");
                    return NotFound(new { Message = "noUserFound" });
                }

                var intern = await _importInternRepository.GetImportInternByImportInternId(
                    internId
                );
                if (intern == null)
                {
                    return NotFound(new { Message = "internNotFound" });
                }
                var base64PDF = "";

                var base64CV = Convert.ToBase64String(intern.CVDescript);
                if (!string.IsNullOrEmpty(base64CV))
                {
                    base64PDF = $"data:application/pdf;base64,{base64CV}";
                }
                return Ok(new { base64PDF });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        // GET: api/Candidate/GetUnAssignCandidate
        /// <summary>
        /// Returns interns not assigned to mentors.
        /// </summary>
        /// <returns>A list of interns not assigned to mentors.</returns>
        [Authorize(Roles = "Lab Manager")]
        [HttpGet("GetUnAssignCandidate")]
        public async Task<ActionResult<IEnumerable<ImportInternDTO>>> GetUnAssignCandidate()
        {
            try
            {
                var labId = int.Parse(User.FindFirstValue("labId"));
                var unassignedInterns = await _importInternRepository.GetUnassignedInternByLab(
                    labId
                );
                var unassignedInternDTOs = _mapper.Map<List<ImportInternDTO>>(unassignedInterns);
                return Ok(new { ImportInterns = unassignedInternDTOs });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        // GET: api/Candidate/GetUnAssignCandidate
        /// <summary>
        /// Returns interns not assigned to mentors.
        /// </summary>
        /// <returns>A list of interns not assigned to mentors.</returns>
        [Authorize(Roles = "Lab Manager")]
        [HttpGet("GetMentors")]
        public async Task<IActionResult> GetMentors()
        {
            try
            {
                var users = await _context.Users.ToListAsync();
                var lab = await _context.Laboratories.FindAsync(
                    int.Parse(User.FindFirstValue("labId"))
                );
                var mentors = users
                    .Where(user =>
                        _userManager.IsInRoleAsync(user, "Mentor").Result
                        && (
                            user.LabId == int.Parse(User.FindFirstValue("labId"))
                            || lab.LabManagerId == user.Id
                        )
                    )
                    .Select(u => new
                    {
                        Id = u.Id,
                        Name = u.FirstName + " " + u.LastName,
                        Email = u.Email,
                    })
                    .ToList();
                return Ok(new { Mentors = mentors });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        // POST: api/Candidate/AssignToMentor/{mentorId}
        /// <summary>
        /// Assign candidate intern to mentors.
        /// </summary>
        [Authorize(Roles = "Lab Manager")]
        [HttpPost("AssignToMentor/{mentorId}")]
        public async Task<IActionResult> AssignImportInternTo(
            string mentorId,
            [FromBody] int internId
        )
        {
            try
            {
                var labId = int.Parse(User.FindFirstValue("labId"));
                var unassignedInterns = await _importInternRepository.GetUnassignedInternByLab(
                    labId
                );
                var mentor = await _userManager
                    .Users.Where(user => user.Id.Equals(mentorId))
                    .FirstOrDefaultAsync();
                if (await _userManager.IsInRoleAsync(mentor, "Mentor") != true)
                {
                    return NotFound(new { Message = "noUserFound" });
                }
                var intern = unassignedInterns.Where(u => u.Id == internId).FirstOrDefault();
                intern.MentorID = mentorId;
                await _importInternRepository.UpdateImportIntern(intern);

                return Ok(new { Message = "assignSuccess" });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        // POST: api/Candidate/ApproveOrRejectInterns
        /// <summary>
        /// Approves or rejects a list of interns assigned to the mentor.
        /// </summary>
        /// <param name="mentorId">The ID of the mentor.</param>
        /// <param name="internApprovals">A list of intern IDs and their approval statuses.</param>
        /// <returns>An IActionResult indicating the result of the operation.</returns>
        [Authorize(Roles = "Mentor")]
        [HttpPost("ApproveOrRejectInterns")]
        public async Task<IActionResult> ApproveOrRejectInterns(
            [FromBody] List<InternApproval> internApprovals
        )
        {
            using var transaction = await _context.Database.BeginTransactionAsync();
            try
            {
                var mentorId = User.FindFirstValue("jti");
                var mentor = _context.Users.SingleOrDefault(m => m.Id.Equals(mentorId));
                var lab = _context.Laboratories.Find(int.Parse(User.FindFirstValue("labId")));
                foreach (var approval in internApprovals)
                {
                    var intern = await _importInternRepository.GetImportInternByImportInternId(
                        approval.InternId
                    );

                    if (intern == null)
                    {
                        continue;
                    }

                    if (!intern.MentorID.Equals(mentorId))
                    {
                        continue;
                    }

                    if (approval.Approved)
                    {
                        await CreateUser(intern, lab, mentor);

                        await _importInternRepository.DeleteImportIntern(intern.Id);
                    }
                    else
                    {
                        intern.MentorID = null;
                        await _importInternRepository.UpdateImportIntern(intern);
                    }
                }
                await transaction.CommitAsync();
                return Ok(new { Message = "updateSuccess" });
            }
            catch (InvalidOperationException ex)
            {
                return StatusCode(409, new { Message = ex.Message });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        // Helper classes
        public class InternApproval
        {
            public int InternId { get; set; }
            public bool Approved { get; set; }
        }

        private async Task CreateUser(ImportIntern intern, Laboratory lab, User mentor)
        {
            var today = DateTime.Now;
            var bufferDays = 10; // Số ngày đệm (ví dụ từ 7 đến 10)

            var currentSemester = await _context
                .Semesters.Where(s => s.StartDate <= today && s.EndDate >= today) // Lấy học kỳ hiện tại
                .OrderBy(s => s.StartDate)
                .FirstOrDefaultAsync();

            if (currentSemester != null)
            {
                // Kiểm tra xem kỳ hiện tại còn dưới 7-10 ngày không
                if ((currentSemester.EndDate - today).TotalDays <= bufferDays)
                {
                    // Nếu còn dưới 7-10 ngày, lấy kỳ tiếp theo
                    var nextSemester = await _context
                        .Semesters.Where(s => s.StartDate > currentSemester.EndDate) // Lấy kỳ sau
                        .OrderBy(s => s.StartDate)
                        .FirstOrDefaultAsync();

                    // Trả về kỳ tiếp theo nếu có, nếu không thì giữ nguyên kỳ hiện tại
                    currentSemester = nextSemester ?? currentSemester;
                }
            }
            else
            {
                throw new InvalidOperationException("noSemesterFoundFuture");
            }

            var password = _importCSVRepository.GenerateRandomString(8);
            var userId = Guid.NewGuid().ToString();

            var user = new User
            {
                Id = userId,
                Email = intern.Email.ToLower(),
                NormalizedEmail = intern.Email.ToLower(),
                BirthDate = intern.DOB ?? DateTime.MinValue,
                LastName = intern.LastName,
                FirstName = intern.FirstName,
                RollName = intern.RollNumber,
                NotifyToMail = true, // Assuming NotifyToMail should be true
                MentorId = intern.MentorID,
                LabId = lab.Id,
                SemesterId = currentSemester?.SemesterId,
                PasswordHash = new PasswordHasher<User>().HashPassword(null, password)
            };
            var role = new IdentityUserRole<string> { UserId = userId, RoleId = "4" };

            await _context.Users.AddAsync(user);
            await _context.UserRoles.AddAsync(role);
            await _context.SaveChangesAsync();
            string emailBody = GetEmbeddedEmailTemplate(
                "LaboratoryInternshipManagement.Resources.account-sending.html"
            );
            string loginUrl = _config["AllowedView"];
            string mentorName = $"{mentor.FirstName} {mentor.LastName}".Trim();
            mentorName = string.IsNullOrEmpty(mentorName) ? mentor.Email.Split('@')[0] : mentorName;
            // Thay thế các placeholder bằng thông tin cụ thể
            emailBody = emailBody
                .Replace("[name]", user.FirstName + " " + user.LastName)
                .Replace("[labName]", lab.LabName)
                .Replace("[mentor]", mentorName)
                .Replace("[roles]", "Thực tập sinh")
                .Replace("[user.Email]", user.Email)
                .Replace("[password]", password)
                .Replace("[Đội ngũ hỗ trợ LIM]", "SEP490-G29 SU24")
                .Replace("[link]", $"<a href=\"{loginUrl}\">Tại Đây</a>");
            ;

            _ = Task.Run(async () =>
            {
                // Gửi email
                await _emailService.SendEmailAsync(
                    user.Email,
                    "Đăng nhập vào hệ thống LIM",
                    emailBody
                );
            });
        }

        private string GetEmbeddedEmailTemplate(string resourceName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceStream = assembly.GetManifestResourceStream(resourceName);

            if (resourceStream == null)
            {
                throw new FileNotFoundException($"Embedded resource '{resourceName}' not found.");
            }

            using (var reader = new StreamReader(resourceStream, Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }
    }

    // DTO class
    public class ImportInternDTO
    {
        public int Id { get; set; }
        public string MentorID { get; set; }
        public int LabId { get; set; }
        public string RollNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
        public string Email { get; set; }
        public string LabName { get; set; }
        public string CVName { get; set; }
        public byte[] CVDescript { get; set; }
    }
}
