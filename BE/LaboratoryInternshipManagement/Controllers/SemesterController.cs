﻿using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LaboratoryInternshipManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SemesterController : ControllerBase
    {
        private readonly ISemesterRepository _semesterRepository;
        private readonly LabInternManagementDBcontext _context;

        public SemesterController(
            ISemesterRepository semesterRepository,
            LabInternManagementDBcontext context
        )
        {
            _semesterRepository = semesterRepository;
            _context = context;
        }

        // GET: api/Semester
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SemesterDTO>>> GetAllSemesters()
        {
            var semesters = await _semesterRepository.GetAllAsync();
            var semesterDTOs = semesters
                .Select(s => new SemesterDTO
                {
                    SemesterId = s.SemesterId,
                    SemesterName = s.SemesterName,
                    StartDate = s.StartDate,
                    EndDate = s.EndDate
                })
                .ToList();

            return Ok(semesterDTOs);
        }

        // GET: api/Semester/{id}
        [Authorize]
        [HttpGet("{id}")]
        public async Task<ActionResult<SemesterDTO>> GetSemesterById(int id)
        {
            var semester = await _semesterRepository.GetSemesterByIdAsync(id);

            if (semester == null)
            {
                return NotFound();
            }

            var semesterDTO = new SemesterDTO
            {
                SemesterId = semester.SemesterId,
                SemesterName = semester.SemesterName,
                StartDate = semester.StartDate,
                EndDate = semester.EndDate
            };

            return Ok(semesterDTO);
        }

        [HttpPut("{id?}")]
        [Authorize(Roles = "System Admin")]
        public async Task<IActionResult> UpsertSemester(int? id, [FromBody] SemesterDTO semesterDto)
        {
            if (!ModelState.IsValid)
                return UnprocessableEntity(ModelState);

            try
            {
                Semester existingSemester = null;

                // Check if we're updating an existing semester or creating a new one
                if (id.HasValue)
                {
                    existingSemester = _context.Semesters.FirstOrDefault(s =>
                        s.SemesterId == id.Value
                    );
                }

                if (existingSemester == null)
                {
                    // If no semester exists with the given ID, create a new one
                    await _semesterRepository.AddAsync(semesterDto);
                    return CreatedAtAction(
                        nameof(GetSemesterById),
                        new { id = semesterDto.SemesterId },
                        new
                        {
                            message = "Semester added successfully!",
                            id = semesterDto.SemesterId
                        }
                    );
                }
                else
                {
                    // Update the existing semester
                    existingSemester.SemesterName = semesterDto.SemesterName;
                    existingSemester.StartDate = semesterDto.StartDate;
                    existingSemester.EndDate = semesterDto.EndDate;

                    _context.SaveChanges();
                    return Ok(new { Message = "Semester updated successfully." });
                }
            }
            catch (InvalidOperationException ex)
            {
                // Handle conflict errors
                return Conflict(new { Message = ex.Message });
            }
            catch (Exception ex)
            {
                // Handle unexpected errors
                return StatusCode(
                    500,
                    new { Message = "An unexpected error occurred.", Details = ex.Message }
                );
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "System Admin")]
        public async Task<ActionResult> DeleteSemester(int id)
        {
            try
            {
                var dto = new SemesterDTO { SemesterId = id };
                await _semesterRepository.DeleteAsync(dto);
                return NoContent(); // Successful deletion
            }
            catch (InvalidOperationException ex)
            {
                return Conflict(new { message = ex.Message });
            }
            catch (KeyNotFoundException ex)
            {
                return NotFound(new { message = ex.Message });
            }
            catch (Exception ex)
            {
                // Log the exception for troubleshooting
                // _logger.LogError(ex, "An error occurred while deleting the semester.");
                return StatusCode(500, new { message = "An unexpected error occurred." });
            }
        }

        [HttpGet("{id}/interns")]
        [Authorize]
        public async Task<IActionResult> GetInternsBySemesterId(int id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Unauthorized("User is not authenticated.");
            }
            string mentorId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);
            int labId = int.Parse(User.FindFirstValue("labId"));
            if (string.IsNullOrEmpty(mentorId))
            {
                return Unauthorized(new { Message = "Unauthorized access.", Status = "error" });
            }
            try
            {
                var interns = await _semesterRepository.GetInternsBySemesterIdAsync(
                    id,
                    mentorId,
                    labId
                );

                return Ok(interns);
            }
            catch (InvalidOperationException ex)
            {
                return Conflict(new { Message = ex.Message, Status = "error" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { ex.Message });
            }
        }

        [HttpGet("currentSemester")]
        public async Task<IActionResult> GetCurrentSemester()
        {
            try
            {
                var semester = await _semesterRepository.GetCurrentSemesterAsync();

                if (semester == null)
                {
                    return NotFound("noSemesterFound");
                }

                return Ok(semester);
            }
            catch (InvalidOperationException ex)
            {
                return Conflict(new { Message = ex.Message, Status = "error" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { ex.Message });
            }
        }

        [HttpGet("laboratories")]
        public async Task<IActionResult> GetLabs()
        {
            try
            {
                var laboratories = await _context.Laboratories.ToListAsync();

                return Ok(laboratories);
            }
            catch (InvalidOperationException ex)
            {
                return Conflict(new { Message = ex.Message, Status = "error" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { ex.Message });
            }
        }

        [HttpGet("lab/{id}")]
        public async Task<IActionResult> GetLab(int id)
        {
            try
            {
                var laboratory = await _context.Laboratories.FindAsync(id);

                return Ok(laboratory);
            }
            catch (InvalidOperationException ex)
            {
                return Conflict(new { Message = ex.Message, Status = "error" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { ex.Message });
            }
        }
    }

    public class SemesterDTO
    {
        public int SemesterId { get; set; }

        [Required(ErrorMessage = "Semester Name is required.")]
        [MaxLength(100, ErrorMessage = "Semester Name cannot exceed 100 characters.")]
        public string SemesterName { get; set; }

        [Required(ErrorMessage = "Start Date is required.")]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = "End Date is required.")]
        [DataType(DataType.Date)]
        [GreaterThan("StartDate", ErrorMessage = "End Date must be after Start Date.")]
        public DateTime EndDate { get; set; }
    }

    // Custom validation attribute to ensure EndDate is after StartDate
    public class GreaterThanAttribute : ValidationAttribute
    {
        private readonly string _startDateProperty;

        public GreaterThanAttribute(string startDateProperty)
        {
            _startDateProperty = startDateProperty;
        }

        protected override ValidationResult IsValid(
            object value,
            ValidationContext validationContext
        )
        {
            var endDate = (DateTime)value;
            var startDateProperty = validationContext.ObjectType.GetProperty(_startDateProperty);
            if (startDateProperty == null)
            {
                return new ValidationResult($"Unknown property: {_startDateProperty}");
            }

            var startDate = (DateTime)startDateProperty.GetValue(validationContext.ObjectInstance);
            if (endDate <= startDate)
            {
                return new ValidationResult(ErrorMessage ?? "End Date must be after Start Date.");
            }

            return ValidationResult.Success;
        }
    }
}
