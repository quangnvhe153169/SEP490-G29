﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.DataAccess.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace LaboratoryInternshipManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashBoardController : ControllerBase
    {
        private readonly IDashBoardRepository _repository;

        public DashBoardController(IDashBoardRepository repository)
        {
            _repository = repository;
        }

        [HttpGet("intern-summary")]
        public async Task<IActionResult> GetInternSummary()
        {
            // Ensure the request is authenticated
            if (!User.Identity.IsAuthenticated)
            {
                return Unauthorized("User is not authenticated.");
            }

            // Extract the intern ID from JWT claims
            var internId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);

            // Check if the internId was retrieved successfully
            if (string.IsNullOrEmpty(internId))
            {
                return Unauthorized("Intern ID not found in claims.");
            }

            try
            {
                // Fetch monthly attendance summary from the repository
                var attendanceSummary = await _repository.GetMonthlyInternAttendanceSummaryAsync(
                    internId
                );

                // Fetch objectives count from the repository
                var totalObjectives = await _repository.CountObjectivesByInternIdAsync(internId);

                // Fetch monthly key results summary from the repository
                var keyResultsSummary = await _repository.GetMonthlyInternKeyResultSummaryAsync(
                    internId
                );

                // Create the response DTO
                var internSummary = new InternSummaryDTO
                {
                    AttendanceSummary = attendanceSummary,
                    TotalObjectives = totalObjectives,
                    KeyResultsSummary = keyResultsSummary
                };

                // Return the summary
                return Ok(internSummary);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "An error occurred while processing the request.");
            }
        }

        [HttpGet("monthly_summary")]
        public async Task<IActionResult> GetMonthlyAttendanceSummary()
        {
            try
            {
                var summary = await _repository.GetMonthlyAttendanceSummaryAsync();
                // Đảm bảo rằng summary là một mảng
                //if (summary == null || !summary.Any())
                //{
                //    return NotFound("No data found.");
                //}
                return Ok(summary);
            }
            catch (Exception ex)
            {
                // Log exception details
                Console.WriteLine($"Exception occurred: {ex.Message}");
                return StatusCode(500, "An error occurred while processing the request.");
            }
        }

        [HttpGet("summarywithinterns/{semesterName}")]
        public async Task<ActionResult<InternCountDTO>> GetSemesterSummaryWithInterns(
            string semesterName
        )
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Unauthorized("User is not authenticated.");
            }

            string mentorId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);
            if (string.IsNullOrEmpty(mentorId))
            {
                return Unauthorized(new { Message = "Unauthorized access.", Status = "error" });
            }

            try
            {
                // Fetch the intern count by semester name
                var count = await _repository.CountInternsBySemesterNameAndMentorAsync(
                    semesterName,
                    mentorId
                );

                // Fetch intern details
                var interns = await _repository.GetInternsBySemesterNameAndMentorAsync(
                    semesterName,
                    mentorId
                );

                // Fetch the semester summary
                var summary = await _repository.GetSemesterSummaryAsync(semesterName, mentorId);

                // Create and return the combined response DTO
                var internCountDTO = new InternCountDTO
                {
                    SemesterName = semesterName,
                    Count = count,
                    Interns = interns ?? new List<InternDTO>(), // Ensure interns is not null
                    Summary = summary // Include the semester summary
                };

                return Ok(internCountDTO);
            }
            catch (ArgumentException argEx)
            {
                // Handle argument exceptions (e.g., invalid input)
                return BadRequest(new { message = argEx.Message });
            }
            catch (KeyNotFoundException knfEx)
            {
                // Handle not found exceptions (e.g., semester not found)
                return NotFound(new { message = knfEx.Message });
            }
            catch (Exception ex)
            {
                // Handle other exceptions (e.g., database errors)
                return StatusCode(
                    StatusCodes.Status500InternalServerError,
                    new { message = "An error occurred while fetching data.", error = ex.Message }
                );
            }
        }

        [HttpGet("summarylabmanage/{semesterName}")]
        public async Task<ActionResult<InternCountDTO>> GetSemesterSummaryLabManage(
            string semesterName
        )
        {
            // Check if the user is authenticated
            if (!User.Identity.IsAuthenticated)
            {
                return Unauthorized("User is not authenticated.");
            }

            // Retrieve the lab manager's ID from the JWT claims
            string labmanageId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);
            int labId = int.Parse(User.FindFirstValue("labId"));
            if (string.IsNullOrEmpty(labmanageId))
            {
                return Unauthorized(new { Message = "Unauthorized access.", Status = "error" });
            }

            try
            {
                // Fetch the count of interns by semester name and lab manager ID
                var count = await _repository.CountInternsBySemesterNameLabManageAsync(
                    semesterName,
                    labmanageId
                );

                // Fetch intern details for the specified semester and lab
                var interns = await _repository.GetSummaryInternLabManageAsync(
                    semesterName,
                    labmanageId
                );

                var mentors = await _repository.GetMentors(labId);

                // Fetch the semester summary details
                var summary = await _repository.GetSemesterSummaryLabAsync(
                    semesterName,
                    labmanageId
                );

                // Create and return the combined response DTO
                var internCountDTO = new InternCountDTO
                {
                    SemesterName = semesterName,
                    Count = count,
                    Interns = interns ?? new List<InternDTO>(), // Ensure interns is not null
                    Mentors = mentors,
                    Summary = summary // Include the semester summary
                };

                return Ok(internCountDTO);
            }
            catch (ArgumentException argEx)
            {
                // Handle argument exceptions (e.g., invalid input)
                return BadRequest(new { message = argEx.Message });
            }
            catch (KeyNotFoundException knfEx)
            {
                // Handle not found exceptions (e.g., semester not found)
                return NotFound(new { message = knfEx.Message });
            }
            catch (Exception ex)
            {
                // Handle other exceptions (e.g., database errors)
                return StatusCode(
                    StatusCodes.Status500InternalServerError,
                    new { message = "An error occurred while fetching data.", error = ex.Message }
                );
            }
        }

        [HttpGet("Semester/{semesterId}/Intern/{internId}/Summary")]
        public async Task<IActionResult> GetSemesterInternKeyResultsSummary(
            int semesterId,
            string internId
        )
        {
            // Validate input
            if (semesterId <= 0 || string.IsNullOrWhiteSpace(internId))
            {
                return BadRequest(new { message = "Invalid semester ID or intern ID." });
            }

            try
            {
                // Call the repository or service method
                var results = await _repository.GetSemesterInternKeyResultsSummaryAsync(
                    semesterId,
                    internId
                );

                // Check if results are found
                if (results == null)
                {
                    return NotFound(
                        new { message = "No data found for the given semester and intern ID." }
                    );
                }

                // Return the results with 200 OK
                return Ok(results);
            }
            catch (ArgumentException ex)
            {
                // Handle argument exceptions with 400 Bad Request
                return BadRequest(new { message = ex.Message });
            }
            catch (InvalidOperationException ex)
            {
                // Handle invalid operation exceptions with 404 Not Found
                return NotFound(new { message = ex.Message });
            }
            catch (Exception ex)
            {
                // Log the exception details for debugging (optional)
                // _logger.LogError(ex, "An unexpected error occurred.");

                // Handle unexpected errors with 500 Internal Server Error
                return StatusCode(
                    500,
                    new { message = "An unexpected error occurred while retrieving the data." }
                );
            }
        }
    }

    public class InternSummaryDTO
    {
        public List<MonthlyInternAttendance> AttendanceSummary { get; set; }
        public int TotalObjectives { get; set; }
        public List<MonthlyInternKeyResults> KeyResultsSummary { get; set; }
    }

    public class InternCountDTO
    {
        public string SemesterName { get; set; }
        public int Count { get; set; }
        public IEnumerable<InternDTO> Interns { get; set; }
        public IEnumerable<dynamic>? Mentors { get; set; }
        public SemesterNameSummary Summary { get; set; } // Added property
    }

    public class InternAttendanceSummary
    {
        public int TotalAbsent { get; set; }
        public int TotalLate { get; set; }
        public int TotalAttendance { get; set; }
    }
}
