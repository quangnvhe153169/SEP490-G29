﻿using System.Security.Claims;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using LaboratoryInternshipManagement.Models.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LaboratoryInternshipManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MemberController : ControllerBase
    {
        private readonly UserManager<User> _userManager;
        private readonly LabInternManagementDBcontext _context;
        private readonly ILogger<MemberController> _logger;
        private readonly IUserRepository _userRepository;

        public MemberController(
            UserManager<User> userManager,
            LabInternManagementDBcontext context,
            ILogger<MemberController> logger,
            IUserRepository userRepository
        )
        {
            _userManager = userManager;
            _context = context;
            _logger = logger;
            _userRepository = userRepository;
        }

        [Authorize(Roles = "Lab Manager")]
        [HttpGet("mentors")]
        public async Task<IActionResult> GetMentors([FromQuery] SearchDTO searchMentor)
        {
            try
            {
                var labId = int.Parse(User.FindFirstValue("labId"));

                var mentors = await _userRepository.SearchMentorLab(searchMentor, labId);

                var result = new
                {
                    Items = mentors,
                    PageNumber = mentors.PageNumber,
                    PageSize = mentors.PageSize,
                    TotalItemCount = mentors.TotalItemCount,
                    TotalPages = mentors.PageCount,
                    HasNextPage = mentors.HasNextPage,
                    HasPreviousPage = mentors.HasPreviousPage
                };

                // Trả về kết quả phân trang cùng với dữ liệu Mentor
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception acquired: {ex.Message}", ex);
                return BadRequest(new { Message = "systemError" });
            }
        }

        [Authorize(Roles = "Lab Manager")]
        [HttpGet("interns")]
        public async Task<IActionResult> GetInterns([FromQuery] SearchInternDTO searchIntern)
        {
            try
            {
                var labId = int.Parse(User.FindFirstValue("labId"));
                var interns = await _userRepository.SearchInternLab(searchIntern, labId);

                var result = new
                {
                    Items = interns, // hoặc data.Items tùy thuộc vào cách bạn trả về dữ liệu
                    PageNumber = interns.PageNumber,
                    PageSize = interns.PageSize,
                    TotalItemCount = interns.TotalItemCount,
                    TotalPages = interns.PageCount,
                    HasNextPage = interns.HasNextPage,
                    HasPreviousPage = interns.HasPreviousPage
                };

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception acquired: {ex}");
                return BadRequest(new { Message = "systemError" });
            }
        }

        [Authorize(Roles = "Lab Manager")]
        [HttpPut("changeStatus/{memberId}")]
        public async Task<IActionResult> ChangeStatus(string memberId)
        {
            try
            {
                var labId = int.Parse(User.FindFirstValue("labId"));

                var member = await _context
                    .Users.Where(u => u.LabId == labId && u.Id.Equals(memberId))
                    .FirstOrDefaultAsync();

                if (member == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }

                var isLabManagerOrAdmin =
                    await _userManager.IsInRoleAsync(member, "Lab Manager")
                    || await _userManager.IsInRoleAsync(member, "System Admin");

                if (isLabManagerOrAdmin)
                {
                    return Conflict(new { Message = "conflictUpdate" });
                }

                member.Status = !member.Status;
                _context.Users.Update(member);
                await _context.SaveChangesAsync();

                return Ok(new { Message = "updateSuccess" });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception acquired: {ex.Message}", ex);
                return BadRequest(new { Message = "systemError" });
            }
        }

        [Authorize(Roles = "Lab Manager")]
        [HttpPut("dispel/{memberId}")]
        public async Task<IActionResult> Dispel(string memberId)
        {
            try
            {
                var labId = int.Parse(User.FindFirstValue("labId"));

                var member = await _context
                    .Users.Where(u => u.LabId == labId && u.Id.Equals(memberId))
                    .FirstOrDefaultAsync();

                if (member == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }

                var isLabManagerOrAdmin =
                    await _userManager.IsInRoleAsync(member, "Lab Manager")
                    || await _userManager.IsInRoleAsync(member, "System Admin");

                if (isLabManagerOrAdmin)
                {
                    return Conflict(new { Message = "conflictUpdate" });
                }

                member.LabId = null;
                member.MentorId = null;
                _context.Users.Update(member);
                await _context.SaveChangesAsync();

                return Ok(new { Message = "updateSuccess" });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception acquired: {ex.Message}", ex);
                return BadRequest(new { Message = "systemError" });
            }
        }
    }
}
