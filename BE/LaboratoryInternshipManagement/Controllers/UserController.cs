﻿using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using LaboratoryInternshipManagement.Models;
using LaboratoryInternshipManagement.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LaboratoryInternshipManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserManager<User> _userManager;
        private readonly LabInternManagementDBcontext _context;
        private readonly IConfiguration _config;
        private readonly ILogger<UserController> _logger;
        private readonly GoogleEmailService _googleEmailService;

        public UserController(
            UserManager<User> userManager,
            IConfiguration config,
            ILogger<UserController> logger,
            LabInternManagementDBcontext context,
            GoogleEmailService googleEmailService
        )
        {
            _userManager = userManager;
            _config = config;
            _logger = logger;
            _context = context;
            _googleEmailService = googleEmailService;
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> GetUser()
        {
            try
            {
                var user = await _userManager.FindByIdAsync(
                    User.FindFirstValue(JwtRegisteredClaimNames.Jti)
                );
                if (user != null)
                {
                    var roles = await _userManager.GetRolesAsync(user);
                    return Ok(
                        new
                        {
                            Email = user?.Email,
                            FirstName = user?.FirstName,
                            LastName = user?.LastName,
                            Cv = (roles.Any(r => r.Equals("Intern")) && user?.Cv != null)
                                ? $"data:application/pdf;base64,{Convert.ToBase64String(user?.Cv)}"
                                : null,
                            Avatar = user?.Avatar != null
                                ? $"data:image/jpeg;base64,{Convert.ToBase64String(user?.Avatar)}"
                                : null,
                            NotifyToMail = user?.NotifyToMail,
                            Language = user?.Language
                        }
                    );
                }

                return NotFound(new { message = "noUserFound" });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return BadRequest(new { Message = "systemError" });
            }
        }

        [Authorize]
        [HttpGet("lab-info")]
        public async Task<IActionResult> GetLabUserInfo()
        {
            try
            {
                var user = await _userManager.FindByIdAsync(
                    User.FindFirstValue(JwtRegisteredClaimNames.Jti)
                );
                if (user != null)
                {
                    var lab = await _context.Laboratories.FirstOrDefaultAsync(x =>
                        x.Id == user.LabId
                    );
                    if (User.IsInRole("Lab Manager"))
                    {
                        lab = await _context.Laboratories.FirstOrDefaultAsync(x =>
                            x.LabManagerId.Equals(user.Id)
                        );
                    }

                    var roles = User
                        .Claims.Where(c => c.Type == ClaimTypes.Role)
                        .Select(c => c.Value)
                        .Distinct()
                        .ToList();

                    var fullname = user.FirstName + " " + user.LastName;

                    string resolvedFullname = string.IsNullOrEmpty(fullname.Trim())
                        ? string.IsNullOrEmpty(user?.Email)
                            ? "Admin"
                            : user.Email?.Split('@')[0]
                        : fullname;

                    if (User.IsInRole("Intern"))
                    {
                        var mentor = await _userManager
                            .Users.Where(x => x.Id.Equals(user.MentorId))
                            .Select(u => new
                            {
                                FirstName = u.FirstName,
                                LastName = u.LastName,
                                Email = u.Email
                            })
                            .SingleOrDefaultAsync();
                        var mentorName = mentor.FirstName + " " + mentor.LastName;

                        string resolvedMentorName = string.IsNullOrEmpty(mentorName.Trim())
                            ? mentor.Email?.Split('@')[0]
                            : mentorName;

                        return Ok(
                            new
                            {
                                User = new
                                {
                                    Fullname = resolvedFullname,
                                    LabName = lab?.LabName,
                                    Roles = roles,
                                    Mentor = resolvedMentorName
                                }
                            }
                        );
                    }

                    // Trả về kết quả
                    return Ok(
                        new
                        {
                            User = new
                            {
                                Fullname = resolvedFullname,
                                LabName = lab?.LabName,
                                Roles = roles
                            }
                        }
                    );
                }

                return NotFound(new { message = "noUserFound" });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return BadRequest(new { Message = "systemError" });
            }
        }

        [Authorize]
        [HttpPost("update")]
        public async Task<IActionResult> UpdateUser([FromForm] UserDTI userData)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = await _userManager.FindByIdAsync(
                        User.FindFirstValue(JwtRegisteredClaimNames.Jti)
                    );
                    if (user != null)
                    {
                        if (!string.IsNullOrEmpty(userData.NewPassword))
                        {
                            var hasher = new PasswordHasher<User>();
                            user.PasswordHash = hasher.HashPassword(null, userData.NewPassword);
                        }

                        var checkEmailUser = await _context.Users.AnyAsync(u =>
                            u.Email == userData.Email
                        );
                        var checkEmailCandidate = await _context.ImportInterns.AnyAsync(u =>
                            u.Email == userData.Email
                        );

                        if (
                            checkEmailCandidate
                            && checkEmailCandidate
                            && !user.Email.Equals(userData.Email)
                        )
                        {
                            return BadRequest(new { Message = "emailIsUsed" });
                        }

                        user.FirstName = userData.FirstName;
                        user.LastName = userData.LastName;
                        if (await _googleEmailService.VerifyEmailAsync(userData.Email) == false)
                        {
                            return BadRequest(new { Message = "emailIsInvalid" });
                        }
                        if (!string.IsNullOrEmpty(userData.Email))
                        {
                            user.Email = userData.Email;
                            user.NormalizedEmail = userData.Email;
                        }

                        user.Language = userData.Language;
                        user.NotifyToMail = userData.NotifyToMail;

                        user.Cv =
                            userData.Cv != null ? await ConvertToByteArray(userData.Cv) : user.Cv;
                        user.Avatar =
                            userData.Avatar != null
                                ? await ConvertToByteArray(userData.Avatar)
                                : user.Avatar;

                        _context.Users.Update(user);
                        await _context.SaveChangesAsync();

                        return Ok(new { Message = "updateSuccess" });
                    }

                    return NotFound(new { Message = "noUserFound" });
                }

                return UnprocessableEntity(ModelState);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return BadRequest(new { Message = "systemError" });
            }
        }

        private async Task<byte[]> ConvertToByteArray(IFormFile file)
        {
            using (var memoryStream = new MemoryStream())
            {
                await file.CopyToAsync(memoryStream);
                return memoryStream.ToArray();
            }
        }

        static string[] GetName(string fullName)
        {
            if (string.IsNullOrWhiteSpace(fullName))
            {
                return null;
            }

            string[] nameParts = fullName.Split(' ');
            return nameParts;
        }
    }

    public class StudentInfo
    {
        public string RollNumber { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
    }

    public class UserDTI
    {
        [Required(ErrorMessage = "required")]
        [RegularExpression(
            @"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$",
            ErrorMessage = "emailValidate"
        )]
        public string Email { get; set; }

        [Required(ErrorMessage = "required")]
        public string FirstName { get; set; } = string.Empty;

        [Required(ErrorMessage = "required")]
        public string LastName { get; set; } = string.Empty;

        [RegularExpression(
            @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$",
            ErrorMessage = "passwordRegex"
        )]
        public string? NewPassword { get; set; }

        [Compare(nameof(NewPassword), ErrorMessage = "passNotMatch")]
        public string? ConfirmPassword { get; set; }

        [Required(ErrorMessage = "required")]
        public string Language { get; set; } = "en";
        public IFormFile? Avatar { get; set; }
        public IFormFile? Cv { get; set; }
        public bool NotifyToMail { get; set; } = false;
    }
}
