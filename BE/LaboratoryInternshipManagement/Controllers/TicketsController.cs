﻿using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using NetTools.Extensions;
using static LaboratoryInternshipManagement.Models.Enums.ConstantEnum;

namespace LaboratoryInternshipManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TicketsController : Controller
    {
        public readonly IUnitOfWork _unitOfWork;
        private readonly LabInternManagementDBcontext _context;
        private readonly UserManager<User> _userManager;
        private readonly ILogger<TicketsController> _logger;

        public TicketsController(
            IUnitOfWork unitOfWork,
            LabInternManagementDBcontext context,
            UserManager<User> userManager,
            ILogger<TicketsController> logger
        )
        {
            _unitOfWork = unitOfWork;
            _context = context;
            _userManager = userManager;
            _logger = logger;
        }

        [HttpGet("getTicketsAttendance")]
        [Authorize]
        public async Task<IActionResult> GetTicketsAttendance(
            [FromQuery] GetTicketsInput getTicketsInput
        )
        {
            try
            {
                var userId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);

                var result = _unitOfWork
                    .TicketsRepository.GetAllTicketByCreator(userId)
                    .Where(x => x.Type == (int)TicketType.ATTENDANCE)
                    .OrderByDescending(x => x.DateRequire);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        [Authorize]
        [HttpGet("getTickets")]
        public async Task<IActionResult> GetTickets([FromQuery] GetTicketsInput getTicketsInput)
        {
            try
            {
                var userId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);

                var result = _unitOfWork
                    .TicketsRepository.GetAllTicketByCreator(userId)
                    .Where(x => x.Type != (int)TicketType.ATTENDANCE);
                if (getTicketsInput.Status != 0)
                {
                    result = result.Where(x => x.Status == getTicketsInput.Status);
                }
                if (!getTicketsInput.Keyword.IsNullOrEmpty())
                {
                    result = result
                        .Where(x =>
                            x.NameCreator.ToLower()
                                .Contains(getTicketsInput.Keyword.ToLower().Trim())
                            || x.NameAssignee.ToLower()
                                .Contains(getTicketsInput.Keyword.ToLower().Trim())
                            || x.Title.ToLower().Contains(getTicketsInput.Keyword.ToLower().Trim())
                        )
                        .ToList();
                }
                return Ok(result.OrderByDescending(x => x.DateRequire));
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        [Authorize(Roles = "Mentor")]
        [HttpGet("getLabManagerTicketsAttendance")]
        public async Task<IActionResult> GetLabManagerTicketsAttendance(
            [FromQuery] GetTicketsInput getTicketsInput
        )
        {
            try
            {
                var userId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);
                var user = await _userManager.FindByIdAsync(userId);
                var role =
                    await _userManager.IsInRoleAsync(user, "Mentor")
                    || await _userManager.IsInRoleAsync(user, "Lab Manager");

                getTicketsInput.Assignee = userId;
                var result = _unitOfWork
                    .TicketsRepository.GetAllTicketByAssignee(getTicketsInput)
                    .Where(x =>
                        x.Type == (int)TicketType.ATTENDANCE
                        && x.Status == (byte)TicketStatus.PENDING
                    )
                    .OrderBy(x => x.DateRequire);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        [Authorize(Roles = "Lab Manager")]
        [HttpGet("getLabManagerTicketsOther")]
        public async Task<IActionResult> GetLabManagerTicketsOther(
            [FromQuery] GetTicketsInput getTicketsInput
        )
        {
            try
            {
                var userId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);

                getTicketsInput.Assignee = userId;
                var result = _unitOfWork
                    .TicketsRepository.GetAllTicketByAssignee(getTicketsInput)
                    .Where(x => (x.Type != (int)TicketType.ATTENDANCE))
                    .OrderBy(x => x.DateRequire);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        [HttpGet("ticket-comments/{id}")]
        public async Task<IActionResult> GetTicketComments(long id)
        {
            try
            {
                var result =
                    from ticketComment in _context.TicketComments
                    join userTicketComment in _context.Users
                        on ticketComment.UserId equals userTicketComment.Id
                    where ticketComment.TicketId == id
                    select new
                    {
                        Id = ticketComment.Id,
                        TicketId = ticketComment.TicketId,
                        UserId = ticketComment.UserId,
                        Description = ticketComment.Description,
                        Name = userTicketComment.FirstName + " " + userTicketComment.LastName,
                    };
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        [Authorize]
        [HttpGet("ticket-details/{id}")]
        public async Task<IActionResult> GetTicketsById(long id)
        {
            try
            {
                var userId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);
                var user = await _userManager.FindByIdAsync(userId);

                var result = _unitOfWork.TicketsRepository.GetTicketById(id);
                if (
                    result == null
                    || (
                        !string.Equals(result?.CreateId, userId)
                        && !string.Equals(result?.AssignId, userId)
                    )
                )
                {
                    return BadRequest(new { Message = "ticket.checkPermissionToView" });
                }
                else
                {
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        [Authorize]
        [HttpPut("update-ticket/{id}")]
        public async Task<IActionResult> UpdateTicketsById(UpdateTicketsByIdInputDto ticket)
        {
            try
            {
                var userId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);

                if (ticket != null)
                {
                    if (ticket.CreateId == null && !string.Equals(ticket?.CreateId, userId))
                    {
                        return BadRequest(new { Message = "ticket.checkPermissionToView" });
                    }
                    else
                    {
                        if (!ticket.Title.IsNullOrEmpty())
                        {
                            _unitOfWork.TicketsRepository.UpdateTicketById(ticket);
                            _context.SaveChanges();
                            return StatusCode(
                                200,
                                new { Message = "ticket.updateTicketSuccessfully" }
                            );
                        }
                        else
                        {
                            return StatusCode(500, new { Message = "ticket.updateTicketFailse" });
                        }
                    }
                }
                return BadRequest(new { Message = "ticket.notFoundTickets" });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        [Authorize]
        [HttpPost("comment-ticket")]
        public async Task<IActionResult> CommentTicket([FromBody] TicketComment input)
        {
            try
            {
                var userId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);
                var user = await _userManager.FindByIdAsync(userId);
                var roleUser = await _userManager.IsInRoleAsync(user, "Lab Manager");
                if (user == null)
                {
                    return StatusCode(500, new { Message = "ticket.needLoginFirst" });
                }
                var ticket = _context.Tickets.Where(x => x.Id == input.TicketId).FirstOrDefault();

                // nếu là người tạo hoặc người assign mới có quyền comment
                if (
                    string.Equals(ticket.CreateId, userId) || string.Equals(ticket.AssignId, userId)
                )
                {
                    if (input != null)
                    {
                        var checkComment = _context
                            .TicketComments.Where(x => x.TicketId == input.TicketId)
                            .OrderByDescending(x => x.Id)
                            .FirstOrDefault();
                        var labManager = await _userManager.FindByIdAsync(checkComment?.UserId);
                        var role = false;
                        if (labManager != null)
                        {
                            role = await _userManager.IsInRoleAsync(labManager, "Lab Manager");
                        }
                        if (ticket != null)
                        {
                            if (role || roleUser)
                            {
                                if (input.Description.IsNullOrEmpty())
                                {
                                    return StatusCode(
                                        500,
                                        new { Message = "ticket.descriptionEmpty" }
                                    );
                                }
                                else
                                {
                                    _context.TicketComments.Add(input);
                                    _context.SaveChanges();
                                    return Ok("ticket.commentSuccess");
                                }
                            }
                            else
                            {
                                return StatusCode(500, new { Message = "ticket.spamComment" });
                            }
                        }
                        else
                        {
                            return StatusCode(500, new { Message = "ticket.notFoundTickets" });
                        }
                    }
                    else
                    {
                        return StatusCode(500, new { Message = "ticket.inputEmpty" });
                    }
                }
                else
                {
                    return StatusCode(500, new { Message = "ticket.checkPermissionToView" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        [Authorize(Roles = "Mentor")]
        [HttpPost("checkStatusTicketAttend")]
        public async Task<IActionResult> ConfirmTicketsByTicketId(
            [FromBody] ConfirmTicketsByTicketIdInput confirmTicketsByTicketIdInput
        )
        {
            try
            {
                var userId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);
                var user = await _userManager.FindByIdAsync(userId);
                if (user == null)
                {
                    return StatusCode(500, new { Message = "ticket.needLoginFirst" });
                }
                var role =
                    await _userManager.IsInRoleAsync(user, "Mentor")
                    || await _userManager.IsInRoleAsync(user, "Lab Manager");
                if (role)
                {
                    var ticket = _unitOfWork.TicketsRepository.GetTicketById(
                        confirmTicketsByTicketIdInput.Id
                    );
                    if (ticket != null)
                    {
                        if (
                            ticket.CreateId == null
                            || !string.Equals(ticket?.CreateId, userId)
                                && !string.Equals(ticket?.AssignId, userId)
                        )
                        {
                            return StatusCode(
                                500,
                                new { Message = "ticket.checkPermissionToView" }
                            );
                        }
                        else
                        {
                            var ticketUpdate = ticket;
                            if (string.Equals(confirmTicketsByTicketIdInput.status, "Approve"))
                            {
                                ticketUpdate.Status = (int)TicketStatus.DONE;
                                var leaveRequestNum = _context
                                    .Laboratories.Where(x => string.Equals(x.LabManagerId, userId))
                                    .FirstOrDefault();
                                if (
                                    _unitOfWork.TicketsRepository.getNumIsInAllowedLimit(
                                        ticketUpdate.CreateId
                                    ) < leaveRequestNum.LeaveRequestNum
                                )
                                {
                                    _context.Attendances.Add(
                                        new Attendance
                                        {
                                            InternId = ticketUpdate.CreateId,
                                            Date = (DateTime)ticketUpdate.DateRequire,
                                            Status = (int)AttendanceStatus.ATTENDED,
                                            IsInAllowedLimit = true,
                                            CheckInTime = leaveRequestNum.CheckInTime,
                                            CheckOutTime = leaveRequestNum.CheckOutTime,
                                            Reason = "Attend in allowed limit."
                                        }
                                    );
                                }
                                else
                                {
                                    _context.Attendances.Add(
                                        new Attendance
                                        {
                                            InternId = ticketUpdate.CreateId,
                                            Date = (DateTime)ticketUpdate.DateRequire,
                                            Status = (int)AttendanceStatus.ABSENT,
                                            IsInAllowedLimit = false,
                                            CheckInTime = leaveRequestNum.CheckInTime,
                                            CheckOutTime = leaveRequestNum.CheckOutTime,
                                            Reason = "Absent out allowed litmit."
                                        }
                                    );
                                }
                                _context.SaveChanges();
                                _unitOfWork.TicketsRepository.UpdateTicketById(ticketUpdate);
                                return StatusCode(
                                    200,
                                    new { Message = "ticket.approveTicketSuccessfully" }
                                );
                            }
                            else
                            {
                                ticketUpdate.Status = (int)TicketStatus.REJECT;
                                var result = _unitOfWork.TicketsRepository.UpdateTicketById(
                                    ticketUpdate
                                );
                                return StatusCode(
                                    200,
                                    new { Message = "ticket.rejectTicketSuccessfully" }
                                );
                            }
                        }
                    }
                    else
                    {
                        return StatusCode(500, new { Message = "ticket.checkPermissionToView" });
                    }
                }
                return BadRequest(new { Message = "ticket.notFoundTickets" });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        [HttpDelete("delete-ticket/{id}")]
        public async Task<IActionResult> DeleteTicketsById(long id)
        {
            try
            {
                var userId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);
                var user = await _userManager.FindByIdAsync(userId);
                if (user == null)
                {
                    return BadRequest(new { Message = "ticket.needLoginFirst" });
                }
                var ticket = await _context.Tickets.FindAsync(id);
                if (ticket == null)
                {
                    return BadRequest(new { Message = "ticket.notFoundTickets" });
                }
                else
                {
                    _context.Tickets.Remove(ticket);
                    await _context.SaveChangesAsync();
                    return Ok("ticket.success");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        [Authorize(Roles = "Intern")]
        [HttpPost("createTicketsAttendance")]
        public async Task<IActionResult> CreateTicketsAttendance(
            [FromBody] CreateTicketsAttendanceInput createTicketsAttendance
        )
        {
            try
            {
                var userId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);
                var user = await _userManager.FindByIdAsync(userId);
                if (user == null)
                {
                    return BadRequest(new { Message = "ticket.needLoginFirst" });
                }
                if (user.LabId == null)
                {
                    return StatusCode(500, new { Message = "ticket.dontHaveLabInfor" });
                }
                if (
                    !createTicketsAttendance.Date.IsNullOrEmpty()
                    && !createTicketsAttendance.Title.IsNullOrEmpty()
                )
                {
                    var checkTicket = _context
                        .Tickets.Where(x =>
                            string.Equals(x.CreateId, userId)
                            && string.Equals(x.AssignId, user.MentorId)
                            && DateTime.Compare(
                                (DateTime)x.DateRequire,
                                DateTime.Parse(createTicketsAttendance.Date)
                            ) == 0
                            && x.Type == (int)TicketType.ATTENDANCE
                        )
                        .ToList();
                    if (checkTicket.Count == 0 || checkTicket == null)
                    {
                        if (user.MentorId != null)
                        {
                            var result = _unitOfWork.TicketsRepository.AddTicketAttendance(
                                userId,
                                user.MentorId,
                                createTicketsAttendance.Date,
                                createTicketsAttendance.Title,
                                createTicketsAttendance.Description
                            );
                            if (result)
                            {
                                return Ok("ticket.createSuccessTicketAttendance");
                            }
                            else
                            {
                                return StatusCode(
                                    500,
                                    new { Message = "ticket.createFaileTicketAttendance" }
                                );
                            }
                        }
                        else
                        {
                            return StatusCode(500, new { Message = "ticket.dontHaveMentor" });
                        }
                    }
                    else
                    {
                        return StatusCode(500, new { Message = "ticket.duplicateTicket" });
                    }
                }
                else
                {
                    return StatusCode(500, new { Message = "ticket.createFaileTicketAttendance" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        [Authorize(Roles = "Mentor, Intern")]
        [HttpPost("create-ticket")]
        public async Task<IActionResult> CreateTickets(CreateTicketsInput input)
        {
            try
            {
                var userId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);
                var user = await _userManager.FindByIdAsync(userId);
                if (user == null)
                {
                    return BadRequest(new { Message = "ticket.needLoginFirst" });
                }
                var labManagerId = _context
                    .Laboratories.Where(x => x.Id == user.LabId)
                    .FirstOrDefault();
                if (!input.title.IsNullOrEmpty())
                {
                    if (labManagerId != null && labManagerId.LabManagerId != null)
                    {
                        _context.Tickets.Add(
                            new Tickets
                            {
                                CreateId = userId,
                                AssignId = labManagerId.LabManagerId,
                                Title = input.title,
                                Description = input.description,
                                Type = (byte)TicketType.OTHER,
                                Status = (byte)TicketStatus.PENDING,
                                DateRequire = DateTime.Now,
                            }
                        );
                        _context.SaveChanges();
                        return Ok("ticket.createSuccessTicket");
                    }
                    else
                    {
                        StatusCode(500, new { Message = "ticket.notFoundLabManager" });
                    }
                }
                return StatusCode(500, new { Message = "ticket.createTicketFailse" });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        public class UpdateTicketsByIdInputDto : Tickets
        {
            public string? Comment { get; set; }
        }

        public class CreateTicketsInput
        {
            public string title { get; set; }
            public string description { get; set; }
        }

        public class ConfirmTicketsByTicketIdInput
        {
            public int Id { get; set; }
            public string status { get; set; }
        }

        public class CreateTicketsAttendanceInput
        {
            public string Description { get; set; }
            public string Date { get; set; }
            public string Title { get; set; }
        }

        public class GetTicketsInput
        {
            public string? Keyword { get; set; }
            public int Status { get; set; }
            public string Assignee { get; set; }
        }
    }
}
