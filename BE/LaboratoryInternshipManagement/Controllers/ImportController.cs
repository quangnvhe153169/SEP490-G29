﻿using System.Data;
using System.Globalization;
using System.Reflection;
using System.Security.Claims;
using System.Text;
using CsvHelper;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using LaboratoryInternshipManagement.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace LaboratoryInternshipManagement.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ImportController : Controller
    {
        private readonly IImportHistoryRepository<ImportHistory> _importHistoryRepository;
        private readonly IMemoryCache _memoryCache;
        private readonly ImportLabService _importLabService;
        private readonly LabInternManagementDBcontext _dbContext;
        private readonly ImportMentorService _importMentorService;
        private readonly IImportInternRepository _importInternRepository;
        private readonly ILogger<ImportController> _logger;
        private readonly IConfiguration _config;

        public ImportController(
            IImportHistoryRepository<ImportHistory> importHistoryRepository,
            IMemoryCache memoryCache,
            ImportLabService importLabService,
            LabInternManagementDBcontext dbContext,
            ImportMentorService importMentorService,
            IImportInternRepository importInternRepository,
            ILogger<ImportController> logger,
            IConfiguration configuration
        )
        {
            _importHistoryRepository = importHistoryRepository;
            _memoryCache = memoryCache;
            _importLabService = importLabService;
            _dbContext = dbContext;
            _importMentorService = importMentorService;
            _importInternRepository = importInternRepository;
            _logger = logger;
            _config = configuration;
        }

        [Authorize(Roles = "System Admin")]
        [HttpPost("csv")]
        public async Task<IActionResult> ImportInternCSV([FromForm] IFormFileCollection file)
        {
            try
            {
                var internSendMails = new Dictionary<string, string>();
                var internMailBodies = new Dictionary<string, string>();
                var importHistory = new ImportHistory
                {
                    FileName = file[0].FileName,
                    ImportDate = DateTime.Now,
                    Success = true,
                    ErrorMessage = string.Empty
                };

                if (!file[0].ContentType.Contains("text/csv"))
                    return await HandleImportFailure(importHistory, "invalidFileFormat");

                var (validRecords, invalidRecords) = await _importInternRepository.ValidateCSV(
                    file[0]
                );

                if (invalidRecords.Any())
                    return await HandleInvalidRecords(importHistory, invalidRecords);
                if (validRecords.Any())
                {
                    await _importInternRepository.SaveValidRecords(validRecords, internSendMails);
                    await _importHistoryRepository.AddImportHistory(importHistory);
                    //await _importInternRepository.InitializeAssignments();
                    var emailBody = GetEmbeddedEmailTemplate(
                        "LaboratoryInternshipManagement.Resources.cv-upload.html"
                    );
                    string viewUrl = _config["AllowedView"];

                    foreach (var intern in internSendMails)
                    {
                        var uploadLink = $"{viewUrl}/ImportCV/{intern.Value}";
                        emailBody = emailBody.Replace("{uploadLink}", uploadLink);
                        internMailBodies.Add(intern.Key, emailBody);
                    }

                    _ = Task.Run(() => _importInternRepository.SendMailToIntern(internMailBodies));

                    return Ok(new { Message = "ImportSuccess", validRecords });
                }

                return NotFound(new { Message = "noRecordFound" });
            }
            catch (HeaderValidationException ex)
            {
                _logger.LogError($"{DateTime.Now} Error Acquired: {ex}");
                return BadRequest(new { Message = "invalidHeader" });
            }
            catch (Exception ex)
            {
                _logger.LogError($"{DateTime.Now} Error Acquired: {ex}");
                return BadRequest(new { Message = "systemError" });
            }
        }

        [HttpGet("takeCv")]
        public async Task<IActionResult> TakeCV(string signature)
        {
            try
            {
                var intern = await _dbContext
                    .ImportInterns.Where(i => i.Signature == signature)
                    .Select(x => new
                    {
                        FullName = x.FirstName + " " + x.LastName,
                        LabName = x.LabName,
                        RollName = x.RollNumber,
                        Dob = x.DOB,
                        Email = x.Email,
                        ExpiredAt = x.ExpiredAt,
                        Cv = !string.IsNullOrEmpty(Convert.ToBase64String(x.CVDescript).Trim())
                        && x.CVDescript != null
                            ? $"data:application/pdf;base64,{Convert.ToBase64String(x.CVDescript)}"
                            : null,
                    })
                    .FirstOrDefaultAsync();
                if (intern == null)
                    return NotFound(new { Message = "noInternFound" });

                return Ok(intern);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error Acquired: " + ex);
                return BadRequest(new { Message = "systemError" });
            }
        }

        [HttpPost("pdf")]
        public async Task<IActionResult> ImportCV(
            [FromForm] IFormFileCollection file,
            string signature
        )
        {
            try
            {
                if (!file[0].ContentType.StartsWith("application/pdf"))
                {
                    return BadRequest(new { Message = "invalidFileFormat" });
                }

                var intern = await _dbContext.ImportInterns.FirstOrDefaultAsync(i =>
                    i.Signature.Equals(signature)
                );

                if (intern.ExpiredAt < DateTime.Now)
                {
                    return Conflict(new { Message = "overTimeToUploadCv" });
                }

                using (var stream = new MemoryStream())
                {
                    await file[0].CopyToAsync(stream);

                    intern.CVName = file[0].FileName;
                    intern.CVDescript = stream.ToArray();

                    _dbContext.ImportInterns.Update(intern);
                    await _dbContext.SaveChangesAsync();
                }
                return Ok(new { Message = "cvImportSuccess" });
            }
            catch (Exception ex)
            {
                _logger.LogError("Error Acquired: " + ex);
                return BadRequest(new { Message = "systemError" });
            }
        }

        [Authorize(Roles = "System Admin")]
        [HttpPost("ShowImportHistory")]
        public async Task<IActionResult> GetImportData(string? fileName)
        {
            var importDataList = await _importHistoryRepository.GetAllImportHistory();
            var dataList = importDataList
                .Select(dl => new
                {
                    dl.Id,
                    dl.FileName,
                    dl.ImportDate,
                    dl.Success,
                    dl.ErrorMessage
                })
                .OrderByDescending(x => x.ImportDate)
                .ToList();
            if (fileName != null)
            {
                var searchList = dataList.Where(f =>
                    f.FileName.ToLower().Contains(fileName.ToLower())
                );
                return Ok(searchList);
            }
            return Ok(dataList);
        }

        [Authorize(Roles = "System Admin")]
        [HttpPost("importLab")]
        public async Task<IActionResult> ImportLaboratories([FromForm] IFormFileCollection file)
        {
            if (!file[0].ContentType.Contains("text/csv"))
            {
                return BadRequest(new { Message = "invalidFileFormat" });
            }

            try
            {
                using (var stream = new StreamReader(file[0].OpenReadStream()))
                {
                    using (var csv = new CsvReader(stream, CultureInfo.InvariantCulture))
                    {
                        var records = csv.GetRecords<LaboratoryCSV>().ToList();
                        if (records.Count <= 0)
                        {
                            return BadRequest(new { Message = "noRecordFound" });
                        }
                        var errorMessages = await _importLabService.ProcessLaboratoryRecords(
                            records
                        );

                        if (errorMessages.Any())
                        {
                            return BadRequest(
                                new { Message = "rowsAreInvalid", ErrorMessages = errorMessages }
                            );
                        }
                    }
                }

                return Ok(new { Message = "importSuccess" });
            }
            catch (HeaderValidationException ex)
            {
                return BadRequest(new { Message = "invalidHeader" });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = "systemError" });
            }
        }

        [Authorize(Roles = "System Admin")]
        [HttpGet("GetLaboratory")]
        public async Task<ActionResult<IEnumerable<dynamic>>> GetLaboratories()
        {
            var laboratories = await (
                from l in _dbContext.Laboratories
                join u in _dbContext.Users on l.LabManagerId equals u.Id
                select new
                {
                    LabName = l.LabName,
                    CheckInTime = l.CheckInTime,
                    CheckOutTime = l.CheckOutTime,
                    LeaveRequestNum = l.LeaveRequestNum,
                    LabManagerEmail = u.Email
                }
            ).ToListAsync();

            return Ok(laboratories);
        }

        [Authorize(Roles = "Lab Manager")]
        [HttpPost("importMentorCSV")]
        public async Task<IActionResult> ImportMentorCSV([FromForm] IFormFileCollection file)
        {
            if (!file[0].ContentType.Contains("text/csv"))
            {
                return BadRequest(new { Message = "invalidFileFormat" });
            }

            try
            {
                using (var stream = new StreamReader(file[0].OpenReadStream()))
                {
                    using (var csv = new CsvReader(stream, CultureInfo.InvariantCulture))
                    {
                        var records = csv.GetRecords<MentorCSV>().ToList();
                        if (!records.Any())
                        {
                            return NotFound(new { Message = "noRecordFound" });
                        }
                        var errorMessages = await _importMentorService.ProcessMentorRecordsCSV(
                            records,
                            int.Parse(User.FindFirstValue("labId"))
                        );

                        if (errorMessages.Any())
                        {
                            return BadRequest(new { Message = "rowsAreInvalid", errorMessages });
                        }
                    }
                }

                return Ok(new { Message = "importSuccess" });
            }
            catch (HeaderValidationException ex)
            {
                return BadRequest(new { Message = "invalidHeader" });
            }
            catch (Exception ex)
            {
                return BadRequest(new { Message = "systemError" });
            }
        }

        [Authorize(Roles = "Lab Manager")]
        [HttpPost("ImportMentor")]
        public async Task<IActionResult> AddMentor([FromBody] MentorCSV mentorInput)
        {
            try
            {
                var errorMessages = await _importMentorService.AddMentor(
                    mentorInput,
                    int.Parse(User.FindFirstValue("labId"))
                );

                if (errorMessages.Any())
                {
                    return BadRequest(new { Message = errorMessages });
                }
                return Ok(new { Message = "mentorAddSuccess" });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Exception acquired: {ex}");
                return BadRequest(new { Message = "systemError" });
            }
        }

        private async Task<IActionResult> HandleImportFailure(
            ImportHistory importHistory,
            string error
        )
        {
            importHistory.Success = false;
            importHistory.ErrorMessage = error;
            await _importHistoryRepository.AddImportHistory(importHistory);
            return BadRequest(new { Message = error });
        }

        private async Task<IActionResult> HandleInvalidRecords(
            ImportHistory importHistory,
            List<ImportInternError> invalidRecords
        )
        {
            importHistory.Success = false;
            importHistory.ErrorMessage = "rowsAreInvalid";
            await _importHistoryRepository.AddImportHistory(importHistory);
            return BadRequest(
                new
                {
                    Message = "rowsAreInvalid",
                    InvalidRecords = invalidRecords.Select(r => new
                    {
                        r.RollNumber,
                        r.FirstName,
                        r.LastName,
                        r.DOB,
                        r.Email,
                        r.LabName,
                        r.ErrorMessage
                    })
                }
            );
        }

        private string GetEmbeddedEmailTemplate(string resourceName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceStream = assembly.GetManifestResourceStream(resourceName);

            if (resourceStream == null)
            {
                throw new FileNotFoundException($"Embedded resource '{resourceName}' not found.");
            }

            using (var reader = new StreamReader(resourceStream, Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
