﻿using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Google.Apis.Auth;
using LaboratoryInternshipManagement.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.IdentityModel.Tokens;

namespace LaboratoryInternshipManagement.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<User> _userManager;
        private readonly IConfiguration _config;
        private readonly ILogger<AuthController> _logger;
        private readonly IMemoryCache _memoryCache;
        private readonly LabInternManagementDBcontext _context;

        public AuthController(
            UserManager<User> userManager,
            IConfiguration config,
            ILogger<AuthController> logger,
            IMemoryCache memoryCache,
            LabInternManagementDBcontext context
        )
        {
            _userManager = userManager;
            _config = config;
            _logger = logger;
            _memoryCache = memoryCache;
            _context = context;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] LoginModel login)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = await _userManager.FindByEmailAsync(login.Username);

                    if (user != null)
                    {
                        var isCorrect = await _userManager.CheckPasswordAsync(user, login.Password);
                        if (!isCorrect)
                        {
                            return NotFound(new { Message = "noUserFound" });
                        }
                        var role = await _userManager.IsInRoleAsync(user, "System Admin");

                        if (!role && user.Status)
                        {
                            var token = await GenerateJwtTokenAsync(user);

                            return Ok(new { Message = "welcomUser", token });
                        }
                        return BadRequest(new { Message = "userDeactivated" });
                    }
                    return NotFound(new { Message = "noUserFound" });
                }

                return UnprocessableEntity(ModelState);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex.StackTrace}");
                return StatusCode(500, new { Message = "systemError" });
                ;
            }
        }

        [HttpPost("admin/login")]
        public async Task<IActionResult> LoginAdmin([FromBody] LoginAdminModel login)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = await _userManager.FindByNameAsync(login.Username.ToLower());
                    if (user != null)
                    {
                        var role = await _userManager.IsInRoleAsync(user, "System Admin");
                        if (await _userManager.CheckPasswordAsync(user, login.Password) && role)
                        {
                            var token = await GenerateJwtTokenAsync(user);

                            return Ok(new { Message = "welcomUser", token });
                        }
                    }

                    return NotFound(new { Message = "noUserFound" });
                }

                return UnprocessableEntity(ModelState);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex.StackTrace}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        [HttpPost("google")]
        public async Task<IActionResult> GoogleLogin([FromBody] GoogleLoginRequest request)
        {
            try
            {
                var payload = await ValidateGoogleToken(request.TokenId);

                if (payload == null)
                {
                    return BadRequest(new { message = "Invalid Google token" });
                }

                var user = await _userManager.FindByEmailAsync(payload.Email);
                if (user != null)
                {
                    var role = await _userManager.IsInRoleAsync(user, "System Admin");
                    if (!role && user.Status)
                    {
                        var token = await GenerateJwtTokenAsync(user);

                        return Ok(new { Message = "welcomUser", token });
                    }
                    return BadRequest(new { Message = "userDeactivated" });
                }

                return NotFound(new { message = "User Not Found" });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex.StackTrace}");
                return StatusCode(500, new { Message = "systemError" });
                ;
            }
        }

        [HttpPost("reset-password")]
        public async Task<IActionResult> ResetPassword([FromBody] PasswordDTI passwordDTI)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = await _userManager.FindByIdAsync(passwordDTI.Id);
                    if (user != null)
                    {
                        user.PasswordHash = passwordDTI.Password;
                        var result = await _userManager.UpdateAsync(user);
                        if (result.Succeeded)
                        {
                            return Ok(new { message = "updateSuccess" });
                        }
                        return BadRequest(new { message = "updateFailed" });
                    }
                    return NotFound(new { Message = "noUserFound" });
                }
                return UnprocessableEntity(ModelState);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        private async Task<GoogleJsonWebSignature.Payload> ValidateGoogleToken(string tokenId)
        {
            try
            {
                var settings = new GoogleJsonWebSignature.ValidationSettings()
                {
                    Audience = new List<string>()
                    {
                        _config["Authentication:Google:ClientId"] ?? ""
                    }
                };

                var payload = await GoogleJsonWebSignature.ValidateAsync(tokenId, settings);
                return payload;
            }
            catch (InvalidJwtException)
            {
                return null;
            }
        }

        private async Task<string> GenerateJwtTokenAsync(User user)
        {
            var securityKey = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(_config["JwtSecret:Key"] ?? "")
            );
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var roles = await _userManager.GetRolesAsync(user);

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, _config["JwtSecret:Subject"] ?? ""),
                new Claim(JwtRegisteredClaimNames.Sub, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Jti, user.Id),
                new Claim("language", user.Language),
                new Claim("isTestAccount", user.IsTestAccount.ToString()),
            };

            if (!roles.Contains("System Admin"))
            {
                var labId = user.LabId.HasValue ? user.LabId.Value.ToString() : "";
                if (roles.Contains("Lab Manager"))
                {
                    var lab = await _context
                        .Laboratories.Where(x => x.LabManagerId.Equals(user.Id))
                        .Select(x => x.Id)
                        .FirstOrDefaultAsync();
                    labId = lab.ToString();
                }
                claims.Add(new Claim("labId", labId));
            }

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
                claims.Add(new Claim("roles", role));
            }

            var token = new JwtSecurityToken(
                issuer: _config["JwtSecret:Issuer"],
                audience: _config["JwtSecret:Audience"],
                claims,
                expires: DateTime.Now.AddDays(30),
                signingCredentials: credentials
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private string GenerateOtp()
        {
            var random = new Random();
            return random.Next(100000, 999999).ToString();
        }
    }

    public class OtpDTI : EmailOTP
    {
        [Required]
        public string OTP { get; set; }
    }

    public class EmailOTP
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }

    public class PasswordDTI
    {
        [Required]
        public string Id { get; set; }

        [Required]
        [RegularExpression(
            @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$",
            ErrorMessage = "passwordRegex"
        )]
        public string Password { get; set; }

        [Compare(nameof(Password))]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [RegularExpression(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$", ErrorMessage = "emailValidate")]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }

    public class LoginAdminModel : LoginModel
    {
        [Required]
        public string Username { get; set; }
    }

    public class GoogleLoginRequest
    {
        public string TokenId { get; set; }
    }
}
