﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using AutoMapper;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace LaboratoryInternshipManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventController : ControllerBase
    {
        private readonly IEventRepository _eventRepository;
        private readonly IEventAttendeeRepository _eventAttendeeRepository;
        private readonly INotificationService _notificationService;
        private readonly UserManager<User> _userManager;
        private readonly ILogger<EventController> _logger;
        private readonly IMapper _mapper;
        private readonly LabInternManagementDBcontext _context;

        public EventController(
            IEventRepository eventRepository,
            IEventAttendeeRepository eventAttendeeRepository,
            INotificationService notificationService,
            UserManager<User> userManager,
            ILogger<EventController> logger,
            IMapper mapper,
            LabInternManagementDBcontext context
        )
        {
            _eventRepository = eventRepository;
            _eventAttendeeRepository = eventAttendeeRepository;
            _notificationService = notificationService;
            _userManager = userManager;
            _logger = logger;
            _mapper = mapper;
            _context = context;
        }

        // GET: api/Event/GetAllEvents
        /// <summary>
        /// Retrieves all events.
        /// </summary>
        /// <returns>An ActionResult containing a list of EventDTOs if successful, or an error message if not.</returns>
        [HttpGet("GetAllEvents")]
        [Authorize]
        public async Task<ActionResult<IEnumerable<EventDTO>>> GetAllEvents(
            [FromQuery] string? searchTerm,
            [FromQuery] string? sortBy,
            [FromQuery] string? sortOrder
        )
        {
            try
            {
                var user = await _userManager.FindByIdAsync(
                    User.FindFirstValue(JwtRegisteredClaimNames.Jti)
                );
                if (user == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }

                IEnumerable<Event> events;

                // Apply search
                if (!string.IsNullOrWhiteSpace(searchTerm))
                {
                    events = await _eventRepository.SearchEvent(searchTerm);
                    if (events == null || !events.Any())
                    {
                        // Log what was searched
                        return NotFound(new { Message = "eventsNotFound" });
                    }
                }
                else
                {
                    // Fetch all events if no searchTerm is provided
                    events = await _eventRepository.GetAllEvent();
                    if (events == null || !events.Any())
                    {
                        // Log that no events were found
                        return NotFound(new { Message = "eventsNotFound" });
                    }
                }
                // Apply sorting
                if (
                    !string.IsNullOrWhiteSpace(sortBy) && !string.IsNullOrEmpty(sortBy)
                    || !string.IsNullOrWhiteSpace(sortOrder) && !string.IsNullOrEmpty(sortOrder)
                )
                {
                    try
                    {
                        events = SortEventsByCriteria(events, sortBy, sortOrder);
                    }
                    catch (ArgumentException ex)
                    {
                        _logger.LogError($"Invalid sort criteria: {ex.Message}");
                        return BadRequest(
                            new { Message = "Invalid sort criteria", ExceptionMessage = ex.Message }
                        );
                    }
                }
                // Map events to DTOs using AutoMapper
                var eventDTOs = _mapper.Map<List<EventDTO>>(events);

                return Ok(new { Message = "eventsFound", Events = eventDTOs });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new { Message = "systemError", ExceptionMessage = ex.Message }
                );
            }
        }

        // GET: api/Event/{id}
        /// <summary>
        /// Retrieves a specific event by its ID.
        /// </summary>
        /// <param name="id">The ID of the event to retrieve.</param>
        /// <returns>An ActionResult containing the EventDTO if successful, or an error message if not.</returns>
        [HttpGet("GetEventById/{id}")]
        [Authorize]
        public async Task<ActionResult<EventDTO>> GetEventById(int id)
        {
            try
            {
                var eventEntity = await _eventRepository.GetEventByEventId(id);
                if (eventEntity == null)
                {
                    return NotFound(new { Message = "eventNotFound" });
                }

                var eventDTO = _mapper.Map<EventDTO>(eventEntity);
                return Ok(new { Message = "eventFound", Event = eventDTO });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new { Message = "systemError", ExceptionMessage = ex.Message }
                );
            }
        }

        // GET: api/Event
        /// <summary>
        /// Get list of UserDTO
        /// </summary>
        /// <returns>An ActionResult containing the created UserDTO if successful, or an error message if not.</returns>
        [HttpGet("CreateEvent")]
        [Authorize]
        public async Task<ActionResult<List<UserDTO>>> CreateEvent()
        {
            try
            {
                var users = await _userManager
                    .Users.Include(x => x.Laboratory)
                    .Where(x =>
                        (x.LabId != null || x.Laboratory.LabManagerId == x.Id)
                        && !x.Id.Equals(User.FindFirstValue("jti"))
                    )
                    .ToListAsync();
                var userDto = new List<UserDTO>();
                foreach (var user in users)
                {
                    userDto.Add(
                        new UserDTO()
                        {
                            UserId = user.Id,
                            FullName = string.Concat(user.FirstName, " ", user.LastName),
                            RollName = user.RollName ?? "(Mentor)"
                        }
                    );
                }
                return Ok(new { Message = "usersFound", Users = userDto });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new { Message = "systemError", ExceptionMessage = ex.Message }
                );
            }
        }

        // POST: api/Event
        /// <summary>
        /// Creates a new event and adds attendees.
        /// </summary>
        /// <param name="eventDTO">The event data to create.</param>
        /// <returns>An ActionResult containing the created EventDTO if successful, or an error message if not.</returns>
        [HttpPost("CreateEvent")]
        [Authorize]
        public async Task<ActionResult<EventDTO>> CreateEvent([FromBody] EventDTO eventDTO)
        {
            using var transaction = await _context.Database.BeginTransactionAsync();
            try
            {
                var user = await _userManager.FindByIdAsync(
                    User.FindFirstValue(JwtRegisteredClaimNames.Jti)
                );
                if (user == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }
                eventDTO.UserId = user.Id;
                var eventEntity = _mapper.Map<Event>(eventDTO);
                await _eventRepository.AddEvent(eventEntity);
                eventDTO.EventId = eventEntity.EventId;

                List<string> attendees;
                // If AttendeeIds is not provided, add all users as attendees
                if (eventDTO.AttendeeIds == null || !eventDTO.AttendeeIds.Any())
                {
                    attendees = _userManager.Users.Select(u => u.Id).ToList();
                }
                else
                {
                    attendees = eventDTO.AttendeeIds;
                }

                foreach (var attendeeId in attendees)
                {
                    var eventAttendee = new EventAttendee
                    {
                        EventId = eventEntity.EventId,
                        UserId = attendeeId,
                        Status = "invited",
                        CreatedAt = DateTime.Now
                    };
                    await _eventAttendeeRepository.AddEventAttendee(eventAttendee);
                }

                await _notificationService.AddNotificationToUsersAsync(
                    new Notification
                    {
                        CreatedByUserId = user.Id,
                        Type = "Event",
                        Message = $"New event created: {eventEntity.Title}",
                        TargetUrl = $"/events/{eventEntity.EventId}"
                    },
                    attendees
                );

                await transaction.CommitAsync();
                return Ok(new { Message = "eventCreated", Event = eventDTO });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                await transaction.RollbackAsync();
                return StatusCode(
                    500,
                    new { Message = "systemError", ExceptionMessage = ex.Message }
                );
            }
        }

        // PUT: api/Event/{id}
        /// <summary>
        /// Updates an existing event.
        /// </summary>
        /// <param name="id">The ID of the event to update.</param>
        /// <param name="eventDTO">The updated event data.</param>
        /// <returns>An ActionResult containing the updated EventDTO if successful, or an error message if not.</returns>
        [HttpPut("UpdateEvent/{id}")]
        [Authorize]
        public async Task<ActionResult<EventDTO>> UpdateEvent(int id, [FromBody] EventDTO eventDTO)
        {
            using var transaction = await _context.Database.BeginTransactionAsync();
            try
            {
                var user = await _userManager.FindByIdAsync(
                    User.FindFirstValue(JwtRegisteredClaimNames.Jti)
                );
                if (user == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }
                var eventEntity = await _eventRepository.GetEventByEventId(id);
                if (eventEntity == null)
                {
                    return NotFound(new { Message = "eventNotFound" });
                }
                // Update event properties
                eventEntity.Title = eventDTO.Title;
                eventEntity.Description = eventDTO.Description;
                eventEntity.StartDate = eventDTO.StartDate;
                eventEntity.EndDate = eventDTO.EndDate;
                eventEntity.Location = eventDTO.Location;

                // Handle changes in AttendeeIds
                var existingAttendees = eventEntity.EventAttendees.Select(ea => ea.UserId).ToList();
                var newAttendees = eventDTO.AttendeeIds ?? new List<string>();

                // Add new attendees
                var attendeesToAdd = newAttendees.Except(existingAttendees).ToList();
                foreach (var userId in attendeesToAdd)
                {
                    eventEntity.EventAttendees.Add(
                        new EventAttendee
                        {
                            EventId = eventEntity.EventId,
                            UserId = userId,
                            Status = "invited"
                        }
                    );
                }

                // Remove attendees
                var attendeesToRemove = existingAttendees.Except(newAttendees).ToList();
                foreach (var userId in attendeesToRemove)
                {
                    var attendeeToRemove = eventEntity.EventAttendees.FirstOrDefault(ea =>
                        ea.UserId == userId
                    );
                    if (attendeeToRemove != null)
                    {
                        await _eventAttendeeRepository.DeleteEventAttendee(
                            attendeeToRemove.EventAttendeeId
                        );
                    }
                }

                await _eventRepository.UpdateEvent(eventEntity);
                await transaction.CommitAsync();
                return Ok(
                    new { Message = "eventUpdated", Event = _mapper.Map<EventDTO>(eventEntity) }
                );
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new { Message = "systemError", ExceptionMessage = ex.Message }
                );
            }
        }

        // DELETE: api/Event/{id}
        /// <summary>
        /// Deletes an existing event.
        /// </summary>
        /// <param name="id">The ID of the event to delete.</param>
        /// <returns>An ActionResult indicating success or failure.</returns>
        [HttpDelete("DeleteEvent/{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteEvent(int id)
        {
            using var transaction = await _context.Database.BeginTransactionAsync();
            try
            {
                var eventEntity = await _eventRepository.GetEventByEventId(id);
                if (eventEntity == null)
                {
                    return NotFound(new { Message = "eventNotFound" });
                }
                var dto = _mapper.Map<EventDTO>(eventEntity);
                // Remove attendees
                var attendeesToRemove = eventEntity.EventAttendees.Select(ea => ea.UserId).ToList();
                foreach (var userId in attendeesToRemove)
                {
                    var attendeeToRemove = eventEntity.EventAttendees.FirstOrDefault(ea =>
                        ea.UserId == userId
                    );
                    if (attendeeToRemove != null)
                    {
                        await _eventAttendeeRepository.DeleteEventAttendee(
                            attendeeToRemove.EventAttendeeId
                        );
                    }
                }
                await _eventRepository.DeleteEvent(id);
                await transaction.CommitAsync();
                return Ok(new { Message = "eventDeleted", Event = dto });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new { Message = "systemError", ExceptionMessage = ex.Message }
                );
            }
        }

        // POST: api/Event/{eventId}/RSVP
        /// <summary>
        /// Responds to an event invitation.
        /// </summary>
        /// <param name="eventId">The ID of the event to RSVP to.</param>
        /// <param name="rsvpDTO">The RSVP data.</param>
        /// <returns>An ActionResult indicating success or failure.</returns>
        [HttpPost("RSVP/{eventId}")]
        [Authorize]
        public async Task<IActionResult> RSVP(int eventId, [FromBody] RSVPDTO rsvpDTO)
        {
            using var transaction = await _context.Database.BeginTransactionAsync();
            try
            {
                var user = await _userManager.FindByIdAsync(
                    User.FindFirstValue(JwtRegisteredClaimNames.Jti)
                );
                if (user == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }
                var eventEntity = await _eventRepository.GetEventByEventId(eventId);

                if (eventEntity == null)
                {
                    return NotFound(new { Message = "eventNotFound" });
                }
                var eventAttendees = await _eventAttendeeRepository.GetEventAttendeeByEventId(
                    eventId
                );
                var rsvp = eventAttendees.FirstOrDefault(et => et.UserId.Equals(user.Id));
                if (rsvp == null)
                {
                    return NotFound(new { Message = "userNotBeenInvited" });
                }
                rsvp.Status = rsvpDTO.Status;
                rsvp.UpdatedAt = DateTime.Now;
                await _eventAttendeeRepository.UpdateEventAttendee(rsvp);
                await transaction.CommitAsync();
                return Ok(
                    new { Message = "rsvpUpdated", RSVP = _mapper.Map<EventAttendeeDTO>(rsvp) }
                );
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new { Message = "systemError", ExceptionMessage = ex.Message }
                );
            }
        }

        [HttpPost]
        [Route("UpdateAttendance/{eventId}")]
        public async Task<IActionResult> UpdateAttendance(
            int eventId,
            [FromBody] AttendanceUpdateModel model
        )
        {
            using var transaction = await _context.Database.BeginTransactionAsync();
            if (model == null || !model.Attendance.Any())
            {
                return BadRequest("Invalid attendance data.");
            }
            _logger.LogError($"event id {eventId}");
            _logger.LogError($"moedl {model.Attendance.First().UserId}");
            _logger.LogError($"moedl {model.Attendance.First().Status}");

            var user = await _userManager.FindByIdAsync(
                User.FindFirstValue(JwtRegisteredClaimNames.Jti)
            );
            if (user == null)
            {
                return NotFound(new { Message = "noUserFound" });
            }
            var eventEntity = await _eventRepository.GetEventByEventId(eventId);

            if (eventEntity == null)
            {
                return NotFound(new { Message = "eventNotFound" });
            }
            foreach (var attendance in model.Attendance)
            {
                var attendee = eventEntity.EventAttendees.FirstOrDefault(ea =>
                    ea.UserId.Equals(attendance.UserId)
                );
                if (attendee != null)
                {
                    attendee.Status = attendance.Status;
                    await _eventAttendeeRepository.UpdateEventAttendee(attendee);
                }
            }
            await transaction.CommitAsync();
            return Ok(new { Message = "Attendance updated successfully." });
        }

        [HttpGet]
        [Route("GetAttendance/{eventId}")]
        public async Task<IActionResult> GetAttendance(int eventId)
        {
            var eventEntity = await _eventRepository.GetEventByEventId(eventId);
            if (eventEntity == null)
            {
                return NotFound(new { Message = "Event not found" });
            }

            var attendanceDetails = eventEntity
                .EventAttendees.Select(a => new { a.UserId, a.Status })
                .ToList();

            return Ok(new { Attendance = attendanceDetails });
        }

        public class AttendanceUpdateModel
        {
            public List<AttendanceModel> Attendance { get; set; }
        }

        public class AttendanceModel
        {
            public string UserId { get; set; }
            public string Status { get; set; }
        }

        // Helper method to determine if the user is an admin
        private bool IsAdminUser()
        {
            // Check if user is an admin (e.g., check roles)
            return User.IsInRole("System Admin") || User.IsInRole("Lab Manager");
        }

        // Helper method to sort events based on criteria
        private IEnumerable<Event> SortEventsByCriteria(
            IEnumerable<Event> events,
            string sortBy,
            string sortOrder
        )
        {
            if (string.IsNullOrEmpty(sortBy))
                return events;

            bool isDesc = string.Equals(sortOrder, "desc", StringComparison.OrdinalIgnoreCase);

            return (sortBy?.ToLower()) switch
            {
                "title"
                    => isDesc
                        ? events.OrderByDescending(o => o.Title).ToList()
                        : events.OrderBy(o => o.Title).ToList(),
                // Add more cases for additional sorting criteria as needed
                _ => throw new ArgumentException("Invalid sort criteria.")
            };
        }

        private async Task<IEnumerable<EventAttendee>> InvitedUser(string userId, int? eventId)
        {
            var eventattendee = await _eventAttendeeRepository.GetAllEventAttendee();
            if (eventId != null)
            {
                eventattendee = eventattendee.FindAll(ea =>
                    ea.UserId.Equals(userId) && ea.EventId == eventId
                );
            }
            else
            {
                eventattendee = eventattendee.FindAll(ea => ea.UserId.Equals(userId));
            }
            return eventattendee;
        }
    }

    public class RSVPDTO
    {
        public string Status { get; set; }
    }

    public class EventDTO
    {
        public int EventId { get; set; }
        public string? UserId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Location { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
        public List<string>? AttendeeIds { get; set; }
    }

    public class EventAttendeeDTO
    {
        public int EventAttendeeId { get; set; }
        public int EventId { get; set; }
        public string UserId { get; set; }
        public string Status { get; set; } // e.g., 'invited', 'confirmed', 'attended', 'declined'
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
    }

    public class UserDTO
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string RollName { get; set; }
    }
}
