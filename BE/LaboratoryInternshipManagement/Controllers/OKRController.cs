using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using AutoMapper;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using LaboratoryInternshipManagement.Services.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Comment = LaboratoryInternshipManagement.Models.Comment;

namespace LaboratoryInternshipManagement.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class OKRController : ControllerBase
    {
        private readonly IObjectiveRepository _objectiveRepository;
        private readonly IKeyResultRepository _keyResultRepository;
        private readonly ITeamRepository _teamRepository;
        private readonly ITeamMembershipRepository _teamMembershipRepository;
        private readonly INotificationService _notificationService;
        private readonly ICommentService _commentService;
        private readonly IEmailService _emailService;
        private readonly IOKRLogRepository _okrLogRepository;
        private readonly UserManager<User> _userManager;
        private readonly ILogger<UserController> _logger;
        private readonly IMapper _mapper;
        private readonly string _baseUrl = "http://localhost:7212";
        private readonly LabInternManagementDBcontext _context;

        public OKRController(
            IObjectiveRepository objectiveRepository,
            IKeyResultRepository keyResultRepository,
            INotificationService notificationService,
            ICommentService commentService,
            UserManager<User> userManager,
            ITeamMembershipRepository teamMembershipRepository,
            ITeamRepository teamRepository,
            IEmailService emailService,
            IMapper mapper,
            ILogger<UserController> logger,
            IOKRLogRepository okrLogRepository,
            LabInternManagementDBcontext context
        )
        {
            _objectiveRepository =
                objectiveRepository ?? throw new ArgumentNullException(nameof(objectiveRepository));
            _keyResultRepository =
                keyResultRepository ?? throw new ArgumentNullException(nameof(keyResultRepository));
            _notificationService =
                notificationService ?? throw new ArgumentNullException(nameof(notificationService));
            _commentService =
                commentService ?? throw new ArgumentNullException(nameof(commentService));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _teamMembershipRepository =
                teamMembershipRepository
                ?? throw new ArgumentNullException(nameof(teamMembershipRepository));
            _teamRepository =
                teamRepository ?? throw new ArgumentNullException(nameof(teamRepository));
            _emailService = emailService ?? throw new ArgumentNullException(nameof(emailService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _okrLogRepository =
                okrLogRepository ?? throw new ArgumentNullException(nameof(okrLogRepository));
            _context = context;
        }

        // GET: api/OKR/GetAllObjectives
        /// <summary>
        /// Retrieves all objectives for the specified team or all objectives if the user has admin access.
        /// </summary>
        /// <param name="currentTeamId">The ID of the current team.</param>
        /// <returns>An ActionResult containing a list of ObjectiveDTOs if successful, or an error message if not.</returns>
        [HttpGet("GetAllObjectives")]
        [Authorize] // Ensure authorized access
        public async Task<ActionResult<IEnumerable<ObjectiveDTO>>> GetAllObjectives(
            [FromQuery] int? currentTeamId
        )
        {
            try
            {
                var user = await GetUserAsync();
                if (user == null)
                    return NotFound(new { Message = "noUserFound" });
                var objectives = await _objectiveRepository.GetAllObjectives();
                var teams = await GetUserTeamsAsync(user);
                int teamId = currentTeamId ?? teams.FirstOrDefault()?.TeamId ?? 0;
                objectives = objectives.Where(o => o.TeamId == teamId).ToList();

                if (!objectives.Any())
                    return NotFound(new { Message = "noObjectivesFound", Objectives = objectives });

                var objectiveDTOs = _mapper.Map<List<ObjectiveDTO>>(objectives);
                return Ok(new { Message = "objectivesFound", Objectives = objectiveDTOs });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // GET: api/OKR/GetObjectiveById/{id}
        /// <summary>
        /// Retrieves an objective by its ID.
        /// </summary>
        /// <param name="id">The ID of the objective to retrieve.</param>
        /// <returns>An ActionResult containing the ObjectiveDTO if found, or an error message if not.</returns>
        [HttpGet("GetObjectiveById/{id}")]
        [Authorize]
        public async Task<ActionResult<ObjectiveDTO>> GetObjectiveById(int id)
        {
            try
            {
                var user = await GetUserAsync();
                if (user == null)
                    return NotFound(new { Message = "noUserFound" });

                var objective = await _objectiveRepository.GetObjectiveByObjectiveId(id);
                if (objective == null)
                    return NotFound(new { Message = "objectiveNotFound" });

                if (!await UserHasAccessToObjective(user, objective))
                    return Unauthorized(new { Message = "unauthorizedAccess" });

                var objectiveDTO = _mapper.Map<ObjectiveDTO>(objective);
                return Ok(new { Message = "objectiveFound", Object = objectiveDTO });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // POST: api/OKR/CreateObjective
        /// <summary>
        /// Creates a new objective.
        /// </summary>
        /// <param name="objectiveDto">The ObjectiveDTO containing objective details.</param>
        /// <param name="currentTeamId">The ID of the current team.</param>
        /// <returns>An ActionResult with the created objective if successful, or an error message if not.</returns>
        [Authorize]
        [HttpPost("CreateObjective")]
        public async Task<ActionResult> CreateObjective(
            [FromBody] ObjectiveDTO objectiveDto,
            [FromQuery] int? currentTeamId
        )
        {
            using var transaction = await _context.Database.BeginTransactionAsync();
            try
            {
                var user = await GetUserAsync();
                if (user == null)
                    return NotFound(new { Message = "noUserFound" });

                var teams = await GetUserTeamsAsync(user);
                int teamId = currentTeamId ?? teams.FirstOrDefault()?.TeamId ?? 0;
                if (objectiveDto.TeamId != teamId)
                    return Unauthorized(new { Message = "unauthorizedAccess" });
                if (!ModelState.IsValid)
                    return BadRequest(
                        new { Message = "invalidObjectiveData", Errors = GetModelErrors() }
                    );

                var objective = _mapper.Map<Objective>(objectiveDto);
                await _objectiveRepository.AddObjective(objective);

                var teamMemberIds =
                    await _teamMembershipRepository.GetTeamMembershipByTeamId_UserId(
                        objective.TeamId,
                        user.Id
                    );
                var users = teamMemberIds.Select(x => x.UserId).ToList();
                await SendNotificationAsync(
                    user,
                    users,
                    "OKR",
                    $"Objective created: Title - {objective.Title}, Description - {objective.Description}, Start Date - {objective.StartDate}, End Date - {objective.EndDate}",
                    $"/api/objectives/{objective.ObjectiveId}"
                );
                var changeDetails =
                    $@"
                    Title: {objective.Title}
                    Description: {objective.Description}
                    Start Date: {objective.StartDate:yyyy-MM-dd}
                    End Date: {objective.EndDate:yyyy-MM-dd}";
                await LogOKRChangeAsync(user, objective.ObjectiveId, "Created", changeDetails);

                var createdObjectiveDto = _mapper.Map<ObjectiveDTO>(objective);
                await transaction.CommitAsync();
                return CreatedAtAction(
                    nameof(GetObjectiveById),
                    new { id = createdObjectiveDto.ObjectiveId },
                    new { Message = "objectiveCreated", Object = createdObjectiveDto }
                );
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // PUT: api/OKR/UpdateObjective/{id}
        /// <summary>
        /// Updates an objective with the specified ID.
        /// </summary>
        /// <param name="id">The ID of the objective to update.</param>
        /// <param name="objectiveDto">The DTO containing updated objective data.</param>
        /// <returns>An ActionResult containing the updated objective if successful, or an error message if not.</returns>
        [Authorize]
        [HttpPut("UpdateObjective/{id}")]
        public async Task<ActionResult> UpdateObjective(
            int id,
            [FromBody] ObjectiveDTO objectiveDto
        )
        {
            using var transaction = await _context.Database.BeginTransactionAsync();
            try
            {
                if (objectiveDto == null)
                    return BadRequest(new { Message = "Objective data is null" });

                var user = await GetUserAsync();
                if (user == null)
                    return NotFound(new { Message = "noUserFound" });

                var existingObjective = await _objectiveRepository.GetObjectiveByObjectiveId(id);
                if (existingObjective == null)
                    return NotFound(new { Message = "objectiveNotFound" });

                if (!await UserHasAccessToObjective(user, existingObjective))
                    return Unauthorized(new { Message = "unauthorizedAccess" });

                if (!ModelState.IsValid)
                    return BadRequest(
                        new { Message = "invalidObjectiveData", Errors = GetModelErrors() }
                    );

                // Map the DTO to the existing entity
                _mapper.Map(objectiveDto, existingObjective);

                await _objectiveRepository.UpdateObjective(existingObjective);

                var teamMemberIds =
                    await _teamMembershipRepository.GetTeamMembershipByTeamId_UserId(
                        existingObjective.TeamId,
                        user.Id
                    );
                var users = teamMemberIds.Select(x => x.UserId).ToList();
                await SendNotificationAsync(
                    user,
                    users,
                    "OKR",
                    $"Objective updated: Title - {existingObjective.Title}, Description - {existingObjective.Description}, Start Date - {existingObjective.StartDate}, End Date - {existingObjective.EndDate}",
                    $"/api/objectives/{existingObjective.ObjectiveId}"
                );
                var changeDetails =
                    $@"
                    Title: {existingObjective.Title}
                    Description: {existingObjective.Description}
                    Start Date: {existingObjective.StartDate:yyyy-MM-dd}
                    End Date: {existingObjective.EndDate:yyyy-MM-dd}";
                await LogOKRChangeAsync(
                    user,
                    existingObjective.ObjectiveId,
                    "Updated",
                    changeDetails
                );

                var updatedObjectiveDto = _mapper.Map<ObjectiveDTO>(existingObjective);
                await transaction.CommitAsync();
                return Ok(new { Message = "objectiveUpdated", Object = updatedObjectiveDto });
            }
            catch (Exception ex)
            {
                // Log error message
                _logger.LogError($"Error occurred: {ex}");

                // Return internal server error message
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // DELETE: api/OKR/DeleteObjective/{id}
        /// <summary>
        /// Deletes an objective with the specified ID.
        /// </summary>
        /// <param name="id">The ID of the objective to delete.</param>
        /// <returns>An ActionResult indicating success or failure with a message.</returns>
        [Authorize]
        [HttpDelete("DeleteObjective/{id}")]
        public async Task<ActionResult> DeleteObjective(int id)
        {
            using var transaction = await _context.Database.BeginTransactionAsync();
            try
            {
                var user = await GetUserAsync();
                if (user == null)
                    return NotFound(new { Message = "noUserFound" });

                var objective = await _objectiveRepository.GetObjectiveByObjectiveId(id);
                if (objective == null)
                    return NotFound(new { Message = "objectiveNotFound" });

                if (!await UserHasAccessToObjective(user, objective))
                    return Unauthorized(new { Message = "unauthorizedAccess" });

                await _objectiveRepository.DeleteObjective(id);

                var teamMemberIds =
                    await _teamMembershipRepository.GetTeamMembershipByTeamId_UserId(
                        objective.TeamId,
                        user.Id
                    );
                var users = teamMemberIds.Select(x => x.UserId).ToList();
                await SendNotificationAsync(
                    user,
                    users,
                    "OKR",
                    $"Objective deleted: Title - {objective.Title}, Description - {objective.Description}, Start Date - {objective.StartDate}, End Date - {objective.EndDate}",
                    "/api/objectives"
                );
                var changeDetails =
                    $@"
                    Title: {objective.Title}
                    Description: {objective.Description}
                    Start Date: {objective.StartDate:yyyy-MM-dd}
                    End Date: {objective.EndDate:yyyy-MM-dd}";
                await LogOKRChangeAsync(user, objective.ObjectiveId, "Deleted", changeDetails);

                var objectiveDTO = _mapper.Map<ObjectiveDTO>(objective);
                await transaction.CommitAsync();
                return Ok(new { Message = "objectiveDeleted", Object = objectiveDTO });
            }
            catch (Exception ex)
            {
                // Log error message
                _logger.LogError($"Error occurred: {ex}");

                // Return internal server error message
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // POST: api/OKR/CreateKeyResult
        /// <summary>
        /// Creates a new key result.
        /// </summary>
        /// <param name="keyResultDTO">The DTO containing key result information to create.</param>
        /// <returns>An ActionResult containing the created key result or an error message if creation fails.</returns>
        [Authorize]
        [HttpPost("CreateKeyResult")]
        public async Task<ActionResult> CreateKeyResult([FromBody] KeyResultDTO keyResultDTO)
        {
            using var transaction = await _context.Database.BeginTransactionAsync();
            try
            {
                var user = await GetUserAsync();
                if (user == null)
                    return NotFound(new { Message = "noUserFound" });

                var objective = await _objectiveRepository.GetObjectiveByObjectiveId(
                    keyResultDTO.ObjectiveId
                );
                if (objective == null)
                    return NotFound(new { Message = "objectiveNotFound" });

                if (!await UserHasAccessToObjective(user, objective))
                    return Unauthorized(new { Message = "unauthorizedAccess" });
                if (keyResultDTO.StartDate < objective.StartDate)
                {
                    ModelState.AddModelError(
                        "StartDate",
                        "Key result start date is not lower than Objective start date"
                    );
                }
                if (keyResultDTO.StartDate > objective.EndDate)
                {
                    ModelState.AddModelError(
                        "StartDate",
                        "Key result start date is not higher than Objective end date"
                    );
                }
                if (keyResultDTO.EndDate > objective.EndDate)
                {
                    ModelState.AddModelError(
                        "StartDate",
                        "Key result end date is not higher than Objective end date"
                    );
                }
                if (!ModelState.IsValid)
                    return BadRequest(
                        new { Message = "invalidKeyResultData", Errors = GetModelErrors() }
                    );

                var keyResult = _mapper.Map<KeyResult>(keyResultDTO);
                await _keyResultRepository.AddKeyResult(keyResult);

                var teamMemberIds =
                    await _teamMembershipRepository.GetTeamMembershipByTeamId_UserId(
                        objective.TeamId,
                        user.Id
                    );
                var users = teamMemberIds.Select(x => x.UserId).ToList();
                await SendNotificationAsync(
                    user,
                    users,
                    "OKR",
                    $"Key Result created: Title - {keyResult.Title}, Priority - {keyResult.Priority}, Assign - {keyResult.Assign}, Status - {keyResult.Status}",
                    $"/api/keyresults/{keyResult.KeyResultId}"
                );
                var changeDetails =
                    $@"
                     Title: {keyResult.Title}
                     Assign: {keyResult.Assign}
                     Priority: {keyResult.Priority}
                     Start Date: {keyResult.StartDate:yyyy-MM-ddTHH:mm}
                     End Date: {keyResult.EndDate:yyyy-MM-ddTHH:mm}
                     Notes: {keyResult.Notes}";
                await LogOKRChangeAsync(user, keyResult.ObjectiveId, "Created", changeDetails);

                var keyResultDto = _mapper.Map<KeyResultDTO>(keyResult);
                await transaction.CommitAsync();
                return Ok(new { Message = "keyResultCreated", KeyResult = keyResultDto });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // PUT: api/OKR/UpdateKeyResult/{keyResultId}
        /// <summary>
        /// Updates an existing key result.
        /// </summary>
        /// <param name="keyResultId">The ID of the key result to update.</param>
        /// <param name="keyResultDTO">The DTO containing updated key result information.</param>
        /// <returns>An ActionResult containing the updated key result or an error message if update fails.</returns>
        [Authorize]
        [HttpPut("UpdateKeyResult/{keyResultId}")]
        public async Task<ActionResult> UpdateKeyResult(
            int keyResultId,
            [FromBody] KeyResultDTO keyResultDTO
        )
        {
            using var transaction = await _context.Database.BeginTransactionAsync();
            try
            {
                var user = await GetUserAsync();
                if (user == null)
                    return NotFound(new { Message = "noUserFound" });

                var existingKeyResult = await _keyResultRepository.GetKeyResultByKeyResultId(
                    keyResultId
                );
                if (existingKeyResult == null)
                    return NotFound(
                        new { Message = "keyResultNotFound", KeyResultId = keyResultId }
                    );

                if (!await UserHasAccessToObjective(user, existingKeyResult.Objective))
                    return Unauthorized(new { Message = "unauthorizedAccess" });
                if (keyResultDTO.StartDate < existingKeyResult.Objective.StartDate)
                {
                    ModelState.AddModelError(
                        "StartDate",
                        "Key result start date is not lower than Objective start date"
                    );
                }
                if (keyResultDTO.StartDate > existingKeyResult.Objective.EndDate)
                {
                    ModelState.AddModelError(
                        "StartDate",
                        "Key result start date is not higher than Objective end date"
                    );
                }
                if (keyResultDTO.EndDate > existingKeyResult.Objective.EndDate)
                {
                    ModelState.AddModelError(
                        "StartDate",
                        "Key result end date is not higher than Objective end date"
                    );
                }
                if (!ModelState.IsValid)
                    return BadRequest(
                        new { Message = "invalidKeyResultData", Errors = GetModelErrors() }
                    );

                _mapper.Map(keyResultDTO, existingKeyResult);
                await _keyResultRepository.UpdateKeyResult(existingKeyResult);

                var teamMemberIds =
                    await _teamMembershipRepository.GetTeamMembershipByTeamId_UserId(
                        existingKeyResult.Objective.TeamId,
                        user.Id
                    );
                var users = teamMemberIds.Select(x => x.UserId).ToList();
                await SendNotificationAsync(
                    user,
                    users,
                    "OKR",
                    $"Key Result updated: Title - {existingKeyResult.Title}, Priority - {existingKeyResult.Priority}, Assign - {existingKeyResult.Assign}, Status - {existingKeyResult.Status}",
                    $"/api/keyresults/{existingKeyResult.KeyResultId}"
                );
                var changeDetails =
                    $@"
                     Title: {existingKeyResult.Title}
                     Assign: {existingKeyResult.Assign}
                     Priority: {existingKeyResult.Priority}
                     Start Date: {existingKeyResult.StartDate:yyyy-MM-ddTHH:mm}
                     End Date: {existingKeyResult.EndDate:yyyy-MM-ddTHH:mm}
                     Notes: {existingKeyResult.Notes}";
                await LogOKRChangeAsync(
                    user,
                    existingKeyResult.ObjectiveId,
                    "Updated",
                    changeDetails
                );

                var updatedKeyResultDto = _mapper.Map<KeyResultDTO>(existingKeyResult);
                await transaction.CommitAsync();
                return Ok(new { Message = "keyResultUpdated", KeyResult = updatedKeyResultDto });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // DELETE: api/OKR/DeleteKeyResult/{keyResultId}
        /// <summary>
        /// Deletes an existing key result.
        /// </summary>
        /// <param name="keyResultId">The ID of the key result to delete.</param>
        /// <returns>An ActionResult indicating success or an error message if deletion fails.</returns>
        [Authorize]
        [HttpDelete("DeleteKeyResult/{keyResultId}")]
        public async Task<ActionResult> DeleteKeyResult(int keyResultId)
        {
            using var transaction = await _context.Database.BeginTransactionAsync();
            try
            {
                var user = await GetUserAsync();
                if (user == null)
                    return NotFound(new { Message = "noUserFound" });

                var existingKeyResult = await _keyResultRepository.GetKeyResultByKeyResultId(
                    keyResultId
                );
                if (existingKeyResult == null)
                    return NotFound(
                        new { Message = "keyResultNotFound", KeyResultId = keyResultId }
                    );

                if (!await UserHasAccessToObjective(user, existingKeyResult.Objective))
                    return Unauthorized(new { Message = "unauthorizedAccess" });

                await _keyResultRepository.DeleteKeyResult(keyResultId);

                var teamMemberIds =
                    await _teamMembershipRepository.GetTeamMembershipByTeamId_UserId(
                        existingKeyResult.Objective.TeamId,
                        user.Id
                    );
                var users = teamMemberIds.Select(x => x.UserId).ToList();
                await SendNotificationAsync(
                    user,
                    users,
                    "OKR",
                    $"Key Result deleted: Title - {existingKeyResult.Title}, Priority - {existingKeyResult.Priority}, Assign - {existingKeyResult.Assign}, Status - {existingKeyResult.Status}",
                    "/api/keyresults"
                );
                var changeDetails =
                    $@"
                      Title: {existingKeyResult.Title}
                      Assign: {existingKeyResult.Assign}
                      Priority: {existingKeyResult.Priority}
                      Notes: {existingKeyResult.Notes}";
                await LogOKRChangeAsync(
                    user,
                    existingKeyResult.ObjectiveId,
                    "Deleted",
                    changeDetails
                );

                var keyResultDTO = _mapper.Map<KeyResultDTO>(existingKeyResult);
                await transaction.CommitAsync();
                return Ok(new { Message = "keyResultDeleted", KeyResult = keyResultDTO });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // GET: api/OKR/GetAllKeyResults
        /// <summary>
        /// Retrieves all key results.
        /// </summary>
        /// <param name="currentTeamId">The ID of the current team.</param>
        /// <returns>An ActionResult containing the found key results or an error message if none are found.</returns>
        [Authorize]
        [HttpGet("GetAllKeyResults")]
        public async Task<ActionResult<IEnumerable<KeyResultDTO>>> GetAllKeyResults(
            [FromQuery] int? currentTeamId
        )
        {
            try
            {
                var user = await GetUserAsync();
                if (user == null)
                    return NotFound(new { Message = "noUserFound" });
                var keyResults = await _keyResultRepository.GetAllKeyResult();
                var teams = await GetUserTeamsAsync(user);
                int teamId = currentTeamId ?? teams.FirstOrDefault()?.TeamId ?? 0;
                keyResults = keyResults
                    .Where(kr =>
                    {
                        if (kr.Objective == null)
                        {
                            return false;
                        }
                        if (kr.Objective.TeamId == null)
                        {
                            return false;
                        }
                        return kr.Objective.TeamId == teamId;
                    })
                    .ToList();
                if (!keyResults.Any())
                {
                    return NotFound(new { Message = "noKeyResultsFound", KeyResults = keyResults });
                }
                var keyResultDTOs = _mapper.Map<List<KeyResultDTO>>(keyResults);
                return Ok(new { Message = "keyResultsFound", KeyResults = keyResultDTOs });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // GET: api/OKR/keyresults/{id}
        /// <summary>
        /// Retrieves a specific key result by its ID. Administrators can access any key result, while other users can only access key results within their team.
        /// </summary>
        /// <param name="id">The ID of the key result to retrieve.</param>
        /// <param name="currentTeamId">The ID of the current team.</param>
        /// <returns>An ActionResult containing the retrieved key result or an error message if not found or unauthorized.</returns>
        [Authorize]
        [HttpGet("GetKeyResultById/{id}")]
        public async Task<ActionResult<KeyResultDTO>> GetKeyResultById(
            int id,
            [FromQuery] int? currentTeamId
        )
        {
            try
            {
                var user = await GetUserAsync();
                if (user == null)
                    return NotFound(new { Message = "noUserFound" });
                var keyResult = await _keyResultRepository.GetKeyResultByKeyResultId(id);
                var teams = await GetUserTeamsAsync(user);
                int teamId = currentTeamId ?? teams.FirstOrDefault()?.TeamId ?? 0;
                if (keyResult.Objective.TeamId != teamId)
                {
                    return Unauthorized(new { Message = "unauthorizedAccess" });
                }
                if (keyResult == null)
                {
                    return NotFound(new { Message = "keyResultNotFound", KeyResultId = id });
                }
                var keyResultDTO = _mapper.Map<KeyResultDTO>(keyResult);
                return Ok(new { Message = "keyResultFound", KeyResult = keyResultDTO });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // GET: api/OKR/SearchObjectives/{searchTerm}
        /// <summary>
        /// Searches objectives based on the specified search term, limited to the user's team.
        /// </summary>
        /// <param name="searchTerm">The search term to filter objectives.</param>
        /// <param name="currentTeamId">The ID of the current team.</param>
        /// <returns>An ActionResult with a list of objectives found matching the search criteria, or an error message if none found.</returns>
        [Authorize]
        [HttpGet("SearchObjectives")]
        public async Task<ActionResult<IEnumerable<ObjectiveDTO>>> SearchObjectives(
            [FromQuery] string? searchTerm,
            [FromQuery] int? currentTeamId
        )
        {
            try
            {
                var user = await GetUserAsync();
                if (user == null)
                    return NotFound(new { Message = "noUserFound" });

                var objectives = await _objectiveRepository.SearchObjective(searchTerm);
                var teams = await GetUserTeamsAsync(user);
                int teamId = currentTeamId ?? teams.FirstOrDefault()?.TeamId ?? 0;
                objectives = objectives.Where(o => o.TeamId == teamId).ToList();
                if (!objectives.Any())
                {
                    return NotFound(new { Message = "noObjectivesFound", Objectives = objectives });
                }

                var objectiveDTOs = _mapper.Map<List<ObjectiveDTO>>(objectives);
                return Ok(new { Message = "objectivesFound", Objectives = objectiveDTOs });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // GET: api/OKR/SortObjectives/{sortBy}/{sortOrder}
        /// <summary>
        /// Sorts objectives based on the specified criteria and order.
        /// </summary>
        /// <param name="sortBy">The field to sort by (e.g., "title", "startdate", "enddate").</param>
        /// <param name="sortOrder">The sort order ("asc" for ascending, "desc" for descending).</param>
        /// <param name="currentTeamId">The ID of the current team.</param>
        /// <returns>An ActionResult with the sorted list of objectives if successful, or an error message if not.</returns>
        [HttpGet("SortObjectives")]
        public async Task<ActionResult<IEnumerable<ObjectiveDTO>>> SortObjectives(
            [FromQuery] string sortBy,
            [FromQuery] string sortOrder,
            [FromQuery] int? currentTeamId
        )
        {
            try
            {
                var user = await GetUserAsync();
                if (user == null)
                    return NotFound(new { Message = "noUserFound" });

                IEnumerable<Objective> objectives = await _objectiveRepository.GetAllObjectives();
                var teams = await GetUserTeamsAsync(user);
                int teamId = currentTeamId ?? teams.FirstOrDefault()?.TeamId ?? 0;
                objectives = objectives.Where(o => o.TeamId == teamId).ToList();

                if (!objectives.Any())
                {
                    return NotFound(new { Message = "noObjectivesFound", Objectives = objectives });
                }

                objectives = SortObjectivesByCriteria(objectives, sortBy, sortOrder);
                var objectiveDTOs = _mapper.Map<List<ObjectiveDTO>>(objectives);
                return Ok(new { Message = "objectivesSorted", Objectives = objectiveDTOs });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // GET: api/OKR/keyresults/search
        /// <summary>
        /// Searches key results based on the specified search term, limited to the user's team.
        /// </summary>
        /// <param name="searchTerm">The search term to filter key results.</param>
        /// <param name="currentTeamId">The ID of the current team.</param>
        /// <returns>An ActionResult with a list of key results found matching the search criteria, or an error message if none found.</returns>
        [Authorize]
        [HttpGet("SearchKeyResults")]
        public async Task<ActionResult<IEnumerable<KeyResultDTO>>> SearchKeyResults(
            [FromQuery] string searchTerm,
            [FromQuery] int? currentTeamId
        )
        {
            try
            {
                var user = await GetUserAsync();
                if (user == null)
                    return NotFound(new { Message = "noUserFound" });

                var keyResults = await _keyResultRepository.SearchKeyResult(searchTerm);
                var teams = await GetUserTeamsAsync(user);
                int teamId = currentTeamId ?? teams.FirstOrDefault()?.TeamId ?? 0;
                keyResults = keyResults.Where(kr => kr.Objective.TeamId == teamId).ToList();

                if (!keyResults.Any())
                {
                    return Unauthorized(new { Message = "unauthorizedAccess" });
                }
                if (!keyResults.Any())
                {
                    return NotFound(new { Message = "noKeyResultsFound", KeyResults = keyResults });
                }
                var keyResultDTOs = _mapper.Map<List<KeyResultDTO>>(keyResults);
                return Ok(new { Message = "keyResultsFound", KeyResults = keyResultDTOs });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // GET: api/OKR/keyresults/sort/{sortBy}/{sortOrder}
        /// <summary>
        /// Sorts key results based on the specified criteria and order.
        /// </summary>
        /// <param name="sortBy">The field to sort by (e.g., "title", "startdate", "enddate").</param>
        /// <param name="sortOrder">The sort order ("asc" for ascending, "desc" for descending).</param>
        /// <param name="currentTeamId">The ID of the current team.</param>
        /// <returns>An ActionResult with the sorted list of key results if successful, or an error message if not.</returns>
        [Authorize]
        [HttpGet("SortKeyResults")]
        public async Task<ActionResult<IEnumerable<KeyResultDTO>>> SortKeyResults(
            [FromQuery] string sortBy,
            [FromQuery] string sortOrder,
            [FromQuery] int? currentTeamId
        )
        {
            try
            {
                var user = await GetUserAsync();
                if (user == null)
                    return NotFound(new { Message = "noUserFound" });

                IEnumerable<KeyResult> keyResults = await _keyResultRepository.GetAllKeyResult();
                keyResults = SortKeyResultsByKeyCriteria(keyResults, sortBy, sortOrder);
                var teams = await GetUserTeamsAsync(user);
                int teamId = currentTeamId ?? teams.FirstOrDefault()?.TeamId ?? 0;
                keyResults = keyResults.Where(kr => kr.Objective.TeamId == teamId).ToList();

                if (!keyResults.Any())
                {
                    return Unauthorized(new { Message = "unauthorizedAccess" });
                }

                if (!keyResults.Any())
                {
                    return NotFound(new { Message = "noKeyResultsFound", KeyResults = keyResults });
                }

                var keyResultDTOs = _mapper.Map<List<KeyResultDTO>>(keyResults);
                return Ok(new { Message = "keyResultsSorted", KeyResults = keyResultDTOs });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // GET: api/OKR/FilterKeyResultsByPerson/{assignUserId}
        /// <summary>
        /// Filters key results based on the specified person.
        /// </summary>
        /// <param name="assignUserId">The person who key result will filter by.</param>
        /// <param name="currentTeamId">The ID of the current team.</param>
        /// <returns>An ActionResult with the filtered list of key results if successful, or an error message if not.</returns>
        [Authorize]
        [HttpGet("FilterKeyResultsByPerson")]
        public async Task<ActionResult<IEnumerable<KeyResultDTO>>> FilterKeyResultsByPerson(
            [FromQuery] string assignUserId,
            [FromQuery] int? currentTeamId
        )
        {
            try
            {
                var user = await GetUserAsync();
                if (user == null)
                    return NotFound(new { Message = "noUserFound" });

                var keyResults = await _keyResultRepository.GetKeyResultByAssignUserId(
                    assignUserId
                );
                var teams = await GetUserTeamsAsync(user);
                int teamId = currentTeamId ?? teams.FirstOrDefault()?.TeamId ?? 0;
                keyResults = keyResults.Where(kr => kr.Objective.TeamId == teamId).ToList();

                if (!keyResults.Any())
                {
                    return Unauthorized(new { Message = "unauthorizedAccess" });
                }
                if (!keyResults.Any())
                {
                    return NotFound(new { Message = "noKeyResultsFound", KeyResults = keyResults });
                }

                var keyResultDTOs = _mapper.Map<List<KeyResultDTO>>(keyResults);
                return Ok(new { Message = "keyResultsFound", KeyResults = keyResultDTOs });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // POST: api/OKR/CreateComment
        /// <summary>
        /// Creates a new comment for a discussion.
        /// </summary>
        /// <param name="commentDTO">The DTO containing comment information to create.</param>
        /// <param name="currentTeamId">The ID of the current team.</param>
        /// <returns>An ActionResult containing the created comment or an error message if creation fails.</returns>
        [Authorize]
        [HttpPost("CreateComment")]
        public async Task<ActionResult<CommentDTO>> CreateComment(
            [FromBody] CommentDTO commentDTO,
            [FromQuery] int? currentTeamId
        )
        {
            using var transaction = await _context.Database.BeginTransactionAsync();
            try
            {
                var user = await GetUserAsync();
                if (user == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }
                List<int> teamIds = new List<int>();
                var teams = await GetUserTeamsAsync(user);
                int teamId = currentTeamId ?? teams.FirstOrDefault()?.TeamId ?? 0;
                if (commentDTO.ReletedObjectType == "Objective")
                {
                    var objective = await _objectiveRepository.GetObjectiveByObjectiveId(
                        commentDTO.ReletedObjectId
                    );
                    if (objective == null || objective.TeamId != teamId)
                    {
                        return Unauthorized(new { Message = "unauthorizedAccess" });
                    }
                }
                else if (commentDTO.ReletedObjectType == "KeyResult")
                {
                    var keyResult = await _keyResultRepository.GetKeyResultByKeyResultId(
                        commentDTO.ReletedObjectId
                    );
                    if (keyResult == null || keyResult.Objective.TeamId != teamId)
                    {
                        return Unauthorized(new { Message = "unauthorizedAccess" });
                    }
                }
                else
                {
                    return BadRequest(new { Message = "invalidObjectType" });
                }

                if (!ModelState.IsValid)
                    return BadRequest(
                        new { Message = "invalidCommentData", Errors = GetModelErrors() }
                    );
                var comment = _mapper.Map<Comment>(commentDTO);
                //comment.MentionedUserIds = ConvertToCommaSeparated(commentDTO.MentionedUserIds ?? Array.Empty<string>());
                await _commentService.AddCommentAsync(comment);

                var teamMemberIds =
                    await _teamMembershipRepository.GetTeamMembershipByTeamId_UserId(
                        currentTeamId ?? teamIds.FirstOrDefault(),
                        user.Id
                    );
                var users = teamMemberIds.Select(x => x.UserId).ToList();

                await SendNotificationAsync(
                    user,
                    users,
                    "Comment",
                    $"Comment Created: Content - {comment.Content}",
                    $"/api/OKR/Comments/{comment.CommentId}"
                );
                await transaction.CommitAsync();

                return Ok(
                    new { Message = "commentCreated", Comment = _mapper.Map<CommentDTO>(comment) }
                );
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // PUT: api/OKR/UpdateComment/{commentId}
        /// <summary>
        /// Updates an existing comment.
        /// </summary>
        /// <param name="commentId">The ID of the comment to update.</param>
        /// <param name="commentDTO">The DTO containing updated comment information.</param>
        /// <param name="currentTeamId">The ID of the current team.</param>
        /// <returns>An ActionResult containing the updated comment or an error message if update fails.</returns>
        [Authorize]
        [HttpPut("UpdateComment/{commentId}")]
        public async Task<ActionResult<CommentDTO>> UpdateComment(
            int commentId,
            [FromBody] CommentDTO commentDTO,
            [FromQuery] int? currentTeamId
        )
        {
            using var transaction = await _context.Database.BeginTransactionAsync();
            try
            {
                var user = await GetUserAsync();
                if (user == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }

                var existingComment = await _commentService.GetCommentByCommentIdAsync(commentId);
                if (existingComment == null)
                {
                    return NotFound(new { Message = "commentNotFound", CommentId = commentId });
                }

                List<Team> teams = await GetUserTeamsAsync(user);
                int teamId = currentTeamId ?? teams.FirstOrDefault()?.TeamId ?? 0;
                // Validate user's team membership before updating
                if (commentDTO.ReletedObjectType == "Objective")
                {
                    var objective = await _objectiveRepository.GetObjectiveByObjectiveId(
                        commentDTO.ReletedObjectId
                    );
                    if (objective == null || objective.TeamId != teamId)
                    {
                        return Unauthorized(new { Message = "unauthorizedAccess" });
                    }
                }
                else if (commentDTO.ReletedObjectType == "KeyResult")
                {
                    var keyResult = await _keyResultRepository.GetKeyResultByKeyResultId(
                        commentDTO.ReletedObjectId
                    );
                    if (keyResult == null || keyResult.Objective.TeamId != teamId)
                    {
                        return Unauthorized(new { Message = "unauthorizedAccess" });
                    }
                }
                else
                {
                    return BadRequest(new { Message = "invalidObjectType" });
                }

                _mapper.Map(commentDTO, existingComment);

                await _commentService.UpdateCommentAsync(existingComment);

                var teamMemberIds =
                    await _teamMembershipRepository.GetTeamMembershipByTeamId_UserId(
                        currentTeamId ?? teams.FirstOrDefault()?.TeamId ?? 0,
                        user.Id
                    );
                var users = teamMemberIds.Select(x => x.UserId).ToList();

                await SendNotificationAsync(
                    user,
                    users,
                    "Comment",
                    $"Comment Updated: Content - {existingComment.Content}",
                    $"/api/OKR/Comments/{existingComment.CommentId}"
                );
                await transaction.CommitAsync();
                return Ok(new { Message = "commentUpdated", Comment = commentDTO });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // DELETE: api/OKR/DeleteComment/{commentId}
        /// <summary>
        /// Deletes an existing comment.
        /// </summary>
        /// <param name="commentId">The ID of the comment to delete.</param>
        /// <param name="currentTeamId">The ID of the current team.</param>
        /// <returns>An ActionResult indicating success or an error message if deletion fails.</returns>
        [Authorize]
        [HttpDelete("DeleteComment/{commentId}")]
        public async Task<ActionResult> DeleteComment(int commentId, [FromQuery] int? currentTeamId)
        {
            using var transaction = await _context.Database.BeginTransactionAsync();
            try
            {
                var user = await GetUserAsync();
                if (user == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }

                var existingComment = await _commentService.GetCommentByCommentIdAsync(commentId);
                if (existingComment == null)
                {
                    return NotFound(new { Message = "commentNotFound", CommentId = commentId });
                }
                var teams = await GetUserTeamsAsync(user);
                int teamId = currentTeamId ?? teams.FirstOrDefault()?.TeamId ?? 0;
                // Validate user's team membership before deleting
                if (existingComment.ReletedObjectType == "Objective")
                {
                    var objective = await _objectiveRepository.GetObjectiveByObjectiveId(
                        existingComment.ReletedObjectId
                    );
                    if (objective == null || objective.TeamId != teamId)
                    {
                        return Unauthorized(new { Message = "unauthorizedAccess" });
                    }
                }
                else if (existingComment.ReletedObjectType == "KeyResult")
                {
                    var keyResult = await _keyResultRepository.GetKeyResultByKeyResultId(
                        existingComment.ReletedObjectId
                    );
                    if (keyResult == null || keyResult.Objective.TeamId != teamId)
                    {
                        return Unauthorized(new { Message = "unauthorizedAccess" });
                    }
                }
                else
                {
                    return BadRequest(new { Message = "invalidObjectType" });
                }
                await _commentService.DeleteCommentAsync(existingComment.CommentId);
                var teamMemberIds =
                    await _teamMembershipRepository.GetTeamMembershipByTeamId_UserId(
                        currentTeamId ?? teamId,
                        user.Id
                    );
                var users = teamMemberIds.Select(x => x.UserId).ToList();
                await SendNotificationAsync(
                    user,
                    users,
                    "Comment",
                    $"Comment Deleted: Content - {existingComment.Content}",
                    $"/api/comments"
                );
                await transaction.CommitAsync();
                return Ok(new { Message = "commentDeleted", CommentId = commentId });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // GET: api/Comments/SearchComments?query={query}
        /// <summary>
        /// Searches comments based on a query string.
        /// </summary>
        /// <param name="query">The search query.</param>
        /// <param name="currentTeamId">The ID of the current team.</param>
        /// <returns>A list of comments matching the search query.</returns>
        [HttpGet("SearchComments")]
        public async Task<ActionResult<IEnumerable<CommentDTO>>> SearchComments(
            [FromQuery] string query,
            [FromQuery] int? currentTeamId
        )
        {
            try
            {
                // Retrieve current user based on JWT token
                var user = await GetUserAsync();
                if (user == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }
                // Retrieve comments based on search query
                var comments = await _commentService.SearchCommentsAsync(query);
                var teams = await GetUserTeamsAsync(user);
                int teamId = currentTeamId ?? teams.FirstOrDefault()?.TeamId ?? 0;
                comments = comments.Where(c => IsCommentAccessibleByUser(c, teamId)).ToList();
                var commentDTOs = _mapper.Map<IEnumerable<CommentDTO>>(comments);
                if (!commentDTOs.Any())
                {
                    return NotFound(new { Message = "noCommentFound", Comments = commentDTOs });
                }

                return Ok(new { Message = "commentFound", Comment = commentDTOs });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // GET: api/Comments/SortComments?sortBy={field}&sortOrder={asc/desc}
        /// <summary>
        /// Retrieves comments sorted by a specified field and order.
        /// </summary>
        /// <param name="sortBy">The field to sort by.</param>
        /// <param name="sortOrder">The sort order (ascending or descending).</param>
        /// <param name="currentTeamId">The ID of the current team.</param>
        /// <returns>A sorted list of comments.</returns>
        [HttpGet("SortComments")]
        public async Task<ActionResult<IEnumerable<CommentDTO>>> SortComments(
            [FromQuery] string sortBy,
            [FromQuery] string sortOrder,
            [FromQuery] int? currentTeamId
        )
        {
            try
            {
                var user = await GetUserAsync();
                if (user == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }

                IEnumerable<Comment> comments = await _commentService.GetAllCommentsAsync();
                comments = SortCommentsByCriteria(comments, sortBy, sortOrder);
                var teams = await GetUserTeamsAsync(user);
                int teamId = currentTeamId ?? teams.FirstOrDefault()?.TeamId ?? 0;
                comments = comments.Where(c => IsCommentAccessibleByUser(c, teamId)).ToList();
                var commentDTOs = _mapper.Map<IEnumerable<CommentDTO>>(comments);
                if (!commentDTOs.Any())
                {
                    return NotFound(new { Message = "noCommentFound", Comments = commentDTOs });
                }

                return Ok(new { Message = "commentsFound", Comments = commentDTOs });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // GET: api/OKR/comments
        /// <summary>
        /// Retrieves all comments for a specific discussion. Administrators can access any comments, while other users can only access comments within their team.
        /// </summary>
        /// <param name="currentTeamId">The ID of the current team.</param>
        /// <returns>An ActionResult containing the list of comments or an error message if not found or unauthorized.</returns>
        [Authorize]
        [HttpGet("GetAllComments")]
        public async Task<ActionResult<List<CommentDTO>>> GetAllComments(
            [FromQuery] int? currentTeamId
        )
        {
            try
            {
                var user = await GetUserAsync();
                if (user == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }
                IEnumerable<Comment> comments;
                var teams = await GetUserTeamsAsync(user);
                int teamId = currentTeamId ?? teams.FirstOrDefault()?.TeamId ?? 0;
                comments = (await _commentService.GetAllCommentsAsync())
                    .Where(c => IsCommentAccessibleByUser(c, teamId))
                    .ToList();
                var commentDTOs = _mapper.Map<List<CommentDTO>>(comments);
                return Ok(new { Message = "commentsFound", Comments = commentDTOs });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // GET: api/OKR/comments/{id}
        /// <summary>
        /// Retrieves a specific comment by its ID. Administrators can access any comment, while other users can only access comments within their team.
        /// </summary>
        /// <param name="id">The ID of the comment to retrieve.</param>
        /// <param name="currentTeamId">The ID of the current team.</param>
        /// <returns>An ActionResult containing the retrieved comment or an error message if not found or unauthorized.</returns>
        [Authorize]
        [HttpGet("GetCommentById/{id}")]
        public async Task<ActionResult<CommentDTO>> GetCommentById(
            int id,
            [FromQuery] int? currentTeamId
        )
        {
            try
            {
                var user = await GetUserAsync();
                if (user == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }

                var comment = await _commentService.GetCommentByCommentIdAsync(id);
                if (comment == null)
                {
                    return NotFound(new { Message = "commentNotFound", CommentId = id });
                }

                var teams = await GetUserTeamsAsync(user);
                int teamId = currentTeamId ?? teams.FirstOrDefault()?.TeamId ?? 0;
                if (!IsCommentAccessibleByUser(comment, teamId))
                {
                    return Unauthorized(new { Message = "unauthorizedAccess" });
                }

                var commentDTO = _mapper.Map<CommentDTO>(comment);
                return Ok(new { Message = "commentFound", Comment = commentDTO });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // GET: api/Notifications/GetAllNotifications
        /// <summary>
        /// Retrieves all notifications for the signed-in user or all notifications for an admin.
        /// </summary>
        /// <returns>A list of notifications.</returns>
        [HttpGet("GetAllNotifications")]
        public async Task<ActionResult<IEnumerable<NotificationDTO>>> GetAllNotifications()
        {
            try
            {
                // Retrieve current user based on JWT token
                var user = await _userManager.FindByIdAsync(
                    User.FindFirstValue(JwtRegisteredClaimNames.Jti)
                );
                if (user == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }

                // Retrieve notifications
                var notifications = await _notificationService.GetAllNotificationsAsync();
                notifications = notifications.Where(n => n.IsRead == false).ToList();
                // Access control: Only notifications of the signed-in user
                notifications = notifications
                    .Where(n => n.Users.Any(u => u.Id.Equals(user.Id)))
                    .ToList();
                if (!notifications.Any())
                {
                    return NotFound(new { Message = "noNotificationFound" });
                }
                // Map to DTOs
                var notificationDTOs = _mapper.Map<IEnumerable<NotificationDTO>>(notifications);

                return Ok(new { Message = "notificationFound", Notifications = notificationDTOs });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        [HttpPut("UpdateNotification/{id}")]
        public async Task<IActionResult> UpdateNotification(
            int id,
            [FromBody] UpdateNotificationRequest request
        )
        {
            try
            {
                // Retrieve current user based on JWT token
                var user = await _userManager.FindByIdAsync(
                    User.FindFirstValue(JwtRegisteredClaimNames.Jti)
                );
                if (user == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }

                // Retrieve the notification by id
                var notification = await _notificationService.GetNotificationByIdAsync(id);
                if (notification == null)
                {
                    return NotFound(new { Message = "notificationNotFound" });
                }

                // Check if the notification belongs to the current user or admin
                if (!notification.Users.Any(u => u.Id.Equals(user.Id)))
                {
                    return Unauthorized(new { Message = "accessDenied" });
                }

                // Update notification
                notification.IsRead = request.IsRead;
                await _notificationService.UpdateNotificationAsync(notification);

                return Ok(new { Message = "notificationUpdated" });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // GET: api/OKRLogs/GetAllOKRLog
        /// <summary>
        /// Retrieves all OKR logs for the signed-in user's team or all OKR logs for an admin.
        /// </summary>
        /// <param name="currentTeamId">The ID of the current team.</param>
        /// <returns>A list of OKR logs.</returns>
        [HttpGet("GetAllOKRLog")]
        public async Task<ActionResult<IEnumerable<OKRLogDTO>>> GetAllOKRLog(
            [FromQuery] int? currentTeamId
        )
        {
            try
            {
                // Retrieve current user based on JWT token
                var user = await _userManager.FindByIdAsync(
                    User.FindFirstValue(JwtRegisteredClaimNames.Jti)
                );
                if (user == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }

                // Retrieve user's team memberships
                var teams = await GetUserTeamsAsync(user);
                int teamId = currentTeamId ?? teams.FirstOrDefault()?.TeamId ?? 0;

                // Retrieve OKR logs with access control and filtering
                var okrLogs = await _okrLogRepository.GetOKRLogByTeamId(teamId);

                if (!okrLogs.Any())
                {
                    return NotFound(new { Message = "noOKRLogFound" });
                }
                // Map to DTOs
                var okrLogDTOs = _mapper.Map<IEnumerable<OKRLogDTO>>(okrLogs);

                return Ok(new { Message = "okrLogFound", OKRLogs = okrLogDTOs });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        /* // GET: api/OKRs/ExportOKR?includeOKRDiscussion={boolean}
         /// <summary>
         /// Exports all OKR data to an Excel file.
         /// </summary>
         /// <param name="includeOKRDiscussion">Whether to include OKR discussions.</param>
         /// <returns>An Excel file containing the OKR data.</returns>
         [HttpGet("ExportOKR")]
         public async Task<IActionResult> ExportOKR(bool includeOKRDiscussion = false)
         {
             try
             {
                 // Retrieve current user based on JWT token
                 var user = await _userManager.FindByIdAsync(User.FindFirstValue(JwtRegisteredClaimNames.Jti));
                 if (user == null)
                 {
                     return NotFound(new { Message = "noUserFound" });
                 }
 
                 // Retrieve OKRs
                 var objectives = await _objectiveRepository.GetAllObjectives();
                 bool isAdmin = await IsAdminUser();
                 if (!isAdmin)
                 {
                     // Access control: Only objectives of the signed-in user's teams
                     var teamIds = GetOrCreateUserTeam(user).Result.TeamId;
                     objectives = objectives.Where(o => o.TeamId == teamIds).ToList();
                 }
 
                 using (var workbook = new XLWorkbook())
                 {
                     var worksheet = workbook.Worksheets.Add("OKRs");
 
                     // Add headers
                     worksheet.Cell(1, 1).Value = "ObjectiveId";
                     worksheet.Cell(1, 2).Value = "Title";
                     worksheet.Cell(1, 3).Value = "Description";
                     worksheet.Cell(1, 4).Value = "StartDate";
                     worksheet.Cell(1, 5).Value = "EndDate";
                     worksheet.Cell(1, 6).Value = "CreatedAt";
                     worksheet.Cell(1, 7).Value = "UpdatedAt";
                     worksheet.Cell(1, 8).Value = "UserId";
                     worksheet.Cell(1, 9).Value = "TeamId";
 
                     // Add objective data and key results
                     var currentRow = 2;
                     foreach (var obj in objectives)
                     {
                         worksheet.Cell(currentRow, 1).Value = obj.ObjectiveId;
                         worksheet.Cell(currentRow, 2).Value = obj.Title;
                         worksheet.Cell(currentRow, 3).Value = obj.Description;
                         worksheet.Cell(currentRow, 4).Value = obj.StartDate;
                         worksheet.Cell(currentRow, 5).Value = obj.EndDate;
                         worksheet.Cell(currentRow, 6).Value = obj.CreatedAt;
                         worksheet.Cell(currentRow, 7).Value = obj.UpdatedAt;
                         worksheet.Cell(currentRow, 8).Value = obj.UserId;
                         worksheet.Cell(currentRow, 9).Value = obj.TeamId;
 
                         // Retrieve key results for the objective
                         var keyResults = await _keyResultRepository.GetKeyResultByObjectiveId(obj.ObjectiveId);
 
                         // Add headers for key results
                         worksheet.Cell(currentRow + 1, 2).Value = "KeyResultId";
                         worksheet.Cell(currentRow + 1, 3).Value = "Title";
                         worksheet.Cell(currentRow + 1, 4).Value = "Priority";
                         worksheet.Cell(currentRow + 1, 5).Value = "Status";
                         worksheet.Cell(currentRow + 1, 6).Value = "TargetValue";
                         worksheet.Cell(currentRow + 1, 7).Value = "CurrentValue";
                         worksheet.Cell(currentRow + 1, 8).Value = "CreatedAt";
                         worksheet.Cell(currentRow + 1, 9).Value = "UpdatedAt";
 
                         // Add key result data
                         foreach (var kr in keyResults)
                         {
                             currentRow++;
                             worksheet.Cell(currentRow, 2).Value = kr.KeyResultId;
                             worksheet.Cell(currentRow, 3).Value = kr.Title;
                             worksheet.Cell(currentRow, 4).Value = kr.Priority;
                             worksheet.Cell(currentRow, 5).Value = kr.Status;
                             worksheet.Cell(currentRow, 6).Value = kr.TargetValue;
                             worksheet.Cell(currentRow, 7).Value = kr.CurrentValue;
                             worksheet.Cell(currentRow, 8).Value = kr.CreatedAt;
                             worksheet.Cell(currentRow, 9).Value = kr.UpdatedAt;
                         }
 
                         currentRow++;
                     }
 
                     if (includeOKRDiscussion)
                     {
                         var discussionWorksheet = workbook.Worksheets.Add("OKR Discussions");
                         discussionWorksheet.Cell(1, 1).Value = "DiscussionId";
                         discussionWorksheet.Cell(1, 2).Value = "ObjectiveId";
                         discussionWorksheet.Cell(1, 3).Value = "Content";
                         discussionWorksheet.Cell(1, 4).Value = "CreatedAt";
                         discussionWorksheet.Cell(1, 5).Value = "UpdatedAt";
                         discussionWorksheet.Cell(1, 6).Value = "UserId";
 
                         currentRow = 2;
                         foreach (var obj in objectives)
                         {
                             var discussion = await _discussionService.GetDiscussionsByObjectiveIdAsync(obj.ObjectiveId);
                             if (discussion != null)
                             {
                                 discussionWorksheet.Cell(currentRow, 1).Value = discussion.DiscussionId;
                                 discussionWorksheet.Cell(currentRow, 2).Value = discussion.ObjectiveId;
                                 discussionWorksheet.Cell(currentRow, 3).Value = discussion.Content;
                                 discussionWorksheet.Cell(currentRow, 4).Value = discussion.CreatedAt;
                                 discussionWorksheet.Cell(currentRow, 5).Value = discussion.UpdatedAt;
                                 discussionWorksheet.Cell(currentRow, 6).Value = discussion.UserId;
 
                                 // Retrieve comments for the discussion
                                 var comments = await _commentService.GetCommentsByDiscussionIdAsync(discussion.DiscussionId);
                                 foreach (var comment in comments)
                                 {
                                     currentRow++;
                                     discussionWorksheet.Cell(currentRow, 2).Value = $"Comment - {comment.CommentId}";
                                     discussionWorksheet.Cell(currentRow, 3).Value = comment.Content;
                                     discussionWorksheet.Cell(currentRow, 4).Value = comment.CreatedAt;
                                     discussionWorksheet.Cell(currentRow, 5).Value = comment.UpdatedAt;
                                     discussionWorksheet.Cell(currentRow, 6).Value = comment.UserId;
 
                                     // Retrieve replies for the comment
                                     var replies = await _replyService.GetRepliesByCommentIdAsync(comment.CommentId);
                                     foreach (var reply in replies)
                                     {
                                         currentRow++;
                                         discussionWorksheet.Cell(currentRow, 2).Value = $"Reply - {reply.ReplyId}";
                                         discussionWorksheet.Cell(currentRow, 3).Value = reply.Content;
                                         discussionWorksheet.Cell(currentRow, 4).Value = reply.CreatedAt;
                                         discussionWorksheet.Cell(currentRow, 5).Value = reply.UpdatedAt;
                                         discussionWorksheet.Cell(currentRow, 6).Value = reply.UserId;
                                     }
                                 }
                                 currentRow++;
                             }
                         }
                     }
 
                     using (var stream = new MemoryStream())
                     {
                         workbook.SaveAs(stream);
                         var content = stream.ToArray();
                         return File(content, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "OKRs.xlsx");
                     }
                 }
             }
             catch (Exception ex)
             {
                 _logger.LogError($"Error occurred: {ex}");
                 return StatusCode(500, new { Message = "systemError", ExceptionMessage = ex.Message, StackTrace = ex.StackTrace });
             }
         }*/

        // POST: api/OKR/InviteMember
        /// <summary>
        /// Sends an email invitation to join the user's team.
        /// </summary>
        /// <param name="inviteDTO">DTO containing userId for the invitation.</param>
        /// <returns>An ActionResult indicating success or failure of the invitation.</returns>
        [Authorize]
        [HttpPost("InviteMember")]
        public async Task<ActionResult> InviteMember(
            [FromBody] InviteDTO inviteDTO,
            [FromQuery] int currentTeamId
        )
        {
            using var transaction = await _context.Database.BeginTransactionAsync();
            try
            {
                var user = await _userManager.FindByIdAsync(
                    User.FindFirstValue(JwtRegisteredClaimNames.Jti)
                );
                if (user == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }

                var invitee = await _userManager.FindByEmailAsync(inviteDTO.Email);
                if (invitee == null)
                {
                    return NotFound(new { Message = "inviteeNotFound" });
                }

                var existingMembership =
                    await _teamMembershipRepository.GetTeamMembershipByTeamId_UserId(
                        currentTeamId,
                        invitee.Id
                    );
                if (existingMembership.Any())
                {
                    return BadRequest(new { Message = "userAlreadyMemberOfTeam" });
                }

                await _teamMembershipRepository.AddTeamMembership(
                    new TeamMembership { TeamId = currentTeamId, UserId = invitee.Id }
                );

                await _okrLogRepository.AddOKRLog(
                    new OKRLog
                    {
                        UserId = user.Id,
                        ChangeDate = DateTime.Now,
                        ChangeType = "A new member",
                        ChangeDetails =
                            $"A new member:{invitee.Id} have been add to team {currentTeamId}"
                    }
                );
                /*
                // Retrieve user's team ID or assign a new team if not already assigned
                int userTeamId = GetOrCreateUserTeam(user).Result.TeamId;

                // Generate a unique invitation token (you can use a GUID or any unique identifier)
                var invitationToken = Guid.NewGuid().ToString();

                // Save the invitation token in the database or cache for validation later
                await _invitationService.SaveInvitation(invitee.Email, invitationToken, userTeamId);

                // Construct the invitation email content
                var apiEndpoint = $"/api/OKR/AcceptInvite/{invitationToken}";
                var invitationLink = $"{_baseUrl}{apiEndpoint}";

                var emailSubject = "Invitation to join our team";
                var emailBody =
                    $"Hello,<br/><br/>You have been invited to join our team. Click <a href='{invitationLink}'>here</a> to accept the invitation.<br/><br/>Best regards,<br/>{user.FirstName + user.LastName}";

                // Send email using your email service or API
                await _emailService.SendEmailAsync(invitee.Email, emailSubject, emailBody);
                //Create OKR Log
                await _okrLogRepository.AddOKRLog(new OKRLog
                {
                    UserId = user.Id,
                    ChangeDate = DateTime.Now,
                    ChangeType = "Invite Member",
                    ChangeDetails = $"Invite Member {invitee} to team"
                });*/
                await transaction.CommitAsync();
                return Ok(new { Message = "invitationSent", InviteeEmail = invitee.Email });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // POST: api/OKR/AcceptInvite/{invitationToken}
        /// <summary>
        /// Accepts the invitation to join a team.
        /// </summary>
        /// <param name="invitationToken">The token from the invitation email.</param>
        /// <returns>An ActionResult indicating success or failure of the invitation acceptance.</returns>
        //[HttpPost("AcceptInvite/{invitationToken}")]
        //public async Task<ActionResult> AcceptInvite(string invitationToken)
        //{
        //    using var transaction = await _context.Database.BeginTransactionAsync();
        //    try
        //    {
        //        // Validate the invitation token
        //        var invite = await _invitationService.ValidateInvitationToken(invitationToken);
        //        if (invite == null)
        //        {
        //            return BadRequest(new { Message = "invalidInvitationToken" });
        //        }

        //        // Retrieve current user based on JWT token
        //        var user = await _userManager.FindByIdAsync(
        //            User.FindFirstValue(JwtRegisteredClaimNames.Jti)
        //        );
        //        if (user == null)
        //        {
        //            return NotFound(new { Message = "noUserFound" });
        //        }
        //        // Check if the user is already a member of the team
        //        var existingMembership =
        //            await _teamMembershipRepository.GetTeamMembershipByTeamId_UserId(
        //                invite.TeamId,
        //                user.Id
        //            );
        //        if (existingMembership != null)
        //        {
        //            return BadRequest(new { Message = "userAlreadyMemberOfTeam" });
        //        }
        //        // Add the user to the team
        //        await _teamMembershipRepository.AddTeamMembership(
        //            new TeamMembership { TeamId = invite.TeamId, UserId = user.Id }
        //        );

        //        // Mark the invitation as used
        //        await _invitationService.MarkInvitationAsUsed(invitationToken);
        //        //Create OKR Log
        //        await _okrLogRepository.AddOKRLog(
        //            new OKRLog
        //            {
        //                UserId = user.Id,
        //                ChangeDate = DateTime.Now,
        //                ChangeType = "A new member",
        //                ChangeDetails = $"A new member:{user.Id} have been add to team"
        //            }
        //        );
        //        await transaction.CommitAsync();
        //        return Ok(new { Message = "invitationAccepted" });
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError($"Error occurred: {ex}");
        //        return StatusCode(
        //            500,
        //            new
        //            {
        //                Message = "systemError",
        //                ExceptionMessage = ex.Message,
        //                StackTrace = ex.StackTrace
        //            }
        //        );
        //    }
        //}

        // DELETE: api/OKR/RemoveMember
        /// <summary>
        /// Removes a member from a team.
        /// </summary>
        /// <param name="teamId">The ID of the team from which the member will be removed.</param>
        /// <param name="userId">The ID of the user to be removed from the team.</param>
        /// <returns>An ActionResult indicating success or failure of the removal.</returns>
        [Authorize]
        [HttpDelete("RemoveMember")]
        public async Task<ActionResult> RemoveMember([FromBody] RemoveMemberDTO removeMemberDTO)
        {
            using var transaction = await _context.Database.BeginTransactionAsync();
            try
            {
                // Retrieve current user based on JWT token
                var user = await _userManager.FindByIdAsync(
                    User.FindFirstValue(JwtRegisteredClaimNames.Jti)
                );
                if (user == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }

                // Check if the user to remove exists
                var member = await _userManager.FindByIdAsync(removeMemberDTO.UserId);
                if (member == null)
                {
                    return NotFound(new { Message = "memberNotFound" });
                }

                // Find and remove the member from the team
                var teamMember = await _teamMembershipRepository.GetTeamMembershipByTeamId_UserId(
                    removeMemberDTO.TeamId,
                    removeMemberDTO.UserId
                );

                if (teamMember == null)
                {
                    return NotFound(new { Message = "teamMemberNotFound" });
                }

                await _teamMembershipRepository.DeleteTeamMembership(
                    removeMemberDTO.TeamId,
                    removeMemberDTO.UserId
                );
                await _context.SaveChangesAsync();

                // Create OKR Log
                await _okrLogRepository.AddOKRLog(
                    new OKRLog
                    {
                        UserId = user.Id,
                        ChangeDate = DateTime.Now,
                        ChangeType = "Remove Member",
                        ChangeDetails = $"Removed Member {member.UserName} from team"
                    }
                );

                await transaction.CommitAsync();
                return Ok(new { Message = "memberRemoved", UserId = removeMemberDTO.UserId });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // GET: api/OKR/GetAllUsers
        /// <summary>
        /// Retrieves a list of all users.
        /// </summary>
        /// <returns>An ActionResult containing a list of users.</returns>
        [Authorize]
        [HttpGet("GetAllUsers")]
        public async Task<ActionResult> GetAllUsers()
        {
            try
            {
                // Retrieve all users in lab
                var labId = int.Parse(User.FindFirstValue("labId"));
                var users = (await _userManager.GetUsersInRoleAsync("Intern"))
                    .Concat(await _userManager.GetUsersInRoleAsync("Mentor"))
                    .Where(x => x.LabId == labId && x.Status != false)
                    .ToList();

                return Ok(new { users });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // GET: api/OKR/GetUserTeamId
        /// <summary>
        /// Retrieves the team ID for the current user.
        /// </summary>
        /// <returns>An ActionResult containing the team ID if successful, or an error message if not.</returns>
        [HttpGet("GetUserTeamId")]
        [Authorize] // Ensure authorized access
        public async Task<ActionResult> GetUserTeamId()
        {
            try
            {
                // Retrieve user's information
                var user = await _userManager.FindByIdAsync(
                    User.FindFirstValue(JwtRegisteredClaimNames.Jti)
                );
                if (user == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }

                // Get or create the user's team
                var teams = await GetUserTeamsAsync(user);
                if (teams == null || !teams.Any())
                {
                    return NotFound(new { Message = "noTeamsFound" });
                }

                // Return the list of team data
                var teamData = teams.Select(t => new { t.TeamId, t.Name }).ToList();
                return Ok(new { Teams = teamData });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // GET: api/OKR/GetTeamMenber/{TeamId}
        /// <summary>
        /// Retrieves the team ID for the current user.
        /// </summary>
        /// <returns>An ActionResult containing the team ID if successful, or an error message if not.</returns>
        [HttpGet("GetTeamMenber/{TeamId}")]
        [Authorize] // Ensure authorized access
        public async Task<ActionResult> GetTeamMenber(int TeamId)
        {
            try
            {
                // Retrieve user's information
                var user = await _userManager.FindByIdAsync(
                    User.FindFirstValue(JwtRegisteredClaimNames.Jti)
                );
                if (user == null)
                {
                    return NotFound(new { Message = "noUserFound" });
                }

                var teammenber = await _teamMembershipRepository.GetAllTeamMembership();
                var memberships = teammenber.Where(tm => tm.TeamId == TeamId).ToList();
                var member = new List<UserInforDTO>();
                foreach (var tm in memberships)
                {
                    member.Add(
                        new UserInforDTO()
                        {
                            UserId = tm.UserId,
                            Username = string.Concat(tm.User.FirstName, " ", tm.User.LastName)
                                .Trim(),
                            Email = tm.User.Email,
                        }
                    );
                }

                if (!member.Any())
                {
                    return NotFound(new { Meassage = "noTeamMemberFound", TeamId = TeamId });
                }
                return Ok(new { TeamMember = member });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "systemError",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // GET: api/OKR/GetUser/{UserId}
        /// <summary>
        /// Retrieves user information based on the provided user ID.
        /// </summary>
        /// <param name="UserId">The ID of the user to retrieve information for.</param>
        /// <returns>An ActionResult containing user information if successful, or an error message if not.</returns>
        [HttpGet("GetUser/{UserId}")]
        [Authorize] // Ensure authorized access
        public async Task<ActionResult> GetUser(string UserId)
        {
            try
            {
                // Retrieve user information
                var user = await _userManager.FindByIdAsync(UserId);
                if (user == null)
                {
                    return NotFound(new { Message = "User not found" });
                }

                // Create a DTO to return
                var userDto = new UserInforDTO
                {
                    UserId = user.Id,
                    Username = string.Concat(user.FirstName, " ", user.LastName),
                    Email = user.Email,
                };

                return Ok(userDto);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "Internal server error",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        [HttpPost("approveObjective/{id}")]
        [Authorize]
        public async Task<IActionResult> ApproveObjective(int id)
        {
            try
            {
                var objective = await _context.Objectives.FindAsync(id);
                if (objective == null)
                {
                    return NotFound();
                }

                objective.IsApprove = !objective.IsApprove;
                await _context.SaveChangesAsync();

                return Ok(objective);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error occurred: {ex}");
                return StatusCode(
                    500,
                    new
                    {
                        Message = "Internal server error",
                        ExceptionMessage = ex.Message,
                        StackTrace = ex.StackTrace
                    }
                );
            }
        }

        // Helper method to sort objectives based on criteria and order
        private IEnumerable<Objective> SortObjectivesByCriteria(
            IEnumerable<Objective> objectives,
            string sortBy,
            string sortOrder
        )
        {
            if (string.IsNullOrEmpty(sortBy))
                return objectives;

            bool isDesc = string.Equals(sortOrder, "desc", StringComparison.OrdinalIgnoreCase);

            return (sortBy?.ToLower()) switch
            {
                "title"
                    => isDesc
                        ? objectives.OrderByDescending(o => o.Title).ToList()
                        : objectives.OrderBy(o => o.Title).ToList(),
                "description"
                    => isDesc
                        ? objectives.OrderByDescending(o => o.Description).ToList()
                        : objectives.OrderBy(o => o.Description).ToList(),
                "startdate"
                    => isDesc
                        ? objectives.OrderByDescending(o => o.StartDate).ToList()
                        : objectives.OrderBy(o => o.StartDate).ToList(),
                "enddate"
                    => isDesc
                        ? objectives.OrderByDescending(o => o.EndDate).ToList()
                        : objectives.OrderBy(o => o.EndDate).ToList(),
                // Add more cases for additional sorting criteria as needed
                _ => throw new ArgumentException("Invalid sort criteria.")
            };
        }

        // Helper method to sort Key result based on criteria and order
        private IEnumerable<KeyResult> SortKeyResultsByKeyCriteria(
            IEnumerable<KeyResult> keyResults,
            string? sortBy,
            string? sortOrder
        )
        {
            if (string.IsNullOrEmpty(sortBy))
                return keyResults;

            bool isDesc = string.Equals(sortOrder, "desc", StringComparison.OrdinalIgnoreCase);

            return sortBy.ToLower() switch
            {
                "title"
                    => isDesc
                        ? keyResults.OrderByDescending(kr => kr.Title)
                        : keyResults.OrderBy(kr => kr.Title),
                "createdat"
                    => isDesc
                        ? keyResults.OrderByDescending(kr => kr.CreatedAt)
                        : keyResults.OrderBy(kr => kr.CreatedAt),
                // Add more sorting criteria as needed
                _ => throw new ArgumentException("Invalid sort criteria.")
            };
        }

        // Helper method to sort Comment based on criteria and order
        private IEnumerable<Comment> SortCommentsByCriteria(
            IEnumerable<Comment> comments,
            string sortBy,
            string sortOrder
        )
        {
            if (string.IsNullOrEmpty(sortBy))
                return comments;

            bool isDesc = string.Equals(sortOrder, "desc", StringComparison.OrdinalIgnoreCase);

            return (sortBy?.ToLower()) switch
            {
                "content"
                    => isDesc
                        ? comments.OrderByDescending(o => o.Content).ToList()
                        : comments.OrderBy(o => o.Content).ToList(),
                "createdat"
                    => isDesc
                        ? comments.OrderByDescending(o => o.CreatedAt).ToList()
                        : comments.OrderBy(o => o.CreatedAt).ToList(),
                "updatedat"
                    => isDesc
                        ? comments.OrderByDescending(o => o.UpdatedAt).ToList()
                        : comments.OrderBy(o => o.UpdatedAt).ToList(),
                // Add more cases for additional sorting criteria as needed
                _ => throw new ArgumentException("Invalid sort criteria.")
            };
        }

        private async Task<IEnumerable<KeyResult>> RetrieveSearchFilteredKeyResults(
            string? searchTerm,
            string? assignUserId
        )
        {
            IEnumerable<KeyResult> keyResults;

            if (!string.IsNullOrWhiteSpace(searchTerm) && !string.IsNullOrWhiteSpace(assignUserId))
            {
                keyResults = await _keyResultRepository.SearchKeyResult(searchTerm);
                keyResults = keyResults.Where(k => k.Assign == assignUserId);
            }
            else if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                keyResults = await _keyResultRepository.SearchKeyResult(searchTerm);
            }
            else if (!string.IsNullOrWhiteSpace(assignUserId))
            {
                keyResults = await _keyResultRepository.GetKeyResultByAssignUserId(assignUserId);
            }
            else
            {
                keyResults = await _keyResultRepository.GetAllKeyResult();
            }

            return keyResults;
        }

        private async Task<User> GetUserAsync()
        {
            return await _userManager.FindByIdAsync(
                User.FindFirstValue(JwtRegisteredClaimNames.Jti)
            );
        }

        // Method to retrieve or create a user's team(s) and return the team IDs
        private async Task<List<Team>> GetUserTeamsAsync(User user)
        {
            try
            {
                // Retrieve user's team memberships asynchronously
                var teamMemberships = await _teamMembershipRepository.GetTeamMembershipsByUserId(
                    user.Id
                );
                var teamIds = teamMemberships.Select(tm => tm.TeamId).ToList();

                var teams = new List<Team>();
                if (teamIds.Any())
                {
                    // Fetch team details for the retrieved team IDs
                    var allTeams = await _teamRepository.GetAllTeam();
                    // Select teams that match the team IDs
                    teams = allTeams.Where(t => teamIds.Contains(t.TeamId)).ToList();
                }

                return teams;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error in GetUserTeamsAsync: {ex.Message}", ex);
                throw;
            }
        }

        private async Task<bool> UserHasAccessToObjective(User user, Objective objective)
        {
            var teams = await GetUserTeamsAsync(user);
            return teams.Any(t => t.TeamId == objective.TeamId);
        }

        private IEnumerable<string> GetModelErrors()
        {
            return ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage);
        }

        private async Task SendNotificationAsync(
            User creator,
            List<string> userIds,
            string type,
            string message,
            string targetUrl
        )
        {
            if (userIds == null || !userIds.Any())
            {
                userIds = new List<string> { creator.Id };
            }

            Notification notification = new Notification
            {
                CreatedByUserId = creator.Id,
                Type = type,
                Message = message,
                TargetUrl = targetUrl
            };

            await _notificationService.AddNotificationToUsersAsync(notification, userIds);
        }

        private async Task LogOKRChangeAsync(
            User user,
            int objectiveId,
            string changeType,
            string changeDetails
        )
        {
            await _okrLogRepository.AddOKRLog(
                new OKRLog
                {
                    ObjectiveId = objectiveId,
                    UserId = user.Id,
                    ChangeType = changeType,
                    ChangeDetails = changeDetails
                }
            );
        }

        // Helper method to check if a comment is accessible by the user's team
        private bool IsCommentAccessibleByUser(Comment comment, int userTeamId)
        {
            if (comment.ReletedObjectType == "Objective")
            {
                var objective = _objectiveRepository
                    .GetObjectiveByObjectiveId(comment.ReletedObjectId)
                    .Result;
                return objective != null && objective.TeamId == userTeamId;
            }

            if (comment.ReletedObjectType == "KeyResult")
            {
                var keyResult = _keyResultRepository
                    .GetKeyResultByKeyResultId(comment.ReletedObjectId)
                    .Result;
                return keyResult != null && keyResult.Objective.TeamId == userTeamId;
            }

            return false;
        }

        public static string[] ConvertToArray(string commaSeparated)
        {
            return string.IsNullOrEmpty(commaSeparated)
                ? Array.Empty<string>()
                : commaSeparated.Split(',');
        }

        // Convert array to comma-separated string
        public static string ConvertToCommaSeparated(string[] array)
        {
            return array.Length == 0 ? null : string.Join(",", array);
        }
    }

    public class UserInforDTO
    {
        public string UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }

        // Add any other properties you need
    }

    public class InviteDTO
    {
        public string Email { get; set; }
    }

    public class RemoveMemberDTO
    {
        public int TeamId { get; set; }
        public string UserId { get; set; }
    }

    public class ObjectiveDTO
    {
        public int ObjectiveId { get; set; }
        public string? CreateBy { get; set; }
        public int TeamId { get; set; } // Foreign key to Team for team-based access

        [Required]
        [StringLength(
            100,
            MinimumLength = 1,
            ErrorMessage = "Title must be between 1 and 100 characters."
        )]
        public string Title { get; set; }
        public bool IsApprove { get; set; }
        public int? Status { get; set; } // Example: 1"Not Started", 2"In Progress", 3"Completed"

        [StringLength(500, ErrorMessage = "Description can be up to 500 characters.")]
        public string Description { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTHH:mm}", ApplyFormatInEditMode = true)]
        [CustomDateValidation(ErrorMessage = "StartDate must be a valid date and before EndDate.")]
        public DateTime StartDate { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTHH:mm}", ApplyFormatInEditMode = true)]
        [CustomDateValidation(ErrorMessage = "EndDate must be a valid date and after StartDate.")]
        public DateTime EndDate { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTHH:mm}", ApplyFormatInEditMode = true)]
        public DateTime CreatedAt { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTHH:mm}", ApplyFormatInEditMode = true)]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
        //public byte[]? RowVersion { get; set; }
    }

    public class KeyResultDTO
    {
        public int KeyResultId { get; set; }

        [Required]
        public int ObjectiveId { get; set; }
        public string? Assign { get; set; } // User ID of the person assigned

        [Required]
        [StringLength(
            100,
            MinimumLength = 1,
            ErrorMessage = "Title must be between 1 and 100 characters."
        )]
        public string Title { get; set; }
        public string? Priority { get; set; } // Example: "High", "Medium", "Low"
        public int? Status { get; set; } // Example: 1"Not Started", 2"In Progress", 3"Completed"
        public string? Notes { get; set; }
        public string? AttachedFiles { get; set; } // Attached files (comma-separated file paths)

        [Range(0, double.MaxValue, ErrorMessage = "TargetValue must be a non-negative number.")]
        public decimal TargetValue { get; set; }

        [Range(0, double.MaxValue, ErrorMessage = "CurrentValue must be a non-negative number.")]
        public decimal CurrentValue { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTHH:mm}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTHH:mm}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }

        [Range(0, double.MaxValue, ErrorMessage = "EstimateMenDay must be a non-negative number.")]
        public decimal EstimateManDay { get; set; }

        [Range(0, double.MaxValue, ErrorMessage = "ActualMenDay must be a non-negative number.")]
        public decimal ActualManDay { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTHH:mm}", ApplyFormatInEditMode = true)]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTHH:mm}", ApplyFormatInEditMode = true)]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
    }

    public class CommentDTO
    {
        public int CommentId { get; set; }

        public int? ParentCommentId { get; set; }

        public string CreateById { get; set; }

        [Required]
        [StringLength(
            1000,
            MinimumLength = 1,
            ErrorMessage = "Content must be between 1 and 1000 characters."
        )]
        public string Content { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTHH:mm}", ApplyFormatInEditMode = true)]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTHH:mm}", ApplyFormatInEditMode = true)]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;
        public int ReletedObjectId { get; set; }
        public string ReletedObjectType { get; set; }
        public string? MentionedUserIds { get; set; } // Mentioned users (comma-separated user IDs)

        //public string[] MentionedUserIds { get; set; } = Array.Empty<string>();

        // Attached files (comma-separated file paths)
        public string? AttachedFiles { get; set; }
    }

    public class NotificationDTO
    {
        public int NotificationId { get; set; }

        //public string UserId { get; set; }  // User who receives the notification
        public string? CreatedByUserId { get; set; } // User who created the notification
        public string Type { get; set; } // Type of notification (e.g., comment, reply, event)
        public string Message { get; set; } // Message or description associated with the notification
        public bool IsRead { get; set; } = false; // Flag to indicate if the notification has been read

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTHH:mm}", ApplyFormatInEditMode = true)]
        public DateTime CreatedAt { get; set; } = DateTime.Now; // Timestamp when the notification was created
        public string TargetUrl { get; set; } // URL or route for redirection
    }

    public class OKRLogDTO
    {
        public int OKRLogId { get; set; } // Primary key
        public int? ObjectiveId { get; set; } // Foreign key to Objective
        public int? KeyResultId { get; set; } // Foreign key to Key Result (nullable if the change is related to an Objective)
        public string UserId { get; set; } // Foreign key to User who made the change

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTHH:mm}", ApplyFormatInEditMode = true)]
        public DateTime ChangeDate { get; set; } = DateTime.Now; // Date and time when the change was made
        public string ChangeType { get; set; } // Type of change (e.g., "Created", "Updated", "Deleted")
        public string ChangeDetails { get; set; } // Details of the change (e.g., "Changed status from 'In Progress' to 'Completed'")\
    }

    public class UpdateNotificationRequest
    {
        public bool IsRead { get; set; }
    }

    public class CustomDateValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(
            object value,
            ValidationContext validationContext
        )
        {
            // Ensure validation context is an ObjectiveDTO
            var objectiveDto = (ObjectiveDTO)validationContext.ObjectInstance;

            if (value is DateTime dateTimeValue)
            {
                if (validationContext.MemberName == nameof(ObjectiveDTO.StartDate))
                {
                    if (dateTimeValue >= objectiveDto.EndDate)
                    {
                        return new ValidationResult("StartDate must be before EndDate.");
                    }
                }
                else if (validationContext.MemberName == nameof(ObjectiveDTO.EndDate))
                {
                    if (dateTimeValue <= objectiveDto.StartDate)
                    {
                        return new ValidationResult("EndDate must be after StartDate.");
                    }
                }
            }
            return ValidationResult.Success;
        }
    }
}
