﻿using System.IdentityModel.Tokens.Jwt;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Security.Claims;
using DocumentFormat.OpenXml;
using LaboratoryInternshipManagement.DataAccess.Infrastructures;
using LaboratoryInternshipManagement.DataAccess.ValidationAttributes;
using LaboratoryInternshipManagement.Models;
using LaboratoryInternshipManagement.Models.Enums;
using LaboratoryInternshipManagement.Services.IServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using X.PagedList;
using static LaboratoryInternshipManagement.DataAccess.Repositories.AttendenceRepository;

namespace LaboratoryInternshipManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AttendanceController : ControllerBase
    {
        private readonly UserManager<User> _userManager;
        private readonly LabInternManagementDBcontext _context;
        private readonly IEmailService _emailService;
        public readonly IUnitOfWork _unitOfWork;
        private readonly IMemoryCache _memoryCache;
        private readonly ILogger<AttendanceController> _logger;
        public readonly IServiceProvider _serviceProvider;

        public AttendanceController(
            UserManager<User> userManager,
            LabInternManagementDBcontext context,
            IUnitOfWork unitOfWork,
            IEmailService emailService,
            ILogger<AttendanceController> logger,
            IMemoryCache memoryCache,
            IServiceProvider serviceProvider
        )
        {
            _serviceProvider = serviceProvider;
            _userManager = userManager;
            _context = context;
            _unitOfWork = unitOfWork;
            _emailService = emailService;
            _memoryCache = memoryCache;
            _logger = logger;
        }

        [Authorize(Roles = "Lab Manager")]
        [HttpGet("getLabInfo")]
        public async Task<IActionResult> getLabInfo()
        {
            try
            {
                var userId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);
                var laboratory = await _context
                    .Laboratories.Where(x => x.LabManagerId.Equals(userId))
                    .FirstOrDefaultAsync();

                return Ok(laboratory);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        [Authorize(Roles = "Lab Manager")]
        [HttpPut("updateStatus")]
        public async Task<IActionResult> UpdateAttendanceStatus(
            [FromBody] UpdateStatusAttendanceInputDto inputDto
        )
        {
            try
            {
                var userId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);
                var user = await _userManager.FindByIdAsync(userId);
                var outputUpdateAttendanceStatus =
                    _unitOfWork.AttendenceRepository.UpdateStatusAttendance(
                        new UpdateStatusAttendanceInputDto()
                        {
                            AttendanceId = inputDto.AttendanceId,
                            CheckInTime = inputDto.CheckInTime,
                            CheckOutTime = inputDto.CheckOutTime,
                            LabManagerId = userId
                        }
                    );
                ;
                var output = new
                {
                    CheckInTime = outputUpdateAttendanceStatus.CheckInTime.Value.ToString(
                        @"hh\:mm"
                    ),
                    CheckOutTime = outputUpdateAttendanceStatus.CheckOutTime.Value.ToString(
                        @"hh\:mm"
                    ),
                    Id = outputUpdateAttendanceStatus.Id,
                    Date = outputUpdateAttendanceStatus.Date.ToString("MM-dd-yyyy"),
                    Status = outputUpdateAttendanceStatus.Status,
                    Reason = outputUpdateAttendanceStatus.Reason
                };
                return Ok(output);
            }
            catch (Exception ex)
            {
                if (ex.Message.Equals("invalidTime"))
                {
                    return StatusCode(422, new { Message = ex.Message });
                }
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        [Authorize(Roles = "Lab Manager")]
        [HttpPut("updateTime")]
        public async Task<IActionResult> UpdateTimeLaboratoryAttendance(
            [FromBody] UpdateTimeLaboratoryAttendanceInputDto inputDto
        )
        {
            try
            {
                var userId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);
                var labId = User.FindFirstValue("labId");
                var user = await _userManager.FindByIdAsync(userId);
                if (labId != null)
                {
                    if (
                        TimeSpan.Parse(inputDto.CheckInTime) < TimeSpan.Parse(inputDto.CheckOutTime)
                    )
                    {
                        var outputUpdateAttendanceStatus =
                            _unitOfWork.AttendenceRepository.UpdateTimeLaboratoryAttendance(
                                new UpdateTimeLaboratoryAttendanceInput()
                                {
                                    LabId = int.Parse(labId),
                                    CheckInTime = TimeSpan.Parse(inputDto.CheckInTime),
                                    CheckOutTime = TimeSpan.Parse(inputDto.CheckOutTime),
                                    LeaveRequestNum = inputDto.LeaveRequestNum,
                                }
                            );

                        if (outputUpdateAttendanceStatus)
                        {
                            return Ok("attendance.updateSuccess");
                        }
                        else
                        {
                            return Ok("settingLab.nothingChange");
                        }
                    }
                    else
                    {
                        return StatusCode(
                            500,
                            new { Message = "attendance.validateTimeCheckInCheckOut" }
                        );
                    }
                }
                return StatusCode(500, new { Message = "attendance.updateFailse" });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        [Authorize(Roles = "Intern")]
        [HttpGet("attendanceLst")]
        public async Task<IActionResult> GetAttendanceIntern(
            [FromQuery] InputGetAttendanceLabManager inputDto
        )
        {
            try
            {
                var userId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);
                var user = await _userManager.FindByIdAsync(userId);

                if (user != null)
                {
                    return Ok(
                        await _unitOfWork.AttendenceRepository.GetListAttendanceIntern(
                            user,
                            inputDto
                        )
                    );
                }
                else
                {
                    return BadRequest(new { Message = "noUserFound" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "attendance.systemError" });
            }
        }

        [Authorize(Roles = "Lab Manager, Mentor")]
        [HttpGet("labAttendanceLst")]
        public async Task<IActionResult> GetAttendance(
            [FromQuery] InputGetAttendanceLabManager inputDto
        )
        {
            try
            {
                return Ok(
                    await _unitOfWork.AttendenceRepository.GetListAttendance(
                        User.FindFirstValue("jti"),
                        inputDto,
                        int.Parse(User.FindFirstValue("labId"))
                    )
                );
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        [Authorize(Roles = "Intern")]
        [HttpGet("get-lab-regist-info")]
        public async Task<IActionResult> GetLabRegistInfo()
        {
            var labRegistInfo = await getOrCreateLabInfo();
            var labTime = _context
                .Laboratories.Select(l => new LabTimeInfoDTI
                {
                    CheckInTime = l.CheckInTime,
                    CheckOutTime = l.CheckOutTime,
                })
                .FirstOrDefault();
            var isAttended = _context
                .Attendances.Where(a =>
                    a.InternId.Equals(User.FindFirstValue(JwtRegisteredClaimNames.Jti))
                    && a.Date == DateTime.Today
                )
                .Select(a => new AttendanceTime
                {
                    CheckInTime = a.CheckInTime,
                    CheckOutTime = a.CheckOutTime,
                })
                .FirstOrDefault();

            return Ok(
                new
                {
                    labRegistInfo.FaceVector,
                    isAttended,
                    labTime
                }
            );
        }

        [Authorize(Roles = "Intern")]
        [HttpPost("sent-otp")]
        public async Task<IActionResult> SentOtpAttendance()
        {
            try
            {
                if (_memoryCache.Get($"OTP_{User.FindFirstValue("rollName")}") != null)
                {
                    return BadRequest(new { Message = "attendance.otpSent5min" });
                }
                var internLabInfo = await getOrCreateLabInfo();
                //if (!CheckMacAndIpAddress(internLabInfo))
                //{
                //    return BadRequest(new { Message = "attendance.wrongIpMac" });
                //}
                var user = await _context.Users.FirstOrDefaultAsync(x =>
                    x.Id.Equals(internLabInfo.InternId)
                );
                if (user == null)
                {
                    return NotFound(new { Message = "attendance.noUserFound" });
                }

                var otp = GenerateOtp();
                await _emailService.SendEmailAsync(user.Email, "Your OTP", $"Your OTP is {otp}");

                // Lưu vào memory với life time là 5'
                _memoryCache.Set(
                    $"OTP_{User.FindFirstValue("rollName")}",
                    otp,
                    TimeSpan.FromMinutes(5)
                );

                return Ok(new { Message = "attendance.otpSent", Otp = otp });
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        [Authorize(Roles = "Intern")]
        [HttpPost("attendanceIn")]
        public async Task<IActionResult> AttendanceIn([FromBody] AttendanceValues attendanceValues)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var internLabInfo = await getOrCreateLabInfo(attendanceValues.FaceVector);
                    //if (!CheckMacAndIpAddress(internLabInfo))
                    //{
                    //    return BadRequest(new { Message = "attendance.wrongIpMac" });
                    //}
                    if (!String.IsNullOrEmpty(attendanceValues.FaceVector))
                    {
                        float[] a = ConvertJsonToFloat32Array(internLabInfo.FaceVector);
                        float[] b = ConvertJsonToFloat32Array(attendanceValues.FaceVector);
                        if (!AreFaceVectorsSimilar(a, b))
                        {
                            return BadRequest(new { Message = "attendance.wrongFaceVector" });
                        }
                    }
                    else if (!String.IsNullOrEmpty(attendanceValues.OTP))
                    {
                        var localOTP = _memoryCache.Get($"OTP_{User.FindFirstValue("rollName")}");
                        if (localOTP == null || !localOTP.Equals(attendanceValues.OTP))
                        {
                            return BadRequest(new { Message = "attendance.wrongOtp" });
                        }
                    }

                    var lab = await _context
                        .Laboratories.Where(x => x.Id == internLabInfo.LabID)
                        .FirstOrDefaultAsync();
                    if (lab == null)
                    {
                        return BadRequest(new { Message = "attendance.noLabFound" });
                    }

                    var internAttendence = await _context
                        .Attendances.Where(x =>
                            x.InternId == User.FindFirstValue(JwtRegisteredClaimNames.Jti)
                            && x.Date == System.DateTime.Today
                        )
                        .FirstOrDefaultAsync();
                    if (internAttendence == null)
                    {
                        var newInternAttendance = new Attendance();

                        var checkInTime = DateTime.Now;
                        var checkInLabTime = DateTime.Today + lab.CheckInTime;
                        var checkOutLabTime = DateTime.Today + lab.CheckOutTime;
                        if (checkInTime >= checkInLabTime)
                        {
                            newInternAttendance.Date = DateTime.Today;
                            newInternAttendance.InternId = internLabInfo.InternId;
                            newInternAttendance.CheckInTime = checkInTime.TimeOfDay;
                            _context.Attendances.Add(newInternAttendance);
                            await _context.SaveChangesAsync();

                            return Ok(new { Message = "attendance.checkInDone" });
                        }

                        return BadRequest(new { Message = "attendance.notInTime" });
                    }

                    return BadRequest(new { Message = "attendance.checkInExisted" });
                }

                return UnprocessableEntity(ModelState);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }

        [Authorize(Roles = "Intern")]
        [HttpPost("attendanceOut")]
        public async Task<IActionResult> AttendanceOut([FromBody] AttendanceValues attendanceValues)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var internLabInfo = await getOrCreateLabInfo(attendanceValues.FaceVector);
                    //if (!CheckMacAndIpAddress(internLabInfo))
                    //{
                    //    return BadRequest(new { Message = "attendance.wrongIpMac" });
                    //}
                    if (!String.IsNullOrEmpty(attendanceValues.FaceVector))
                    {
                        float[] a = ConvertJsonToFloat32Array(internLabInfo.FaceVector);
                        float[] b = ConvertJsonToFloat32Array(attendanceValues.FaceVector);
                        if (!AreFaceVectorsSimilar(a, b))
                        {
                            return BadRequest(new { Message = "attendance.wrongFaceVector" });
                        }
                    }
                    else if (!String.IsNullOrEmpty(attendanceValues.OTP))
                    {
                        var localOTP = _memoryCache.Get($"OTP_{User.FindFirstValue("rollName")}");
                        if (localOTP == null || !localOTP.Equals(attendanceValues.OTP))
                        {
                            return BadRequest(new { Message = "wrongOtp" });
                        }
                    }
                    var lab = await _context
                        .Laboratories.Where(x => x.Id == Int32.Parse(User.FindFirstValue("labId")))
                        .FirstOrDefaultAsync();
                    if (lab == null)
                    {
                        return BadRequest(new { Message = "attendance.noLabFound" });
                    }

                    var checkOutAttendance = await _context
                        .Attendances.Where(a =>
                            a.Date == DateTime.Today && a.InternId.Equals(internLabInfo.InternId)
                        )
                        .FirstOrDefaultAsync();

                    if (checkOutAttendance != null)
                    {
                        var checkOutTime = DateTime.Now;
                        var checkOutLabTime = DateTime.Today + lab.CheckOutTime;
                        if (checkOutTime >= checkOutLabTime)
                        {
                            checkOutAttendance.CheckOutTime = checkOutTime.TimeOfDay;
                            _context.Update(checkOutAttendance);
                            _context.SaveChanges();
                            return Ok(new { Message = "attendance.checkOutDone" });
                        }
                        return BadRequest(new { Message = "attendance.notInTime" });
                    }
                    return BadRequest(new { Message = "attendance.notCheckInYet" });
                }
                return UnprocessableEntity(ModelState);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return BadRequest(new { Message = "systemError" });
            }
        }

        [Authorize(Roles = "System Admin")]
        [HttpPost("autoBatchAttendance")]
        public async Task<IActionResult> AutoBatchAttendance([FromBody] string status)
        {
            try
            {
                _logger.LogInformation($"{DateTime.Today} Attendance batch is starting...");
                if (status.IsNullOrEmpty())
                {
                    return BadRequest(new { Message = "attendance.statusBatchNullOrEmpty" });
                }
                else if (status.Equals("off"))
                {
                    var today = DateTime.Today;
                    var isAttendedToday = await _context
                        .Attendances.Where(x => x.Date == today)
                        .ToListAsync();
                    if (isAttendedToday.Count > 0)
                    {
                        var nullTime = new TimeSpan(0, 0, 0);
                        var isAbsent = isAttendedToday
                            .Where(x => x.CheckInTime == nullTime && x.CheckOutTime == nullTime)
                            .ToList();
                        var isAttending = isAttendedToday.Except(isAbsent).ToList();
                        isAttending
                            .Where(x => x.CheckOutTime == null)
                            .ToList()
                            .ForEach(attendee =>
                            {
                                attendee.Status = 0;
                                attendee.Reason = string.Empty;
                            });
                        _context.RemoveRange(isAbsent);
                        _context.UpdateRange(isAttending);
                        await _context.SaveChangesAsync();
                        return Ok(new { Message = "attendance.cleanBatch" });
                    }

                    return Ok(new { Message = "attendance.batchEnd" });
                }
                else if (status.Equals("on"))
                {
                    if (!IsWeekend(DateTime.Today))
                    {
                        using (var scope = _serviceProvider.CreateScope())
                        {
                            var DbContext =
                                scope.ServiceProvider.GetRequiredService<LabInternManagementDBcontext>();
                            var userManager = scope.ServiceProvider.GetRequiredService<
                                UserManager<User>
                            >();
                            var today = DateTime.Today;

                            var isAttendedToday = await DbContext
                                .Attendances.Where(x => x.Status != 0 && x.Date == today)
                                .ToListAsync();

                            if (isAttendedToday.Count > 0)
                            {
                                return Ok(new { Message = "attendance.batchEnd" });
                            }

                            // Fetch attendence records for today
                            var allInternUsers = await userManager.GetUsersInRoleAsync("Intern");
                            var internIds = new HashSet<string>(allInternUsers.Select(x => x.Id));

                            // Fetch all interns, their attendance for today, and their laboratory information
                            var allInterns = await DbContext
                                .Users.Include(u => u.Laboratory)
                                .ThenInclude(x => x.DayOffs)
                                .Include(u =>
                                    u.Attendances.Where(a => a.Date == today && a.Status == 0)
                                )
                                .Where(u => internIds.Contains(u.Id) && u.Laboratory != null)
                                .ToListAsync();

                            var lstInternChecked = new List<Attendance>();
                            var lstInternNotCheckIn = new List<User>();

                            foreach (var intern in allInterns)
                            {
                                var attendance = intern?.Attendances?.FirstOrDefault();
                                if (attendance != null)
                                {
                                    lstInternChecked.Add(attendance);
                                }
                                else
                                {
                                    lstInternNotCheckIn.Add(intern);
                                }
                            }

                            if (lstInternNotCheckIn.Any())
                            {
                                foreach (var student in lstInternNotCheckIn)
                                {
                                    var lstDayOff = student
                                        .Laboratory?.DayOffs?.Select(x => x.Date)
                                        .ToList();

                                    if (lstDayOff.IsNullOrEmpty() || !IsHolidays(lstDayOff))
                                    {
                                        DbContext.Attendances.Add(
                                            new Attendance
                                            {
                                                InternId = student.Id,
                                                Date = today,
                                                CheckInTime = new TimeSpan(0, 0, 0),
                                                CheckOutTime = new TimeSpan(0, 0, 0),
                                                Status = (int)ConstantEnum.AttendanceStatus.ABSENT,
                                                Reason = Enum.GetName(
                                                    typeof(ConstantEnum.AttendanceStatus),
                                                    ConstantEnum.AttendanceStatus.ABSENT
                                                ),
                                            }
                                        );
                                    }
                                }
                            }
                            foreach (var attend in lstInternChecked)
                            {
                                var internLab = attend.Intern?.Laboratory;

                                if (internLab == null)
                                {
                                    continue;
                                }
                                else
                                {
                                    if (
                                        attend.CheckInTime >= internLab.CheckInTime
                                        && attend.CheckInTime
                                            <= internLab.CheckInTime.Add(new TimeSpan(0, 30, 0))
                                        && attend.CheckOutTime >= internLab.CheckOutTime
                                    )
                                    {
                                        attend.Status = (int)ConstantEnum.AttendanceStatus.ATTENDED;
                                        attend.Reason = Enum.GetName(
                                            typeof(ConstantEnum.AttendanceStatus),
                                            ConstantEnum.AttendanceStatus.ATTENDED
                                        );
                                    }
                                    else if (attend.CheckOutTime == null)
                                    {
                                        attend.Status = (int)ConstantEnum.AttendanceStatus.SOONOUT;
                                        attend.Reason = Enum.GetName(
                                            typeof(ConstantEnum.AttendanceStatus),
                                            ConstantEnum.AttendanceStatus.SOONOUT
                                        );
                                    }
                                    else
                                    {
                                        attend.Status = (int)ConstantEnum.AttendanceStatus.LATEIN;
                                        attend.Reason = Enum.GetName(
                                            typeof(ConstantEnum.AttendanceStatus),
                                            ConstantEnum.AttendanceStatus.LATEIN
                                        );
                                    }
                                }
                            }
                            DbContext.Attendances.UpdateRange(lstInternChecked);
                            await DbContext.SaveChangesAsync();

                            _logger.LogInformation($"{DateTime.Today} Attendance batch ended.");
                        }
                        return Ok(new { Message = "attendance.successBatch" });
                    }
                    else
                    {
                        _logger.LogInformation(
                            $"{DateTime.Today} Attendance batch is stopped. Happy Holiday/Weekend!"
                        );
                        return Ok(new { Message = "attendance.checkBatch" });
                    }
                }
                return Ok(new { Message = "attendance.batchEnd" });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return BadRequest(new { Message = "systemError" });
            }
        }

        // Hàm kiểm tra xem ngày có phải là ngày nghỉ không
        static bool IsWeekend(DateTime date)
        {
            // Kiểm tra nếu ngày là thứ Bảy hoặc Chủ Nhật
            if (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
            {
                return true;
            }

            return false;
        }

        // Hàm kiểm tra xem ngày có phải là ngày nghỉ không
        static bool IsHolidays(List<DateTime> dayOff)
        {
            // Kiểm tra nếu ngày là ngày nghỉ lễ
            if (dayOff.Contains(DateTime.Today))
            {
                return true;
            }

            return false;
        }

        static double CalculateEuclideanDistance(float[] vector1, float[] vector2)
        {
            if (vector1.Length != vector2.Length)
            {
                throw new ArgumentException("Vectors must have the same length");
            }

            double sum = 0;
            for (int i = 0; i < vector1.Length; i++)
            {
                sum += Math.Pow(vector1[i] - vector2[i], 2);
            }

            return Math.Sqrt(sum);
        }

        public static float[] ConvertJsonToFloat32Array(string jsonString)
        {
            // Deserialize JSON string to dictionary
            var dict = JsonConvert.DeserializeObject<Dictionary<string, float>>(jsonString);

            if (dict == null || dict.Count == 0)
            {
                throw new ArgumentException("Invalid JSON string");
            }

            // Create a new Float32Array with 128 elements
            float[] floatArray = new float[128];

            // Map dictionary values to Float32Array
            for (int i = 0; i < 128; i++)
            {
                string key = i.ToString();
                if (dict.ContainsKey(key))
                {
                    floatArray[i] = dict[key];
                }
                else
                {
                    throw new ArgumentException($"Key {key} not found in JSON");
                }
            }

            return floatArray;
        }

        static bool AreFaceVectorsSimilar(float[] vector1, float[] vector2, double threshold = 0.6)
        {
            double distance = CalculateEuclideanDistance(vector1, vector2);
            return distance < threshold;
        }

        private string GenerateOtp()
        {
            var random = new Random();
            return random.Next(100000, 999999).ToString();
        }

        /// <summary>
        /// Lấy địa chỉ ip
        /// </summary>
        /// <param name="context">Chuỗi kết nối</param>
        /// <returns>địa chỉ ip của máy</returns>
        private string GetIpAddress(HttpContext context)
        {
            var ip = context.Request.Headers["X-Forwarded-For"].FirstOrDefault();
            if (string.IsNullOrEmpty(ip))
            {
                ip = context.Connection.RemoteIpAddress?.ToString();
            }
            return ip;
        }

        /// <summary>
        /// Lấy ra địa chỉ Ip khi kết nối wifi
        /// </summary>

        static List<string> GetListLocalIPv4()
        {
            List<string> ipAddresses = new List<string>();
            foreach (NetworkInterface item in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (
                    item.OperationalStatus == OperationalStatus.Up
                    && (
                        item.NetworkInterfaceType == NetworkInterfaceType.Ethernet
                        || item.NetworkInterfaceType == NetworkInterfaceType.Wireless80211
                    )
                )
                {
                    foreach (
                        UnicastIPAddressInformation ip in item.GetIPProperties().UnicastAddresses
                    )
                    {
                        if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                        {
                            ipAddresses.Add(ip.Address.ToString());
                        }
                    }
                }
            }
            return ipAddresses;
        }

        static string GetLocalIPv4()
        {
            string ipAddresses = string.Empty;
            foreach (NetworkInterface item in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (
                    item.OperationalStatus == OperationalStatus.Up
                    && (
                        item.NetworkInterfaceType == NetworkInterfaceType.Ethernet
                        || item.NetworkInterfaceType == NetworkInterfaceType.Wireless80211
                    )
                )
                {
                    foreach (
                        UnicastIPAddressInformation ip in item.GetIPProperties().UnicastAddresses
                    )
                    {
                        if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                        {
                            ipAddresses = ip.Address.ToString();
                        }
                    }
                }
            }
            return ipAddresses;
        }

        /// <summary>
        /// Lấy ra tất cả địa chỉ MAC của lap đang hoạt động
        /// </summary>
        private List<string> GetListMacAdd()
        {
            List<string> macAddresses = new List<string>();
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (
                    nic.OperationalStatus == OperationalStatus.Up
                    && (
                        nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet
                        || nic.NetworkInterfaceType == NetworkInterfaceType.Wireless80211
                    )
                )
                {
                    macAddresses.Add(nic.GetPhysicalAddress().ToString());
                }
            }
            return macAddresses;
        }

        private string GetMacAdd()
        {
            string macAddresses = string.Empty;
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (
                    nic.OperationalStatus == OperationalStatus.Up
                    && (
                        nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet
                        || nic.NetworkInterfaceType == NetworkInterfaceType.Wireless80211
                    )
                )
                {
                    macAddresses = nic.GetPhysicalAddress().ToString();
                }
            }
            return macAddresses;
        }

        private async Task<LabInformationRegist> getOrCreateLabInfo(string? faceVector = "")
        {
            var internLabInfo = await _context
                .LabInformationRegist.Where(x =>
                    x.InternId.Equals(User.FindFirstValue(JwtRegisteredClaimNames.Jti))
                )
                .FirstOrDefaultAsync();
            if (internLabInfo == null)
            {
                internLabInfo = new LabInformationRegist()
                {
                    InternId = User.FindFirstValue(JwtRegisteredClaimNames.Jti),
                    //IpAdress = GetLocalIPv4(),
                    //MacAdress = GetMacAdd(),
                    LabID = int.Parse(User.FindFirstValue("labId")),
                    FaceVector = faceVector
                };

                await _context.LabInformationRegist.AddAsync(internLabInfo);
                await _context.SaveChangesAsync();
            }

            if (String.IsNullOrEmpty(internLabInfo.FaceVector) && !String.IsNullOrEmpty(faceVector))
            {
                internLabInfo.FaceVector = faceVector.Trim();
                _context.LabInformationRegist.Update(internLabInfo);
                await _context.SaveChangesAsync();
            }

            return internLabInfo;
        }

        private bool CheckMacAndIpAddress(LabInformationRegist labInfo)
        {
            var matchIp = false;
            var matchMac = false;
            foreach (var ip in GetListLocalIPv4())
            {
                if (ip.Equals(labInfo.IpAdress))
                {
                    matchIp = true;
                    break;
                }
            }
            foreach (var mac in GetListMacAdd())
            {
                if (mac.Equals(labInfo.MacAdress))
                {
                    matchMac = true;
                    break;
                }
            }

            if (matchMac && matchIp)
            {
                return true;
            }
            return false;
        }
    }

    internal class AttendanceTime
    {
        public TimeSpan? CheckInTime { get; set; }
        public TimeSpan? CheckOutTime { get; set; }
    }

    // Models/CheckIpMacRequest.cs
    public class LabTimeInfoDTI
    {
        public TimeSpan CheckInTime { get; set; }
        public TimeSpan CheckOutTime { get; set; }
    }

    // Attendance
    public class AttendanceValues
    {
        [RequiredIfOneExistsAttendance("FaceVector")]
        public string? OTP { get; set; }

        [RequiredIfOneExistsAttendance("OTP")]
        public string? FaceVector { get; set; }
    }

    public class InputGetAttendanceIntern
    {
        public int PageNumber { get; set; } = 1;
        public string? StartDate { get; set; } = DateTime.Now.AddDays(-7).Date.ToString();
        public string? EndDate { get; set; } = DateTime.Now.Date.ToString();
    }

    public class InputGetAttendanceLabManager : InputGetAttendanceIntern
    {
        public string? Keyword { get; set; }
    }
}
