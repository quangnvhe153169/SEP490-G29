﻿using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Reflection;
using System.Security.Claims;
using DocumentFormat.OpenXml.Wordprocessing;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.DataAccess.Repositories;
using LaboratoryInternshipManagement.Models;
using LaboratoryInternshipManagement.Models.DTOs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;

namespace LaboratoryInternshipManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ReportController : ControllerBase
    {
        private readonly IReportRepository _reportRepository;
        private readonly ExcelService _excelService;
        private readonly ISemesterRepository _semesterRepository;
        private readonly LabInternManagementDBcontext _context;

        public ReportController(
            IReportRepository reportRepository,
            ExcelService excelService,
            ISemesterRepository semesterRepository,
            LabInternManagementDBcontext context
        )
        {
            _reportRepository = reportRepository;
            _excelService = excelService;
            _semesterRepository = semesterRepository;
            _context = context;
        }

        [HttpGet]
        [Authorize(Roles = "Mentor")]
        public async Task<IActionResult> ListReports([FromQuery] SearchReportDTO searchReport)
        {
            try
            {
                // Retrieve the mentorId from the authenticated user's claims
                var mentorId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);

                // Call the GetAllReports method to get the list of reports for the mentor
                var reportsByIntern = await _reportRepository.GetReportGroupByInterns(
                    mentorId,
                    searchReport
                );

                // Return the list of reports as OK with a status code of 200
                return Ok(new { reportsByIntern, reportsByIntern.TotalItemCount });
            }
            catch (Exception ex)
            {
                // Return a BadRequest error if something goes wrong
                return BadRequest($"Error fetching reports: {ex.Message}");
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetReportById(int id)
        {
            var report = _reportRepository.GetReportById(id);
            if (report == null)
                return NotFound("Report not found.");

            return Ok(report);
        }

        [HttpGet("rollname/{rollname}")]
        public IActionResult GetReportsByRollName(string rollname)
        {
            var reports = _reportRepository.GetReportsByRollName(rollname);
            if (!reports.Any())
                return NotFound("No reports found for the given RollName.");

            return Ok(reports);
        }

        [HttpPost]
        [Authorize(Roles = "Mentor")]
        public IActionResult AddReport([FromBody] ReportDTO reportDto)
        {
            if (reportDto == null)
            {
                return BadRequest(
                    new { Message = "Report information is required.", Status = "error" }
                );
            }

            if (!ModelState.IsValid)
            {
                return UnprocessableEntity(
                    new
                    {
                        Message = "Invalid report data.",
                        Status = "error",
                        Errors = ModelState.Values.SelectMany(v =>
                            v.Errors.Select(e => e.ErrorMessage)
                        )
                    }
                );
            }

            try
            {
                _reportRepository.AddReport(reportDto);
                return Ok(new { Message = "Report added successfully.", Status = "success" });
            }
            catch (InvalidOperationException ex)
            {
                return Conflict(new { Message = ex.Message, Status = "error" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = ex.Message, Status = "error" });
            }
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Mentor")]
        public IActionResult UpdateReport(int id, ReportDTO reportDto)
        {
            if (reportDto == null)
                return BadRequest("Report information is required.");

            if (id != reportDto.ReportId)
                return BadRequest("ID mismatch between URL and request body.");

            try
            {
                var existingReport = _reportRepository.GetReportById(id);
                if (existingReport == null)
                    return NotFound("Report not found.");

                _reportRepository.UpdateReport(reportDto); // Call synchronous method
                return Ok(new { Message = "Report updated successfully." });
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Error: {ex.Message}");
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Mentor")]
        public IActionResult DeleteReport(int id)
        {
            try
            {
                // Retrieve the report to ensure it exists
                var report = _reportRepository.GetReportById(id);
                if (report == null)
                {
                    return NotFound(new { Message = "Report not found." });
                }

                // Perform the deletion in the repository
                _reportRepository.DeleteReport(id);
                return Ok(new { Message = "Report deleted successfully." });
            }
            catch (KeyNotFoundException ex)
            {
                // Handle cases where the report is not found
                return NotFound(new { Message = ex.Message });
            }
            catch (InvalidOperationException ex)
            {
                // Handle cases where deletion is not allowed based on the time frame
                return BadRequest(new { Message = ex.Message });
            }
            catch (Exception ex)
            {
                return StatusCode(
                    StatusCodes.Status500InternalServerError,
                    new { Message = $"{ex.Message}" }
                );
            }
        }

        [HttpGet("export/excel/{labId}/{semesterId}")]
        public async Task<IActionResult> ExporLabReportsToExcel(int semesterId, int labId)
        {
            try
            {
                // Retrieve the semester details based on the provided semesterId
                var semester = await _semesterRepository.GetSemesterByIdAsync(semesterId);
                if (semester == null)
                    return NotFound(new { Message = "Semester not found.", Status = "NotFound" });

                var lab = await _context.Laboratories.FindAsync(labId);
                if (lab == null)
                    return NotFound(new { Message = "Laboratory not found.", Status = "NotFound" });

                // Get reports asynchronously, filtered by semesterId and date range
                var reports = await _reportRepository.GetLabFinalReportsBySemesterId(
                    semesterId,
                    lab.LabName
                );

                // Check if reports are null or empty
                if (reports == null || !reports.Any())
                    return NotFound(
                        new { Message = "No reports found to export.", Status = "NoReportsFound" }
                    );

                var assembly = Assembly.GetExecutingAssembly();
                var resourceName = "LaboratoryInternshipManagement.Resources.ReportFinal.xlsx";

                using (Stream templateStream = assembly.GetManifestResourceStream(resourceName))
                {
                    if (templateStream == null)
                    {
                        return NotFound(
                            new
                            {
                                Message = "Template File is not found",
                                Status = "TemplateNotFound"
                            }
                        );
                    }

                    // Tạo file Excel mới dựa trên template
                    using (MemoryStream outputStream = new MemoryStream())
                    {
                        // Sử dụng EPPlus để xử lý file Excel
                        using (var package = new ExcelPackage(templateStream))
                        {
                            // Thêm dữ liệu vào Excel
                            _excelService.AddDataAndGenerateCopy(package, reports, semester);

                            // Lưu file Excel vào MemoryStream
                            package.SaveAs(outputStream);
                        }

                        // Trả file Excel về client mà không lưu lại trên server
                        outputStream.Position = 0;
                        var fileDownloadName =
                            $"Final-Report-{lab.LabName}-{semester.SemesterName}.xlsx";
                        Response.Headers.Add(
                            "Content-Disposition",
                            $"attachment; filename=\"{fileDownloadName}\""
                        );
                        return new FileContentResult(
                            outputStream.ToArray(),
                            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                        )
                        {
                            FileDownloadName = fileDownloadName
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                // Log the exception details here
                return StatusCode(
                    500,
                    new
                    {
                        Message = $"Error generating Excel file: {ex.Message}",
                        Status = "InternalError"
                    }
                );
            }
        }

        [HttpGet("{id}/interns")]
        [Authorize]
        public async Task<IActionResult> GetInternsReportBySemesterId(int id)
        {
            string mentorId = User.FindFirstValue(JwtRegisteredClaimNames.Jti);
            int labId = int.Parse(User.FindFirstValue("labId"));
            if (string.IsNullOrEmpty(mentorId))
            {
                return Unauthorized(new { Message = "Unauthorized access.", Status = "error" });
            }
            try
            {
                var interns = await _semesterRepository.GetInternsBySemesterIdAsync(
                    id,
                    mentorId,
                    labId
                );

                return Ok(interns);
            }
            catch (InvalidOperationException ex)
            {
                return Conflict(new { Message = ex.Message, Status = "error" });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { ex.Message });
            }
        }

        [HttpGet("exportMiddle/excel/{labId}/{semesterId}")]
        public async Task<IActionResult> ExporLabMiddleReportsToExcel(int semesterId, int labId)
        {
            try
            {
                // Retrieve the semester details based on the provided semesterId
                var semester = await _semesterRepository.GetSemesterByIdAsync(semesterId);
                if (semester == null)
                    return NotFound(new { Message = "Semester not found.", Status = "NotFound" });

                var lab = await _context.Laboratories.FindAsync(labId);
                if (lab == null)
                    return NotFound(new { Message = "Laboratory not found.", Status = "NotFound" });

                // Get reports asynchronously, filtered by semesterId and date range
                var reports = await _reportRepository.GetLabMidTermReportBySemsterId(
                    semesterId,
                    lab.LabName
                );

                // Check if reports are null or empty
                if (reports == null || !reports.Any())
                    return NotFound(
                        new { Message = "No reports found to export.", Status = "NoReportsFound" }
                    );

                var assembly = Assembly.GetExecutingAssembly();
                var resourceName = "LaboratoryInternshipManagement.Resources.ReportMiddle.xlsx";

                using (Stream templateStream = assembly.GetManifestResourceStream(resourceName))
                {
                    if (templateStream == null)
                    {
                        return NotFound(
                            new
                            {
                                Message = "Template File is not found",
                                Status = "TemplateNotFound"
                            }
                        );
                    }

                    // Tạo file Excel mới dựa trên template
                    using (MemoryStream outputStream = new MemoryStream())
                    {
                        // Sử dụng EPPlus để xử lý file Excel
                        using (var package = new ExcelPackage(templateStream))
                        {
                            // Thêm dữ liệu vào Excel
                            _excelService.AddDataAndGenerateCopyV1(package, reports, semester);

                            // Lưu file Excel vào MemoryStream
                            package.SaveAs(outputStream);
                        }

                        // Trả file Excel về client mà không lưu lại trên server
                        outputStream.Position = 0;
                        return new FileContentResult(
                            outputStream.ToArray(),
                            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                        )
                        {
                            FileDownloadName =
                                $"Middle-Report-{lab.LabName}-{semester.SemesterName}.xlsx"
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                // Log the exception details here
                return StatusCode(
                    500,
                    new
                    {
                        Message = $"Error generating Excel file: {ex.Message}",
                        Status = "InternalError"
                    }
                );
            }
        }

        [HttpGet("GetByName")]
        public ActionResult<ReportDTO> GetReportByName(string name)
        {
            try
            {
                var report = _reportRepository.GetReportByName(name);
                var reportDto = new ReportDTO
                {
                    ReportId = report.ReportId,
                    StaffId = report.StaffId,
                    RollName = report.RollName,
                    MentorId = report.MentorId,
                    NameIntern = report.NameIntern,
                    NameLab = report.NameLab,
                    NameMentor = report.NameMentor,
                    Allowance = report.Allowance,
                    ComOfCompany = report.ComOfCompany,
                    MajorSkill = report.MajorSkill,
                    SoftSkill = report.SoftSkill,
                    Attitude = report.Attitude,
                    SemesterId = report.SemesterId
                };
                return Ok(reportDto);
            }
            catch (KeyNotFoundException ex)
            {
                return NotFound(new { message = ex.Message });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { message = ex.Message });
            }
        }
    }

    public class ReportDTO
    {
        public int ReportId { get; set; }

        [Required(ErrorMessage = "RollName is required.")]
        public string RollName { get; set; }

        [Required(ErrorMessage = "Mentor ID is required.")]
        public string MentorId { get; set; }

        [Required(ErrorMessage = "Name Intern is required.")]
        [MaxLength(50, ErrorMessage = "Name Intern cannot exceed 50 characters.")]
        public string NameIntern { get; set; }

        [Required(ErrorMessage = "Name Lab is required.")]
        [MaxLength(50, ErrorMessage = "Name Lab cannot exceed 50 characters.")]
        public string NameLab { get; set; }

        [Required(ErrorMessage = "Name Mentor is required.")]
        [MaxLength(50, ErrorMessage = "Name Mentor cannot exceed 50 characters.")]
        public string NameMentor { get; set; }

        public string? Allowance { get; set; }
        public string? ComOfCompany { get; set; }

        [Required(ErrorMessage = "Major Knowledge Skill is required.")]
        [Range(0, 10, ErrorMessage = "Major Knowledge Skill must be between 0 and 10.")]
        public double MajorSkill { get; set; }

        [Required(ErrorMessage = "Soft Skill is required.")]
        [Range(0, 10, ErrorMessage = "Soft Skill must be between 0 and 10.")]
        public double SoftSkill { get; set; }

        [Required(ErrorMessage = "Attitude is required.")]
        [Range(0, 10, ErrorMessage = "Attitude must be between 0 and 10.")]
        public double Attitude { get; set; }

        public double? FinalResult => Attitude + SoftSkill + MajorSkill;

        [Required(ErrorMessage = "StaffId Mentor is required.")]
        [MaxLength(50, ErrorMessage = "StaffId Mentor cannot exceed 50 characters.")]
        public string StaffId { get; set; }

        public int? SemesterId { get; set; }

        [Required(ErrorMessage = "Intern ID is required.")]
        public string InternId { get; set; }
        public int? ReportType { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
    }
}
