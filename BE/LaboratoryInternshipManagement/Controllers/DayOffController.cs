﻿using System.Security.Claims;
using LaboratoryInternshipManagement.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace LaboratoryInternshipManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DayOffController : ControllerBase
    {
        private readonly LabInternManagementDBcontext _context;
        private readonly ILogger<AuthController> _logger;

        public DayOffController(
            LabInternManagementDBcontext context,
            ILogger<AuthController> logger
        )
        {
            _context = context;
            _logger = logger;
        }

        [Authorize(Roles = "Lab Manager")]
        [HttpGet("getDayOff")]
        public async Task<IActionResult> GetDayOffInYear()
        {
            try
            {
                var dayOffs = await _context
                    .DayOff.Where(x => x.Date.Year == DateTime.Now.Year)
                    .OrderBy(x => x.Date)
                    .ToListAsync();

                return Ok(dayOffs);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }

            return NoContent();
        }

        [Authorize(Roles = "Lab Manager")]
        [HttpDelete("deleteDayOff/{id}")]
        public async Task<IActionResult> DeleteDayOffInYear(int id)
        {
            try
            {
                var dayOffInfor = _context.DayOff.Where(x => x.Id == id).FirstOrDefault();
                if (dayOffInfor != null)
                {
                    _context.DayOff.Remove(dayOffInfor);
                    await _context.SaveChangesAsync();
                    return Ok("settingLab.deleteDayOffSuccess");
                }
                else
                {
                    return StatusCode(500, new { Message = "settingLab.noDayOffFound" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }

            return NoContent();
        }

        [Authorize(Roles = "Lab Manager")]
        [HttpPost("updateDayOff")]
        public async Task<IActionResult> updateDayOffInYear(string DayOff)
        {
            try
            {
                _context.DayOff.Update(
                    new DayOff
                    {
                        Date = DateTime.Parse(DayOff),
                        LabId = Int32.Parse(User.FindFirstValue("labId"))
                    }
                );

                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

            return NoContent();
        }

        [Authorize(Roles = "Lab Manager")]
        [HttpPost("createDayOff/{date}")]
        public async Task<IActionResult> CreateDayOffInYear(string date)
        {
            try
            {
                if (!date.IsNullOrEmpty())
                {
                    var dayOff = DateTime.Parse(date);

                    _context.DayOff.Add(
                        new DayOff
                        {
                            Date = DateTime.Parse(date),
                            LabId = Int32.Parse(User.FindFirstValue("labId")),
                            Year = dayOff.Year,
                        }
                    );
                }
                var result = await _context.SaveChangesAsync();
                if (result > 0)
                {
                    return Ok("settingLab.CreatedSuccess");
                }
                else
                {
                    return StatusCode(500, new { Message = "settingLab.CreateFailse" });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error acquired: {ex}");
                return StatusCode(500, new { Message = "systemError" });
            }
        }
    }
}
