﻿using System.ComponentModel.DataAnnotations.Schema;

namespace LaboratoryInternshipManagement.Models
{
    public class LabInformationRegist
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int LabID { get; set; }
        public string? MacAdress { get; set; }
        public string? IpAdress { get; set; }
        public string? FaceVector { get; set; }
        public string? InternId { get; set; }
        public virtual User? Intern { get; set; }
    }
}
