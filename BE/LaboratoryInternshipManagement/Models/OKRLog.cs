﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaboratoryInternshipManagement.Models
{
    public class OKRLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OKRLogId { get; set; } // Primary key
        public int? ObjectiveId { get; set; } // Foreign key to Objective
        public int? KeyResultId { get; set; } // Foreign key to Key Result (nullable if the change is related to an Objective)
        public string UserId { get; set; } // Foreign key to User who made the change

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime ChangeDate { get; set; } // Date and time when the change was made
        public string ChangeType { get; set; } // Type of change (e.g., "Created", "Updated", "Deleted")
        public string ChangeDetails { get; set; } // Details of the change (e.g., "Changed status from 'In Progress' to 'Completed'")\

        /*public KeyResult KeyResult { get; set; } // Navigation property to Key Result
        public Objective Objective { get; set; } // Navigation property to Objective*/
        public virtual User User { get; set; } // Navigation property to User
    }
}
