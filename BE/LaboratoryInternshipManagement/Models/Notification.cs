﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaboratoryInternshipManagement.Models
{
    public class Notification
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NotificationId { get; set; }

        // public string UserId { get; set; }  // User who receives the notification
        public string? CreatedByUserId { get; set; } // User who created the notification
        public string Type { get; set; } // Type of notification (e.g., comment, reply, event)
        public string Message { get; set; } // Message or description associated with the notification
        public bool IsRead { get; set; } = false; // Flag to indicate if the notification has been read

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedAt { get; set; } // Timestamp when the notification was created
        public string TargetUrl { get; set; } // URL or route for redirection

        public virtual ICollection<User> Users { get; set; } // User who receives the notification
        public virtual User CreatedByUser { get; set; } // User who created the notification
    }
}
