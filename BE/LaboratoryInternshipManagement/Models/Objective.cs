﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LaboratoryInternshipManagement.Models.Enums;

namespace LaboratoryInternshipManagement.Models
{
    public class Objective
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ObjectiveId { get; set; }
        public string CreateBy { get; set; }
        public int TeamId { get; set; } // Foreign key to Team for team-based access
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsApprove { get; set; }

        [Required]
        [EnumDataType(typeof(ConstantEnum.ObjectiveStatus))]
        public ConstantEnum.ObjectiveStatus Status { get; set; } // Use  enum
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedAt { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime UpdatedAt { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual User User { get; set; }
        public virtual Team Team { get; set; }
        public virtual ICollection<KeyResult> KeyResults { get; set; }
    }
}
