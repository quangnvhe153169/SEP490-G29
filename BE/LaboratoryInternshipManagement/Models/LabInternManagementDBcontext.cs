﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using static LaboratoryInternshipManagement.Models.Enums.ConstantEnum;

namespace LaboratoryInternshipManagement.Models
{
    public class LabInternManagementDBcontext : IdentityDbContext
    {
        public LabInternManagementDBcontext() { }

        public LabInternManagementDBcontext(DbContextOptions<LabInternManagementDBcontext> option)
            : base(option) { }

        public virtual DbSet<User> Users { get; set; } = null!;
        public virtual DbSet<Attendance> Attendances { get; set; } = null!;
        public virtual DbSet<Laboratory> Laboratories { get; set; } = null!;
        public virtual DbSet<LabInformationRegist> LabInformationRegist { get; set; } = null!;
        public virtual DbSet<DayOff> DayOff { get; set; } = null!;
        public virtual DbSet<Tickets> Tickets { get; set; }
        public virtual DbSet<TicketComment> TicketComments { get; set; }
        public virtual DbSet<ImportHistory> ImportHistories { get; set; }
        public virtual DbSet<ImportIntern> ImportInterns { get; set; }
        public virtual DbSet<ImportInternError> ImportInternErrors { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<Objective> Objectives { get; set; }
        public virtual DbSet<KeyResult> KeyResults { get; set; }
        public virtual DbSet<ProgressPerformance> ProgressPerformances { get; set; }
        public virtual DbSet<Event> Events { get; set; }
        public virtual DbSet<EventAttendee> EventAttendees { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Team> Teams { get; set; }
        public virtual DbSet<TeamMembership> TeamMemberships { get; set; }
        public virtual DbSet<OKRLog> OKRLogs { get; set; }
        public DbSet<Semester> Semesters { get; set; } = null!;

        public virtual DbSet<Report> Reports { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var hasher = new PasswordHasher<User>();

            modelBuilder
                .Entity<User>()
                .HasData(
                    new User
                    {
                        Id = "1",
                        UserName = "admin",
                        NormalizedUserName = "admin",
                        EmailConfirmed = true,
                        IsTestAccount = true,
                        PasswordHash = hasher.HashPassword(null, "adminlabsu24"),
                        SecurityStamp = string.Empty
                    }
                );

            modelBuilder
                .Entity<IdentityRole>()
                .HasData(
                    new IdentityRole
                    {
                        Id = "1",
                        Name = "System Admin",
                        NormalizedName = "System Admin"
                    },
                    new IdentityRole
                    {
                        Id = "2",
                        Name = "Lab Manager",
                        NormalizedName = "Lab Manager"
                    },
                    new IdentityRole
                    {
                        Id = "3",
                        Name = "Mentor",
                        NormalizedName = "Mentor"
                    },
                    new IdentityRole
                    {
                        Id = "4",
                        Name = "Intern",
                        NormalizedName = "Intern"
                    }
                );

            modelBuilder
                .Entity<IdentityUserRole<string>>()
                .HasData(new IdentityUserRole<string> { RoleId = "1", UserId = "1" });

            modelBuilder.Entity<Laboratory>(entity =>
            {
                entity
                    .HasOne(lab => lab.LabManager)
                    .WithOne()
                    .HasForeignKey<Laboratory>(lab => lab.LabManagerId)
                    .IsRequired();

                entity
                    .HasMany(lab => lab.Members)
                    .WithOne(user => user.Laboratory)
                    .HasForeignKey(user => user.LabId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity
                    .HasMany(lab => lab.DayOffs)
                    .WithOne(dayOff => dayOff.Laboratory)
                    .HasForeignKey(dayOff => dayOff.LabId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity
                    .HasOne(mi => mi.Mentor)
                    .WithMany(u => u.Interns)
                    .HasForeignKey(mi => mi.MentorId)
                    .OnDelete(DeleteBehavior.Restrict);
                entity
                    .Property(o => o.JoinedAt)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAdd();
            });

            modelBuilder.Entity<Attendance>(entity =>
            {
                entity
                    .HasOne(a => a.Intern)
                    .WithMany(i => i.Attendances)
                    .HasForeignKey(a => a.InternId)
                    .HasConstraintName("FK_Attendance_Intern")
                    .OnDelete(DeleteBehavior.Restrict);
            });
            modelBuilder.Entity<TicketComment>(entity =>
            {
                entity
                    .HasOne(tc => tc.Ticket)
                    .WithMany(t => t.TicketComments)
                    .HasForeignKey(tc => tc.TicketId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder
                .Entity<Tickets>()
                .HasOne<User>(t => t.Creator)
                .WithMany(u => u.CreatedTickets)
                .HasForeignKey(t => t.CreateId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder
                .Entity<Tickets>()
                .HasOne<User>(t => t.Assignee)
                .WithMany(u => u.AssignedTickets)
                .HasForeignKey(t => t.AssignId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ImportIntern>(entity =>
            {
                entity
                    .HasOne(e => e.User) // Each ImportIntern has one User (mentor)
                    .WithMany(u => u.ImportInterns) // Each User (mentor) has many ImportInterns
                    .HasForeignKey(e => e.MentorID) // Foreign key property in ImportIntern
                    .HasConstraintName("FK_ImprotIntern_Mentor")
                    .OnDelete(DeleteBehavior.Restrict); // Optional: Configure delete behavior
            });
            // Notification relationships
            modelBuilder.Entity<Notification>(entity =>
            {
                entity
                    .HasMany(n => n.Users)
                    .WithMany(u => u.Notifications)
                    .UsingEntity<Dictionary<string, object>>(
                        "NotificationUser",
                        j => j.HasOne<User>().WithMany().HasForeignKey("UserId"),
                        j => j.HasOne<Notification>().WithMany().HasForeignKey("NotificationId")
                    );

                entity
                    .HasOne(n => n.CreatedByUser)
                    .WithMany()
                    .HasForeignKey(n => n.CreatedByUserId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity
                    .Property(e => e.CreatedAt)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAdd();
            });

            // Objective relationships
            modelBuilder.Entity<Objective>(entity =>
            {
                entity
                    .HasOne(o => o.User)
                    .WithMany(u => u.Objectives)
                    .HasForeignKey(o => o.CreateBy);

                entity.HasOne(o => o.Team).WithMany(t => t.Objectives).HasForeignKey(o => o.TeamId);

                entity
                    .Property(o => o.CreatedAt)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAdd();

                entity
                    .Property(o => o.UpdatedAt)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate()
                    .Metadata.SetAfterSaveBehavior(
                        Microsoft.EntityFrameworkCore.Metadata.PropertySaveBehavior.Save
                    );

                entity
                    .Property(o => o.Status)
                    .HasConversion(
                        v => v.ToString(),
                        v => (ObjectiveStatus)Enum.Parse(typeof(ObjectiveStatus), v)
                    );
            });

            // KeyResult relationships and properties
            modelBuilder.Entity<KeyResult>(entity =>
            {
                entity
                    .HasOne(kr => kr.Objective)
                    .WithMany(o => o.KeyResults)
                    .HasForeignKey(kr => kr.ObjectiveId);
                entity
                    .HasOne(kr => kr.User)
                    .WithMany(u => u.KeyResults)
                    .HasForeignKey(kr => kr.Assign)
                    .OnDelete(DeleteBehavior.Restrict);
                entity
                    .Property(e => e.CreatedAt)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAdd();

                entity
                    .Property(e => e.UpdatedAt)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate()
                    .Metadata.SetAfterSaveBehavior(
                        Microsoft.EntityFrameworkCore.Metadata.PropertySaveBehavior.Save
                    );

                entity
                    .Property(e => e.Status)
                    .HasConversion(
                        v => v.ToString(),
                        v => (KeyResultStatus)Enum.Parse(typeof(KeyResultStatus), v)
                    );

                entity.Property(k => k.CurrentValue).HasColumnType("decimal(18, 2)");

                entity.Property(k => k.TargetValue).HasColumnType("decimal(18, 2)");

                entity.Property(k => k.ActualManDay).HasColumnType("decimal(18, 2)");

                entity.Property(k => k.EstimateManDay).HasColumnType("decimal(18, 2)");
            });

            // ProgressPerformance relationships and properties
            modelBuilder.Entity<ProgressPerformance>(entity =>
            {
                entity
                    .HasOne(pp => pp.KeyResult)
                    .WithMany(kr => kr.ProgressPerformances)
                    .HasForeignKey(pp => pp.KeyResultId);

                entity
                    .Property(e => e.ReportDate)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAdd();

                entity.Property(b => b.CurrentValue).HasColumnType("decimal(18, 2)");

                entity.Property(b => b.TargetValue).HasColumnType("decimal(18, 2)");

                entity.Property(b => b.Variance).HasColumnType("decimal(18, 2)");
            });

            // EventAttendee relationships
            modelBuilder.Entity<EventAttendee>(entity =>
            {
                entity
                    .HasOne(ea => ea.Event)
                    .WithMany(e => e.EventAttendees)
                    .HasForeignKey(ea => ea.EventId);

                entity
                    .HasOne(ea => ea.User)
                    .WithMany(u => u.EventAttendees)
                    .HasForeignKey(ea => ea.UserId);

                entity
                    .Property(e => e.CreatedAt)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAdd();

                entity
                    .Property(e => e.UpdatedAt)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate()
                    .Metadata.SetAfterSaveBehavior(
                        Microsoft.EntityFrameworkCore.Metadata.PropertySaveBehavior.Save
                    );
            });

            modelBuilder.Entity<Event>(entity =>
            {
                entity
                    .Property(e => e.CreatedAt)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAdd();

                entity
                    .Property(e => e.UpdatedAt)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate()
                    .Metadata.SetAfterSaveBehavior(
                        Microsoft.EntityFrameworkCore.Metadata.PropertySaveBehavior.Save
                    );
                entity
                    .HasOne(e => e.User)
                    .WithMany(u => u.Events)
                    .HasForeignKey(e => e.UserId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            // TeamMembership relationships
            modelBuilder.Entity<TeamMembership>(entity =>
            {
                entity.HasKey(tm => new { tm.TeamId, tm.UserId });

                entity
                    .HasOne(tm => tm.Team)
                    .WithMany(t => t.Members)
                    .HasForeignKey(tm => tm.TeamId);

                entity
                    .HasOne(tm => tm.User)
                    .WithMany(u => u.TeamMemberships)
                    .HasForeignKey(tm => tm.UserId);
            });

            // Comment relationships and properties
            modelBuilder.Entity<Comment>(entity =>
            {
                entity
                    .Property(e => e.CreatedAt)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAdd();

                entity
                    .Property(e => e.UpdatedAt)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate()
                    .Metadata.SetAfterSaveBehavior(
                        Microsoft.EntityFrameworkCore.Metadata.PropertySaveBehavior.Save
                    );

                entity
                    .HasOne(c => c.ParentComment)
                    .WithMany(c => c.Replies)
                    .HasForeignKey(c => c.ParentCommentId)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            // OKRLog relationships
            modelBuilder.Entity<OKRLog>(entity =>
            {
                entity
                    .HasOne(u => u.User)
                    .WithMany(u => u.OKRLogs)
                    .HasForeignKey(u => u.UserId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity
                    .Property(e => e.ChangeDate)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();
            });

            modelBuilder.Entity<Report>(entity =>
            {
                entity
                    .HasOne(a => a.Mentor)
                    .WithMany(i => i.Reports)
                    .HasForeignKey(a => a.MentorId)
                    .HasConstraintName("FK_Reports_Mentor")
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity
                    .HasOne(u => u.Semester)
                    .WithMany(s => s.Interns)
                    .HasForeignKey(u => u.SemesterId)
                    .HasConstraintName("ForeignKey_User_Semester_Interns")
                    .IsRequired(false)
                    .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<Report>(entity =>
            {
                entity
                    .HasOne(r => r.Semester)
                    .WithMany(s => s.Reports)
                    .HasForeignKey(r => r.SemesterId)
                    .HasConstraintName("ForeignKey_Reports_Semester")
                    .OnDelete(DeleteBehavior.SetNull);
            });
        }
    }
}
