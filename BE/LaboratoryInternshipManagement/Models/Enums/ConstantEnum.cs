﻿namespace LaboratoryInternshipManagement.Models.Enums
{
    public class ConstantEnum
    {
        public enum AttendanceStatus
        {
            ATTENDED = 1,
            LATEIN = 2,
            SOONOUT = 3,
            ABSENT = 4,
        }

        public enum pagging
        {
            paggingSizeNumber = 45,
            pagging20size = 20,
            pagging5Size = 5,
        };

        public enum TicketStatus
        {
            PENDING = 1,
            APPROVE = 2,
            INPROGESS = 3,
            DONE = 4,
            REJECT = 5,
        }

        public enum TicketType
        {
            ATTENDANCE = 1,
            OTHER = 2,
        }

        public enum KeyResultStatus
        {
            Not_Started = 1,
            In_Progress = 2,
            Completed = 3,
            Failed = 4,
        }

        public enum ObjectiveStatus
        {
            Not_Started = 1,
            In_Progress = 2,
            Completed = 3,
            Failed = 4,
        }

        public enum ReportType
        {
            MID_TERM = 1,
            END_OF_TERM = 2
        }
    }
}
