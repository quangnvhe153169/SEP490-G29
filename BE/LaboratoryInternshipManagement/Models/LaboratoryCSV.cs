﻿namespace LaboratoryInternshipManagement.Models
{
    public class LaboratoryCSV
    {
        public string? LabName { get; set; }
        public string CheckInTime { get; set; }
        public string CheckOutTime { get; set; }
        public int? LeaveRequestNum { get; set; }
        public string? LabManagerEmail { get; set; }
    }
}
