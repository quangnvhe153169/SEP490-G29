﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Identity;

namespace LaboratoryInternshipManagement.Models
{
    public class User : IdentityUser
    {
        public int? LabId { get; set; }
        public string? MentorId { get; set; }
        public DateTime BirthDate { get; set; }
        public string? LastName { get; set; }
        public string? FirstName { get; set; }
        public string? RollName { get; set; }
        public bool Status { get; set; } = true;
        public bool IsTestAccount { get; set; } = false;

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime JoinedAt { get; set; }

        [JsonIgnore]
        public User? Mentor { get; set; }

        [JsonIgnore]
        public Laboratory? Laboratory { get; set; }

        public ICollection<User>? Interns { get; set; }

        [JsonIgnore]
        public ICollection<ImportIntern>? ImportInterns { get; set; }

        [JsonIgnore]
        public virtual ICollection<Objective> Objectives { get; set; }

        [JsonIgnore]
        public virtual ICollection<KeyResult> KeyResults { get; set; }

        [JsonIgnore]
        public virtual ICollection<Event> Events { get; set; }

        [JsonIgnore]
        public virtual ICollection<Notification> Notifications { get; set; } =
            new List<Notification>();

        [JsonIgnore]
        public virtual ICollection<EventAttendee> EventAttendees { get; set; }

        [JsonIgnore]
        public virtual ICollection<Comment> Comments { get; set; }

        [JsonIgnore]
        public virtual ICollection<TeamMembership> TeamMemberships { get; set; }

        [JsonIgnore]
        public virtual ICollection<OKRLog> OKRLogs { get; set; }

        [JsonIgnore]
        public ICollection<Attendance>? Attendances { get; set; }

        [JsonIgnore]
        public ICollection<Tickets> CreatedTickets { get; set; }

        [JsonIgnore]
        public ICollection<Tickets> AssignedTickets { get; set; }
        public byte[]? Avatar { get; set; }
        public byte[]? Cv { get; set; }
        public string Language { get; set; } = "en";
        public bool NotifyToMail { get; set; } = true;
        public ICollection<Report>? Reports { get; set; }
        public int? SemesterId { get; set; }
        public virtual Semester? Semester { get; set; }
    }
}
