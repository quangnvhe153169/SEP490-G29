﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaboratoryInternshipManagement.Models
{
    public class ProgressPerformance
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProgressPerformanceId { get; set; }
        public int KeyResultId { get; set; }
        public virtual KeyResult KeyResult { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime ReportDate { get; set; }
        public decimal CurrentValue { get; set; }
        public decimal TargetValue { get; set; } = 100; // Default TargetValue to 100
        public decimal? Variance { get; set; }
        public string Comments { get; set; }
        public string Insights { get; set; }
        public string Recommendations { get; set; }
    }
}
