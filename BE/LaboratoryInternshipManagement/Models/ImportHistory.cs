﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaboratoryInternshipManagement.Models
{
    public class ImportHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string FileName { get; set; }
        public DateTime ImportDate { get; set; }
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}
