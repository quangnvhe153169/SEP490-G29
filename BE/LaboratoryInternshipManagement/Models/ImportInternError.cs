﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaboratoryInternshipManagement.Models
{
    public class ImportInternError
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string RollNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
        public string Email { get; set; }
        public string LabName { get; set; }
        public string ErrorMessage { get; set; }

        public ImportInternError() { }

        public ImportInternError(
            string rollNumber,
            string firstName,
            string lastName,
            DateTime? dOB,
            string email,
            string labName,
            string errorMessage
        )
        {
            RollNumber = rollNumber;
            FirstName = firstName;
            LastName = lastName;
            DOB = dOB;
            Email = email;
            LabName = labName;
            ErrorMessage = errorMessage;
        }
    }
}
