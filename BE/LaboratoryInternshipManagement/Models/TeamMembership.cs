﻿namespace LaboratoryInternshipManagement.Models
{
    public class TeamMembership
    {
        public int TeamId { get; set; }
        public string UserId { get; set; }

        // Navigation properties
        public virtual Team Team { get; set; }
        public virtual User User { get; set; }
    }
}
