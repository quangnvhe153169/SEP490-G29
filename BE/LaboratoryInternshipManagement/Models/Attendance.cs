﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaboratoryInternshipManagement.Models
{
    public class Attendance
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int Status { get; set; }
        public TimeSpan? CheckInTime { get; set; }
        public TimeSpan? CheckOutTime { get; set; }
        public string InternId { get; set; }
        public string? Reason { get; set; }
        public bool? IsInAllowedLimit { get; set; }
        public bool? IsSuspense { get; set; }
        public DateTime Date { get; set; }
        public virtual User? Intern { get; set; }
    }
}
