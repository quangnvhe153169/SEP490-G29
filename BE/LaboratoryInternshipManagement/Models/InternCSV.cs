﻿namespace LaboratoryInternshipManagement.Models
{
    public class InternCSV
    {
        public string RollNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
        public string Email { get; set; }
        public string LabName { get; set; }
        public string CVName { get; set; }
        public byte[] CVDescript { get; set; }
    }
}
