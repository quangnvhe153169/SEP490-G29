﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaboratoryInternshipManagement.Models
{
    public class TicketComment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public Int64 Id { get; set; }

        [Required]
        public Int64 TicketId { get; set; }

        [Required]
        [MaxLength(450)]
        public string UserId { get; set; }

        public string Description { get; set; }

        // Navigation property
        public virtual Tickets Ticket { get; set; }
    }
}
