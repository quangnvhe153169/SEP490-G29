﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LaboratoryInternshipManagement.Models.Enums;

namespace LaboratoryInternshipManagement.Models
{
    public class KeyResult
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int KeyResultId { get; set; }

        public int ObjectiveId { get; set; }

        public string? Assign { get; set; } // User ID of the person assigned

        public string Title { get; set; }

        public string? Priority { get; set; } // Example: "High", "Medium", "Low"

        [Required]
        [EnumDataType(typeof(ConstantEnum.KeyResultStatus))]
        public ConstantEnum.KeyResultStatus Status { get; set; } // Use KeyResultStatus enum

        public string? Notes { get; set; }

        public string? AttachedFiles { get; set; } // Attached files (comma-separated file paths)

        public decimal TargetValue { get; set; } = 100; // Default TargetValue to 100

        public decimal CurrentValue { get; set; } = 0;

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTHH:mm}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-ddTHH:mm}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }
        public decimal EstimateManDay { get; set; }

        public decimal ActualManDay { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedAt { get; set; } = DateTime.Now;

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime UpdatedAt { get; set; } = DateTime.Now;

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual Objective Objective { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<ProgressPerformance> ProgressPerformances { get; set; }
    }
}
