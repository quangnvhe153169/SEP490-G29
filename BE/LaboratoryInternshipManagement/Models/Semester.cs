﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace LaboratoryInternshipManagement.Models
{
    public class Semester
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SemesterId { get; set; }
        public string SemesterName { get; set; }

        // Thời gian bắt đầu và kết thúc của học kỳ
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        [JsonIgnore]
        public virtual ICollection<Report>? Reports { get; set; }

        [JsonIgnore]
        public virtual ICollection<User>? Interns { get; set; }
    }
}
