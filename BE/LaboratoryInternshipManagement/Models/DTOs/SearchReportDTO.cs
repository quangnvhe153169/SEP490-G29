﻿namespace LaboratoryInternshipManagement.Models.DTOs
{
    public class SearchReportDTO
    {
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 20;
        public string? Keyword { get; set; } = "";
        public int? SemesterId { get; set; }
    }
}
