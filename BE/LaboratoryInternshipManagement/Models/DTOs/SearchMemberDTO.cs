﻿using LaboratoryInternshipManagement.Models.Enums;

namespace LaboratoryInternshipManagement.Models.DTOs
{
    public class SearchDTO
    {
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = (int)ConstantEnum.pagging.pagging20size;
        public string? Keyword { get; set; }
        public bool? isActive { get; set; }
    }

    public class SearchInternDTO : SearchDTO
    {
        public int? SemesterId { get; set; }
    }
}
