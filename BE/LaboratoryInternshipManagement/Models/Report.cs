﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using LaboratoryInternshipManagement.Models.Enums;

namespace LaboratoryInternshipManagement.Models
{
    public class Report
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ReportId { get; set; }
        public string StaffId { get; set; } // sẽ bằng email bỏ đuôi @gmail.com
        public string RollName { get; set; }
        public string MentorId { get; set; }
        public string InternId { get; set; }
        public string NameIntern { get; set; }
        public string NameLab { get; set; }
        public string NameMentor { get; set; }
        public string? Allowance { get; set; }
        public string? ComOfCompany { get; set; }
        public double MajorSkill { get; set; } = 0;
        public double SoftSkill { get; set; } = 0;
        public double Attitude { get; set; } = 0;
        public DateTime? CreatedDate { get; set; } = DateTime.Now;
        public int ReportType { get; set; } = (int)ConstantEnum.ReportType.MID_TERM;

        // Read-only property for FinalResult
        public double FinalResult => Attitude + SoftSkill + MajorSkill;

        [JsonIgnore]
        public virtual User? Mentor { get; set; }
        public int? SemesterId { get; set; }

        [JsonIgnore]
        public virtual Semester? Semester { get; set; }
    }
}
