﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaboratoryInternshipManagement.Models
{
    public class DayOff
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int LabId { get; set; }

        public DateTime Date { get; set; }
        public int Year { get; set; }
        public virtual Laboratory? Laboratory { get; set; }
    }
}
