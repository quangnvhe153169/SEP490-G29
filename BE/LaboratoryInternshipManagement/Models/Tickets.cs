﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace LaboratoryInternshipManagement.Models
{
    public class Tickets
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required]
        public Int64 Id { get; set; }

        [Required]
        [MaxLength(450)]
        public string CreateId { get; set; }

        [Required]
        [MaxLength(450)]
        public string AssignId { get; set; }

        [Required]
        [MaxLength(256)]
        public string Title { get; set; }

        public string Description { get; set; }
        public DateTime? DateRequire { get; set; }

        [Required]
        public byte Type { get; set; }

        [Required]
        public byte Status { get; set; }

        // Navigation property
        public virtual ICollection<TicketComment> TicketComments { get; set; }

        [JsonIgnore]
        public virtual User Creator { get; set; }

        [JsonIgnore]
        public virtual User Assignee { get; set; }
    }
}
