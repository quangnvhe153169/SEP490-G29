﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaboratoryInternshipManagement.Models
{
    public class ImportIntern
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string? MentorID { get; set; }
        public string RollNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DOB { get; set; }
        public string Email { get; set; }
        public string LabName { get; set; }
        public string CVName { get; set; }
        public byte[] CVDescript { get; set; }

        public virtual User User { get; set; }
        public string? Signature { get; set; }
        public DateTime? ExpiredAt { get; set; }
    }
}
