﻿namespace LaboratoryInternshipManagement.Models
{
    public class SignatureIntern
    {
        public string Email { get; set; }
        public string HashedSignature { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}
