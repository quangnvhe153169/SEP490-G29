﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaboratoryInternshipManagement.Models
{
    public class Comment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CommentId { get; set; }
        public int? ParentCommentId { get; set; }

        [Required]
        public string CreateById { get; set; }

        [Required]
        public string Content { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedAt { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime UpdatedAt { get; set; }

        [Required]
        public int ReletedObjectId { get; set; }

        [Required]
        public string ReletedObjectType { get; set; }
        public string? MentionedUserIds { get; set; } // Mentioned users (comma-separated user IDs)
        public string? AttachedFiles { get; set; } // Attached files (comma-separated file paths)

        public virtual Comment? ParentComment { get; set; }
        public virtual ICollection<Comment> Replies { get; set; } = new List<Comment>();
        public virtual User CreateBy { get; set; }
    }
}
