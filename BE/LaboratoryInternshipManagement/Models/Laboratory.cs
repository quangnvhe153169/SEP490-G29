﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaboratoryInternshipManagement.Models
{
    public class Laboratory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string? LabName { get; set; }
        public TimeSpan CheckInTime { get; set; }
        public TimeSpan CheckOutTime { get; set; }
        public int? LeaveRequestNum { get; set; }
        public string? LabManagerId { get; set; }
        public virtual User? LabManager { get; set; }
        public virtual ICollection<User>? Members { get; set; }
        public virtual ICollection<DayOff>? DayOffs { get; set; }
    }
}
