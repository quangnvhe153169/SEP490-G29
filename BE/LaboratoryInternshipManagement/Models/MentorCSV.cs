﻿using System.ComponentModel.DataAnnotations;

namespace LaboratoryInternshipManagement.Models
{
    public class MentorCSV
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Email { get; set; }
    }
}
