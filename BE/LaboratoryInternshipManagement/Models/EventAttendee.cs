﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LaboratoryInternshipManagement.Models
{
    public class EventAttendee
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EventAttendeeId { get; set; }
        public int EventId { get; set; }
        public string UserId { get; set; }
        public string Status { get; set; } // e.g., 'invited', 'confirmed', 'attended', 'declined'

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedAt { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime UpdatedAt { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual Event Event { get; set; }
        public virtual User User { get; set; }
    }
}
