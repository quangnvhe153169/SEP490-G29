﻿using System.Security.Cryptography;
using System.Text;
using LaboratoryInternshipManagement.DataAccess.IRepository;

namespace LaboratoryInternshipManagement.Services
{
    public class SignatureService
    {
        private readonly IImportCSVRepository _importCSVRepository;
        private readonly ISignatureInternRepository _signatureInternRepository;

        public SignatureService(
            IImportCSVRepository importCSVRepository,
            ISignatureInternRepository signatureInternRepository
        )
        {
            _importCSVRepository = importCSVRepository;
            _signatureInternRepository = signatureInternRepository;
        }

        //public string GenerateSignature(string internEmail)
        //{
        //    var token = Guid.NewGuid().ToString("N");
        //    var signture = $"{token}_{internEmail}";
        //    var hashedSignature = HashSignature(signture);
        //    var existingSignature = _signatureInternRepository.GetSignatureByEmail(internEmail);
        //    if(existingSignature != null)
        //    {
        //        _signatureInternRepository.DeleteSignature(internEmail);
        //    }
        //    var newSignature = new SignatureIntern
        //    {
        //        Email = internEmail,
        //        HashedSignature = hashedSignature,
        //        ExpirationDate = DateTime.UtcNow.AddDays(3),
        //    };
        //    _importCSVRepository.AddSignature(newSignature);
        //    return hashedSignature;
        //}
        private string HashSignature(string signature)
        {
            using (var sha256 = SHA256.Create())
            {
                var hashBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(signature));
                return Convert.ToBase64String(hashBytes);
            }
        }
    }
}
