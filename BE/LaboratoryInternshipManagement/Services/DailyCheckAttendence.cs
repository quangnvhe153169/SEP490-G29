﻿using LaboratoryInternshipManagement.Models;
using LaboratoryInternshipManagement.Models.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Quartz;

namespace LaboratoryInternshipManagement.Services
{
    public class DailyCheckAttendence : IJob
    {
        public readonly IServiceProvider _serviceProvider;
        public readonly ILogger<DailyCheckAttendence> _logger;

        public DailyCheckAttendence(
            IServiceProvider serviceProvider,
            ILogger<DailyCheckAttendence> logger
        )
        {
            _serviceProvider = serviceProvider;
            _logger = logger;
        }

        public async Task Execute(IJobExecutionContext context)
        {
            try
            {
                _logger.LogInformation(
                    $"{DateTime.Today.AddDays(-1)} Attendance batch is starting..."
                );
                if (!IsWeekend(DateTime.Today.AddDays(-1)))
                {
                    using (var scope = _serviceProvider.CreateScope())
                    {
                        var DbContext =
                            scope.ServiceProvider.GetRequiredService<LabInternManagementDBcontext>();
                        var userManager = scope.ServiceProvider.GetRequiredService<
                            UserManager<User>
                        >();
                        var today = DateTime.Today.AddDays(-1);
                        // Fetch attendence records for today
                        var allInternUsers = await userManager.GetUsersInRoleAsync("Intern");
                        var internIds = new HashSet<string>(allInternUsers.Select(x => x.Id));

                        // Fetch all interns, their attendance for today, and their laboratory information
                        var allInterns = await DbContext
                            .Users.Include(u => u.Laboratory)
                            .ThenInclude(x => x.DayOffs)
                            .Include(u =>
                                u.Attendances.Where(a => a.Date == today && a.Status == 0)
                            )
                            .Where(u => internIds.Contains(u.Id) && u.Laboratory != null)
                            .ToListAsync();

                        var lstInternChecked = new List<Attendance>();
                        var lstInternNotCheckIn = new List<User>();

                        foreach (var intern in allInterns)
                        {
                            var attendance = intern?.Attendances?.FirstOrDefault();
                            if (attendance != null)
                            {
                                lstInternChecked.Add(attendance);
                            }
                            else
                            {
                                lstInternNotCheckIn.Add(intern);
                            }
                        }

                        if (lstInternNotCheckIn.Any())
                        {
                            foreach (var student in lstInternNotCheckIn)
                            {
                                var lstDayOff = student
                                    .Laboratory?.DayOffs?.Select(x => x.Date)
                                    .ToList();
                                if (lstDayOff.IsNullOrEmpty() || !IsHolidays(lstDayOff))
                                {
                                    DbContext.Attendances.Add(
                                        new Attendance
                                        {
                                            InternId = student.Id,
                                            Date = today,
                                            CheckInTime = new TimeSpan(0, 0, 0),
                                            CheckOutTime = new TimeSpan(0, 0, 0),
                                            Status = (int)ConstantEnum.AttendanceStatus.ABSENT,
                                            Reason = Enum.GetName(
                                                typeof(ConstantEnum.AttendanceStatus),
                                                ConstantEnum.AttendanceStatus.ABSENT
                                            ),
                                        }
                                    );
                                }
                            }
                        }
                        foreach (var attend in lstInternChecked)
                        {
                            var internLab = attend.Intern?.Laboratory;

                            if (internLab == null)
                            {
                                continue;
                            }
                            else
                            {
                                if (
                                    attend.CheckInTime >= internLab.CheckInTime
                                    && attend.CheckInTime
                                        <= internLab.CheckInTime.Add(new TimeSpan(0, 30, 0))
                                    && attend.CheckOutTime >= internLab.CheckOutTime
                                )
                                {
                                    attend.Status = (int)ConstantEnum.AttendanceStatus.ATTENDED;
                                    attend.Reason = Enum.GetName(
                                        typeof(ConstantEnum.AttendanceStatus),
                                        ConstantEnum.AttendanceStatus.ATTENDED
                                    );
                                }
                                else if (attend.CheckOutTime == null)
                                {
                                    attend.Status = (int)ConstantEnum.AttendanceStatus.SOONOUT;
                                    attend.Reason = Enum.GetName(
                                        typeof(ConstantEnum.AttendanceStatus),
                                        ConstantEnum.AttendanceStatus.SOONOUT
                                    );
                                }
                                else
                                {
                                    attend.Status = (int)ConstantEnum.AttendanceStatus.LATEIN;
                                    attend.Reason = Enum.GetName(
                                        typeof(ConstantEnum.AttendanceStatus),
                                        ConstantEnum.AttendanceStatus.LATEIN
                                    );
                                }
                            }
                        }
                        DbContext.Attendances.UpdateRange(lstInternChecked);
                        await DbContext.SaveChangesAsync();

                        _logger.LogInformation(
                            $"{DateTime.Today.AddDays(-1)} Attendance batch ended."
                        );
                    }
                }
                else
                {
                    _logger.LogInformation(
                        $"{DateTime.Today.AddDays(-1)} Attendance batch is stopped. Happy Holiday/Weekend!"
                    );
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(
                    $"{DateTime.Today.AddDays(-1)} Attendance batch Failed: {ex.StackTrace}"
                );
            }
        }

        // Hàm kiểm tra xem ngày có phải là ngày nghỉ không
        static bool IsWeekend(DateTime date)
        {
            // Kiểm tra nếu ngày là thứ Bảy hoặc Chủ Nhật
            if (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
            {
                return true;
            }

            return false;
        }

        // Hàm kiểm tra xem ngày có phải là ngày nghỉ không
        static bool IsHolidays(List<DateTime> dayOff)
        {
            // Kiểm tra nếu ngày là ngày nghỉ lễ
            if (dayOff.Contains(DateTime.Today.AddDays(-1)))
            {
                return true;
            }

            return false;
        }
    }
}
