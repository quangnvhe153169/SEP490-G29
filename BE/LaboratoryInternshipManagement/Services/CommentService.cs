﻿using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Hubs;
using LaboratoryInternshipManagement.Models;
using Microsoft.AspNetCore.SignalR;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class CommentService : ICommentService
    {
        private readonly ICommentRepository _commentRepository;
        private readonly IHubContext<CommentHub> _hubContext;
        private readonly ILogger<CommentService> _logger;

        public CommentService(
            ICommentRepository commentRepository,
            IHubContext<CommentHub> hubContext,
            ILogger<CommentService> logger
        )
        {
            _hubContext = hubContext;
            _commentRepository = commentRepository;
            _logger = logger;
        }

        public async Task AddCommentAsync(Comment comment)
        {
            if (comment == null)
            {
                throw new ArgumentNullException(nameof(comment));
            }
            await _commentRepository.AddCommentAsync(comment);
            // await signalR(comment.DiscussionId);
        }

        public async Task DeleteCommentAsync(int commentId)
        {
            var comment = await _commentRepository.GetCommentByCommentIdAsync(commentId);
            if (comment != null)
            {
                await _commentRepository.DeleteCommentAsync(commentId);
            }
            // await signalR(comment.DiscussionId);
        }

        public async Task<List<Comment>> GetAllCommentsAsync()
        {
            return await _commentRepository.GetAllCommentsAsync();
        }

        public async Task<Comment> GetCommentByCommentIdAsync(int commentId)
        {
            return await _commentRepository.GetCommentByCommentIdAsync(commentId);
        }

        public async Task<List<Comment>> GetCommentsByUserIdAsync(string userId)
        {
            return await _commentRepository.GetCommentsByUserIdAsync(userId);
        }

        public async Task<List<Comment>> SearchCommentsAsync(string searchTerm)
        {
            return await _commentRepository.SearchCommentsAsync(searchTerm);
        }

        public async Task UpdateCommentAsync(Comment comment)
        {
            await _commentRepository.UpdateCommentAsync(comment);
            //await signalR(comment.DiscussionId);
        }
        /* private async Task signalR(int discussionId)
         {
             try
             {
                 var allcomments = await _commentRepository.GetAllCommentsAsync();
                 var userids = allcomments.Where(c => c.DiscussionId == discussionId)
                     .Select(c => c.UserId)
                     .Distinct()
                     .ToList();
                 foreach (var user in userids)
                 {
                     await _hubContext.Clients.User(user).SendAsync("ReceiveComment");
                 }
             }
             catch (Exception ex)
             {
                 _logger.LogError($"Error sending SignalR notifications: {ex}");
             }
         }*/
    }
}
