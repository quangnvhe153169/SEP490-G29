﻿using Microsoft.Azure.CognitiveServices.Vision.Face;
using Microsoft.Azure.CognitiveServices.Vision.Face.Models;

namespace LaboratoryInternshipManagement.Services
{
    public class FaceRecognitionService
    {
        private readonly IFaceClient _faceClient;

        public FaceRecognitionService(IConfiguration configuration)
        {
            var subscriptionKey = configuration["AzureFaceApi:SubscriptionKey"];
            var endpoint = configuration["AzureFaceApi:Endpoint"];
            _faceClient = new FaceClient(new ApiKeyServiceClientCredentials(subscriptionKey))
            {
                Endpoint = endpoint
            };
        }

        public async Task<IList<DetectedFace>> DetectFacesAsync(Stream imageStream)
        {
            var faceAttributes = new List<FaceAttributeType>
            {
                FaceAttributeType.Age,
                FaceAttributeType.Gender,
                FaceAttributeType.HeadPose,
                FaceAttributeType.Smile,
                FaceAttributeType.FacialHair,
                FaceAttributeType.Glasses,
                FaceAttributeType.Emotion,
                FaceAttributeType.Hair,
                FaceAttributeType.Makeup,
                FaceAttributeType.Occlusion,
                FaceAttributeType.Accessories,
                FaceAttributeType.Blur,
                FaceAttributeType.Exposure,
                FaceAttributeType.Noise
            };
            try
            {
                var faces = await _faceClient.Face.DetectWithStreamAsync(
                    imageStream,
                    returnFaceAttributes: faceAttributes
                );
                return faces;
            }
            catch (APIErrorException e)
            {
                // Log error message and rethrow
                Console.WriteLine($"Error: {e.Message}");
                throw;
            }
        }
    }
}
