﻿using AutoMapper;
using LaboratoryInternshipManagement.Controllers;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Hubs;
using LaboratoryInternshipManagement.Models;
using Microsoft.AspNetCore.SignalR;

namespace LaboratoryInternshipManagement.DataAccess.Repositories
{
    public class NotificationService : INotificationService
    {
        private readonly INotificationRepository _notificationRepository;
        private readonly IHubContext<NotificationHub> _hubContext;
        private readonly ILogger<NotificationService> _logger;
        private readonly IMapper _mapper;

        public NotificationService(
            INotificationRepository notificationRepository,
            IHubContext<NotificationHub> hubContext,
            ILogger<NotificationService> logger,
            IMapper mapper
        )
        {
            _hubContext = hubContext;
            _notificationRepository = notificationRepository;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task AddNotificationToUsersAsync(
            Notification notification,
            List<string> userIds
        )
        {
            if (notification == null)
            {
                throw new ArgumentNullException(nameof(notification));
            }
            if (userIds == null || !userIds.Any())
            {
                throw new ArgumentNullException(nameof(userIds));
            }
            // Ensure CreatedByUserId is not null or empty
            if (string.IsNullOrEmpty(notification.CreatedByUserId))
            {
                throw new ArgumentNullException(nameof(notification.CreatedByUserId));
            }
            await _notificationRepository.AddNotificationToUsersAsync(notification, userIds);
            var notificationDto = _mapper.Map<NotificationDTO>(notification);
            foreach (var userId in userIds)
            {
                try
                {
                    await _hubContext
                        .Clients.User(userId)
                        .SendAsync("ReceiveNotification", notificationDto)
                        .ContinueWith(task =>
                        {
                            if (task.IsFaulted)
                            {
                                _logger.LogError(
                                    $"Failed to send notification to user {userId}. Exception: {task.Exception?.GetBaseException()}"
                                );
                            }
                            else
                            {
                                _logger.LogInformation(
                                    $"Notification successfully sent to user {userId}"
                                );
                            }
                        });
                }
                catch (Exception ex)
                {
                    _logger.LogError($"Error sending notification to user {userId}: {ex.Message}");
                    // Handle the error (e.g., retry, log, etc.)
                }
            }
        }

        public async Task DeleteNotificationAsync(int notificationId)
        {
            var notification = await _notificationRepository.GetNotificationByIdAsync(
                notificationId
            );
            if (notification != null)
            {
                await _notificationRepository.DeleteNotificationAsync(notificationId); // Save changes to persist the deletion
            }
            // Check if notification and users are retrieved
            if (notification != null && notification.Users != null)
            {
                // Send notification to each user
                foreach (var user in notification.Users)
                {
                    await _hubContext
                        .Clients.User(user.Id)
                        .SendAsync("ReceiveNotification", notification.Message);
                }
            }
        }

        public async Task<List<Notification>> GetAllNotificationsAsync()
        {
            return await _notificationRepository.GetAllNotificationsAsync();
        }

        public async Task<Notification> GetNotificationByIdAsync(int notificationId)
        {
            return await _notificationRepository.GetNotificationByIdAsync(notificationId);
        }

        public async Task<List<Notification>> GetNotificationsByTypeAsync(string type)
        {
            return await _notificationRepository.GetNotificationsByTypeAsync(type);
        }

        public async Task<List<Notification>> GetNotificationsByUserIdAsync(string userId)
        {
            return await _notificationRepository.GetNotificationsByUserIdAsync(userId);
        }

        public async Task<List<Notification>> GetUnreadNotificationsAsync()
        {
            return await _notificationRepository.GetUnreadNotificationsAsync();
        }

        public async Task<List<Notification>> GetNotificationsByUserInvolvementAsync(string userId)
        {
            return await _notificationRepository.GetNotificationsByUserInvolvementAsync(userId);
        }

        public async Task UpdateNotificationAsync(Notification notification)
        {
            await _notificationRepository.UpdateNotificationAsync(notification); // Save changes to persist the update
            // Retrieve the notification along with users in a single query
            var notificationWithUsers = await _notificationRepository.GetNotificationByIdAsync(
                notification.NotificationId
            );

            // Check if notification and users are retrieved
            if (notificationWithUsers != null && notificationWithUsers.Users != null)
            {
                // Send notification to each user
                foreach (var user in notificationWithUsers.Users)
                {
                    await _hubContext
                        .Clients.User(user.Id)
                        .SendAsync("ReceiveNotification", notification.Message);
                }
            }
        }
    }
}
