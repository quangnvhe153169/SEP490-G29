﻿using Newtonsoft.Json;

namespace LaboratoryInternshipManagement.Services
{
    public class GoogleEmailService
    {
        private readonly HttpClient _httpClient;
        private readonly string _apiKey;

        public GoogleEmailService(HttpClient httpClient)
        {
            _httpClient = httpClient;
            _apiKey = "89db2b850f4b988fe92e12a9d1e3f4f89d6480da";
        }

        public async Task<bool?> VerifyEmailAsync(string email)
        {
            try
            {
                // Đặt endpoint API của Hunter.io
                var apiUrl =
                    $"https://api.hunter.io/v2/email-verifier?email={email}&api_key={_apiKey}";

                // Gửi yêu cầu GET đến API của Hunter.io
                using (var response = await _httpClient.GetAsync(apiUrl))
                {
                    response.EnsureSuccessStatusCode();

                    // Đọc và phân tích kết quả từ phản hồi JSON
                    var jsonString = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<dynamic>(jsonString);

                    // Xác định kết quả từ Hunter.io
                    bool isEmailValid = result.data.result == "deliverable";
                    Console.WriteLine(isEmailValid);

                    return isEmailValid;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex.Message}");
                return null;
            }
        }
    }
}
