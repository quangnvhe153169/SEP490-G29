﻿using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using LaboratoryInternshipManagement.Services.IServices;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using NetTools.Extensions;

namespace LaboratoryInternshipManagement.Services
{
    public class ImportLabService
    {
        private readonly LabInternManagementDBcontext _context;
        private readonly IImportCSVRepository _importCSVRepository;
        private readonly IEmailService _emailService;
        private readonly IConfiguration _config;
        private readonly IServiceProvider _serviceProvider;

        public ImportLabService(
            LabInternManagementDBcontext context,
            IImportCSVRepository importCSVRepository,
            IEmailService emailService,
            IConfiguration configuration,
            IServiceProvider serviceProvider
        )
        {
            _context = context;
            _importCSVRepository = importCSVRepository;
            _emailService = emailService;
            _config = configuration;
            _serviceProvider = serviceProvider;
        }

        public async Task<List<string>> ProcessLaboratoryRecords(IEnumerable<LaboratoryCSV> records)
        {
            var errorMessages = new List<string>();
            var users = new Dictionary<User, string>();
            var labManagers = new List<User>();

            foreach (var record in records)
            {
                var (lineErrorMessages, labManager, password) = await ValidateRecord(record);
                var recordName = !string.IsNullOrEmpty(record.LabName) ? record.LabName : "N/A";

                if (lineErrorMessages.Any())
                {
                    errorMessages.Add($"{recordName}: {string.Join('.', lineErrorMessages)}");
                    continue;
                }

                var laboratory = CreateLaboratory(record, labManager.Id);
                users.Add(labManager, password);
                labManagers.Add(labManager);

                _context.Users.Add(labManager);
                _context.UserRoles.AddRange(CreateUserRoles(labManager.Id));
                _context.Laboratories.Add(laboratory);
            }

            if (errorMessages.Count < 1)
            {
                await SaveChangesAndSendEmails(labManagers, users);
            }

            return errorMessages;
        }

        private async Task<(List<string> errors, User labManager, string password)> ValidateRecord(
            LaboratoryCSV record
        )
        {
            var importInternEmails = await _context
                .ImportInterns.Select(i => i.Email.ToLower())
                .ToListAsync();

            var userEmails = await _context
                .Users.Where(u => u.Email != null && u.Email != "")
                .Select(u => u.Email.ToLower())
                .ToListAsync();

            var existingEmails = importInternEmails.Concat(userEmails).ToList();
            var lineErrorMessages = new List<string>();
            var isExisted = existingEmails.Contains(record.LabManagerEmail.ToLower());

            if (string.IsNullOrEmpty(record.LabManagerEmail))
            {
                lineErrorMessages.Add("labManagerEmailIsNull");
            }

            if (isExisted)
            {
                lineErrorMessages.Add("userExisted");
            }

            if (!IsValidEmail(record.LabManagerEmail))
            {
                lineErrorMessages.Add("labManagerEmailIsInvalid");
            }

            if (string.IsNullOrEmpty(record.LabName))
            {
                lineErrorMessages.Add("labNameIsNull");
            }
            else if (await _context.Laboratories.AnyAsync(x => x.LabName.Equals(record.LabName)))
            {
                lineErrorMessages.Add("labNameIsExist");
            }

            if (record.LeaveRequestNum <= 0)
            {
                lineErrorMessages.Add("invalidLeaveRequestNumb");
            }

            if (lineErrorMessages.Any())
            {
                return (lineErrorMessages, null, null);
            }

            var hasher = new PasswordHasher<User>();

            // Create a new User for labManager
            string id = Guid.NewGuid().ToString();
            string password = _importCSVRepository.GenerateRandomString(8);
            var labManager = new User
            {
                Id = id,
                Email = record.LabManagerEmail,
                NormalizedEmail = record.LabManagerEmail.ToUpper(),
                PasswordHash = hasher.HashPassword(null, password),
            };

            return (lineErrorMessages, labManager, password);
        }

        private Laboratory CreateLaboratory(LaboratoryCSV record, string labManagerId)
        {
            return new Laboratory
            {
                LabName = record.LabName,
                CheckInTime = TimeSpan.Parse(record.CheckInTime),
                CheckOutTime = TimeSpan.Parse(record.CheckOutTime),
                LeaveRequestNum = record.LeaveRequestNum,
                LabManagerId = labManagerId
            };
        }

        private List<IdentityUserRole<string>> CreateUserRoles(string userId)
        {
            return new List<IdentityUserRole<string>>
            {
                new IdentityUserRole<string> { UserId = userId, RoleId = "2" },
                new IdentityUserRole<string> { UserId = userId, RoleId = "3" }
            };
        }

        private bool IsValidEmail(string email)
        {
            string emailPattern = @"^[^@\s]+@[^@\s]+\.[^@\s]+$";
            return Regex.IsMatch(email, emailPattern);
        }

        private async Task SaveChangesAndSendEmails(
            IEnumerable<User> labManagers,
            IDictionary<User, string> users
        )
        {
            try
            {
                // Save the initial changes
                await _context.SaveChangesAsync();

                // Create teams and add team memberships
                foreach (var labManager in labManagers)
                {
                    var fullName = labManager.FirstName + " " + labManager.LastName;
                    var teamName = string.IsNullOrEmpty(fullName.Trim())
                        ? labManager.Email.Split('@')[0]
                        : fullName.Trim();
                    var newTeam = new Team { Name = teamName };
                    _context.Teams.Add(newTeam);
                    await _context.SaveChangesAsync();

                    var teamMembership = new TeamMembership
                    {
                        UserId = labManager.Id,
                        TeamId = newTeam.TeamId
                    };

                    _context.TeamMemberships.Add(teamMembership);
                }

                await _context.SaveChangesAsync();

                var labManagerIds = users.Keys.Select(u => u.Id).ToList();

                var labs = await _context
                    .Laboratories.Where(lab => labManagerIds.Contains(lab.LabManagerId))
                    .ToListAsync();

                // Fire-and-forget email sending with scoped services
                var emailSendings = new Dictionary<string, string>();
                string emailBody = GetEmbeddedEmailTemplate(
                    "LaboratoryInternshipManagement.Resources.account-sending.html"
                );

                foreach (var user in users)
                {
                    var lab = labs.FirstOrDefault(x => x.LabManagerId.Equals(user.Key.Id));

                    if (lab == null)
                    {
                        Console.WriteLine($"No lab found for user {user.Key.Email}");
                        continue; // Skip sending email for this user if no lab is found
                    }

                    // Replace placeholders in the email template
                    string loginUrl = _config["AllowedView"];
                    var fullName = user.Key.FirstName + " " + user.Key.LastName;
                    fullName = string.IsNullOrEmpty(fullName.Trim())
                        ? user.Key.Email.Split('@')[0]
                        : fullName;
                    var emailBodySend = emailBody
                        .Replace("[name]", fullName)
                        .Replace("[labName]", lab.LabName)
                        .Replace("[mentor]", "")
                        .Replace("[roles]", "Quản lý Phòng Lab, Cố vấn")
                        .Replace("[user.Email]", user.Key.Email)
                        .Replace("[password]", user.Value)
                        .Replace("[Đội ngũ hỗ trợ LIM]", "SEP490-G29 SU24")
                        .Replace("[link]", $"<a href=\"{loginUrl}\">Tại Đây</a>");
                    emailSendings.Add(user.Key.Email, emailBodySend);
                }

                _ = Task.Run(async () =>
                {
                    Console.WriteLine($"Start sending email...");
                    foreach (var email in emailSendings)
                    {
                        await _emailService.SendEmailAsync(
                            email.Key,
                            "Đăng nhập vào hệ thống LIM",
                            email.Value
                        );

                        Console.WriteLine($"Email sent to user ${email.Key}");
                    }
                    Console.WriteLine($"Sent successfully.");
                });
            }
            catch (Exception ex)
            {
                // Log any exceptions that happen during the SaveChanges process
                Console.WriteLine($"Error in SaveChangesAndSendEmails: {ex.Message}");
            }
        }

        private string GetEmbeddedEmailTemplate(string resourceName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceStream = assembly.GetManifestResourceStream(resourceName);

            if (resourceStream == null)
            {
                throw new FileNotFoundException($"Embedded resource '{resourceName}' not found.");
            }

            using (var reader = new StreamReader(resourceStream, Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
