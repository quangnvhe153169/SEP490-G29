﻿using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using LaboratoryInternshipManagement.DataAccess.IRepository;
using LaboratoryInternshipManagement.Models;
using LaboratoryInternshipManagement.Services.IServices;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace LaboratoryInternshipManagement.Services
{
    public class ImportMentorService
    {
        private readonly LabInternManagementDBcontext _context;
        private readonly IImportCSVRepository _importCSVRepository;
        private readonly IEmailService _emailService;
        private readonly GoogleEmailService _googleEmailService;
        private readonly ITeamRepository _teamRepository;
        private readonly ITeamMembershipRepository _teamMembershipRepository;
        private readonly IConfiguration _config;
        private readonly IServiceProvider _serviceProvider;

        public ImportMentorService(
            LabInternManagementDBcontext context,
            IImportCSVRepository importCSVRepository,
            IEmailService emailService,
            GoogleEmailService googleEmailService,
            ITeamRepository teamRepository,
            ITeamMembershipRepository teamMembershipRepository,
            IConfiguration configuration,
            IServiceProvider serviceProvider
        )
        {
            _context = context;
            _importCSVRepository = importCSVRepository;
            _emailService = emailService;
            _googleEmailService = googleEmailService;
            _teamRepository = teamRepository;
            _teamMembershipRepository = teamMembershipRepository;
            _config = configuration;
            _serviceProvider = serviceProvider;
        }

        public bool IsValidEmail(string Email)
        {
            const string emailRegex = @"^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*\.(\w{2,})$";
            return Regex.IsMatch(Email, emailRegex, RegexOptions.IgnoreCase);
        }

        public async Task<List<string>> ProcessMentorRecordsCSV(
            IEnumerable<MentorCSV> records,
            int labId
        )
        {
            var errorMessages = new List<string>();
            int lineNumber = 1;
            var validRecords = new Dictionary<User, string>();

            var importInternEmails = await _context
                .ImportInterns.Select(i => i.Email.ToLower())
                .ToListAsync();

            var userEmails = await _context
                .Users.Where(u => u.Email != null && u.Email != "")
                .Select(u => u.Email.ToLower())
                .ToListAsync();
            var existingEmails = importInternEmails.Concat(userEmails).ToList();
            var hasher = new PasswordHasher<User>();

            foreach (var record in records)
            {
                lineNumber++;

                if (string.IsNullOrEmpty(record.Email))
                {
                    errorMessages.Add($"Line {lineNumber}: emailIsNull");
                }
                else if (!IsValidEmail(record.Email))
                {
                    errorMessages.Add($"Line {lineNumber}: emailIsNotValid");
                }
                else if (existingEmails.Contains(record.Email.ToLower()))
                {
                    errorMessages.Add($"Line {lineNumber}: emailIsExistInDatabase");
                }
                if (errorMessages.Count == 0)
                {
                    string password = _importCSVRepository.GenerateRandomString(8);

                    var mentorId = Guid.NewGuid().ToString();
                    var mentor = new User
                    {
                        Id = mentorId,
                        LabId = labId,
                        FirstName = record.FirstName,
                        LastName = record.LastName,
                        Email = record.Email,
                        NormalizedEmail = record.Email,
                        PasswordHash = hasher.HashPassword(null, password)
                    };
                    var role = new IdentityUserRole<string> { UserId = mentorId, RoleId = "3" };

                    _context.Users.Add(mentor);
                    _context.UserRoles.Add(role);
                    await _context.SaveChangesAsync();
                    await AssignUserToNewTeam(mentor);

                    validRecords.Add(mentor, password);
                }
            }

            if (errorMessages.Count == 0)
            {
                await SendMail(validRecords);
            }

            return errorMessages;
        }

        public async Task<List<string>> AddMentor(MentorCSV mentorInput, int labId)
        {
            var hasher = new PasswordHasher<User>();
            var errorMessages = new List<string>();
            var importInternEmails = await _context
                .ImportInterns.Select(i => i.Email.ToLower())
                .ToListAsync();

            var userEmails = await _context.Users.Select(u => u.Email.ToLower()).ToListAsync();
            var existingEmails = importInternEmails.Concat(userEmails).ToList();

            if (string.IsNullOrEmpty(mentorInput.Email))
            {
                errorMessages.Add($"emailIsNull");
            }
            else if (!IsValidEmail(mentorInput.Email))
            {
                errorMessages.Add($"emailIsNotValid");
            }
            else if (existingEmails.Contains(mentorInput.Email.ToLower()))
            {
                errorMessages.Add($"emailIsExistInDatabase");
            }

            if (errorMessages.Count == 0)
            {
                var mentorId = Guid.NewGuid().ToString();
                string password = _importCSVRepository.GenerateRandomString(8);
                var mentor = new User
                {
                    Id = mentorId,
                    LabId = labId,
                    FirstName = mentorInput.FirstName,
                    LastName = mentorInput.LastName,
                    Email = mentorInput.Email.ToLower(),
                    NormalizedEmail = mentorInput.Email.ToLower(),
                    PasswordHash = hasher.HashPassword(null, password)
                };
                var role = new IdentityUserRole<string> { UserId = mentorId, RoleId = "3" };

                _context.Users.Add(mentor);
                _context.UserRoles.Add(role);
                await _context.SaveChangesAsync();
                await AssignUserToNewTeam(mentor);
                string emailBody = GetEmbeddedEmailTemplate(
                    "LaboratoryInternshipManagement.Resources.account-sending.html"
                );
                string loginUrl = _config["AllowedView"];
                var lab = await _context.Laboratories.SingleOrDefaultAsync(x =>
                    x.Id.Equals(mentor.LabId)
                );
                // Thay thế các placeholder bằng thông tin cụ thể
                emailBody = emailBody
                    .Replace("[name]", mentor.Email.Split('@')[0])
                    .Replace("[labName]", lab.LabName)
                    .Replace("[mentor]", "")
                    .Replace("[roles]", "Cố vấn")
                    .Replace("[user.Email]", mentor.Email)
                    .Replace("[password]", password)
                    .Replace("[Đội ngũ hỗ trợ LIM]", "SEP490-G29 SU24")
                    .Replace("[link]", $"<a href=\"{loginUrl}\">Tại Đây</a>");
                ;
                _ = Task.Run(async () =>
                {
                    // Gửi email
                    await _emailService.SendEmailAsync(
                        mentor.Email,
                        "Đăng nhập vào hệ thống LIM",
                        emailBody
                    );
                });
            }
            return errorMessages;
        }

        private async Task AssignUserToNewTeam(User user)
        {
            var newTeam = new Team { Name = user.FirstName + " " + user.LastName };

            await _teamRepository.AddTeam(newTeam);

            var teamMembership = new TeamMembership { UserId = user.Id, TeamId = newTeam.TeamId };

            await _teamMembershipRepository.AddTeamMembership(teamMembership);
        }

        private async Task SendMail(Dictionary<User, string> users)
        {
            // Fire-and-forget email sending with scoped services
            var labIds = users.Keys.Select(u => u.LabId).ToList();

            var labs = await _context
                .Laboratories.Where(lab => labIds.Contains(lab.Id))
                .ToListAsync();

            // Fire-and-forget email sending with scoped services
            var emailSendings = new Dictionary<string, string>();
            string emailBody = GetEmbeddedEmailTemplate(
                "LaboratoryInternshipManagement.Resources.account-sending.html"
            );

            foreach (var user in users)
            {
                var lab = labs.FirstOrDefault(x => x.Id == user.Key.LabId);

                if (lab == null)
                {
                    Console.WriteLine($"No lab found for user {user.Key.Email}");
                    continue; // Skip sending email for this user if no lab is found
                }

                // Replace placeholders in the email template
                string loginUrl = _config["AllowedView"];
                var fullName = user.Key.FirstName + " " + user.Key.LastName;
                fullName = string.IsNullOrEmpty(fullName.Trim())
                    ? user.Key.Email.Split('@')[0]
                    : fullName;
                var emailBodySend = emailBody
                    .Replace("[name]", fullName)
                    .Replace("[labName]", lab.LabName)
                    .Replace("[mentor]", "")
                    .Replace("[roles]", "Quản lý Phòng Lab, Cố vấn")
                    .Replace("[user.Email]", user.Key.Email)
                    .Replace("[password]", user.Value)
                    .Replace("[Đội ngũ hỗ trợ LIM]", "SEP490-G29 SU24")
                    .Replace("[link]", $"<a href=\"{loginUrl}\">Tại Đây</a>");
                emailSendings.Add(user.Key.Email, emailBodySend);
            }

            _ = Task.Run(async () =>
            {
                try
                {
                    Console.WriteLine($"Start sending email...");
                    foreach (var email in emailSendings)
                    {
                        await _emailService.SendEmailAsync(
                            email.Key,
                            "Đăng nhập vào hệ thống LIM",
                            email.Value
                        );

                        Console.WriteLine($"Email sent to user ${email.Key}");
                    }
                    Console.WriteLine($"Sent successfully.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            });
        }

        private string GetEmbeddedEmailTemplate(string resourceName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceStream = assembly.GetManifestResourceStream(resourceName);

            if (resourceStream == null)
            {
                throw new FileNotFoundException($"Embedded resource '{resourceName}' not found.");
            }

            using (var reader = new StreamReader(resourceStream, Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
